#!/bin/sh

# Package dependencies (as of Ubuntu 7.04):
# - subversion-tools
# - xsltproc 

svn2cl -i --group-by-day --separate-daylogs --authors="scripts/authors.xml"
