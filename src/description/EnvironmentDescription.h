/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _ENVIRONMENT_DESCRIPTION_H_
#define _ENVIRONMENT_DESCRIPTION_H_

#include <description/DescriptionHelper.h>
#include <util/defines.h>

#define ED_POINT_LIGHT 0

/* describes a single segment, if motor == NULL, then this segment is a rigid
 * body, else it is a motor */
struct SingleElement
{
  Position position;
  int type; //is either BOX_GEOM,SPHERE_GEOM, CYLINDER_GEOM or CAPPED_CYLINDER_GEOM
  /* is type BOX_GEOM, then dimension.x .y and .y has to be set
     is type CAPPED_CYLINDER_GEOM, then dimensions .radius .length has to be set
     is type SPHERE_GEOM, then dimensions.radius has to be set */
  Position dimensions;
  Position rotation;
  Position init_rotation;
  Color color; /* rgb */
};

struct LightSource
{
  Position pos;
  int type; /* is POINT_LIGHT */
  Color color; /*rgb, just for drawing the source */
  double intensity;
  bool drawRange;
};

typedef vector<struct SingleElement> ElementVector;
typedef vector<struct LightSource>   LightVector;

class EnvironmentDescription
{
  public:
    EnvironmentDescription();
    ~EnvironmentDescription();

    /*-------------------- BEGIN OBJECT STUFF ---------------------------- */
    
    /* add one Element to the environment vector */
    void addElement(SingleElement);
 
    /* returns pointer to the environment vector */
    ElementVector* getElements();
    
    /* returns the number of elements */
    unsigned int getNumberOfElements();
    
    /* returns a single element by its index */
    SingleElement* getElement(unsigned int index);

    /*-------------------- END OBJECT STUFF ------------------------------ */

    
    /*-------------------- BEGIN LIGHT STUFF ---------------------------- */

    /* set the ambient light intensity */
    void setAmbientLight(double intensity);

    /* get the ambient light intensity */
    double getAmbientLight();

    /* add an light element to the environment */
    void addLight(LightSource);

    /* returns pointer to the light source vector */
    LightVector* getLights();

    /* returns the number of lights */
    unsigned int getNumberOfLights();

    /* returns a single light source by its index */
    LightSource* getLight(unsigned int index);

    void setLightPosition(unsigned int index, Position pos);
    
    void setLightIntensity(unsigned int index, double intensity);
    
    void setAmbientLightRandomise(RandomiseScalar randomise);

    void randomiseAmbientLight();

    /*-------------------- END LIGHT STUFF ------------------------------- */
    
  private:
    /* vector of all compounds */
    ElementVector allElements_;

    /* vector of all lights */
    LightVector   allLights_;

    /* defines the ambient light intensity */
    double ambientLight_;

    /* defines the ambient light randomisation */
    RandomiseScalar _randomise;
};

#endif
