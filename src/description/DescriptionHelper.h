/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _DESCRIPTION_HELPER_H_
#define _DESCRIPTION_HELPER_H_

#include <fstream>
#include <vector>

#define RD_UNDEFINED_GEOM         -1
#define RD_BOX_GEOM               0
#define RD_CAPPED_CYLINDER_GEOM   1
#define RD_SPHERE_GEOM            2
#define RD_CYLINDER_GEOM          3
#define RD_COMPOSITE_GEOM         4

#define RD_CONN_FIX               4
#define RD_CONN_HINGE             5
#define RD_CONN_HINGE_2           6
#define RD_CONN_BALL              7

#define RD_ANGLE_MOTOR            8 
#define RD_VEL_MOTOR              9 
#define RD_PASSIVE_JOINT          10

#define RD_CONN_SLIDER            11 

#define RD_CONN_COMPLEX_HINGE     80

#define RD_RANDOMISE_METHOD_ADDITIVE 0
#define RD_RANDOMISE_METHOD_GLOBAL   1
                                     
#define RD_RANDOMISE_TYPE_LIST       3
#define RD_RANDOMISE_TYPE_INTERVAL   4


using namespace std;
struct P3D
{
  double x;
  double y;
  double z;
};

struct P3DColor
{
  double x;
  double y;
  double z;
  double r;
  double g;
  double b;
};

struct RandomiseScalar
{
  bool   enabled;
  int    type;
  int    method;
  int    last_index;
  double min;
  double max;
  double variance;
  double probability;
  std::vector<double> entries;
};


struct Randomise
{
  bool enabled;
  int  type;
  int  method;
  int  last_index;
  P3D  min;
  P3D  max;
  P3D  variance;
  P3D  probability;
  std::vector<P3D> entries;
};

struct Position
{
  double x;
  double y;
  double z;
  /* only necessary for RD_CAPPED_CYLINDER_GEOM (radius and length) and
   * RD_SPHERE (radius) */
  double radius;
  double length;
  Randomise randomise;
};

struct Color
{
  float r;
  float g;
  float b;
  float alpha;
};

#endif
