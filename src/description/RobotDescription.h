/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _ROBOT_DESCRIPTION_H_
#define _ROBOT_DESCRIPTION_H_

#include <util/defines.h>
#include <description/DescriptionHelper.h>
#include <description/SensorDescription.h>
#include <stdio.h>

#include <string>
#include <iostream>

#define CN_NOISE_NONE      0 // dont use noise, ignore the value in "noise"
#define CN_NOISE_GAUSS     1 // use Gauss-Noise
#define CN_NOISE_NORM      2 // use Normal-Noise

#define RD_TYPE_ACTIVE     0
#define RD_TYPE_PASSIVE    1
#define RD_TYPE_CONTROLLED 2
#define RD_TYPE_MOVING     3

typedef vector  <SensorDescription> SensDesVector;
typedef vector  <struct ConnectorDescription> ConnectorVector;

struct CompoundDescription 
{
  string name;
  vector<struct SingleSegment> segments;
};


struct TracePointDescription
{
  bool   enabled;
  Color  color;
  double size;
  int    iterations;
};

struct TraceLineDescription
{
  bool  enabled;
  Color color;
  int   iterations;
};

// Pulse width mapping
struct PWMappingDescription
{
  int    type;
  double minActivation;
  double maxActivation;
  double xOff;
  double yOff;
  double constant;
  double exponent;
  vector<struct InterpolationDataPointSet> interpolationData;
};

struct JointFrictionDescription
{
  bool   active;
  bool   staticActive;
  bool   dynamicActive;
  bool   minVelActive;
  double staticMaxForce;
  double staticVelThres;
  double dynamicConstant;
  int    dynamicType;
  double dynamicMinVel;
  double dynamicOffset;
};

struct MotorDescription
{
  double maxTorque;
  double maxVelocity;
  double maxAcceleration;
  double efficiency;
  double stallTorque;
  double maxContinuousTorque;
  double noLoadSpeed;
  double mechanicalTimeConstant;
  double maxEfficiency;
  double brakeEfficiency;
  JointFrictionDescription  brakeFriction;
  double torqueCurrentConstant;
  double speedVoltageConstant;
  double pwmThreshold;
  PWMappingDescription pwToStallTorque;
  PWMappingDescription pwToStallCurrent;   
  PWMappingDescription pwToNoLoadSpeed;    
  PWMappingDescription pwToNoLoadCurrent;
  PWMappingDescription pwToBrakeEfficiency;
  double noise[2];
  int    noiseType[2];
};

struct GearDescription
{
  bool   active;
  double transmissionRatio;
  double backlash;
  double efficiency;
  double initialBacklashPosition;
  double noise;
  int    noiseType;
};

struct SpringCouplingDescription
{
  bool   active;
  double minTorque;
  double springConstant;
  double maxDeflection;
  double noise;
  int    noiseType;
};

struct SpringDescription
{
  bool   active;
  double springConstant;
  double initialPosition;
  double noise;
  int    noiseType;
};

struct DamperDescription
{
  bool   active;
  double dampingConstant;
};

struct FilterDescription
{
  int    type;
  int    windowSize;
  double initValue;
  bool   recursive;
  int    fullRecalc;
};

struct ConnectorDescription
{
  // name of the connector (motor, joint)
  string                    name;
  // defines the number of compound from which should
  // connected (0 .. n) only has to be set if
  // it is NOT type of SingleSegment
  int                       sourceCompoundNum;

  // defines the number of compound to which should
  // connected (0 .. n) only has to be set if it
  // is NOT type of SingleSegment
  int                       aimCompoundNum;
  // defines the aim segment of the aimed compound to
  // which should be connected (0..n) only has to
  // be set if it is NOT type of SingleSegment
  int                       sourceSegNum;

  // defines the aim segment of the aimed compound to
  // which should be connected (0..n)
  int                       aimSegNum;

  // defines the connector type (fix, hinge etc.)
  int                       type;

  // defines the anchor, where the fixed joint
  // is located
  Position                  anchorPos;

  // defines the anchor, where the fixed joint
  // is located
  Position                  anchorInitPos;
  // has to be set only, if type is not fix
  double                    fMax;
  // has to be set only, if type is not fix
  double                    velMax;
  // has to be set only, if type is not fix
  Position                  rotationAxis;
  // has to be set only, if type is hinge2
  Position                  rotationAxis2;
  // has to be set only, if type is not fix
  Position                  initRotationAxis;
  // has to be set only, if type is hinge2
  Position                  initRotationAxis2;

  Position                  sliderAxis;
  Position                  initSliderAxis;

  // joint limits, has to be set only, if type is not fix
  double                    min_deflection;
  double                    max_deflection;

  // defines the range which are going in the simulation
  double                    startValue;
  // TODO: maybe use a vector here
  double                    endValue;

  // in rad
  // slowdown describes at which difference to the desired angle the motor
  // begins to slow down, stop defines the in which range the difference to the
  // desired angle is always zero, and so the motor stands still
  // only necessary for RD_ANGLE_MOTOR

  double                    slowDownBias;
  // in rad
  // should be RD_ANGLE_MOTOR (for servo) or
  // RD_VEL_MOTOR (controlled via velocity) or
  // RD_PASSIVE_JOINT (no motor, the joint is passive)

  double                    stopBias;
  int                       motorType;

  // defines the noise, which will be put to the motor command
  // value in the range of 0..1 defines the gaussian standard deviation,
  // wherby zero means no noise */
  double                    noise;
  // defines what kind of type of noise will be calculated
  // for the range given in "noise"
  // possible values, see above, starting with CN_NOISE_

  int                       noise_type;
  MotorDescription          motor;
  GearDescription           gear;
  SpringCouplingDescription springCoupling;
  SpringDescription         spring;
  DamperDescription         damper;
  JointFrictionDescription  jointFriction;
  FilterDescription         forceFeedbackFilter;
  FilterDescription         stopTorqueAccuFilter;

  // TODO: if we use a vector for start/endValue
  double                    startValue2;
  // we can get away without this
  double                    endValue2;

  bool                      isPositionExceedable;
  double                    minExceedPosition;
  double                    maxExceedPosition;

  bool                      isForceExceedable;
  double                    minExceedForce;
};


/* describes a single segment, if motor == NULL, then this segment is a rigid
 * body, else it is a motor */
struct SingleSegment
{
  Position              position;
  Position              init_position;
  // is either BOX_GEOM or CAPPED_CYLINDER_GEOM
  // is type BOX_GEOM, then dimension.x .y and .y has to be set
  // is type CAPPED_CYLINDER_GEOM, then dimensions .radius .length has to be set
  int                   type;
  Position              dimensions;
  Position              init_rotation;
  Position              rotation;
  TracePointDescription trace_point;
  TraceLineDescription  trace_line;
  Color                 color;

  double                mass;
  int                   massGeometry;
  Position              massOffset;
  Position              massDimension;
  Position              massRelativeRotation;

  ConnectorVector       connectors;

  // TODO: this is a quick hack!!! We should have a special geom struct and
  // store all geom related information there, only keeping the global and rigid
  // body info in the SingleSegment struct plus a Vector of the geom structs
  CompoundDescription         geoms;

  // same choices as above
  int                   rigidBodyType;            
  // should the mass be visualized ??
  bool                  drawRigidBody; 

  // name of the segment
  string                name;
  string                rigidBodyName;

  // collision stuff
  // has to be set in the following way:
  // e.g. contactMode = dContactBounce | dContactSlip1 | dContactSlip2;
  // if the correspondig variables are set to zero, than DON'T set the flags to
  // the mode
  int                   contactMode;

  // Restitution parameter (0..1). 0 means the surfaces are not bouncy at all,
  // 1 is maximum bouncyness. If this is set > 0, cBounceVel has to set too
  // Flag: dContactBounce
  double                cBounce;

  // The minimum incoming velocity necessary for bounce (in m/s).
  // Incoming velocities below this will effectively have a bounce parameter of
  // 0. If this is set > 0, cBounce has to set too
  // Flag: dContactBounce
  double                cBounceVel; 

  // The coefficient of force-dependent-slip (FDS) for friction direction 1
  //Flag: dContactSlip1
  double                cSlip1;            

  // The coefficient of force-dependent-slip (FDS) for friction direction 2
  //Flag: dContactSlip2
  double                cSlip2;    

  // the constraint force mixing parameter of the contact normal
  // Flag: dContactSoftCFM
  double                cSoftCFM;            

  // the error reduction parameter of the contact normal
  // Flag: dContactSoftERP
  double                cSoftErp;   

  // !!!!!!! This must always be set !!!!!!
  // Coulomb friction coefficient. This must be in the range 0 to dInfinity. 0
  // results in a frictionless contact, and dInfinity results in a contact that
  // never slips. Note that frictionless contacts are less time consuming to
  // compute than ones with friction, and infinite friction contacts can be
  // cheaper than contacts with finite friction. This must always be set.
  double                cMu;

  // if not set, use cMu for both friction directions. If set, use sMu for
  // friction direction 1, use cMu2 for friction direction 2.
  // Flag: dContactMu2
  double                cMu2;                

  // if true, then this segment is checked for bumps with the ground plate
  bool                  isBumpable;

  // if true, then write contact information to file
  bool                  traceContact;
};


class RobotDescriptionException : public std::exception
{
  public:
    explicit RobotDescriptionException(const std::string& what)
      :
        m_what(what)
  {}

    virtual ~RobotDescriptionException() throw() {}

    virtual const char * what() const throw()
    {
      return m_what.c_str();
    }

    virtual void message() const throw()
    {
      cerr << "RobotDescriptionException: " << m_what << endl;
    }


  private:
    std::string m_what;
};



class RobotDescription
{
  public:
    RobotDescription();
    // copy constructor 
    RobotDescription(RobotDescription* robotDes);
    ~RobotDescription();


    // store the initial position values of the robot 
    void setInitialPosition(Position pos);

    // store the initial orientation values of the robot 
    void setInitialOrientation(Position pos);

    void setType(int type);

    void setController(string controller);

    string getController();

    // get the initial position values of the robot
    Position getInitialPosition();

    int getType();

    // -------------BEGIN SEGMENTS FUNCTIONS---------------------------------

    //  randomise the initial position 
    void randomiseInitialPosition();

    //  add one Compound to the segmentCompounds Vector 
    void addCompoundDescription(CompoundDescription segVec);

    //  returns pointer to the segmentCompounds vector 
    vector<CompoundDescription>* getCompounds();

    //  returns the number of segment compounds 
    unsigned int getCompoundNumber();

    //  returns a single compound by its index 
    CompoundDescription* getCompound(unsigned int index);

    /* help function to fix the angle sensor bug
       not the best solution, but the fastest to fix the bug */
    unsigned int getTotalNumOfSegJoints(unsigned int segCompound, unsigned int
        seg);

    // -------------END SEGMENTS FUNCTIONS-----------------------------------


    // -------------BEGIN COMPOUND CONNECTOR FUNCTIONS--------------------------

    //  add one connector point between 2 compounds 
    void addCompoundConnector(ConnectorDescription connector);

    //  returns a pointer to the connector vector 
    ConnectorVector* getCompoundConnectors();

    //  returns the numbers of the connector points 
    unsigned int getCompoundConnectorNumber();

    //  returns a pointer to a connector, specified by the index number 
    ConnectorDescription* getCompoundConnector(unsigned int index);

    // -------------END COMPOUND CONNECTOR FUNCTIONS----------------------------


    // -------------BEGIN SENSOR FUNCTIONS-----------------------------------

    //  add one sensor 
    void addSensor(SensorDescription *sensDes);

    //  returns a pointer to the sensor vector 
    SensDesVector* getSensors();

    //  returns the numbers of the sensors 
    unsigned int getSensorNumber();

    //  returns a pointer to a sensor, specified by the index number 
    SensorDescription* getSensor(unsigned int index);

    // -------------END SENSOR FUNCTIONS-------------------------------------
    void turnAllSegments(Position offset);
    void moveAllObjects(Position offset);
    void copyPosition(Position *dest, Position source);
    void writeAndVerifyNameTags();

    // ------------- BEGIN NAME FUNCTIONS-----------------------------------
    void getName(string &name);
    void setName(string name);
    // ------------- END   NAME FUNCTIONS-----------------------------------
  private:

    void initPosition(Position *pos);
    void rotate(Position *pos, Position rotation);
    void printNameInformation();
    void concatNames(string &destination, string source, string alternative,
        int index);
    void writeNewNameTags() throw(RobotDescriptionException);
    void verifyNameTags() throw(RobotDescriptionException);
    void addConnectorName(ConnectorDescription *c, string segmentName,
        int index) throw(RobotDescriptionException);

    // vector of all connectors between the compounds
    ConnectorVector       _compoundConnectors;
    // vector of all sensors
    SensDesVector         _sensors;

    // initial position of the robot
    Position              _initialPosition;

    // initial orientation of the robot
    Position              _initialOrientation;

    // type of the movable: passive or active
    int                   _moveableType;

    string                _controller;
    string                _robotName;

    // vector of all compounds
    vector<CompoundDescription> _segmentCompounds;

    vector<string> _allNameTagsVector;

};

#endif
