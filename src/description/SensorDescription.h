/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _SENSOR_DESCRIPTION_H_
#define _SENSOR_DESCRIPTION_H_

#include <description/DescriptionHelper.h>
#include <util/defines.h>

#include <string>

#define SD_ANGLE_SENSOR               0
#define SD_ANGLE_VEL_SENSOR           1
#define SD_IR_SENSOR                  2
#define SD_LDR_SENSOR                 3
#define SD_SHARP_GP2D12_37_SENSOR     4
#define SD_COORDINATE_SENSOR          5
#define SD_ANGLE_FEEDBACK_SENSOR      6
#define SD_SLIDER_POSITION_SENSOR     7
#define SD_SLIDER_VEL_SENSOR          8
#define SD_AMBIENT_LIGHT_SENSOR       9
#define SD_SHARP_DM2Y3A003K0F_SENSOR 10
#define SD_DIRECTED_CAMERA_SENSOR    11
#define SD_GEN_ROTATION_SENSOR       12

#define COORDINATE_SENSOR_UNDEFINED -1
#define COORDINATE_SENSOR_X          0
#define COORDINATE_SENSOR_Y          1
#define COORDINATE_SENSOR_Z          2

#define FEEDBACK_SENSOR_UNDEFINED      -1
#define FEEDBACK_SENSOR_FORCE           0
#define FEEDBACK_SENSOR_TORQUE          1
#define FEEDBACK_SENSOR_SOURCE          0
#define FEEDBACK_SENSOR_DESTINATION     1
#define FEEDBACK_SENSOR_X               0
#define FEEDBACK_SENSOR_Y               1
#define FEEDBACK_SENSOR_Z               2
#define FEEDBACK_SENSOR_HINGE_AXIS      3
#define FEEDBACK_SENSOR_SLIDER_AXIS     4

#define G_ROT_SENSOR_TYPE_UNDEFINED       -1
#define G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW   0
#define G_ROT_SENSOR_TYPE_VECTOR_ANGLE     1
#define G_ROT_SENSOR_TYPE_UNIT_SPHERE_XYZ  2
#define G_ROT_SENSOR_ABSOLUTE              0
#define G_ROT_SENSOR_DIFFERENTIAL          1
#define G_ROT_SENSOR_ACCELERATION          2
#define G_ROT_SENSOR_LENGTH_CONSTANT       0
#define G_ROT_SENSOR_LENGTH_PROPORTIONAL   1

#define DEF_SENSOR_USED_AS_CONTROL_INPUT   0
#define DEF_SENSOR_USED_AS_AUX             1
#define DEF_SENSOR_USED_INTERNALLY         2
#define DEF_SENSOR_USED_AS_DEBUG           3

struct SensorStruct
{
  int type;  // defines type of sensor, e.g. angle sensor, force sensor ...
  int usage; // defines whether the sensor is used as input for control, if it
             //   is used otherwise (aux), e.g. for analysis, logging, fitness
             //   function ..., or internally

  /* defines the noise, which will be put to the sensor 
   * value in the range of 0..1 defines the gaussian standard deviation,
   * wherby zero means no noise */
  double noise;
  int noiseType;
  int srcCompound;
  int srcSeg;
};

struct AmbientLightSensorStruct
{  
  // returns the current intensity of the ambient light
  double intensity;
};


struct CoordinateSensorStruct
{  
  // defines which coordinate will be read and fed back to client
  int coordinate;
  // defines compound will be read
  int srcCompound;
  // defines seg of the compound (s.a) will be read
  int srcSeg;
};

struct AngleSensorStruct
{  
  /* defines the joint (NOT MOTOR) number of the segment, from which the angle
   * should be read */
  int connectorNum;

  /* defines the working range, in rad */
  double startAngle;
  double endAngle;
  
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;
};

struct AngleVelSensorStruct
{
  int connectorNum;

  /*defines working range */
  double startVel;
  double endVel;
  
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;
};

struct SliderPositionSensorStruct
{  
  /* defines the joint (NOT MOTOR) number of the segment, from which the angle
   * should be read */
  int connectorNum;

  /* defines the working range, in rad */
  double startPosition;
  double endPosition;
  
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;
};

struct SliderVelSensorStruct
{
  int connectorNum;

  /*defines working range */
  double startVel;
  double endVel;
  
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;
};

struct AngleFeedbackSensorStruct
{
  int connectorNum;

  int type;
  int feedbackTarget;
  int direction;

  /*defines working range */
  double startFeedback;
  double endFeedback;
  
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;
};

struct SharpGP2D12_37_SensorStruct
{
  /* defines the start position of the sensor in global coordinates */
  Position startPos;
  Position initStartPos;

   /* defines the rotation in euler angle, in radr*/
  Position direction;

  /* defines the mapping of the sensor outputs */
  double startValue;
  double endValue;

  /* defines the range of the sensor outputs */
  double startRange;
  double endRange;
};

struct SharpDM2Y3A003K0F_SensorStruct
{
  /* defines the start position of the sensor in global coordinates */
  Position startPos;
  Position initStartPos;

  /* defines the rotation in euler angle, in radr*/
  Position direction;

  /* defines the mapping of the sensor outputs */
  double startValue;
  double endValue;

  /* defines the range of the sensor outputs */
  double startRange;
  double endRange;
};


struct IrSensorStruct
{
  /* defines the start position of the sensor in global coordinates */
  Position startPos;
  Position initStartPos;

   /* defines the rotation in euler angle, in radr*/
  Position direction;

  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;

  /* defines in which angle (in rad) from the line between start and end pos,
   * the ir detect objects, e.g. 0.1 rad means that the sensor spread 0.05 rad
   * around */
  double spreadAngleX;
  double spreadAngleY;

  /* defines the range of the sensor outputs */
  double startRange;
  double endRange;

};

// TODO
struct GenericRotationSensorStruct
{
  int type;
  int orderDerivative;

  /* defines the range of the sensor outputs */
  double startRange;
  double endRange;
  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;

  /* defines the start position of the sensor in global coordinates */
  Position startPos;
  Position initStartPos;

   /* defines the rotation in euler angle, in radr*/
  Position rotation;

  bool drawRayRot;
  bool drawRayRef;

  double lengthRayRot;
  double lengthRayRef;

  int rayRotType;
  int rayRefType;

  Color rayRotColor;
  Color rayRefColor;
};

/* not finished yet */
struct LdrSensorStruct
{
  /* defines the position of the sensor in global coordinates */
  Position pos;
  Position init_pos;

  /* defines the range of the sensor outputs */
  double startValue;
  double endValue;

  /* defines the rotation in euler angle, in radr*/
  Position direction;

  /* defines at which light intensity the sensor starts to react and at which
   * itensity it ends */
  double startIntensity;
  double endIntensity;
  /* defines in which angle (in rad) from the line between start and end pos,
   * the ir detect objects, e.g. 0.1 rad means that the sensor spread 0.05 rad
   * around */
  double spreadAngleX;
  double spreadAngleY;
};

/**
 ** amount of sensor values of a DirectedCamera: x_pixel * y_pixel * colorType
 ** colorType: r/g/b -> 1
 ** colorType: greyscale -> 1
 ** colortype: RGB -> 3
 **/
struct DirectedCameraStruct
{
  //opening-angel of the camera used
  GLfloat openingAngle;
  Position pos;
  Position init_pos;
  // Mapping-values
  double startValue;
  double endValue;

  Position direction;

  //kind of colorInformation used: R, G, B, RGB, greyscale
  //influences the amount of sensor values 
  int colorType;

  //amount of pixels used for representing the view of the robot. 
  // influences the amount of sensors (sensorvalues)
  int xPixel;
  int yPixel;

  // size of the window where the camera view is displayed
  int light;
};


class SensorDescription
{
  public:
    SensorDescription(SensorStruct sensStruct);
    /* copy constructor */
    SensorDescription(SensorDescription* sensDes);
    ~SensorDescription();


    /*------------ BEGIN SET FUNCTIONS ----------------------------*/    
    
    void setName(string name);

    void setUsage(int usage);

    /* set the data for a coordinate sensor */
    void setCoordinateSensor(CoordinateSensorStruct sCoordinateStruct);

    /* set the data for a ambient light sensor */
    void setAmbientLightSensor(AmbientLightSensorStruct sAmbientLightStruct);

    /* set the data for an angle sensor */
    void setAngleSensor(AngleSensorStruct sAngleStruct);
 
    /* set the data for an angle vel sensor */
    void setAngleVelSensor(AngleVelSensorStruct sAngleStruct);
 
    /* set the data for an slider position sensor */
    void setSliderPositionSensor(SliderPositionSensorStruct sSliderStruct);
 
    /* set the data for an slider vel sensor */
    void setSliderVelSensor(SliderVelSensorStruct sSliderStruct);
 
    /* set the data for an angle force sensor */
    void setAngleFeedbackSensor(AngleFeedbackSensorStruct sAngleStruct);
 
    /* set the data for a ir sensor */
    void setIrSensor(IrSensorStruct sIrStruct);

    /* set the data for a generic rotation sensor */
    void setGenericRotationSensor(GenericRotationSensorStruct
        sGenericRotationStruct);

    /* set the data for a ir sensor */
    void setSharpGP2D12_37_Sensor(SharpGP2D12_37_SensorStruct sIrStruct);

    /* set the data for a ir sensor */
    void setSharpDM2Y3A003K0F_Sensor(SharpDM2Y3A003K0F_SensorStruct sIrStruct);  

    /* set the data for a ldr sensor */
    void setLdrSensor(LdrSensorStruct sLdrStruct);

    /* set the data for a camera sensor */
    void setDirectedCamera(DirectedCameraStruct sCameraStruct);

    /*------------ END SET FUNCTIONS ----------------------------*/    


    /*------------ BEGIN GET FUNCTIONS ----------------------------*/   

    void getName(string &name);

    /* get the data for a angle sensor */
    CoordinateSensorStruct* getCoordinateSensor();

    /* get the data for a ambient light sensor */
    AmbientLightSensorStruct* getAmbientLightSensor();

    /* get the data for a angle sensor */
    AngleSensorStruct* getAngleSensor();

    /* get the data for a angle vel sensor */
    AngleVelSensorStruct* getAngleVelSensor();

    /* get the data for a slider position sensor */
    SliderPositionSensorStruct* getSliderPositionSensor();

    /* get the data for a slider vel sensor */
    SliderVelSensorStruct* getSliderVelSensor();

    /* get the data for a angle vel sensor */
    AngleFeedbackSensorStruct* getAngleFeedbackSensor();

    /* get the data for a ir sensor */
    IrSensorStruct* getIrSensor();

    /* get the data for a ir sensor */
    GenericRotationSensorStruct* getGenericRotationSensor();
 
    /* get the data for a ir sensor */
    SharpGP2D12_37_SensorStruct* getSharpGP2D12_37_Sensor();

    /* get the data for a ir sensor */
    SharpDM2Y3A003K0F_SensorStruct* getSharpDM2Y3A003K0F_Sensor();    

    /* get the data for a ldr sensor */
    LdrSensorStruct* getLdrSensor();

    DirectedCameraStruct* getDirectedCamera();

    /*------------ END GET FUNCTIONS ----------------------------*/   

    /* return the sensor main data */
    SensorStruct* getSensorStruct();

  private:
    SensorStruct sStruct_;
    
    CoordinateSensorStruct         sCoordinateStruct_;
    AmbientLightSensorStruct       sAmbientLightStruct_;
    AngleSensorStruct              sAngleStruct_;
    AngleVelSensorStruct           sAngleVelStruct_;
    SliderPositionSensorStruct     sSliderPositionStruct_;
    SliderVelSensorStruct          sSliderVelStruct_;
    AngleFeedbackSensorStruct      sAngleFeedbackStruct_;
    LdrSensorStruct                sLdrStruct_;
    IrSensorStruct                 sIrStruct_;
    SharpGP2D12_37_SensorStruct    sharpIrGP2D12_37Struct_;
    SharpDM2Y3A003K0F_SensorStruct sharpIrDM2Y3A003K0FStruct_;
    DirectedCameraStruct           sDirectedCamera_;
    GenericRotationSensorStruct    sGenericRotationStruct_;
    string                         _name;

};


#endif
