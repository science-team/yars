/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "RobotDescription.h"
#include <util/RandomiseFunctions.h>
#include <math.h>
#include <string.h>
#include <sstream>


RobotDescription::RobotDescription()
{
  initPosition(&_initialPosition);
  initPosition(&_initialOrientation);
  _moveableType = -1;
  _controller   = "";
}

RobotDescription::RobotDescription(RobotDescription* clone)
{
  copyPosition(&_initialPosition, clone->_initialPosition);
  copyPosition(&_initialOrientation, clone->_initialOrientation);
  this->_segmentCompounds   = *( clone->getCompounds() );
  this->_compoundConnectors = *( clone->getCompoundConnectors() );
  this->_sensors         = *( clone->getSensors() );
  this->_moveableType    = clone->getType();
  this->_controller      = clone->getController();
}

void RobotDescription::copyPosition(Position *dest, Position source)
{
  dest->x                       = source.x;
  dest->y                       = source.y;
  dest->z                       = source.z;
  dest->radius                  = source.radius;
  dest->length                  = source.length;
  dest->randomise.enabled       = source.randomise.enabled;
  dest->randomise.type          = source.randomise.type;
  dest->randomise.method        = source.randomise.method;
  dest->randomise.last_index    = source.randomise.last_index;

  dest->randomise.min.x         = source.randomise.min.x;
  dest->randomise.min.y         = source.randomise.min.y;
  dest->randomise.min.z         = source.randomise.min.z;

  dest->randomise.max.x         = source.randomise.max.x;
  dest->randomise.max.y         = source.randomise.max.y;
  dest->randomise.max.z         = source.randomise.max.z;

  dest->randomise.variance.x    = source.randomise.variance.x;
  dest->randomise.variance.y    = source.randomise.variance.y;
  dest->randomise.variance.z    = source.randomise.variance.z;
  dest->randomise.probability.x = source.randomise.probability.x;
  dest->randomise.probability.y = source.randomise.probability.y;
  dest->randomise.probability.z = source.randomise.probability.z;

  dest->randomise.entries       = source.randomise.entries;
}


RobotDescription::~RobotDescription()
{
}

void RobotDescription::setInitialPosition(Position pos)
{
  copyPosition(&_initialPosition, pos);

  Y_DEBUG("RobotDescription::setInitialPosition to %f, %f, %f",
      _initialPosition.x,
      _initialPosition.y,
      _initialPosition.z);
}

void RobotDescription::setInitialOrientation(Position pos)
{
  copyPosition(&_initialOrientation, pos);

  Y_DEBUG("RobotDescription::setInitialOrientation to %f, %f, %f",
      _initialOrientation.x,
      _initialOrientation.y,
      _initialOrientation.z);
}

void RobotDescription::setType(int type) 
{
  _moveableType = type;
}

void RobotDescription::setController( string controller)
{
  _controller = controller;

}

string RobotDescription::getController()
{
  return _controller; 
}

int RobotDescription::getType()
{
  return _moveableType;
}

Position RobotDescription::getInitialPosition()
{
  return _initialPosition;
}

/*-------------BEGIN SEGMENTS FUNCTIONS---------------------------------*/

void RobotDescription::randomiseInitialPosition()
{
  if(_initialPosition.randomise.enabled)
  {
    Y_DEBUG("randomising initial position %f,%f,%f",
        _initialPosition.x,
        _initialPosition.y,
        _initialPosition.z);

    RandomiseFunctions::randomisePosition(&_initialPosition);

    Y_DEBUG("--> new initial position %f,%f,%f",
        _initialPosition.x,
        _initialPosition.y,
        _initialPosition.z);
  }

  if(_initialOrientation.randomise.enabled)
  {
    Y_DEBUG("randomising initial orientation %f,%f,%f",
        _initialOrientation.x,
        _initialOrientation.y,
        _initialOrientation.z);

    RandomiseFunctions::randomisePosition(&_initialOrientation);

    Y_DEBUG("--> new initial orientation %f,%f,%f",
        _initialOrientation.x,
        _initialOrientation.y,
        _initialOrientation.z);

  }

  moveAllObjects(_initialPosition);
  turnAllSegments(_initialOrientation);
}

void RobotDescription::addCompoundDescription(CompoundDescription segVec)
{
  Y_DEBUG("RobotDescription::addCompoundDescription");
  CompoundDescription tmp = segVec;
  _segmentCompounds.push_back(tmp);  
  Y_DEBUG("RobotDescription::addCompoundDescription: size of vector is now: %d",
      _segmentCompounds.size() );
}

vector<CompoundDescription>* RobotDescription::getCompounds()
{
  return &_segmentCompounds;
}

unsigned int RobotDescription::getCompoundNumber()
{
  return _segmentCompounds.size();
}

CompoundDescription* RobotDescription::getCompound(unsigned int index)
{
  if(index >= this->getCompoundNumber() )
  {
    Y_FATAL("RobotDescription::getCompound");
    Y_FATAL("\t Segment Compound number: %d doesn't exist.", index);
    return NULL;
  }

  return &(_segmentCompounds[index]);
}

unsigned int RobotDescription::getTotalNumOfSegJoints(unsigned int
    segCompound, unsigned int seg)
{
  int connNum = 0;
  if(segCompound >= this->getCompoundNumber() )
  {
    Y_FATAL("RobotDescription::getTotalNumOfSegJoints");
    Y_FATAL("\t Segment Compound number: %d doesn't exist", segCompound);
    return 0;
  }
  if(seg >= _segmentCompounds[segCompound].segments.size() )
  {
    Y_FATAL("RobotDescription::getTotalNumOfSegJoints");
    Y_FATAL("\t Segment number: %d doesn't exist!", seg);
    return 0;
  }

  /* _all object connectors */
  connNum = ((_segmentCompounds[segCompound]).segments[seg]).connectors.size();

  /* now look if this segment owns additional compound connectors */
  for(unsigned int i = 0; i < _compoundConnectors.size(); ++i)
  {
    if(_compoundConnectors[i].sourceCompoundNum == int(segCompound) &&
         _compoundConnectors[i].sourceSegNum == int(seg))
    {
      connNum++;
    }
  }

  return connNum;
}

/*-------------END SEGMENTS FUNCTIONS-----------------------------------*/


/*-------------BEGIN COMPOUND CONNECTOR FUNCTIONS--------------------------*/

void RobotDescription::addCompoundConnector(ConnectorDescription connector)
{
  ConnectorDescription tmp = connector;
  _compoundConnectors.push_back(tmp);
}

ConnectorVector* RobotDescription::getCompoundConnectors()
{
  return &_compoundConnectors;
}

unsigned int RobotDescription::getCompoundConnectorNumber()
{
  return _compoundConnectors.size();
}

ConnectorDescription* RobotDescription::getCompoundConnector(unsigned int index)
{
  if(index >= this->getCompoundConnectorNumber() )
  {
    Y_FATAL("RobotDescription::getCompoundConnector");
    Y_FATAL("\t CompoundConnector number: %d doesn't exist.", index);
    return NULL;
  }

  return &(_compoundConnectors[index]);
}

/*-------------END COMPOUND CONNECTOR FUNCTIONS----------------------------*/


/*-------------BEGIN SENSOR FUNCTIONS-----------------------------------*/

void RobotDescription::addSensor(SensorDescription *sensDes)
{
  SensorDescription *tmp = new SensorDescription(sensDes);
  _sensors.push_back(tmp);
  delete tmp;
}

SensDesVector* RobotDescription::getSensors()
{
  return &_sensors;
}

unsigned int RobotDescription::getSensorNumber()
{
  return _sensors.size();
}

SensorDescription* RobotDescription::getSensor(unsigned int index)
{
  if(index >= this->getSensorNumber() )
  {
    Y_FATAL("RobotDescription::getSensor");
    Y_FATAL("\t Sensor number: %d doesn't exist.", index);
    return NULL;
  }

  return &(_sensors[index]);
}
/*-------------END SENSOR FUNCTIONS-------------------------------------*/


void RobotDescription::moveAllObjects(Position offset)
{
  CompoundDescription                  *sc;
  SingleSegment                  *segment;
  ConnectorVector                *cv;
  ConnectorDescription                      *c;
  ConnectorVector                *cc;
  SensDesVector                  *sensorVector;
  SensorDescription              *sensorDescription;
  IrSensorStruct                 *irSensor;
  GenericRotationSensorStruct    *gRotSensor;
  SharpGP2D12_37_SensorStruct    *sIrSensor;
  SharpDM2Y3A003K0F_SensorStruct *sIrDM2Y3A003K0FSensor;
  LdrSensorStruct                *ldrSensor;
  DirectedCameraStruct           *cameraSensor;
  double x = offset.x;
  double y = offset.y;
  double z = offset.z;

  Y_DEBUG("RobotDescription::moveAllObjects to %f, %f, %f",x,y,z);
  for(uint i=0; i<this->getCompoundNumber(); i++)
  {

    sc = this->getCompound(i);
    for(uint j=0; j < sc->segments.size(); j++)
    {
      segment = &((*sc).segments[j]);
      segment->position.x = segment->init_position.x + x;
      segment->position.y = segment->init_position.y + y;
      segment->position.z = segment->init_position.z + z;
      cv = &(segment->connectors);
      for(uint k=0; k< cv->size(); k++)
      {
        c = &((*cv)[k]);
        c->anchorPos.x = c->anchorInitPos.x + x;
        c->anchorPos.y = c->anchorInitPos.y + y;
        c->anchorPos.z = c->anchorInitPos.z + z;
        copyPosition(&(c->rotationAxis), c->initRotationAxis);
        copyPosition(&(c->rotationAxis2), c->initRotationAxis2);
        copyPosition(&(c->sliderAxis), c->initSliderAxis);
      }
    }
  }

  cc = this->getCompoundConnectors();

  for(uint i = 0; i < cc->size(); i++)
  {
    c = &((*cc)[i]);
    c->anchorPos.x = c->anchorInitPos.x + x;
    c->anchorPos.y = c->anchorInitPos.y + y;
    c->anchorPos.z = c->anchorInitPos.z + z;
    copyPosition(&(c->rotationAxis), c->initRotationAxis);
    copyPosition(&(c->rotationAxis2), c->initRotationAxis2);
    copyPosition(&(c->sliderAxis), c->initSliderAxis);
    Y_DEBUG("CompoundConnector %f, %f, %f", 
        c->rotationAxis.x,
        c->rotationAxis.y,
        c->rotationAxis.z);
  }

  sensorVector = this->getSensors();

  for(uint i=0; i < sensorVector->size(); i++)
  {
    sensorDescription = &((*sensorVector)[i]);

    switch(sensorDescription->getSensorStruct()->type)
    {
      case SD_IR_SENSOR:
        irSensor = sensorDescription->getIrSensor();
        irSensor->startPos.x = irSensor->initStartPos.x + x;
        irSensor->startPos.y = irSensor->initStartPos.y + y;
        irSensor->startPos.z = irSensor->initStartPos.z + z;
        break;
      case SD_GEN_ROTATION_SENSOR:
        gRotSensor = sensorDescription->getGenericRotationSensor();
        gRotSensor->startPos.x = gRotSensor->initStartPos.x + x;
        gRotSensor->startPos.y = gRotSensor->initStartPos.y + y;
        gRotSensor->startPos.z = gRotSensor->initStartPos.z + z;
        break;
      case SD_SHARP_GP2D12_37_SENSOR:
        sIrSensor = sensorDescription->getSharpGP2D12_37_Sensor();
        sIrSensor->startPos.x = sIrSensor->initStartPos.x + x;
        sIrSensor->startPos.y = sIrSensor->initStartPos.y + y;
        sIrSensor->startPos.z = sIrSensor->initStartPos.z + z;
        Y_DEBUG("SensorPosition %f, %f, %f", 
                     sIrSensor->startPos.x,
                     sIrSensor->startPos.y,
                     sIrSensor->startPos.z);
        break;
      case SD_SHARP_DM2Y3A003K0F_SENSOR:
        sIrDM2Y3A003K0FSensor = 
          sensorDescription->getSharpDM2Y3A003K0F_Sensor();
        sIrDM2Y3A003K0FSensor->startPos.x =
          sIrDM2Y3A003K0FSensor->initStartPos.x + x;
        sIrDM2Y3A003K0FSensor->startPos.y =
          sIrDM2Y3A003K0FSensor->initStartPos.y + y;
        sIrDM2Y3A003K0FSensor->startPos.z =
          sIrDM2Y3A003K0FSensor->initStartPos.z + z;
        break;  
      case SD_LDR_SENSOR:
        ldrSensor = sensorDescription->getLdrSensor();
        ldrSensor->pos.x = ldrSensor->init_pos.x + x;
        ldrSensor->pos.y = ldrSensor->init_pos.y + y;
        ldrSensor->pos.z = ldrSensor->init_pos.z + z;
        break;

      case SD_DIRECTED_CAMERA_SENSOR:
        cameraSensor = sensorDescription->getDirectedCamera();
        cameraSensor->pos.x = cameraSensor->init_pos.x + x;
        cameraSensor->pos.y = cameraSensor->init_pos.y + y;
        cameraSensor->pos.z = cameraSensor->init_pos.z + z;
        break;
    }
  }
}

void RobotDescription::initPosition(Position *pos)
{
  pos->x                       = 0;
  pos->y                       = 0;
  pos->z                       = 0;
  pos->radius                  = 0;
  pos->length                  = 0;
  pos->randomise.enabled       = false;
  pos->randomise.type          = 0;
  pos->randomise.method        = 0;
  pos->randomise.last_index    = 0;

  pos->randomise.min.x         = 0;
  pos->randomise.min.y         = 0;
  pos->randomise.min.z         = 0;

  pos->randomise.max.x         = 0;
  pos->randomise.max.y         = 0;
  pos->randomise.max.z         = 0;

  pos->randomise.variance.x    = 0;
  pos->randomise.variance.y    = 0;
  pos->randomise.variance.z    = 0;
  pos->randomise.probability.x = 0;
  pos->randomise.probability.y = 0;
  pos->randomise.probability.z = 0;

  pos->randomise.entries.clear();
}


void RobotDescription::rotate(Position *pos, Position rotation)
{

  double x  = pos->x;
  double y  = pos->y;
  double z  = pos->z;

  double cx = cos(rotation.x);
  double cy = cos(rotation.y);
  double cz = cos(rotation.z);

  double sx = sin(rotation.x);
  double sy = sin(rotation.y);
  double sz = sin(rotation.z);

  pos->x = cz * ( cy * x + sy * z ) + sz * ( sx * ( cy * z - sy * x ) - cx * y);
  pos->y = sz * ( cy * x + sy * z ) - cz * ( sx * ( cy * z - sy * x ) - cx * y);
  pos->z = cx * ( cy * z - sy * x ) + sx * y;
}

void RobotDescription::turnAllSegments(Position offset)
{
  Y_DEBUG("RobotDescription::turnAllSegments %f %f %f",
      offset.x, offset.y, offset.z);
  CompoundDescription               *sc;
  SingleSegment               *segment;
  ConnectorVector             *cv;
  ConnectorDescription                   *c;
  ConnectorVector             *cc;
  SensDesVector               *sensorVector;
  SensorDescription           *sensorDescription;
  IrSensorStruct              *irSensor;
  GenericRotationSensorStruct *gRotSensor;
  SharpGP2D12_37_SensorStruct *sIrSensor;
  LdrSensorStruct             *ldrSensor;
  double x = offset.x;
  double y = offset.y;
  double z = offset.z;


  for(uint i=0; i<getCompoundNumber(); i++)
  {
    sc = getCompound(i);
    for(uint j=0; j < sc->segments.size(); j++)
    {
      segment = &((*sc).segments[j]);
      Y_DEBUG("rotating body %f %f %f with %f %f %f", 
          segment->position.x,
          segment->position.y,
          segment->position.z,
          offset.x,
          offset.y,
          offset.z);

      segment->position.x -= _initialPosition.x;
      segment->position.y -= _initialPosition.y;
      segment->position.z -= _initialPosition.z;

      rotate(&(segment->position), offset);

      segment->position.x += _initialPosition.x;
      segment->position.y += _initialPosition.y;
      segment->position.z += _initialPosition.z;

      segment->rotation.x = segment->init_rotation.x - offset.x;
      segment->rotation.y = segment->init_rotation.y - offset.y;
      segment->rotation.z = segment->init_rotation.z - offset.z;


      cv = &(segment->connectors);
      for(uint k=0; k< cv->size(); k++)
      {
        c = &((*cv)[k]);
        
        c->anchorPos.x -= _initialPosition.x;
        c->anchorPos.y -= _initialPosition.y;
        c->anchorPos.z -= _initialPosition.z;
        
        rotate(&(c->anchorPos), offset);
        rotate(&(c->rotationAxis), offset);
        rotate(&(c->rotationAxis2), offset);
        rotate(&(c->sliderAxis), offset);

        c->anchorPos.x +=  _initialPosition.x;
        c->anchorPos.y +=  _initialPosition.y;
        c->anchorPos.z +=  _initialPosition.z;
      }
    }
  }

  cc = getCompoundConnectors();

  for(uint i = 0; i < cc->size(); i++)
  {
    Y_DEBUG("rotating compound connector");
    c = &((*cc)[i]);

    sc = getCompound(c->sourceCompoundNum);
    SingleSegment *sourceSegment = &((*sc).segments[c->sourceSegNum]);

    c->anchorPos.x -= sourceSegment->position.x;
    c->anchorPos.y -= sourceSegment->position.y;
    c->anchorPos.z -= sourceSegment->position.z;

    rotate(&(c->anchorPos), offset);

    c->anchorPos.x += sourceSegment->position.x;
    c->anchorPos.y += sourceSegment->position.y;
    c->anchorPos.z += sourceSegment->position.z;

    rotate(&(c->rotationAxis), offset);
    rotate(&(c->rotationAxis2), offset);
    rotate(&(c->sliderAxis), offset);
    Y_DEBUG("CompoundConnector after turning anchorPos %f, %f, %f", 
                 c->anchorPos.x,
                 c->anchorPos.y,
                 c->anchorPos.z);

    Y_DEBUG("CompoundConnector after turning rotationAxis %f, %f, %f", 
        c->rotationAxis.x,
        c->rotationAxis.y,
        c->rotationAxis.z);

  }

}

void RobotDescription::writeAndVerifyNameTags()
{
  Y_DEBUG("### RobotDescription::writeAndVerifyNameTags ###");
  printNameInformation();
  writeNewNameTags();
  verifyNameTags();
  printNameInformation();
  Y_DEBUG("### RobotDescription::writeAndVerifyNameTags -- end ###");
}

void RobotDescription::verifyNameTags() throw(RobotDescriptionException)
{
  vector<string> falisifedNames;
  string s;
  string t;
  for(int i = 0; i < _allNameTagsVector.size()-1; i++)
  {
    s = _allNameTagsVector[i];
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::DEBUG << "name " << i << ": \"" << s << "\""
      << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT

  }

  for(int i = 0; i < _allNameTagsVector.size()-1; i++)
  {
    s = _allNameTagsVector[i];
    for(int j = (i+1); j < _allNameTagsVector.size(); j++)
    {
      t = _allNameTagsVector[j];
      if (t==s)
      {
        falisifedNames.push_back(s);
      }
    }
  }

  if(falisifedNames.size() > 0)
  {
    ostringstream oss;
    oss << "Tag names are not unique. Please check the following names" << endl;
    for(int i=0; i < falisifedNames.size(); i++)
    {
      oss << falisifedNames[i] << endl;
    }
    throw RobotDescriptionException(oss.str());
  }
}

void RobotDescription::setName(string name)
{
  _robotName = name;
}

void RobotDescription::getName(string &name)
{
  name = _robotName;
}

void RobotDescription::printNameInformation()
{
  CompoundDescription               *sc;
  SingleSegment               *segment;
  ConnectorVector             *cv;
  ConnectorDescription        *c;
  ConnectorVector             *cc;
  SensDesVector               *sensorVector;
  SensorDescription           *sensorDescription;
  string                      s; // temporary variable for sensor description read outs

#ifdef USE_LOG4CPP_OUTPUT
  logger << log4cpp::Priority::DEBUG << "Robot name is \""
    << _robotName << "\"" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT

  for(uint i=0; i < getCompoundNumber(); i++)
  {
    sc = getCompound(i);
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::DEBUG << "Compound name is \""
      << sc->name << "\"" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
    for(uint j=0; j < sc->segments.size(); j++)
    {
      segment = &((*sc).segments[j]);
      //Y_DEBUG 
#ifdef USE_LOG4CPP_OUTPUT
      logger << log4cpp::Priority::DEBUG << "Segment " <<  j << " is named \""
        << segment->name << "\"" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
      cv = &(segment->connectors);

      for(uint k=0; k< cv->size(); k++)
      {
        c = &((*cv)[k]);

#ifdef USE_LOG4CPP_OUTPUT
        logger << log4cpp::Priority::DEBUG << "Connector " <<  k
          << " is named \"" << c->name << "\""
          << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT

      }
    }
  }

  cc = getCompoundConnectors();

  for(uint i = 0; i < cc->size(); i++)
  {
    c = &((*cc)[i]);
    //Y_DEBUG("Compound Connector %d is named: \"%s\"", i, c->name);
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::DEBUG << "Compound Connector " <<  i
      << " is named \"" << c->name << "\"" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT

  }

  sensorVector = getSensors();

  for(uint i = 0; i < sensorVector->size(); i++)
  {
    sensorDescription = &((*sensorVector)[i]);
    sensorDescription->getName(s);
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::DEBUG << "Sensor " <<  i
      << " is named \"" << s << "\"" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
  }

}

void RobotDescription::concatNames(string &destination, string source,
    string alternative, int index)
{
  ostringstream oss;
  if(destination == DEF_UNAMED)
  {
    oss << source << DEF_NAME_SEPERATOR << alternative;

    if(index >= 0)
    {
      oss << " " << index;
    }
  }
  else
  {
    oss << source << DEF_NAME_SEPERATOR << destination;
  }

  destination = oss.str();
}

void RobotDescription::writeNewNameTags() throw(RobotDescriptionException)
{
  CompoundDescription         *compound;
  SingleSegment               *segment;
  ConnectorVector             *cv;
  ConnectorDescription        *c;
  ConnectorVector             *cc;
  SensDesVector               *sensorVector;
  SensorDescription           *sensorDescription;
  string                      s; // temporary variable for sensor description read outs
  string                      name; 
  string                      segmentName; 
  SensorStruct                *sensorStruct;

  _robotName = DEF_NAME_DELIMITER + _robotName;

  for(uint i=0; i < getCompoundNumber(); i++)
  {

    compound = getCompound(i);
    concatNames(compound->name, _robotName, DEF_COMPOUND_ALTERNATIVE_STRING, -1);

    for(uint j=0; j < compound->segments.size(); j++)
    {
      segment = &((*compound).segments[j]);

      concatNames(segment->name, compound->name, DEF_SEGMENT_ALTERNATIVE_STRING, -1);

      cv = &(segment->connectors);

      for(uint k=0; k< cv->size(); k++)
      {
        c = &((*cv)[k]);

        addConnectorName(c, segment->name, k);

      }
    }
  }

  cc = getCompoundConnectors();

  for(uint i = 0; i < cc->size(); i++)
  {
    c = &((*cc)[i]);
    compound = getCompound(i);
    addConnectorName(c, (*compound).name, i);
  }

  sensorVector = getSensors();

  for(uint i = 0; i < sensorVector->size(); i++)
  {
    sensorDescription = &((*sensorVector)[i]);
    sensorDescription->getName(name);
    sensorStruct = sensorDescription->getSensorStruct();
    if(sensorStruct->srcCompound < 0) // then by default last segment of last compound
    {
      compound = getCompound(getCompoundNumber()-1);
      segment = &((*compound).segments.back());
    }
    else
    {
      compound = getCompound(sensorStruct->srcCompound);
      segment = &((*compound).segments[sensorStruct->srcSeg]);
    }
    segmentName = segment->name;
    switch(sensorStruct->type)
    {
      case SD_ANGLE_SENSOR:
        concatNames(name, segmentName, DEF_SD_ANGLE_SENSOR_STRING, i);
        break;

      case SD_ANGLE_VEL_SENSOR:
        concatNames(name, segmentName, DEF_SD_ANGLE_VEL_SENSOR_STRING, i);
        break;

      case SD_IR_SENSOR:
        concatNames(name, segmentName, DEF_SD_IR_SENSOR_STRING, i);
        break;

      case SD_LDR_SENSOR:
        concatNames(name, segmentName, DEF_SD_LDR_SENSOR_STRING, i);
        break;

      case SD_SHARP_GP2D12_37_SENSOR:
        concatNames(name, segmentName, DEF_SD_SHARP_GP2D12_37_SENSOR_STRING, i);
        break;

      case SD_COORDINATE_SENSOR:
        concatNames(name, segmentName, DEF_SD_COORDINATE_SENSOR_STRING, i);
        break;

      case SD_ANGLE_FEEDBACK_SENSOR:
        concatNames(name, segmentName, DEF_SD_ANGLE_FEEDBACK_SENSOR_STRING, i);
        break;

      case SD_SLIDER_POSITION_SENSOR:
        concatNames(name, segmentName, DEF_SD_SLIDER_POSITION_SENSOR_STRING, i);
        break;

      case SD_SLIDER_VEL_SENSOR:
        concatNames(name, segmentName, DEF_SD_SLIDER_VEL_SENSOR_STRING, i);
        break;

      case SD_AMBIENT_LIGHT_SENSOR:
        concatNames(name, segmentName, DEF_SD_AMBIENT_LIGHT_SENSOR_STRING, i);
        break;

      case SD_SHARP_DM2Y3A003K0F_SENSOR:
        concatNames(name, segmentName, DEF_SD_SHARP_DM2Y3A003K0F_SENSOR_STRING, i);
        break;

      case SD_DIRECTED_CAMERA_SENSOR:
        concatNames(name, segmentName, DEF_SD_DIRECTED_CAMERA_SENSOR_STRING, i);
        break;

      case SD_GEN_ROTATION_SENSOR:
        concatNames(name, segmentName, DEF_SD_GEN_ROTATION_SENSOR_STRING, i);
        break;
      default:
        throw RobotDescriptionException("unknown sensor type");
        break;
    }
    name += DEF_NAME_DELIMITER;
    _allNameTagsVector.push_back(name);
    sensorDescription->setName(name);
  }

  for(uint i=0; i < getCompoundNumber(); i++)
  {

    compound = getCompound(i);
    compound->name += DEF_NAME_DELIMITER;

    for(uint j=0; j < compound->segments.size(); j++)
    {
      segment = &((*compound).segments[j]);

      segment->name += DEF_NAME_DELIMITER;
      _allNameTagsVector.push_back(segment->name);
    }
  }
  _robotName += DEF_NAME_DELIMITER;
  _allNameTagsVector.push_back(_robotName);
}

void RobotDescription::addConnectorName(ConnectorDescription *c,
    string segmentName, int index) throw(RobotDescriptionException)
{
  switch(c->type)
  {
    case RD_CONN_FIX:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_FIX_STRING, index);
      break;
    case RD_CONN_HINGE:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_HINGE_STRING, index);
      break;
    case RD_CONN_HINGE_2:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_HINGE_2_STRING, index);
      break;
    case RD_CONN_BALL:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_BALL_STRING, index);
      break;
    case RD_ANGLE_MOTOR:
      concatNames(c->name, segmentName,
          DEF_RD_ANGLE_MOTOR_STRING, index);
      break;
    case RD_VEL_MOTOR:
      concatNames(c->name, segmentName,
          DEF_RD_VEL_MOTOR_STRING, index);
      break;
    case RD_PASSIVE_JOINT:
      concatNames(c->name, segmentName,
          DEF_RD_PASSIVE_JOINT_STRING, index);
      break;
    case RD_CONN_SLIDER:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_SLIDER_STRING, index);
      break;
    case RD_CONN_COMPLEX_HINGE:
      concatNames(c->name, segmentName,
          DEF_RD_CONN_COMPLEX_HINGE_STRING, index);
      break;
    default:
      throw RobotDescriptionException("unknown connector type");
      break;
  }
  c->name += DEF_NAME_DELIMITER;
  _allNameTagsVector.push_back(c->name);
}
