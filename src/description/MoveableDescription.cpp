/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "MoveableDescription.h"

MoveableDescription::MoveableDescription()
{
  moveables_.clear();

  _numberOfDrawLines          = 0;

  _numberOfActiveMovables     = 0;
  _numberOfControlledMovables = 0;
  _numberOfMovingMovables     = 0;
}

MoveableDescription::~MoveableDescription()
{
  for(unsigned int i = 0; i  < moveables_.size(); ++i)
  {
    delete moveables_[i];
  }
}

RobotDescription* MoveableDescription::getRobotDescription(int robotIndex)
{
  return moveables_[robotIndex];
}

vector<RobotDescription*>* MoveableDescription::getRobotDescriptions()
{
  return &moveables_;
}

void MoveableDescription::addMovable(RobotDescription* robotDes)
{

  RobotDescription *tmp = new RobotDescription(robotDes);

  /** numberOfActiveMovables: number of needed client-Communcations
    create the vector moveables_ of all movables in the xml file. The vector
    contains all robots order by their type. The order of movables is: active,
    controlled, moving, passive at the same time the counters for the amount of
    active, controlled and moving movables is incremented adequately **/
  if(tmp->getType() == RD_TYPE_PASSIVE || moveables_.size() == 0)
  {
    moveables_.push_back(tmp);
    Y_DEBUG("MoveableDescription::moveables.size() = %d", moveables_.size());

    if(robotDes->getType() == RD_TYPE_ACTIVE) 
    {   
      _numberOfActiveMovables++;
    }
    if(robotDes->getType() == RD_TYPE_CONTROLLED)
    {
      _numberOfControlledMovables++;
    }
   if(robotDes->getType() == RD_TYPE_MOVING)
    {
      _numberOfMovingMovables++;
    }

    return;
  }

  // iterator needed for vector insert function
  vector<RobotDescription*>::iterator iterator = moveables_.begin();

  if(robotDes->getType() == RD_TYPE_ACTIVE) 
  {   
    _numberOfActiveMovables++;

    for(int i = 0; i < moveables_.size(); i++)
    {
        
      if (moveables_[i]->getType() == (RD_TYPE_CONTROLLED || RD_TYPE_MOVING ||RD_TYPE_PASSIVE)){
        break;
      }
      iterator++;
    }
  }
  if(robotDes->getType() == RD_TYPE_CONTROLLED)
  {
    _numberOfControlledMovables++;
  
  for(int i = 0; i < moveables_.size(); i++)
  {
        
    if (moveables_[i]->getType() == (RD_TYPE_PASSIVE || RD_TYPE_MOVING) ){
      break;
    }
    iterator++;
  }
  }

  if(robotDes->getType() == RD_TYPE_MOVING)
  {
    _numberOfMovingMovables++;
  
    for(int i = 0; i < moveables_.size(); i++)
    {
      if (moveables_[i]->getType() == RD_TYPE_PASSIVE)
      {
        break;
      }
      iterator++;
    }
  }

  moveables_.insert(iterator, tmp); 

  Y_DEBUG("MoveableDescription::moveables.size() = %d", moveables_.size());
  for(int i = 0; i < moveables_.size(); i++)
  {
    Y_DEBUG("MoveableDescription::addMovable found type = %d", moveables_[i]->getType());
  }
}

int MoveableDescription::getNumberOfActiveMovables(){
  Y_DEBUG("MoveableDescription::NrOfActiveMovables = %d", _numberOfActiveMovables);
  return _numberOfActiveMovables;
}

int MoveableDescription::getNumberOfControlledMovables(){
  Y_DEBUG("MoveableDescription::NrOfControlledMovables = %d", _numberOfControlledMovables);
  return _numberOfControlledMovables;
}


int MoveableDescription::getNumberOfMovingMovables(){
  Y_DEBUG("MoveableDescription::NrOfMovingMovables = %d", _numberOfMovingMovables);
  return _numberOfMovingMovables;
}


unsigned int MoveableDescription::getNumberOfMovables()
{
  return moveables_.size();
}

void MoveableDescription::setNumberOfDrawLines(int nr)
{
  _numberOfDrawLines = nr;
}

int MoveableDescription::getNumberOfDrawLines()
{
  return _numberOfDrawLines;
}

void MoveableDescription::printMoveableData()
{
  Y_DEBUG("MoveableDescription::moveables.size() = %d", moveables_.size());
  for(int i = 0; i < moveables_.size(); i++)
  {
    Y_DEBUG("MoveableDescription::addMovable found type = %d", moveables_[i]->getType());
  }
}
