/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _MOVEABLE_DESCRIPTION_H_
#define _MOVEABLE_DESCRIPTION_H_

#include <description/RobotDescription.h>
#include <string>
#include <vector>

using namespace std;

class MoveableDescription
{
  public:
    MoveableDescription();
    ~MoveableDescription();

    /* returns the the description of a singel robot */
    RobotDescription* getRobotDescription(int robotIndex);

    vector<RobotDescription*>* getRobotDescriptions();
    
    /* add one robotDescription to the movable vector */
    void addMovable(RobotDescription* robotDes);
 
    /* retunrs the number of robots */
    unsigned int getNumberOfMovables();
   
    int getNumberOfActiveMovables();
 
    int getNumberOfControlledMovables();
    /* returns the number of moving robots (controlled by forces)*/
    int getNumberOfMovingMovables();
    
    void setNumberOfDrawLines(int nr);
 
    int getNumberOfDrawLines();

  private:
    /* vector of all movables in order
        -> first all active movables, then the passive ones*/
    vector<RobotDescription*> moveables_;

    int _numberOfDrawLines;

    int _numberOfActiveMovables;

    int _numberOfControlledMovables;
    
    int _numberOfMovingMovables;

    void printMoveableData();
};

#endif
