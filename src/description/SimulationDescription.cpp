/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "SimulationDescription.h"

SimulationDescription::SimulationDescription()
{
  /* default */
  _cameraPos.x = 0.0;
  _cameraPos.y = 0.0;
  _cameraPos.z = 5.0;
  
  _cameraRot.x = 0.0;
  _cameraRot.y = 0.0;
  _cameraRot.z = 0.0;

  _physSimFreq = -1.0;
  _netFreq     = -1.0;

  _drawAxes    = true;
  _axesScaling = 1.0;
}

SimulationDescription::~SimulationDescription()
{
}

void SimulationDescription::setCamera(Position pos, Position rot)
{
  _cameraPos = pos;
  _cameraRot = rot;
}

void SimulationDescription::setCameraPosition(double x, double y, double z)
{
  _cameraPos.x = x;
  _cameraPos.y = y;
  _cameraPos.z = z;
}
 
void SimulationDescription::setCameraOrientation(double x, double y, double z)
{
  _cameraRot.x = x;
  _cameraRot.y = y;
  _cameraRot.z = z;
}
 
void SimulationDescription::setWindowSize(double x, double y)
{
  _windowSize.x = x;
  _windowSize.y = y;
  _windowSize.z = 0;
}

void SimulationDescription::setUpdateFrequency(double physSimFreq, 
    double netFreq)
{
  _physSimFreq = physSimFreq;
  _netFreq     = netFreq;
}
 
Position SimulationDescription::getCameraPos()
{
  return _cameraPos;
}
    
Position SimulationDescription::getCameraRot()
{
  return _cameraRot;
}

Position SimulationDescription::getWindowSize()
{
  return _windowSize;
}

double SimulationDescription::getPhysSimFrequency()
{
  return _physSimFreq;
}

double SimulationDescription::getNetFrequency()
{
  return _netFreq;
}

void SimulationDescription::setAxesVisualization(
    bool visualization, double axesScaling)
{
  this->_drawAxes    = visualization;
  this->_axesScaling = axesScaling;
}

bool SimulationDescription::isAxesVisualization()
{
  return this->_drawAxes;
}

double SimulationDescription::getAxesScaling()
{
  return this->_axesScaling;
}

