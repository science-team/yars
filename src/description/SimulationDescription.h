/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 

#ifndef _SIMULATION_DESCRIPTION_H_
#define _SIMULATION_DESCRIPTION_H_

#include "DescriptionHelper.h"


class SimulationDescription
{
  public:
    SimulationDescription();
    ~SimulationDescription();

    // set the position and orientation for the camera, orientation in deg 
    void setCamera(Position pos, Position rot);
 
    // returns cameras position 
    Position getCameraPos();
    
    // returns the camera orientation
    Position getCameraRot();

    // returns the window configuration
    Position getWindowSize();

    // returns the update frequency of the physical simulation
    double getPhysSimFrequency();

    // returns the update frequency of the neural network 
    double getNetFrequency();

    // set the position of the camera 
    void setCameraPosition(double x, double y, double z);

    // set the orientation of the camera 
    void setCameraOrientation(double x, double y, double z);

    // set the window config 
    void setWindowSize(double x, double y);

    // set the update frequency of physical sim and net
    void setUpdateFrequency(double physSimFreq, double netFreq);

    // set the axes visualization
    void setAxesVisualization(bool visualization, double axesScaling);

    bool isAxesVisualization();

    double getAxesScaling();

  private:
    Position _cameraPos;
    Position _cameraRot;
    Position _windowSize;
    double   _physSimFreq;
    double   _netFreq;
    bool     _drawAxes;
    double   _axesScaling;
};

#endif
