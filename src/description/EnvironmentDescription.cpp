/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "EnvironmentDescription.h"
#include <util/RandomiseFunctions.h>

EnvironmentDescription::EnvironmentDescription()
{
  _randomise.enabled     = false;
  _randomise.type        = -1;
  _randomise.last_index  = -1;
  _randomise.min         = 0;
  _randomise.max         = 0;
  _randomise.variance    = 0;
  _randomise.probability = 0;
}

EnvironmentDescription::~EnvironmentDescription()
{

}

/*-------------------- BEGIN OBJECT STUFF ---------------------------- */

void EnvironmentDescription::addElement(SingleElement elem)
{
  SingleElement tmp = elem;
  allElements_.push_back(tmp);  
}

ElementVector* EnvironmentDescription::getElements()
{
 return &allElements_;
}

unsigned int EnvironmentDescription::getNumberOfElements()
{
  return allElements_.size();
}

SingleElement* EnvironmentDescription::getElement(unsigned int index)
{
  return &(allElements_[index]);
}

/*-------------------- END OBJECT STUFF ------------------------------ */


/*-------------------- BEGIN LIGHT STUFF ---------------------------- */

void EnvironmentDescription::setAmbientLight(double intensity)
{
  ambientLight_ = intensity;
}

void EnvironmentDescription::setAmbientLightRandomise(RandomiseScalar randomise)
{
  _randomise = randomise;
  Y_DEBUG("got RandomiseScalar:");
  Y_DEBUG("enabled     = %d", randomise.enabled);
  Y_DEBUG("type        = %d", randomise.type);
  Y_DEBUG("method      = %d", randomise.method);
  Y_DEBUG("last_index  = %d", randomise.last_index);
  Y_DEBUG("min         = %f", randomise.min);
  Y_DEBUG("max         = %f", randomise.max);
  Y_DEBUG("variance    = %f", randomise.variance);
  Y_DEBUG("probability = %f", randomise.probability);
  for(int i=0; i < randomise.entries.size(); i++)
  {
    Y_DEBUG("entry %d = %f",i, randomise.entries[i]);
  }
}

void EnvironmentDescription::randomiseAmbientLight()
{
  Y_DEBUG("EnvironmentDescription::randomiseAmbientLight: start %f",
      ambientLight_);
  RandomiseFunctions::randomiseScalar(&ambientLight_, &_randomise);
  Y_DEBUG("EnvironmentDescription::randomiseAmbientLight: end %f",
      ambientLight_);
}

double EnvironmentDescription::getAmbientLight()
{
  return ambientLight_;
}

void EnvironmentDescription::addLight(LightSource light)
{
  LightSource tmp = light;
  allLights_.push_back(tmp);
}

LightVector* EnvironmentDescription::getLights()
{
  return &allLights_;
}

unsigned int EnvironmentDescription::getNumberOfLights()
{
  return allLights_.size();
}

LightSource* EnvironmentDescription::getLight(unsigned int index)
{
  return &(allLights_[index]);
}

void EnvironmentDescription::setLightPosition(unsigned int index, Position pos)
{
  allLights_[index].pos = pos;
}

void EnvironmentDescription::setLightIntensity(unsigned int index, double
    intensity)
{
  allLights_[index].intensity = intensity; 
}
/*-------------------- END LIGHT STUFF ------------------------------- */
