/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "SensorDescription.h"

#include <string.h>

SensorDescription::SensorDescription(SensorStruct sensStr)
{
  SensorStruct tmp = sensStr;
  sStruct_         = tmp;
}

SensorDescription::SensorDescription(SensorDescription* clone)
{
  this->sStruct_                   = *( clone->getSensorStruct() );

  this->sAngleStruct_              = *( clone->getAngleSensor() );
  this->sAngleVelStruct_           = *( clone->getAngleVelSensor() );
  this->sSliderPositionStruct_     = *( clone->getSliderPositionSensor() );
  this->sSliderVelStruct_          = *( clone->getSliderVelSensor() );
  this->sAngleFeedbackStruct_      = *( clone->getAngleFeedbackSensor() );
  this->sIrStruct_                 = *( clone->getIrSensor() );
  this->sGenericRotationStruct_    = *( clone->getGenericRotationSensor() );
  this->sLdrStruct_                = *( clone->getLdrSensor() );
  this->sCoordinateStruct_         = *( clone->getCoordinateSensor());
  this->sAmbientLightStruct_       = *( clone->getAmbientLightSensor());
  this->sharpIrGP2D12_37Struct_    = *( clone->getSharpGP2D12_37_Sensor() );
  this->sharpIrDM2Y3A003K0FStruct_ = *( clone->getSharpDM2Y3A003K0F_Sensor() );
  this->sDirectedCamera_           = *( clone->getDirectedCamera());

  clone->getName(_name);

}


SensorDescription::~SensorDescription()
{
}

/*------------ BEGIN SET FUNCTIONS ----------------------------*/    

void SensorDescription::setName(string name)
{
  _name = name;
}

void SensorDescription::setCoordinateSensor(
    CoordinateSensorStruct sCoordinateStruct)
{
  CoordinateSensorStruct tmp = sCoordinateStruct;
  sCoordinateStruct_      = tmp;
}

void SensorDescription::setAmbientLightSensor(
    AmbientLightSensorStruct sAmbientLightStruct)
{
  AmbientLightSensorStruct tmp = sAmbientLightStruct;
  sAmbientLightStruct          = tmp;
}


void SensorDescription::setAngleSensor(AngleSensorStruct sAngleStruct)
{
  AngleSensorStruct tmp = sAngleStruct;
  sAngleStruct_      = tmp;
}

void SensorDescription::setAngleVelSensor(AngleVelSensorStruct sAngleVelStruct)
{
  AngleVelSensorStruct tmp = sAngleVelStruct;
  sAngleVelStruct_   = tmp;
}

void SensorDescription::setSliderPositionSensor(
    SliderPositionSensorStruct sSliderPositionStruct)
{
  SliderPositionSensorStruct tmp = sSliderPositionStruct;
  sSliderPositionStruct_         = tmp;
}

void SensorDescription::setSliderVelSensor(
    SliderVelSensorStruct sSliderVelStruct)
{
  SliderVelSensorStruct tmp = sSliderVelStruct;
  sSliderVelStruct_   = tmp;
}

void SensorDescription::setAngleFeedbackSensor(
    AngleFeedbackSensorStruct sAngleFeedbackStruct)
{
  AngleFeedbackSensorStruct tmp = sAngleFeedbackStruct;
  sAngleFeedbackStruct_   = tmp;
}

void SensorDescription::setIrSensor(IrSensorStruct sIrStruct)
{
  IrSensorStruct tmp = sIrStruct;
  sIrStruct_   = tmp;
}

void SensorDescription::setGenericRotationSensor(GenericRotationSensorStruct
    sGenericRotationStruct)
{
  GenericRotationSensorStruct tmp = sGenericRotationStruct;
  sGenericRotationStruct_         = tmp;
}

void SensorDescription::setSharpGP2D12_37_Sensor(SharpGP2D12_37_SensorStruct
    sIrStruct)
{
  SharpGP2D12_37_SensorStruct tmp = sIrStruct;
  sharpIrGP2D12_37Struct_   = tmp;
}
void SensorDescription::setSharpDM2Y3A003K0F_Sensor(SharpDM2Y3A003K0F_SensorStruct
    sIrStruct)
{
  SharpDM2Y3A003K0F_SensorStruct tmp = sIrStruct;
  sharpIrDM2Y3A003K0FStruct_   = tmp;
}


void SensorDescription::setLdrSensor(LdrSensorStruct sLdrStruct)
{
  LdrSensorStruct tmp = sLdrStruct;
  sLdrStruct_   = tmp;
}

void SensorDescription::setDirectedCamera(DirectedCameraStruct sCameraStruct)
{
  DirectedCameraStruct tmp = sCameraStruct;
  sDirectedCamera_ = tmp;
}

/*------------ END SET FUNCTIONS ----------------------------*/    


/*------------ BEGIN GET FUNCTIONS ----------------------------*/   

void SensorDescription::getName(string &name)
{
  name = _name;
}

CoordinateSensorStruct* SensorDescription::getCoordinateSensor()
{
  return &sCoordinateStruct_;
}

AmbientLightSensorStruct* SensorDescription::getAmbientLightSensor()
{
  return &sAmbientLightStruct_;
}

AngleSensorStruct* SensorDescription::getAngleSensor()
{
  return &sAngleStruct_;
}

AngleVelSensorStruct* SensorDescription::getAngleVelSensor()
{
  return &sAngleVelStruct_;
}

SliderPositionSensorStruct* SensorDescription::getSliderPositionSensor()
{
  return &sSliderPositionStruct_;
}

SliderVelSensorStruct* SensorDescription::getSliderVelSensor()
{
  return &sSliderVelStruct_;
}

AngleFeedbackSensorStruct* SensorDescription::getAngleFeedbackSensor()
{
  return &sAngleFeedbackStruct_;
}

IrSensorStruct* SensorDescription::getIrSensor()
{
  return &sIrStruct_;
}

GenericRotationSensorStruct* SensorDescription::getGenericRotationSensor()
{
  return &sGenericRotationStruct_;
}

SharpGP2D12_37_SensorStruct* SensorDescription::getSharpGP2D12_37_Sensor()
{
  return &sharpIrGP2D12_37Struct_;  
}

SharpDM2Y3A003K0F_SensorStruct* SensorDescription::getSharpDM2Y3A003K0F_Sensor()
{
  return &sharpIrDM2Y3A003K0FStruct_;  
}


LdrSensorStruct* SensorDescription::getLdrSensor()
{
  return &sLdrStruct_;
}

DirectedCameraStruct* SensorDescription::getDirectedCamera()
{
  return &sDirectedCamera_;
}

/*------------ END GET FUNCTIONS ----------------------------*/   

SensorStruct* SensorDescription::getSensorStruct()
{
  return &sStruct_;
}
