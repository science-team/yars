/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Rotate.h"

using namespace std;


void Rotate::update()
{
  double fx, fy, fz;
  i++;
  
  if(i<12)
  {
  fx = -0.2*sin(0.4*i);
  fy = 0.2*cos(0.4*i);
  fz = 0;
  }
  else {
    if(i<24)
    {
      fx = 0.2*sin(0.4*i);
      fy = -0.2*cos(0.4*i);
          fz = 0;
    }
    else{
      i=0;
    fx=0;
    fy=0;
    fz=0;
  }
  }
  setForces(fx, fy, fz);  
}

void Rotate::init()
{
  _forces.resize(3);
  _forces[0] = 0;
  _forces[1] = 0;
  _forces[2] = 0;
  i =0;
}

extern "C" ObjectController* createController() {
  ObjectController *controller = new Rotate;
  controller->init();
  return controller;
}
