/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Oscillator.h"

using namespace std;

int i=0;

void Oscillator::update()
{
  
  // diagonal front and back moving
  double fx, fy, fz;
  i++;
  fx = 0.1*cos(0.01*i);
  if(fx < 0)
  {
    fx= fx-2.85;
  } else
  {
    fx = fx + 2.85;
  }
  fy = 0.1*cos(0.01*i);
  if(fy < 0)
  {
    fy= fy-2.85;
  } else
  {
    fy = fy + 2.85;
  }
  fz = 0;
  setForces(fx, fy, fz);  
}



extern "C" ObjectController* createController() {
  ObjectController *controller = new Oscillator;
  controller->init();
  return controller;
}
