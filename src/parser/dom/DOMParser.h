/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef __DOMPARSER_H__
#define __DOMPARSER_H__

#include <xercesc/util/PlatformUtils.hpp>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMWriter.hpp>

#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>

#include <description/MoveableDescription.h>
#include <description/RobotDescription.h>
#include <description/EnvironmentDescription.h>
#include <description/SimulationDescription.h>
#include <description/SensorDescription.h>
#include <util/defines.h>


#define DP_READ_OK    0
#define DP_READ_ERROR 1
#define DP_OFFSET     90

XERCES_CPP_NAMESPACE_USE

class DOMParser 
{

  public:
    DOMParser(string pathToXsd);
    ~DOMParser();
    int read(char *filename,
        MoveableDescription *md,
        EnvironmentDescription *ed,
        SimulationDescription *sd);

  protected:
  private:
    void printAllNodes(DOMNode *doc);
    void printNodeList(DOMNodeList *nodeList, int treedepth);
    void parseDocument(DOMNode *rootNode);
    void processEnvironmentTag(DOMNode *environmentNode);
    void processSimulationTag(DOMNode *simulationTag);
    void extractCameraPosition(DOMNode *cameraPositionNode);
    void extractCameraOrientation(DOMNode *cameraOrientationNode);
    void extractCoordinate(DOMNode *coordinateNode, Position *position);
    void extractOrientation(DOMNode *orientationNode, Position *rotation);
    void extractWindowSize(DOMNode *windowSizeNode);
    void extractUpdateFrequency(DOMNode *updateFrequencyNode);
    void extractAxesVisualization(DOMNode *updateFrequencyNode);
    void extractGraphicalRepresentation(DOMNode *coordinateNode, 
        SingleSegment *segment);
    void extractColor(DOMNode *colorNode, Color *color);
    void extractDimension(DOMNode *dimensionNode, Position *position);
    void extractRandomiseList(DOMNode *orientationNode, Randomise *randomise);
    void extractRandomiseInterval(DOMNode *orientationNode, 
        Randomise *randomise);
    void extractP3D(DOMNode *probabilityNode, P3D *p3d);
    void extractIntervall(DOMNode *intervalNode, P3D *min, P3D *max);
    void extractEntry(DOMNode *entryNode, vector<P3D> *entries);

    void processCompositeTag(DOMNode *compositeNode, 
        SingleSegment *segment, RobotDescription *robotDescription, 
        CompoundDescription *compound);
    void processRigidBodyTag(DOMNode *rBNode, SingleSegment *segment,
        RobotDescription *robotDescription, CompoundDescription *compound);
    void processGeomsRelativeToRBTag(DOMNode *gRelTag, SingleSegment *segment,
        RobotDescription *robotDescription, CompoundDescription *compound);
    void processObjectTag(DOMNode *boxNode, SingleSegment *segment,
        RobotDescription *robotDescription, 
        CompoundDescription *compound);
    void initialiseSingleSegment(SingleSegment *segment);

    void initialiseRandomiseScalar(RandomiseScalar *randomise);

    void initPosition(Position *pos);

    void processRandomBars(DOMNode *randomBarsNode);

    void extractRandomiseScalarInterval(DOMNode *randomiseNode,
        RandomiseScalar *randomise);

    void copySegmentToElement(SingleElement *element, 
        SingleSegment segment);
    void printElement(SingleElement element);
    void printSegment(SingleSegment segment);


    void processMovableTag(DOMNode *movableTag);

    void processCompoundTag(DOMNode *compoundNode, 
        RobotDescription *robotDescription);

    void processBodyTag(DOMNode *bodyNode, 
        RobotDescription *robotDescription);

    void processInitialPositionTag(DOMNode *node, 
        RobotDescription *robotDescription);

    
    void processControllerTag(DOMNode *controllerNode);
   
    void processControllerListTag(DOMNode *controllerListNode);
    
    void processInitialOrientationTag(DOMNode *initialOrientationNode, 
        RobotDescription *robotDescription);

    void turnAllSegments(RobotDescription *robotDescription,
        double x, double y, double z);

    void processCompoundConnectorTag(DOMNode *compoundConnectorTag,
        RobotDescription *robotDescription);

    void processObjectConnectorTag(
        DOMNode *objectConnectorNode,
        CompoundDescription *compound,
        RobotDescription *robotDescription);

    void processHingeTag(DOMNode *node, ConnectorDescription *connector,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processSliderJointTag(DOMNode *sliderJointNode, 
        ConnectorDescription *connector,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processComplexHingeJointTag(DOMNode *complexHingeNode, 
        ConnectorDescription *connector,
        RobotDescription *robotDescription, CompoundDescription *compound);

    void processBasicHingeNode(DOMNode *basicHingeNode, ConnectorDescription
        *connector);

    void processMotorNode(DOMNode *motorNode, ConnectorDescription
        *connector);

    void processGearNode(DOMNode *gearNode, ConnectorDescription *connector);

    void processSpringCouplingNode(DOMNode *springCouplingNode,
        ConnectorDescription *connector);

    void processSpringNode(DOMNode *springNode, ConnectorDescription
        *connector);

    void processPWMappingGroupNode(DOMNode *pwMappingGroupNode, MotorDescription
        *motor);
    void processPWMappingNode(DOMNode *pwMappingNode, PWMappingDescription
        *pwMapping);

    void processGenericDoubleAttributeNode(DOMNode *doubleAttributeNode, double
        *attributeValue, string attributeName);

    void processFixedJointTag(DOMNode *fixedJointNode, 
        ConnectorDescription *connector);

    void initialiseConnector(ConnectorDescription *connector);
    void initialiseComplexHingeConnector(ConnectorDescription *connector);

    void initialisePWMappingDescriptionStruct(PWMappingDescription *pwMapping);

    void processObjectConnectorSourceTag(
        DOMNode *objectConnectorSourceTag,
        ConnectorDescription *connector,
        CompoundDescription *compound);

    void processObjectConnectorDestinationTag(
        DOMNode *objectConnectorSourceTag,
        ConnectorDescription *connector,
        CompoundDescription *compound);

    void processMaxForceTag(DOMNode *maxForceNode, ConnectorDescription *connector);

    void processAmbientLightSensorTag(DOMNode *ambientLightSensorTag,
        RobotDescription *robotDescription);

    void processMaxVelocity(DOMNode *maxVelocityNode, 
        ConnectorDescription *connector);

    void processAnchorPointTag(DOMNode *anchorPointTag, 
        ConnectorDescription *connector);

    void processDeflectionTag(DOMNode *deflectionTag,
        ConnectorDescription *connector);

    void processPositionExceedableTag(DOMNode *positionExceedableTag,
        ConnectorDescription *connector);

    void processForceExceedableTag(DOMNode *forceExceedableTag,
        ConnectorDescription *connector);

    void processRotationAxisTag(DOMNode *rotationAxisTag,
        ConnectorDescription *connector);

    void processSliderAxisTag(DOMNode *sliderAxisTag,
        ConnectorDescription *connector);

    void processCompoundConnectorSourceTag(
        DOMNode *compoundSourceTag,
        ConnectorDescription *connector,
        RobotDescription *robotDescription);

    void processCompoundConnectorDestinationTag(
        DOMNode *compoundDestinationTag,
        ConnectorDescription *connector,
        RobotDescription *robotDescription);

    void printConnectorData(ConnectorDescription connector);
    void printComplexHingeData(ConnectorDescription connector);

    void printPWMappingDescriptionData(PWMappingDescription *pwMapping);
    void printInterpolationDataSets(
        vector<struct InterpolationDataPointSet> *interpolationData);

    void processInfraredDistanceSensorTag(DOMNode *node,
        RobotDescription *robotDescription,
        CompoundDescription *compound,
        SingleSegment *segment);

    void initialiseInfraredDistanceSensor(IrSensorStruct *iS);

    void processGenericRotationSensorTag(DOMNode *node,
        RobotDescription *robotDescription,
        CompoundDescription *compound,
        SingleSegment *segment);

    void processRayPropTag(DOMNode *rayPropNode, bool *drawRay,
        double *lengthRay, int *rayType, Color *rayColor);

    void processRayLengthTag(DOMNode *rayLengthNode, double *lengthRay,
        int *rayType);

    void initialiseGenericRotationSensor(GenericRotationSensorStruct *gRS);

    void processSensorRangeTag(DOMNode *sensorRangeTag, 
        double *min,
        double *max);

    void printInfraredSensorInformation(IrSensorStruct iS);

    void printGenericRotationSensorInformation(GenericRotationSensorStruct gRS);

    void printLdrSensorInformation(LdrSensorStruct lS);

    void printSharpGP2D12_37InfraredSensorInformation(
        SharpGP2D12_37_SensorStruct sharpS);

    void checkForJoints(char *name, DOMNode *node, ConnectorDescription *connector,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processOpeningAngleTag(DOMNode *openingAngleNode,
        double *spreadAngleX, double *spreadAngleY);

    void printSensorStructInformation(SensorStruct s);

    void processMappingNode(DOMNode *mappingNode, 
        double *startValue, double *endValue);

    void processValuePairTag(DOMNode *valuePairNode, 
        vector<struct InterpolationDataPointSet> *iD);
    void postProcessValuePairs(vector<struct InterpolationDataPointSet> *iD);

    void processAngleNode(DOMNode *mappingNode,
        GLfloat *angle);

    void processSensorValuesNode(DOMNode *mappingNode,
        int *xPixel, int *yPixel);

    void processLightTypeNode(DOMNode *mappingNode,
        int *light);

    void processColorTypeNode(DOMNode *sensorRangeTag,int *colorType);

    void processNoiseTag(DOMNode *noiseNode,
        double *noise, int *noiseType);

    void processInternalFiltersTag(DOMNode *internalFiltersNode,
        ConnectorDescription *connector);

    void processFilterNode(DOMNode *filterNode, FilterDescription *fd);

    void processMovingAverageFilterNode(DOMNode *movingAverageFilterNode,
        FilterDescription *fd);

    void processDamperNode(DOMNode *damperNode, 
        ConnectorDescription *connector);

    void processJointFrictionNode(DOMNode *jointFrictionNode, 
        JointFrictionDescription *jFD);

    void processStaticJointFrictionNode(DOMNode *staticJointFrictionNode,
        JointFrictionDescription *jFD);

    void processDynamicJointFrictionNode(DOMNode *dynamicJointFrictionNode,
        JointFrictionDescription *jFD);

    void initialiseSensorStruct(SensorStruct *s);

    void processServoTag(DOMNode *sensorNode, 
        ConnectorDescription *connector);

    void initialseLightSource(LightSource *lightSource);

    void processLightSourceTag(DOMNode *lightSourceNode,
        LightSource *lightSource);

    double processIntensityAttribute(DOMNode *node);

    void processLdrSensorTag(
        DOMNode *irDistNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound,
        SingleSegment *segment);

    void processHinge2Tag(DOMNode *hinge2Node, 
        ConnectorDescription *c1, ConnectorDescription *c2, RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processPhysicalRepresentationTag(
        DOMNode *physicalRepTag,
        SingleSegment *segment);

    void processGenericMassGeomTag(
        DOMNode *massGeomNode, 
        SingleSegment *segment);

    void processMassTag(DOMNode *massNode, double *mass);

    void processBounceTag(DOMNode *node, 
        double *bounce,
        double *bounceVel,
        int *contactMode);

    void processSlipTag(DOMNode *slipNode,
        double *slip1,
        double *slip2,
        int *contactMode);

    void processConstraintForceMixingTag(DOMNode *cfmNode, 
        double *cfm, 
        int *contactMode);

    void processErrorReductionParameterTag(DOMNode *erpNode, 
        double *erp, 
        int *contactMode);

    void processCoulombFriction(DOMNode *frictionNode,
        double *friction1,
        double *friction2,
        int *contactMode);

    void processAngleSensorTag(DOMNode *angleSensorNode,
        RobotDescription *robotDescription, CompoundDescription *compound);

    void processAngleVelocitySensorTag(DOMNode *angleSensorNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processSliderVelocitySensorTag(DOMNode *sliderSensorNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processSliderPositionSensorTag(DOMNode *sliderSensorNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void processAngleFeedbackSensorTag(DOMNode *angleSensorNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound);

    void initialiseAngleSensorStruct(AngleSensorStruct *aS);

    void printAngleSensorStructInformation(AngleSensorStruct aS);

    void initialiseAngleVelocitySensorStruct(AngleVelSensorStruct *avs);

    void printAngleVelocitySensorStructInformation(AngleVelSensorStruct avs);

    void initialiseAngleFeedbackSensorStruct(AngleFeedbackSensorStruct *afs);

    void printAngleFeedbackSensorStructInformation(AngleFeedbackSensorStruct afs);

    void printSliderPositionSensorStructInformation(
        SliderPositionSensorStruct sps);

    void printSliderVelocitySensorStructInformation(
        AngleVelSensorStruct svs);

    void initialiseSliderPositionSensorStruct(SliderPositionSensorStruct *sps);

    void initialiseSliderVelocitySensorStruct(SliderVelSensorStruct *svs);

    void initialiseLdrSensorStruct(LdrSensorStruct *lS);

    void initialiseDirectedCameraStruct(DirectedCameraStruct *lS);

    void processDirectedCameraTag(
        DOMNode *cameraSensorNode, 
        RobotDescription *robotDescription, 
        CompoundDescription *compound, 
        SingleSegment *segment);


    void processSharpGP2D12_37(
        DOMNode *irDistNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound,
        SingleSegment *segment);

    void initialiseSharpGP2D12_37Struct(SharpGP2D12_37_SensorStruct *sS);

    void processSharpDM2Y3A003K0F(
        DOMNode *irDistNode,
        RobotDescription *robotDescription,
        CompoundDescription *compound,
        SingleSegment *segment);

    void initialiseSharpDM2Y3A003K0FStruct(SharpDM2Y3A003K0F_SensorStruct *sS);

    void processGlobalSensors(DOMNode *globalSensorNode, 
        RobotDescription *robotDescription);

    void initialiseCoordinateSensor(CoordinateSensorStruct *cS);

    void processCoordinateSensorTag(DOMNode *coordinateSensorTag, 
        RobotDescription *robotDescription);

    void extractBumpable(DOMNode *bumpableNode, bool *isBumpable);
    void extractTraceContact(DOMNode *traceContactNode, bool *traceContact);
    void extractTracePoint(DOMNode *tracePointTag, TracePointDescription *segment);
    void extractTraceLine(DOMNode *traceLineTag, TraceLineDescription *trace_line);

    void processAmbientLightTag(DOMNode *ambientLightNode);
    void processMinimalSensorInformation(DOMNode *sensorNode, SensorDescription *sD);
    void processMinimalConnectorInformation(DOMNode *sensorNode, ConnectorDescription *cD);

    void print3D(P3D p3d, char *prefix);
    void printPosition(Position pos, char *prefix);
    void printLightSource(LightSource ls);
    void printRandiomise(Randomise randmoise, char *prefix);
    void printColor(Color color, char *prefix);
    void printTracePoint(TracePointDescription trace_point, char* prefix);

    void initialisePos(Position *pos);
    void initialseP3D(P3D *p3d);
    void initialseRandomise(Randomise *randomise);

    void copyPosition(Position *dest, Position source);

    void rotate(Position *pos, Position rotation);

    void initTracePoint(TracePointDescription *trace_position);
    void initTraceLine(TraceLineDescription *trace_line);
    void initColor(Color *color);

    void extractRandomiseScalarList(DOMNode *randomiseListNode,
        RandomiseScalar *randomise);

    MoveableDescription *moveableDescription;
    EnvironmentDescription *environmentDescription;
    SimulationDescription *simulationDescription;

    int numberOfDrawLines;
    int robotCopies;

    
//     Vector of initial Positions and Vector of initial Orientations for movables where attribute robotQuantity is set
    vector<DOMNode*> positionList;
    vector<DOMNode*> orientationList;
        
    vector<string> controller;

    string _pathToXsd;

};





#endif  // __DOMPARSER_H__
