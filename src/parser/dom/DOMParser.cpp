/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
// ---------------------------------------------------------------------------
//  Includes
// ---------------------------------------------------------------------------
#include <parser/dom/DOMParser.h>
#include <parser/dom/DOMTreeErrorReporter.hpp>

#include <util/defines.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <ode/ode.h>
#include <util/defines.h>

#include <vector>
#include <sstream>

// **************************************************************************
// tag names
// **************************************************************************
#define SIMULATION_TAG                "simulator"
#define CAMERA_POSITION_TAG           "cameraPosition"
#define CAMERA_ORIENTATION_TAG        "cameraOrientation"
#define WINDOW_SIZE_TAG               "windowSize"
#define UPDATE_FREQUENCY_TAG          "updateFrequency"
#define AXES                          "axes"

#define ENVIRONMENT_TAG               "environment"

#define ENTRY_TAG                     "entry"
#define MIN_TAG                       "min"
#define MAX_TAG                       "max"
#define INTERVAL_TAG                  "interval"
#define VARIANCE_TAG                  "variance"
#define PROBABILITY_TAG               "probability"
#define INITIAL_POSITION_TAG          "initialPosition"
#define INITIAL_ORIENTATION_TAG       "initialOrientation"
#define INITIAL_POSITION_LIST_TAG     "initialPositionList"
#define INITIAL_ORIENTATION_LIST_TAG  "initialOrientationList"
#define MOVABLE_TAG                   "movable"
#define BODY_TAG                      "body"
#define COMPOUND_TAG                  "compound"
#define OBJECT_CONNECTOR_TAG          "objectConnector"
#define COMPOUND_CONNECTOR_TAG        "compoundConnector"
#define SOURCE_TAG                    "source"
#define DESTINATION_TAG               "destination"
#define BOX_TAG                       "box"
#define SPHERE_TAG                    "sphere"
#define CAPPED_CYLINDER_TAG           "cappedCylinder"
#define CYLINDER_TAG                  "cylinder"
#define COMPOSITE_TAG                 "composite"
#define RIGID_BODY_TAG                "rigidBody"

#define GEOMS_RELATIVE_TO_RB_TAG      "geomsRelativeToRigidBody"
#define COORDINATE_TAG                "coordinate"
#define ORIENTATION_TAG               "orientation"
#define VARIANCE_ORIENTATION_TAG      "varianceOrientation"
#define DIMENSION_TAG                 "dimension"
#define VARIANCE_DIMENSION_TAG        "varianceDimension"
#define VARIANCE_DISTANCE_TAG         "varianceDistance"
#define DISTANCE_TAG                  "distance"
#define GRAPHICAL_REPRESENTATION_TAG  "graphicalRepresentation"
#define COLOR_TAG                     "color"
#define RANDOMISE_INTERVAL_TAG        "randomiseInterval"
#define RANDOMISE_LIST_TAG            "randomiseList"

#define ROBOTCONTROLLER_TAG           "robotController"
#define ROBOTCONTROLLER_LIST_TAG      "robotControllerList"

#define BUMPABLE_TAG                  "bumpable"
#define TRACE_CONTACT_TAG             "traceContact"
#define TRACE_POINT_TAG               "tracePosition"
#define TRACE_LINE_TAG                "traceLine"
#define POSITION_EXCEEDABLE_TAG       "positionLimitExceedable"
#define FORCE_EXCEEDABLE_TAG          "forceLimitExceedable"

#define MAX_VELOCITY_TAG              "maxVelocity"
#define MAX_FORCE_TAG                 "maxForce"
#define DEFLECTION_TAG                "deflection"
#define ANCHORPOINT_TAG               "anchorPoint"
#define ROTATION_AXIS_TAG             "rotationAxis"
#define SLIDER_AXIS_TAG               "sliderAxis"

#define FIXED_JOINT_TAG               "fixed"
#define HINGE_JOINT_TAG               "hinge"
#define HINGE2_JOINT_TAG              "hinge2"
#define FIRST_HINGE_TAG               "firstHinge"
#define SECOND_HINGE_TAG              "secondHinge"
#define SLIDER_JOINT_TAG              "slider"
#define COMPLEX_HINGE_JOINT_TAG             "complexHinge"

#define SHARP_GP2D12_37_TAG           "sharpGP2D12-37"
#define SHARP_DM2Y3A003K0F_TAG        "sharpDM2Y3A003K0F"

#define INFRARED_DISTANCE_SENSOR_TAG  "infraredDistanceSensor"
#define RANGE_TAG                     "range"

#define GENERIC_ROTATION_SENSOR_TAG   "genericRotationSensor"
#define ROTATION_RAY_TAG              "rotationRay"
#define REFERENCE_RAY_TAG             "referenceRay"
#define LENGTH_TAG                    "length"

#define LDR_SENSOR_TAG                "ldrSensor"
#define DIRECTED_CAMREA_TAG           "directedCamera"

#define OPENING_ANGLE_TAG             "openingAngle"

#define MAPPING_TAG                   "mapping"

#define SENSOR_VALUES_TAG             "sensorValues"

#define LIGHT_TYPE_TAG                "light"

#define COLOR_TYPE_TAG                "colorType"

#define NOISE_TAG                     "noise"

#define SERVO_TAG                     "servo"

#define SPRING_TAG                    "spring"
#define SPRING_CONSTANT_TAG           "springConstant"
#define INITIAL_POSITION_TAG          "initialPosition"

#define DAMPER_TAG                    "damper"
#define JOINT_FRICTION_TAG            "friction"
#define STATIC_TAG                    "static"
#define DYNAMIC_TAG                   "dynamic"
#define INTERNAL_FILTERS_TAG          "internalFilters"
#define FORCE_FEEDBACK_FILTER_TAG     "forceFeedbackFilter"
#define STOP_TORQUE_ACCU_FILTER_TAG   "stopTorqueAccuFilter"
#define MOVING_AVERAGE_FILTER_TAG     "movingAverage"

#define LIGHT_SOURCE_TAG              "lightSource"
#define AMBIENT_LIGHT_SOURCE_TAG      "ambientLightSource"

#define PHYSICAL_REPRESENTATION_TAG   "physicalRepresentation"
#define MASS_TAG                      "mass"
#define BOUNCE_TAG                    "bounce"
#define SLIP_TAG                      "slip"

#define MASS_GEOM_BOX_TAG             "massGeomBox"
#define MASS_GEOM_SPHERE_TAG          "massGeomSphere"
#define MASS_GEOM_CYLINDER_TAG        "massGeomCylinder"
#define MASS_GEOM_CAPPED_CYLINDER_TAG "massGeomCappedCylinder"

#define RELATIVE_ROTATION_TAG         "relativeRotation"
#define OFFSET_TAG                    "offset"

#define CONSTRAINT_FORCE_MIXING_TAG   "constraintForceMixing"
#define ERROR_REDUCTION_PARAMETER_TAG "errorReductionParameter"
#define COULOMB_FRICTION_TAG          "friction"

#define ANGLE_SENSOR_TAG              "angleSensor"
#define ANGLE_VELOCITY_SENSOR_TAG     "velocitySensor"
#define ANGLE_FEEDBACK_SENSOR_TAG     "feedbackSensor"
#define SLIDER_POSITION_SENSOR_TAG    "sliderPositionSensor"
#define SLIDER_VELOCITY_SENSOR_TAG    "sliderVelocitySensor"

#define GLOBAL_SENSORS_TAG            "globalSensors"
#define AMBIENT_LIGHT_SENSORS_TAG     "ambientLightSensor"
#define COORDINATE_SENSOR_TAG         "coordinateSensor"

#define RANDOM_BARS_TAG               "randomBars"

// Complex hinge stuff
#define BASIC_HINGE_TAG                "basicHinge"
#define MOTOR_TAG                      "motor"
#define GEAR_TAG                       "gear"
#define SPRING_COUPLING_TAG            "springCoupling"
#define AGONIST_MAPPING_TAG            "agonistMapping"
#define ANTAGONIST_MAPPING_TAG         "antagonistMapping"
#define AGONIST_NOISE_TAG              "agonistNoise"
#define ANTAGONIST_NOISE_TAG           "antagonistNoise"

#define STALL_TORQUE_TAG               "stallTorque"
#define MAX_CONTINUOUS_TORQUE_TAG      "maxContinuousTorque"
#define NO_LOAD_SPEED_TAG              "noLoadSpeed"
#define MECHANICAL_TIME_CONST_TAG      "mechanicalTimeConstant"
#define MAX_EFFICIENCY_TAG             "maxEfficiency"
#define BRAKE_EFFICIENCY_TAG           "brakeEfficiency"
#define BRAKE_FRICTION_TAG             "brakeFriction"
#define SPEED_VOLTAGE_CONST_TAG        "speedVoltageConstant"
#define TORQUE_CURRENT_CONST_TAG       "torqueCurrentConstant"
#define PWM_THRESHOLD_TAG              "PWMThreshold"
#define PW_Mapping_TAG                 "pwMapping"
#define PW_TO_STALL_TORQUE_TAG         "pwToStallTorque"
#define PW_TO_STALL_CURRENT_TAG        "pwToStallCurrent"
#define PW_TO_NO_LOAD_SPEED_TAG        "pwToNoLoadSpeed"
#define PW_TO_NO_LOAD_CURRENT_TAG      "pwToNoLoadCurrent"
#define PW_TO_BRAKE_EFFICIENCY_TAG     "pwToBrakeEfficiency"
#define VALUE_PAIR_TAG                 "valuePair"

#define TRANSMISSION_RATIO_TAG         "transmissionRatio"
#define BACKLASH_TAG                   "backlash"
#define EFFICIENCY_TAG                 "efficiency"
#define INITIAL_BACKLASH_POS_TAG       "initialBacklashPosition"

#define MIN_TORQUE_TAG                 "minTorque"
#define MAX_DEFLECTION_TAG             "maxDeflection"
//

// **************************************************************************
// attribute names
// **************************************************************************
#define PHYS_SIM_ATTRIBUTE            "physSim"
#define NET_ATTRIBUTE                 "net"
#define PROBABILITY_ATTRIBUTE          "probability"
#define VARIANCE_ATTRIBUTE             "variance"
#define METHOD_ATTRIBUTE               "method"

#define X_ATTRIBUTE                    "x"
#define Y_ATTRIBUTE                    "y"
#define Z_ATTRIBUTE                    "z"

#define SIZE_ATTRIBUTE                 "size"
#define ITERATIONS_ATTRIBUTE           "iteration"

#define ALPHA_ATTRIBUTE                "alpha"
#define BETA_ATTRIBUTE                 "beta"
#define GAMMA_ATTRIBUTE                "gamma"

#define COLOR_RGB_R_ATTRIBUTE          "r"
#define COLOR_RGB_G_ATTRIBUTE          "g"
#define COLOR_RGB_B_ATTRIBUTE          "b"
#define COLOR_ALPHA_ATTRIBUTE          "alpha"

#define DIMENSION_HEIGHT_ATTRIBUTE     "height"
#define DIMENSION_WIDTH_ATTRIBUTE      "width"
#define DIMENSION_DEPTH_ATTRIBUTE      "depth"
#define DIMENSION_RADIUS_ATTRIBUTE     "radius"

#define NAME_ATTRIBUTE                 "name"

#define TYPE_ATTRIBUTE                 "type"

#define USAGE_ATTRIBUTE                "usage"

#define VALUE_ATTRIBUTE                "value"


#define MODE_ATTRIBUTE                 "mode"

#define DRAWING_ATTRIBUTE              "drawing"

#define JOINT_TYPE_VELOCITY_ATTRIBUTE  "velocity"
#define JOINT_TYPE_ANGULAR_ATTRIBUTE   "angular"
#define JOINT_TYPE_PASSIVE_ATTRIBUTE   "passive"

#define DEGREE_ATTRIBUTE               "degree"
#define RADIAN_ATTRIBUTE               "radian"

#define MIN_ATTRIBUTE                  "min"
#define MAX_ATTRIBUTE                  "max"

#define COMPOUND_NAME_ATTRIBUTE        "compoundName"
#define OBJECT_NAME_ATTRIBUTE          "objectName"

#define RANGE_ATTRIBUTE                "range"

#define STOP_BIAS_ATTRIBUTE            "stopBias"
#define SLOW_DOWN_ATTRIBUTE            "slowDownBias"

#define INTENSITY_ATTRIBUTE            "intensity"

#define VELOCITY_ATTRIBUTE             "velocity"

#define TARGET_ATTRIBUTE               "target"

#define DIRECTION_ATTRIBUTE            "direction"

#define SOURCE_ATTRIBUTE               "source"
#define DESTINATION_ATTRIBUTE          "destination"

#define FORCE_ATTRIBUTE                "force"
#define TORQUE_ATTRIBUTE               "torque"

#define MAX_FORCE_ATTRIBUTE            "maxForce"
#define VEL_THRESHOLD_ATTRIBUTE        "velThreshold"
#define VEL_DEPENDENCY_ATTRIBUTE       "velDependency"
#define OFFSET_ATTRIBUTE               "offset"
#define MIN_VEL_ATTRIBUTE              "minVel"

#define HINGE_AXIS_ATTRIBUTE           "hingeAxis"
#define SLIDER_AXIS_ATTRIBUTE          "sliderAxis"

#define DRAW_RANGE_ATTRIBUTE           "draw"
#define TRUE_ATTRIBUTE                 "true"
#define FALSE_ATTRIBUTE                "false"

#define QUANTITY_ATTRIBUTE             "quantity"

#define ROBOT_COPIES_ATTRIBUTE         "robotQuantity"

#define PITCH_ROLL_YAW_ATTRIBUTE       "pitchRollYaw"
#define PITCH_ROLL_YAW_VEL_ATTRIBUTE   "pitchRollYawVel"
#define PITCH_ROLL_YAW_ACCEL_ATTRIBUTE "pitchRollYawAccel"
#define VECTOR_ANGLE_ATTRIBUTE         "vectorAngle"
#define UNIT_SPHERE_XYZ_ATTRIBUTE      "unitSphereXYZ"

#define CONSTANT_ATTRIBUTE             "constant"
#define PROPORTIONAL_ATTRIBUTE         "proportional"

#define VISUALIZATION_ATTRIBUTE        "visualization"
#define SCALING_ATTRIBUTE              "scale"

#define WINDOW_SIZE_ATTRIBUTE          "windowSize"
#define INIT_VALUE_ATTRIBUTE           "initValue"
#define RECURSIVE_ATTRIBUTE            "recursive"
#define FULL_RECALC_ATTRIBUTE          "fullRecalc"

#define MIN_ACTIVATION_ATTRIBUTE       "minAct"
#define MAX_ACTIVATION_ATTRIBUTE       "maxAct"
#define X_OFF_ATTRIBUTE                "xOff"
#define Y_OFF_ATTRIBUTE                "yOff"
#define EXPONENT_ATTRIBUTE             "exponent"

// **************************************************************************
// type value
// **************************************************************************

#define NOISE_TYPE_GAUSS              "gauss"
#define RANDOMISE_METHOD_ADDITIVE     "additive"
#define RANDOMISE_METHOD_GLOBAL       "global"

#define ACTIVE_VALUE                  "active"
#define PASSIVE_VALUE                 "passive"
#define CONTROLLED_VALUE              "controlled"
#define MOVING_VALUE                  "moving"

#define ROBOT_COPIES                  "robotQuantity"

#define SENSOR_USAGE_CONTROL          "controlInput"
#define SENSOR_USAGE_AUX              "aux"
#define SENSOR_USAGE_INTERNAL         "internal"
#define SENSOR_USAGE_DEBUG            "debug"

#define NONE                          "none"
#define CONSTANT                      "constant"
#define LINEAR                        "linear"
#define QUADRATIC                     "quadratic"
#define POW                           "pow"
#define SQRT                          "sqrt"

#define LINEAR_INTERPOLATION          "linearInterpolation"

#include <string.h>


using namespace std;

int controlledRobot = 0;
int controlledObject = 0;


DOMParser::DOMParser(string pathToXsd)
{

  numberOfDrawLines = 0;
  _pathToXsd = pathToXsd;

}

DOMParser::~DOMParser()
{

}


// ---------------------------------------------------------------------------
//
//  main
//
// ---------------------------------------------------------------------------
int DOMParser::read(char *filename,
    MoveableDescription *md,
    EnvironmentDescription *ed,
    SimulationDescription *sd)
{
  int retval = DP_READ_OK;

  moveableDescription = md;
  environmentDescription = ed;
  simulationDescription = sd;

  // Initialize the XML4C2 system
  try
  {
    XMLPlatformUtils::Initialize();
  }
  catch(const XMLException &toCatch)
  {
    XERCES_STD_QUALIFIER cerr << "Error during Xerces-c Initialization.\n"
      << "  Exception message:"
      << XMLString::transcode(toCatch.getMessage()) << XERCES_STD_QUALIFIER endl;
    return 1;
  }

  XercesDOMParser *parser = new XercesDOMParser;
  parser->setValidationScheme(XercesDOMParser::Val_Always);
  parser->setDoNamespaces(true);
  parser->setDoSchema(true);
  parser->setValidationSchemaFullChecking(true);
  parser->setCreateEntityReferenceNodes(false);
  ostringstream xsd;
  xsd << _pathToXsd <<  "/xsd/RoSiML.xsd";
  cout << "DOMParser::xsd: " << xsd.str() << endl;
  parser->setExternalNoNamespaceSchemaLocation( (char*)xsd.str().c_str());




  DOMTreeErrorReporter *errReporter = new DOMTreeErrorReporter();
  parser->setErrorHandler(errReporter);

  //
  //  Parse the XML file, catching any XML exceptions that might propogate
  //  out of it.
  //
  bool errorsOccured = false;
  try
  {
    parser->parse(filename);
  }
  catch (const XMLException& e)
  {
    XERCES_STD_QUALIFIER cerr << "An error occurred during parsing\n   Message: "
      << XMLString::transcode(e.getMessage()) << XERCES_STD_QUALIFIER endl;
    errorsOccured = true;
  }
  catch (const DOMException& e)
  {
    const unsigned int maxChars = 2047;
    XMLCh errText[maxChars + 1];

    XERCES_STD_QUALIFIER cerr << "\nDOM Error during parsing: '" 
      << filename << "'\n" << "DOMException code is:  "
      << e.code << XERCES_STD_QUALIFIER endl;

    if (DOMImplementation::loadDOMExceptionMsg(e.code, errText, maxChars))
      XERCES_STD_QUALIFIER cerr << "Message is: " 
        << XMLString::transcode(errText) 
        << XERCES_STD_QUALIFIER endl;

    errorsOccured = true;
  }
  catch (...)
  {
    XERCES_STD_QUALIFIER cerr << "An error occurred during parsing\n " 
      << XERCES_STD_QUALIFIER endl;
    errorsOccured = true;
  }

  // If the parse was successful, output the document data from the DOM tree
  if (!errorsOccured && !errReporter->getSawErrors())
  {
    // get the DOM representation
    DOMNode *doc = parser->getDocument();

    DOMNode *rootNode = doc->getFirstChild();
    //printAllNodes(doc);

    parseDocument(rootNode); // first node is document node. so  start with the
    // children, which is only the root node


  }
  else
  {
    retval = 4;
  }

  //
  //  Clean up the error handler. The parser does not adopt handlers
  //  since they could be many objects or one object installed for multiple
  //  handlers.
  //
  delete errReporter;

  //
  //  Delete the parser itself.  Must be done prior to calling Terminate, below.
  //
  delete parser;

  // And call the termination method
  XMLPlatformUtils::Terminate();

  return retval;
}


void DOMParser::parseDocument(DOMNode *rootNode)
{
  // node list can only contain 
  // <Simulator>
  // <Environment>
  // <Movable>
  DOMNodeList *nodeList = rootNode->getChildNodes();
  DOMNode     *node     = 0;
  char        *name     = 0;

  for(uint i=0; i < nodeList->getLength(); i++)
  {

    node = nodeList->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, ENVIRONMENT_TAG) == 0)
    {
      processEnvironmentTag(node);
    }

    if(strcmp(name, SIMULATION_TAG) == 0)
    {
      processSimulationTag(node);
    }

    if(strcmp(name, MOVABLE_TAG) == 0)
    {
      processMovableTag(node);
    }
  }


}

void DOMParser::processMovableTag(DOMNode *movableTag)
{

  RobotDescription *robotDescription = new RobotDescription();

  DOMNodeList     *children            = movableTag->getChildNodes();
  DOMNamedNodeMap *attributeNode       = movableTag->getAttributes();
  DOMNode         *attributeNodeCopies = 0;
  DOMNode         *node                = 0;
  DOMNode         *attributeType       = 0;
  DOMNode         *attributeName       = 0;
  char            *name                = 0;
  string          robotName;
  controlledRobot                      = 0;
  controlledObject                     = 0;


  attributeType = 
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));

  attributeName = 
    attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));

  robotName = XMLString::transcode(attributeName->getNodeValue());

  robotDescription->setName(robotName);

  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        ACTIVE_VALUE) == 0)
  {
    robotDescription->setType(RD_TYPE_ACTIVE);
  }

  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        PASSIVE_VALUE) == 0)
  {
    robotDescription->setType(RD_TYPE_PASSIVE);
  }
  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        CONTROLLED_VALUE) == 0)
  {
    controlledRobot = 1;
    robotDescription->setType(RD_TYPE_CONTROLLED);
  }
 if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
     MOVING_VALUE) == 0)
  {
    controlledObject=1;
    robotDescription->setType(RD_TYPE_MOVING);
  }
  
  attributeNodeCopies = attributeNode->getNamedItem(XMLString::transcode(ROBOT_COPIES_ATTRIBUTE));
  if(attributeNodeCopies != NULL)
  {
    robotCopies = atoi(XMLString::transcode(attributeNodeCopies->getNodeValue()));
    Y_DEBUG("DOMParser::processMoveableTag: found Robot-Copies = %d", robotCopies);
  }
  else
  {
    robotCopies = 1; 
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, BODY_TAG) == 0)
    {
      processBodyTag(node, robotDescription);
    }
    if(strcmp(name, GLOBAL_SENSORS_TAG) == 0)
    {
      processGlobalSensors(node, robotDescription);
    }
    // collect all initial Orientations in a vector before processing them
    if(strcmp(name, INITIAL_ORIENTATION_TAG) == 0)
    {
      // to enable one orientation for all copies
      for(int i=0; i<robotCopies; i++)
      {
        orientationList.push_back(node);
      }
    }


    // process initalOrientationList-Tag by collectiong all initialOrientations in a vector
    // changed because of including possibility of copies of one movable
    if(strcmp(name, INITIAL_ORIENTATION_LIST_TAG) == 0)
    {
      DOMNodeList *children = node->getChildNodes();
      DOMNode *orientationNode   = 0;

      for(uint i=0; i< children->getLength(); i++)
      {
        orientationNode = children->item(i);
        name=XMLString::transcode(orientationNode->getNodeName());
        if(strcmp(name, INITIAL_ORIENTATION_TAG)==0)
        {
          orientationList.push_back(orientationNode);
        }
      }
    }

    if(strcmp(name, INITIAL_POSITION_TAG) == 0)
    {
      positionList.push_back(node);
    }

    if(strcmp(name, INITIAL_POSITION_LIST_TAG) == 0)
    {
      DOMNodeList *children = node->getChildNodes();
      DOMNode *positionNode   = 0;

      for(uint i=0; i< children->getLength(); i++)
      {
        positionNode = children->item(i);
        name=XMLString::transcode(positionNode->getNodeName());
        if(strcmp(name, INITIAL_POSITION_TAG)==0)
        {
          positionList.push_back(positionNode);
        }
      }
    }


    if(strcmp(name, ROBOTCONTROLLER_LIST_TAG)==0)
    {
      processControllerListTag(node); 
    }

    if(strcmp(name, ROBOTCONTROLLER_TAG)==0)
    {
      for(int i=0; i< robotCopies; i++)
      {
        processControllerTag(node);
      }
    }

  }

  Y_DEBUG("DOMParser::processMoveableTag: found type = %d", robotDescription->getType());


  //   processing all initialOrientations and initialPositions of the movable tag and creating the robotcopies
  for(uint j=0; j<robotCopies; j++)
  {
    RobotDescription *listRobot = new RobotDescription(robotDescription);
    DOMNode *initialPosition = positionList[j];
    DOMNode *initialOrientation = orientationList[j];
    processInitialPositionTag(initialPosition, listRobot);
    processInitialOrientationTag(initialOrientation, listRobot);
    if(controlledRobot)
    {

      listRobot->setController(controller[j]);

    }
    if(controlledObject)
    {
      listRobot->setController(controller[j]); 
    }

    moveableDescription->addMovable(listRobot);
    delete listRobot;
  }
  positionList.clear();
  orientationList.clear();
  controller.clear();

  robotDescription->writeAndVerifyNameTags();
  moveableDescription->setNumberOfDrawLines(numberOfDrawLines);
}


void DOMParser::processGlobalSensors(DOMNode *globalSensorNode, 
    RobotDescription *robotDescription)
{
  DOMNodeList *nodeList = globalSensorNode->getChildNodes();
  DOMNode     *node     = 0;
  char        *name     = 0;


  for(uint i=0; i < nodeList->getLength(); i++)
  {

    node = nodeList->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COORDINATE_SENSOR_TAG) == 0)
    {
      processCoordinateSensorTag(node, robotDescription);
    }
    if(strcmp(name, AMBIENT_LIGHT_SENSORS_TAG) == 0)
    {
      processAmbientLightSensorTag(node, robotDescription);
    }
  }

}

void DOMParser::processMinimalSensorInformation(DOMNode *sensorNode, SensorDescription *sD)
{
  Y_DEBUG("DOMParser::processMinimalSensorInformation");
  DOMNamedNodeMap *attributeNode  = sensorNode->getAttributes();
  DOMNode *attributeNameTag = 0;
  DOMNode *attributeUsageTag = 0;
  
  SensorStruct *sS = sD->getSensorStruct();

  attributeNameTag = attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  if(attributeNameTag != NULL)
  {
    sD->setName(XMLString::transcode(attributeNameTag->getNodeValue()));
    Y_DEBUG("setting name %s", XMLString::transcode(attributeNameTag->getNodeValue()));
  }
  else
  {
    sD->setName(DEF_UNAMED);
  }

  attributeUsageTag = attributeNode->getNamedItem(XMLString::transcode(USAGE_ATTRIBUTE));

  if(attributeUsageTag != NULL)
  {
    char* usageString = XMLString::transcode(attributeUsageTag->getNodeValue());
    if(strcmp(usageString, SENSOR_USAGE_CONTROL) == 0)
    {
      sS->usage = DEF_SENSOR_USED_AS_CONTROL_INPUT;
    }
    else if(strcmp(usageString, SENSOR_USAGE_AUX) == 0)
    {
      sS->usage = DEF_SENSOR_USED_AS_AUX;
    }
    else if(strcmp(usageString, SENSOR_USAGE_INTERNAL) == 0)
    {
      sS->usage = DEF_SENSOR_USED_INTERNALLY;
    }
    else if(strcmp(usageString, SENSOR_USAGE_DEBUG) == 0)
    {
      sS->usage = DEF_SENSOR_USED_AS_DEBUG;
    }
  }
}

void DOMParser::processMinimalConnectorInformation(DOMNode *sensorNode, ConnectorDescription *cD)
{
  DOMNamedNodeMap *attributeNode  = sensorNode->getAttributes();
  DOMNode *attributeNameTag = 0;

  attributeNameTag = attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  if(attributeNameTag != NULL)
  {
    cD->name = XMLString::transcode(attributeNameTag->getNodeValue());
  }
  else
  {
    cD->name = DEF_UNAMED;
  }
}


void DOMParser::processAmbientLightSensorTag(DOMNode *ambientLightSensorTag, 
    RobotDescription *robotDescription)
{
  SensorStruct s;
  AmbientLightSensorStruct aSs;

  initialiseSensorStruct(&s);
  aSs.intensity = 0.0;

  s.type     = SD_AMBIENT_LIGHT_SENSOR;
  s.usage    = DEF_SENSOR_USED_AS_AUX;      // as global sensor default should be
                                        //   aux

  SensorDescription *sD = new SensorDescription(s);
  sD->setAmbientLightSensor(aSs);
  processMinimalSensorInformation(ambientLightSensorTag, sD);

  robotDescription->addSensor(sD);

  delete sD;
}


void DOMParser::processCoordinateSensorTag(DOMNode *coordinateSensorTag, 
    RobotDescription *robotDescription)
{
  DOMNamedNodeMap *attributeNode  = coordinateSensorTag->getAttributes();
  DOMNode *attributeCoordinate = 0;
  DOMNode *attributeSourceObject = 0;
  DOMNode *attributeSourceCompound = 0;

  attributeCoordinate = 
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  attributeSourceObject = 
    attributeNode->getNamedItem(XMLString::transcode(OBJECT_NAME_ATTRIBUTE));
  attributeSourceCompound = 
    attributeNode->getNamedItem(XMLString::transcode(COMPOUND_NAME_ATTRIBUTE));

  SensorStruct s;
  CoordinateSensorStruct cSs;

  initialiseSensorStruct(&s);
  initialiseCoordinateSensor(&cSs);


  if(strcmp(XMLString::transcode(attributeCoordinate->getNodeValue()),
        X_ATTRIBUTE) == 0)
  {
    cSs.coordinate = COORDINATE_SENSOR_X;
  }

  if(strcmp(XMLString::transcode(attributeCoordinate->getNodeValue()),
        Y_ATTRIBUTE) == 0)
  {
    cSs.coordinate = COORDINATE_SENSOR_Y;
  }

  if(strcmp(XMLString::transcode(attributeCoordinate->getNodeValue()),
        Z_ATTRIBUTE) == 0)
  {
    cSs.coordinate = COORDINATE_SENSOR_Z;
  }

  // **************************************************************************
  // TODO: make a function out of this, because this it is used more than once
  // **************************************************************************
  CompoundDescription *compound;
  SingleSegment *segment;
  string objectName   =
    XMLString::transcode(attributeSourceObject->getNodeValue());
  string compoundName =
    XMLString::transcode(attributeSourceCompound->getNodeValue());

  for(uint i = 0; i < robotDescription->getCompoundNumber(); i++)
  {
    compound = robotDescription->getCompound(i);
    segment = &((*compound).segments[0]); // just need the name, so the first one

    // will do
    if(compound->name == compoundName) 
    {
      cSs.srcCompound = i;
      for(uint j=0; j < compound->segments.size(); j++)
      {
        segment = &((*compound).segments[j]);
        if(segment->name == objectName)
        {
          cSs.srcSeg = j;
          break;
        }
      }
      break;
    }
  }

  // TODO: error handling 
  if(cSs.srcCompound == -1)
  {
    // TODO
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::INFO << 
      "DOMParser::processCoordinateSensorTag -- compoundName " << compoundName 
      << " does not exist!" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
    throw("DOMParser::processCoordinateSensorTag -- compoundName does not exist!");
  }
  if(cSs.srcSeg == -1)
  {
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::INFO << 
      "DOMParser::processCoordinateSensorTag -- objectName " << objectName 
      << " does not exist!" << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
    throw("DOMParser::processCoordinateSensorTag -- objectName does not exist!");
  }

  s.type     = SD_COORDINATE_SENSOR;
  s.usage    = DEF_SENSOR_USED_AS_AUX;   // As global sensor the default should be aux

  SensorDescription *sD = new SensorDescription(s);
  sD->setCoordinateSensor(cSs);
  processMinimalSensorInformation(coordinateSensorTag, sD);

  robotDescription->addSensor(sD);

  delete sD;
}


void DOMParser::processInitialOrientationTag(DOMNode *initialOrientationNode, 
    RobotDescription *robotDescription)
{
  DOMNamedNodeMap *attributeNode  = initialOrientationNode->getAttributes();
  DOMNode         *attributeNodeX = 0;
  DOMNode         *attributeNodeY = 0;
  DOMNode         *attributeNodeZ = 0;
  DOMNodeList     *children       = initialOrientationNode->getChildNodes();
  DOMNode         *node           = 0;
  char            *name           = 0;
  double           dx             = 0;
  double           dy             = 0;
  double           dz             = 0;

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  dx = atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  dy = atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  dz = atof(XMLString::transcode(attributeNodeZ->getNodeValue()));

  Position inOrientation;
  initPosition(&inOrientation);
  inOrientation.x = dx;
  inOrientation.y = dy;
  inOrientation.z = dz;
  Y_DEBUG(
      "--> DOMParser::processInitialOrientationTag: %f %f %f %s %s %s",dx, dy, dz,
      attributeNodeX->getNodeValue(), 
      attributeNodeY->getNodeValue(), 
      attributeNodeZ->getNodeValue());

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANDOMISE_LIST_TAG) == 0)
    {
      Y_DEBUG("DOMParser::processInitialOrientationTag: found RANDOMISE_LIST_TAG");
      extractRandomiseList(node, &(inOrientation.randomise));
    }

    if(strcmp(name, RANDOMISE_INTERVAL_TAG) == 0)
    {
      Y_DEBUG("DOMParser::processInitialOrientationTag: found RANDOMISE_INTERVAL_TAG");
      extractRandomiseInterval(node, &(inOrientation.randomise));
    }
  }


  robotDescription->setInitialOrientation(inOrientation);
  printPosition(inOrientation,"InitialOrientation.");

  //printExtractedNameTags(robotDescription);
  robotDescription->turnAllSegments( inOrientation);
}

void DOMParser::processControllerListTag(DOMNode *controllerListNode)
{
  DOMNodeList *children = controllerListNode->getChildNodes();
  DOMNode *controllerNode= 0;
  char        *name = 0;
  for(uint i=0; i< children->getLength(); i++)
  {
    controllerNode = children->item(i);
    name=XMLString::transcode(controllerNode->getNodeName());
    if(strcmp(name, ROBOTCONTROLLER_TAG)==0)
    {
      processControllerTag(controllerNode);
    }
  }
}

void DOMParser::processControllerTag(DOMNode *controllerNode){
  DOMNamedNodeMap *attributeNode = controllerNode->getAttributes();
  DOMNode *name = 0;
  name =
    attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  string controllerName;
  controllerName = XMLString::transcode(name->getNodeValue());
  controller.push_back(controllerName);
}

void DOMParser::processInitialPositionTag(DOMNode *initialPositionNode, 
    RobotDescription *robotDescription)
{
  DOMNamedNodeMap *attributeNode  = initialPositionNode->getAttributes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;
  DOMNode *attributeNodeZ = 0;
  DOMNodeList *children = initialPositionNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;


  double dx = 0;
  double dy = 0;
  double dz = 0;

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  dx = atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  dy = atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  dz = atof(XMLString::transcode(attributeNodeZ->getNodeValue()));     


  Y_DEBUG("DOMParser::processInitialPositionTag: x %f y%f z %f", dx, dy, dz);

  Position inPos;
  initPosition(&inPos);
  inPos.x = dx;
  inPos.y = dy;
  inPos.z = dz;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANDOMISE_LIST_TAG) == 0)
    {
      Y_DEBUG("DOMParser::processInitialPositionTag: found RANDOMISE_LIST_TAG");
      extractRandomiseList(node, &(inPos.randomise));
    }

    if(strcmp(name, RANDOMISE_INTERVAL_TAG) == 0)
    {
      Y_DEBUG("DOMParser::processInitialPositionTag: found RANDOMISE_INTERVAL_TAG");
      extractRandomiseInterval(node, &(inPos.randomise));
    }
  }


  robotDescription->setInitialPosition(inPos);
  printPosition(inPos,"InitialPosition.");

  robotDescription->moveAllObjects(inPos);

}

void DOMParser::processBodyTag(DOMNode *bodyNode, 
    RobotDescription *robotDescription)
{
  DOMNodeList *children = bodyNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processBodyTag");

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COMPOUND_TAG) == 0)
    {
      processCompoundTag(node, robotDescription);
    }
    if(strcmp(name, COMPOUND_CONNECTOR_TAG) == 0)
    {
      processCompoundConnectorTag(node, robotDescription);
    }
  }
}

void DOMParser::checkForJoints(char *name, DOMNode *node, ConnectorDescription *connector,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  if(strcmp(name, FIXED_JOINT_TAG) == 0)
  {
    Y_DEBUG("DOMParser::checkForJoints FIXED_JOINT_TAG (%s:%d)", __FILE__, __LINE__);
    processFixedJointTag(node, connector);
    processMinimalConnectorInformation(node, connector);
  }

  if(strcmp(name, HINGE_JOINT_TAG) == 0)
  {
    Y_DEBUG("DOMParser::checkForJoints HINGE_JOINT_TAG (%s:%d)", __FILE__, __LINE__);
    processHingeTag(node, connector, robotDescription, compound);
    processMinimalConnectorInformation(node, connector);
    printConnectorData(*connector);
  }

  if(strcmp(name, HINGE2_JOINT_TAG) == 0)
  {
    Y_DEBUG("DOMParser::checkForJoints HINGE2_JOINT_TAG (%s:%d)", __FILE__, __LINE__);
    ConnectorDescription connector2;
    processHinge2Tag(node, connector, &connector2,
        robotDescription, compound);
    copyPosition(&(connector->rotationAxis2), connector2.rotationAxis);
    copyPosition(&(connector->initRotationAxis2), connector2.rotationAxis);
    connector->type = RD_CONN_HINGE_2;
    processMinimalConnectorInformation(node, connector);
    printConnectorData(*connector);
  }

  if(strcmp(name, SLIDER_JOINT_TAG) == 0)
  {
    Y_DEBUG("DOMParser::checkForJoints SLIDER_JOINT_TAG (%s:%d)", __FILE__, __LINE__);
    processSliderJointTag(node, connector, robotDescription, compound);
    processMinimalConnectorInformation(node, connector);
    printConnectorData(*connector);
  }

  if(strcmp(name, COMPLEX_HINGE_JOINT_TAG) == 0)
  {
    Y_DEBUG("DOMParser::checkForJoints COMPLEX_HINGE_JOINT_TAG (%s:%d)", __FILE__, __LINE__);
    processComplexHingeJointTag(node, connector, robotDescription, compound);
    processMinimalConnectorInformation(node, connector);
    printConnectorData(*connector);
  }
}

void DOMParser::processHinge2Tag(DOMNode *hinge2Node, 
    ConnectorDescription *c1, ConnectorDescription *c2, RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  DOMNodeList *children = hinge2Node->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, FIRST_HINGE_TAG) == 0)
    {
      processHingeTag(node, c1, robotDescription, compound);
    }

    if(strcmp(name, SECOND_HINGE_TAG) == 0)
    {
      processHingeTag(node, c2, robotDescription, compound);
    }
  }
}

void DOMParser::processCompoundConnectorTag(DOMNode *compoundConnectorTag,
    RobotDescription *robotDescription)
{
  DOMNodeList *children = compoundConnectorTag->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  ConnectorDescription   connector;
  AngleSensorStruct *aS = NULL;
  AngleVelSensorStruct *aVS = NULL;
  AngleFeedbackSensorStruct *aFS = NULL;
  SliderPositionSensorStruct *sPS = NULL;
  SliderVelSensorStruct *sVS = NULL;

  Y_DEBUG("DOMParser::processCompoundConnectorTag");

  for(uint i = 0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    checkForJoints(name, node, &connector, robotDescription, NULL);

    if(strcmp(name, SOURCE_TAG) == 0)
    {
      processCompoundConnectorSourceTag(node, &connector, robotDescription);
    }

    if(strcmp(name, DESTINATION_TAG) == 0)
    {
      processCompoundConnectorDestinationTag(node, &connector, robotDescription);
    }
  }

  Y_DEBUG(
      "DOMParser::processCompoundConnectorTag (%s:%d) connector.sourceSegNum %d",
      __FILE__, __LINE__, connector.sourceSegNum);
  //processMinimalConnectorInformation(compoundConnectorTag, &connector);
  robotDescription->addCompoundConnector(connector);

  // TODO
  // maybe not the fastest solution to iterate through ALL Sensors ??
  for(uint i = 0; i < robotDescription->getSensorNumber(); i++)
  {
    SensorDescription *sD =
      (robotDescription->getSensor(robotDescription->getSensorNumber()-1-i));
    SensorStruct *s = sD->getSensorStruct();

    if(s->srcSeg == -1)
    {
      s->srcSeg   = connector.sourceSegNum;
      s->srcCompound = connector.sourceCompoundNum;
      switch(s->type)
      {
        case SD_ANGLE_SENSOR:
          aS = sD->getAngleSensor();
          // changed to fix angle sensor bug, not best but fastest solution
          // aS->connectorNum = robotDescription->getCompoundConnectorNumber() - 1;
          aS->connectorNum =
            robotDescription->getTotalNumOfSegJoints(s->srcCompound, s->srcSeg) - 1;
          break;
        case SD_ANGLE_VEL_SENSOR:
          aVS = sD->getAngleVelSensor();
          // changed to fix angle sensor bug, not best but fastest solution
          // aVS->connectorNum = robotDescription->getCompoundConnectorNumber() - 1;
          aVS->connectorNum =
            robotDescription->getTotalNumOfSegJoints(s->srcCompound, s->srcSeg) - 1;
          printAngleVelocitySensorStructInformation(*aVS);
          break;
        case SD_SLIDER_POSITION_SENSOR:
          sPS = sD->getSliderPositionSensor();
          sPS->connectorNum =
            robotDescription->getTotalNumOfSegJoints(s->srcCompound, s->srcSeg) - 1;
          break;
        case SD_SLIDER_VEL_SENSOR:
          sVS = sD->getSliderVelSensor();
          sVS->connectorNum =
            robotDescription->getTotalNumOfSegJoints(s->srcCompound, s->srcSeg) - 1;
          break;
        case SD_ANGLE_FEEDBACK_SENSOR:
          aFS = sD->getAngleFeedbackSensor();
          aFS->connectorNum = 
            robotDescription->getTotalNumOfSegJoints(s->srcCompound, s->srcSeg) - 1;
          //          printAngleFeedbackSensorStructInformation(*aFS);
          break;
      }
    }
  }


  //printSensorStructInformation(*s);

}

void DOMParser::processCompoundConnectorDestinationTag(
    DOMNode *compoundDestinationTag,
    ConnectorDescription *connector,
    RobotDescription *robotDescription)
{
  DOMNamedNodeMap *attributeNode  = compoundDestinationTag->getAttributes();
  DOMNode *attributeCompoundName  = 0;
  DOMNode *attributeObjectName    = 0;
  string destinationCompoundName;
  string destinationObjectName;
  CompoundDescription *compound; 
  SingleSegment *segment;

  attributeCompoundName = 
    attributeNode->getNamedItem(XMLString::transcode(COMPOUND_NAME_ATTRIBUTE));
  attributeObjectName = 
    attributeNode->getNamedItem(XMLString::transcode(OBJECT_NAME_ATTRIBUTE));

  destinationCompoundName =
    XMLString::transcode(attributeCompoundName->getNodeValue());
  destinationObjectName =
    XMLString::transcode(attributeObjectName->getNodeValue());


  for(uint i = 0; i < robotDescription->getCompoundNumber(); i++)
  {
    compound = robotDescription->getCompound(i);
    segment = &((*compound).segments[0]); // just need the name, so the first one
    // will do
    if(compound->name == destinationCompoundName)
    {
      connector->aimCompoundNum = i;
      for(uint j=0; j < compound->segments.size(); j++)
      {
        segment = &((*compound).segments[j]);
        if(segment->name == destinationObjectName)
        {
          connector->aimSegNum = j;
          return; // done
        }
      }
    }
  }
}


void DOMParser::processCompoundConnectorSourceTag(
    DOMNode *compoundSourceTag,
    ConnectorDescription *connector,
    RobotDescription *robotDescription)
{
  DOMNamedNodeMap *attributeNode  = compoundSourceTag->getAttributes();
  DOMNode *attributeCompoundName  = 0;
  DOMNode *attributeObjectName    = 0;
  string sourceCompoundName;
  string sourceObjectName;
  CompoundDescription *compound; 
  SingleSegment *segment;

  attributeCompoundName = 
    attributeNode->getNamedItem(XMLString::transcode(COMPOUND_NAME_ATTRIBUTE));
  attributeObjectName = 
    attributeNode->getNamedItem(XMLString::transcode(OBJECT_NAME_ATTRIBUTE));

  sourceCompoundName =
    XMLString::transcode(attributeCompoundName->getNodeValue());
  sourceObjectName =
    XMLString::transcode(attributeObjectName->getNodeValue());


  for(uint i = 0; i < robotDescription->getCompoundNumber(); i++)
  {
    compound = robotDescription->getCompound(i);
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::DEBUG << 
      "DOMParser::processCompoundConnectorSourceTag (" << __FILE__ << ":"
      << __LINE__ << " compound->name \"" << compound->name <<
      "\" == sourceCompoundName \"" << sourceCompoundName << "\"" << 
      log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT


    if(compound->name == sourceCompoundName)
    {
      connector->sourceCompoundNum = i;
      for(uint j=0; j < compound->segments.size(); j++)
      {
        segment = &((*compound).segments[j]);

        if(segment->name == sourceObjectName)
        {
          connector->sourceSegNum = j;
          connector->anchorInitPos.x += segment->init_position.x;
          connector->anchorInitPos.y += segment->init_position.y;
          connector->anchorInitPos.z += segment->init_position.z;
          return; // done
        }
      }
    }
  }
}

void DOMParser::processCompoundTag(DOMNode *compoundNode, 
    RobotDescription *robotDescription)
{
  CompoundDescription compound;
  SingleSegment       segment;
  DOMNodeList        *children       = compoundNode->getChildNodes();
  DOMNamedNodeMap    *attributeNode  = compoundNode->getAttributes();
  DOMNode            *node           = 0;
  char               *name           = 0;
  char               *compoundName   = 0;

  Y_DEBUG("DOMParser::processCompoundTag");

  // get compoundName
  node = attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));

  compoundName = XMLString::transcode(node->getNodeValue());
  compound.name = compoundName;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, BOX_TAG) == 0)
    {
      Y_DEBUG("start box: %s",name);
      processObjectTag(node, &segment, robotDescription, &compound);
      segment.type = RD_BOX_GEOM;
      compound.segments.push_back(segment);
      printSegment(segment);
      Y_DEBUG("end box: %s",name);
    }
    if(strcmp(name, SPHERE_TAG) == 0)
    {
      Y_DEBUG("start sphere: %s",name);
      processObjectTag(node, &segment, robotDescription, &compound);
      segment.type = RD_SPHERE_GEOM;
      compound.segments.push_back(segment);
      Y_DEBUG("end sphere: %s",name);
    }
    if(strcmp(name, CAPPED_CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start capped cylinder: %s",name);
      processObjectTag(node, &segment, robotDescription, &compound);
      segment.type = RD_CAPPED_CYLINDER_GEOM;
      compound.segments.push_back(segment);
      Y_DEBUG("end capped cylinder: %s",name);
    }
    if(strcmp(name, CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start cylinder: %s",name);
      processObjectTag(node, &segment, robotDescription, &compound);
      segment.type = RD_CYLINDER_GEOM;
      compound.segments.push_back(segment);
      Y_DEBUG("end cylinder: %s",name);
    }
    if(strcmp(name, COMPOSITE_TAG) == 0)
    {
      Y_DEBUG("start composite: %s",name);
      processCompositeTag(node, &segment, robotDescription, &compound);
      segment.type = RD_COMPOSITE_GEOM;
      compound.segments.push_back(segment);
      Y_DEBUG("end composite: %s",name);
    }

    if(strcmp(name, OBJECT_CONNECTOR_TAG) == 0)
    {
      Y_DEBUG("start object connector: %s",name);
      processObjectConnectorTag(node, &compound, robotDescription);
      Y_DEBUG("end object connector: %s",name);
    }
  }

  Y_DEBUG("almost done");
  robotDescription->addCompoundDescription(compound);
  Y_DEBUG("done");
}

void DOMParser::processCompositeTag(DOMNode *compositeNode, 
    SingleSegment *segment, RobotDescription *robotDescription, 
    CompoundDescription *compound)
{
  DOMNodeList *children = compositeNode->getChildNodes();
  DOMNamedNodeMap *attributeNode  = compositeNode->getAttributes();
  DOMNode     *node = 0;
  char        *name = 0;
  char        segmentName[1024];

  Y_DEBUG("DOMParser::processCompositeTag");

  initialiseSingleSegment(segment);

  // get composite Name
  node = attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  // we cannot directly assign the name, because the segment struct will be
  //   initialised hereafter --> this is a workaround
  strcpy(segmentName, XMLString::transcode(node->getNodeValue()));

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RIGID_BODY_TAG) == 0)
    {
      Y_DEBUG("start rigid body: %s",name);
      processRigidBodyTag(node, segment, robotDescription, compound);
      Y_DEBUG("end rigid body: %s",name);
    }
    if(strcmp(name, GEOMS_RELATIVE_TO_RB_TAG) == 0)
    {
      Y_DEBUG("start geomsRelativeToRigidBody: %s",name);
      processGeomsRelativeToRBTag(node, segment, robotDescription,
          compound);
      Y_DEBUG("end geomsRelativeToRigidBody: %s",name);
    }
  }

  segment->rigidBodyName = segment->name;
  segment->name = segmentName;

  Y_DEBUG("DOMParser::processCompositeTag end");
}

void DOMParser::processRigidBodyTag(DOMNode *rBNode, SingleSegment *segment,
    RobotDescription *robotDescription, CompoundDescription *compound)
{
  DOMNodeList *children = rBNode->getChildNodes();
  DOMNamedNodeMap *attributeNode  = rBNode->getAttributes();
  DOMNode *node         = 0;
  char    *name         = 0;
  bool    drawRigidBody = false;

  Y_DEBUG("DOMParser::processRigidBodyTag");

  node = attributeNode->getNamedItem(XMLString::transcode(VISUALIZATION_ATTRIBUTE));

  if(strcmp(TRUE_ATTRIBUTE, XMLString::transcode(node->getNodeValue())) == 0)
  {
    drawRigidBody = true;
  }
  else 
  {
    drawRigidBody = false;
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, BOX_TAG) == 0)
    {
      Y_DEBUG("start rigid body box: %s",name);
      segment->type = RD_COMPOSITE_GEOM;
      processObjectTag(node, segment, robotDescription, compound);
      segment->type = RD_COMPOSITE_GEOM;
      segment->rigidBodyType = RD_BOX_GEOM;
      printSegment(*segment);
      Y_DEBUG("end rigid body box: %s",name);
    }
    if(strcmp(name, SPHERE_TAG) == 0)
    {
      Y_DEBUG("start rigid body sphere: %s",name);
      processObjectTag(node, segment, robotDescription, compound);
      segment->type = RD_COMPOSITE_GEOM;
      segment->rigidBodyType = RD_SPHERE_GEOM;
      printSegment(*segment);
      Y_DEBUG("end rigid body sphere: %s",name);
    }
    if(strcmp(name, CAPPED_CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start rigid body capped cylinder: %s",name);
      processObjectTag(node, segment, robotDescription, compound);
      segment->type = RD_COMPOSITE_GEOM;
      segment->rigidBodyType = RD_CAPPED_CYLINDER_GEOM;
      printSegment(*segment);
      Y_DEBUG("end rigid body capped cylinder: %s",name);
    }
    if(strcmp(name, CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start rigid body cylinder: %s",name);
      processObjectTag(node, segment, robotDescription, compound);
      segment->type = RD_COMPOSITE_GEOM;
      segment->rigidBodyType = RD_CYLINDER_GEOM;
      printSegment(*segment);
      Y_DEBUG("end rigid body cylinder: %s",name);
    }
  }

  segment->drawRigidBody = drawRigidBody;

  Y_DEBUG("rigid body done");
}

void DOMParser::processGeomsRelativeToRBTag(DOMNode *gRelTag, SingleSegment
    *segment, RobotDescription *robotDescription, CompoundDescription *compound)
{
  SingleSegment geomSegment;
  DOMNodeList *children = gRelTag->getChildNodes();
  DOMNamedNodeMap *attributeNode  = gRelTag->getAttributes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processGeomsRelativeToRigidBody");

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, BOX_TAG) == 0)
    {
      Y_DEBUG("start geomRelativeToRB box: %s",name);
      processObjectTag(node, &geomSegment, robotDescription, compound);
      geomSegment.type = RD_BOX_GEOM;
      segment->geoms.segments.push_back(geomSegment);
      printSegment(geomSegment);
      Y_DEBUG("end geomRelativeToRB box: %s",name);
    }
    if(strcmp(name, SPHERE_TAG) == 0)
    {
      Y_DEBUG("start geomRelativeToRB sphere: %s",name);
      processObjectTag(node, &geomSegment, robotDescription, compound);
      geomSegment.type = RD_SPHERE_GEOM;
      segment->geoms.segments.push_back(geomSegment);
      printSegment(geomSegment);
      Y_DEBUG("end geomRelativeToRB sphere: %s",name);
    }
    if(strcmp(name, CAPPED_CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start geomRelativeToRB capped cylinder: %s",name);
      processObjectTag(node, &geomSegment, robotDescription, compound);
      geomSegment.type = RD_CAPPED_CYLINDER_GEOM;
      segment->geoms.segments.push_back(geomSegment);
      printSegment(geomSegment);
      Y_DEBUG("end geomRelativeToRB capped cylinder: %s",name);
    }
    if(strcmp(name, CYLINDER_TAG) == 0)
    {
      Y_DEBUG("start geomRelativeToRB cylinder: %s",name);
      processObjectTag(node, &geomSegment, robotDescription, compound);
      geomSegment.type = RD_CYLINDER_GEOM;
      segment->geoms.segments.push_back(geomSegment);
      printSegment(geomSegment);
      Y_DEBUG("end geomRelativeToRB cylinder: %s",name);
    }
  }
  Y_DEBUG("geoms relative to rigid body done");
}

void DOMParser::processObjectConnectorTag(
    DOMNode *objectConnectorNode,
    CompoundDescription *compound,
    RobotDescription *robotDescription)
{
  DOMNodeList *children = objectConnectorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  ConnectorDescription   connector;
  SingleSegment *segment;
  AngleSensorStruct *aS = NULL;
  AngleVelSensorStruct *aVS = NULL;
  AngleFeedbackSensorStruct *aFS = NULL;
  SliderPositionSensorStruct *sPS = NULL;
  SliderVelSensorStruct *sVS = NULL;
  initialiseConnector(&connector);

  for(uint i = 0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    checkForJoints(name, node, &connector, robotDescription, compound);

    if(strcmp(name, SOURCE_TAG) == 0)
    {
      processObjectConnectorSourceTag(node, &connector, compound);
    }

    if(strcmp(name, DESTINATION_TAG) == 0)
    {
      processObjectConnectorDestinationTag(node, &connector, compound);
    }
  }

  Y_DEBUG(
      "DOMParser::processObjectConnectorTag (%s:%d) connector.sourceSegNum %d",
      __FILE__, __LINE__, connector.sourceSegNum);
  segment = &((*compound).segments[connector.sourceSegNum]);
  segment->connectors.push_back(connector);


  // TODO
  // maybe not the fastest solution to iterate through ALL Sensors ??
  for(uint i = 0; i < robotDescription->getSensorNumber(); i++)
  {
    SensorDescription *sD =
      (robotDescription->getSensor(robotDescription->getSensorNumber()-1-i));
    SensorStruct *s = sD->getSensorStruct();

    if(s->srcCompound == -1)
    {
      s->srcSeg = connector.sourceSegNum;
      s->srcCompound = robotDescription->getCompoundNumber();
      switch(s->type)
      {
        case SD_ANGLE_SENSOR:
          aS = sD->getAngleSensor();
          aS->connectorNum = segment->connectors.size() - 1;
          printSensorStructInformation(*s);
          printAngleSensorStructInformation(*aS);
          break;
        case SD_ANGLE_VEL_SENSOR:
          aVS = sD->getAngleVelSensor();
          aVS->connectorNum = segment->connectors.size() - 1;
          printSensorStructInformation(*s);
          printAngleVelocitySensorStructInformation(*aVS);
          break;
        case SD_SLIDER_POSITION_SENSOR:
          sPS = sD->getSliderPositionSensor();
          sPS->connectorNum = segment->connectors.size() - 1;
          printSensorStructInformation(*s);
          printSliderPositionSensorStructInformation(*sPS);
          break;
        case SD_SLIDER_VEL_SENSOR:
          sVS = sD->getSliderVelSensor();
          sVS->connectorNum = segment->connectors.size() - 1;
          printSensorStructInformation(*s);
          //printSliderVelocitySensorStructInformation(*sVS);
          break;
        case SD_ANGLE_FEEDBACK_SENSOR:
          aFS = sD->getAngleFeedbackSensor();
          aFS->connectorNum = segment->connectors.size() - 1;
          //          printSensorStructInformation(*s);
          //          printAngleFeedbackSensorStructInformation(*aFS);
          break;
      }
    }
  }
}

void DOMParser::processHingeTag(DOMNode *hingeNode, 
    ConnectorDescription *connector,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{

  initialiseConnector(connector);
  DOMNodeList *children = hingeNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  DOMNamedNodeMap *attributeNode  = hingeNode->getAttributes();
  DOMNode         *nameNode =
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  char *typeName = XMLString::transcode(nameNode->getNodeValue());

  connector->type = RD_CONN_HINGE;


  if(strcmp(typeName, JOINT_TYPE_VELOCITY_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_VEL_MOTOR;
  }
  else if(strcmp(typeName, JOINT_TYPE_ANGULAR_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_ANGLE_MOTOR;
    // default values:
    connector->slowDownBias = 0.2;
    connector->stopBias = 0.02;
  }
  else if(strcmp(typeName, JOINT_TYPE_PASSIVE_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_PASSIVE_JOINT;
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MAX_FORCE_TAG) == 0)
    {
      processMaxForceTag(node, connector);
    }

    if(strcmp(name, MAX_VELOCITY_TAG) == 0)
    {
      processMaxVelocity(node, connector);
    }

    if(strcmp(name, ANCHORPOINT_TAG) == 0)
    {
      processAnchorPointTag(node, connector);
    }

    if(strcmp(name, DEFLECTION_TAG) == 0)
    {
      processDeflectionTag(node, connector);
    }

    if(strcmp(name, ROTATION_AXIS_TAG) == 0)
    {
      processRotationAxisTag(node, connector);
    }

    if(strcmp(name, SPRING_TAG) == 0)
    {
      processSpringNode(node, connector);
    }

    if(strcmp(name, DAMPER_TAG) == 0)
    {
      processDamperNode(node, connector);
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->noise), &(connector->noise_type));
    }

    if(strcmp(name, SERVO_TAG) == 0)
    {
      processServoTag(node, connector);
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(connector->startValue), &(connector->endValue));
    }

    if(strcmp(name, ANGLE_SENSOR_TAG) == 0)
    {
      processAngleSensorTag(node, robotDescription, compound);
    }

    if(strcmp(name, ANGLE_VELOCITY_SENSOR_TAG) == 0)
    {
      processAngleVelocitySensorTag(node, robotDescription, compound);
    }

    if(strcmp(name, ANGLE_FEEDBACK_SENSOR_TAG) == 0)
    {
      processAngleFeedbackSensorTag(node, robotDescription, compound);
    }
  }
}

void DOMParser::processSliderJointTag(DOMNode *sliderJointNode, 
    ConnectorDescription *connector,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{

  initialiseConnector(connector);
  DOMNodeList *children = sliderJointNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  DOMNamedNodeMap *attributeNode  = sliderJointNode->getAttributes();
  DOMNode         *nameNode =
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  char *typeName = XMLString::transcode(nameNode->getNodeValue());

  connector->type = RD_CONN_SLIDER;

  // maybe we should change the Attribute Name to something more general here
  if(strcmp(typeName, JOINT_TYPE_VELOCITY_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_VEL_MOTOR;
  }

  // maybe we should change the Attribute Name to something more general here
  if(strcmp(typeName, JOINT_TYPE_ANGULAR_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_ANGLE_MOTOR;
  }

  if(strcmp(typeName, JOINT_TYPE_PASSIVE_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_PASSIVE_JOINT;
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MAX_FORCE_TAG) == 0)
    {
      processMaxForceTag(node, connector);
    }

    if(strcmp(name, MAX_VELOCITY_TAG) == 0)
    {
      processMaxVelocity(node, connector);
    }

    if(strcmp(name, DEFLECTION_TAG) == 0)
    {
      processDeflectionTag(node, connector);
    }

    if(strcmp(name, SLIDER_AXIS_TAG) == 0)
    {
      processSliderAxisTag(node, connector);
    }

    if(strcmp(name, SPRING_TAG) == 0)
    {
      processSpringNode(node, connector);
    }

    if(strcmp(name, DAMPER_TAG) == 0)
    {
      processDamperNode(node, connector);
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->noise), &(connector->noise_type));
    }

    if(strcmp(name, SERVO_TAG) == 0)
    {
      processServoTag(node, connector);
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(connector->startValue), &(connector->endValue));
    }

    // maybe we should change the TAG Name to something more general here
    // (merge with ANGLE_SENSOR_BLA ?)
    if(strcmp(name, SLIDER_POSITION_SENSOR_TAG) == 0)
    {
      processSliderPositionSensorTag(node, robotDescription, compound);
    }

    // maybe we should change the TAG Name to something more general here
    if(strcmp(name, SLIDER_VELOCITY_SENSOR_TAG) == 0)
    {
      processSliderVelocitySensorTag(node, robotDescription, compound);
    }

    // maybe we should change the TAG Name to something more general here
    if(strcmp(name, ANGLE_FEEDBACK_SENSOR_TAG) == 0)
    {
      processAngleFeedbackSensorTag(node, robotDescription, compound);
    }
  }
}

void DOMParser::processComplexHingeJointTag(DOMNode *complexHingeNode, 
    ConnectorDescription *connector,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{

  initialiseConnector(connector);
  initialiseComplexHingeConnector(connector);
  DOMNodeList *children = complexHingeNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  DOMNamedNodeMap *attributeNode  = complexHingeNode->getAttributes();
  DOMNode         *nameNode =
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  char *typeName = XMLString::transcode(nameNode->getNodeValue());

  connector->type = RD_CONN_COMPLEX_HINGE;
  connector->motorType = RD_VEL_MOTOR;

  if(strcmp(typeName, JOINT_TYPE_VELOCITY_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_VEL_MOTOR;
    // TODO: put defaults here
  }
  else if(strcmp(typeName, JOINT_TYPE_PASSIVE_ATTRIBUTE) == 0)
  {
    connector->motorType = RD_PASSIVE_JOINT;
    // TODO: put defaults here
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, BASIC_HINGE_TAG) == 0)
    {
      processBasicHingeNode(node, connector);
    }
    else if(strcmp(name, MOTOR_TAG) == 0)
    {
      processMotorNode(node, connector);
    }
    else if(strcmp(name, GEAR_TAG) == 0)
    {
      processGearNode(node, connector);
    }
    else if(strcmp(name, SPRING_COUPLING_TAG) == 0)
    {
      processSpringCouplingNode(node, connector);
    }
    else if(strcmp(name, SPRING_TAG) == 0)
    {
      processSpringNode(node, connector);
    }
    else if(strcmp(name, DAMPER_TAG) == 0)
    {
      processDamperNode(node, connector);
    }
    else if(strcmp(name, JOINT_FRICTION_TAG) == 0)
    {
      processJointFrictionNode(node, &(connector->jointFriction));
    }
    else if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->noise), &(connector->noise_type));
    }
    else if(strcmp(name, INTERNAL_FILTERS_TAG) == 0)
    {
      processInternalFiltersTag(node, connector);
    }
    else if(strcmp(name, ANGLE_SENSOR_TAG) == 0)
    {
      processAngleSensorTag(node, robotDescription, compound);
    }
    else if(strcmp(name, ANGLE_VELOCITY_SENSOR_TAG) == 0)
    {
      processAngleVelocitySensorTag(node, robotDescription, compound);
    }
    else if(strcmp(name, ANGLE_FEEDBACK_SENSOR_TAG) == 0)
    {
      processAngleFeedbackSensorTag(node, robotDescription, compound);
    }
  }
}

// TODO: adapt RobotDescription to basicHinge
void DOMParser::processBasicHingeNode(DOMNode *basicHingeNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = basicHingeNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processBasicHingeNode");

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, DEFLECTION_TAG) == 0)
    {
      processDeflectionTag(node, connector);
    }
    else if(strcmp(name, ANCHORPOINT_TAG) == 0)
    {
      processAnchorPointTag(node, connector);
    }
    else if(strcmp(name, ROTATION_AXIS_TAG) == 0)
    {
      processRotationAxisTag(node, connector);
    }
    else if(strcmp(name, POSITION_EXCEEDABLE_TAG) == 0)
    {
      connector->isPositionExceedable = true;
      processPositionExceedableTag(node, connector);
    }
    else if(strcmp(name, FORCE_EXCEEDABLE_TAG) == 0)
    {
      connector->isForceExceedable = true;
      processForceExceedableTag(node, connector);
    }
  }
}

void DOMParser::processMotorNode(DOMNode *motorNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = motorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processMotorNode");

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, STALL_TORQUE_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->motor.stallTorque),
          VALUE_ATTRIBUTE);
    }
    if(strcmp(name, MAX_CONTINUOUS_TORQUE_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->motor.maxContinuousTorque), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, NO_LOAD_SPEED_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->motor.noLoadSpeed),
          VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, MECHANICAL_TIME_CONST_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->motor.mechanicalTimeConstant), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, MAX_EFFICIENCY_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->motor.maxEfficiency),
          VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, BRAKE_EFFICIENCY_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->motor.brakeEfficiency), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, BRAKE_FRICTION_TAG) == 0)
    {
      processJointFrictionNode(node, &(connector->motor.brakeFriction));
    }
    else if(strcmp(name, SPEED_VOLTAGE_CONST_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->motor.speedVoltageConstant), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, TORQUE_CURRENT_CONST_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->motor.torqueCurrentConstant), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, PWM_THRESHOLD_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->motor.pwmThreshold),
          VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, PW_Mapping_TAG) == 0)
    {
      processPWMappingGroupNode(node, &(connector->motor));
    }
    else if(strcmp(name, AGONIST_MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(connector->startValue), &(connector->endValue));
    }
    else if(strcmp(name, ANTAGONIST_MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(connector->startValue2), &(connector->endValue2));
    }
    else if(strcmp(name, AGONIST_NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->motor.noise[0]),
          &(connector->motor.noiseType[0]));
    }
    else if(strcmp(name, ANTAGONIST_NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->motor.noise[1]),
          &(connector->motor.noiseType[1]));
    }
  }
}

void DOMParser::processGearNode(DOMNode *gearNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = gearNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processGearNode");

  connector->gear.active = true;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, TRANSMISSION_RATIO_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->gear.transmissionRatio), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, BACKLASH_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->gear.backlash),
          VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, EFFICIENCY_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node, &(connector->gear.efficiency),
          VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, INITIAL_BACKLASH_POS_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->gear.initialBacklashPosition), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->gear.noise),
          &(connector->gear.noiseType));
    }
  }
}

void DOMParser::processSpringCouplingNode(DOMNode *springCouplingNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = springCouplingNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processSpringCouplingNode");

  connector->springCoupling.active = true;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MIN_TORQUE_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->springCoupling.minTorque), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, SPRING_CONSTANT_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->springCoupling.springConstant), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, MAX_DEFLECTION_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->springCoupling.maxDeflection), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->springCoupling.noise),
          &(connector->springCoupling.noiseType));
    }
  }
}

void DOMParser::processSpringNode(DOMNode *springNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = springNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processSpringNode");

  connector->spring.active = true;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, SPRING_CONSTANT_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->spring.springConstant), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, INITIAL_POSITION_TAG) == 0)
    {
      processGenericDoubleAttributeNode(node,
          &(connector->spring.initialPosition), VALUE_ATTRIBUTE);
    }
    else if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(connector->spring.noise),
          &(connector->spring.noiseType));
    }
  }
}

void DOMParser::processGenericDoubleAttributeNode(DOMNode *doubleAttributeNode, 
    double *attributeValue, string attributeName)
{
  DOMNamedNodeMap *attributeNode = doubleAttributeNode->getAttributes();
  DOMNode *valueAttribute = 0;

  valueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(attributeName.c_str()));

  *attributeValue = 
    atof(XMLString::transcode(valueAttribute->getNodeValue()));

  Y_DEBUG("DOMParser::processGenericDoubleAttributeNode - %f", *attributeValue);
}

void DOMParser::processPWMappingGroupNode(DOMNode *pwMappingGroupNode,
    MotorDescription *motor)
{
  DOMNodeList *children = pwMappingGroupNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processPWMappingGroupNode");

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, PW_TO_STALL_TORQUE_TAG) == 0)
    {
      processPWMappingNode(node, &(motor->pwToStallTorque));
    }
    else if(strcmp(name, PW_TO_STALL_CURRENT_TAG) == 0)
    {
      processPWMappingNode(node, &(motor->pwToStallCurrent));
    }
    else if(strcmp(name, PW_TO_NO_LOAD_SPEED_TAG) == 0)
    {
      processPWMappingNode(node, &(motor->pwToNoLoadSpeed));
    }
    else if(strcmp(name, PW_TO_NO_LOAD_CURRENT_TAG) == 0)
    {
      processPWMappingNode(node, &(motor->pwToNoLoadCurrent));
    }
    else if(strcmp(name, PW_TO_BRAKE_EFFICIENCY_TAG) == 0)
    {
      processPWMappingNode(node, &(motor->pwToBrakeEfficiency));
    }
  }
}

void DOMParser::processPWMappingNode(DOMNode *pwMappingNode,
    PWMappingDescription *pwMapping)
{
  DOMNodeList *children = pwMappingNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  DOMNamedNodeMap *attributeNode  = pwMappingNode->getAttributes();
  DOMNode *attributeType          = 0;
  DOMNode *attributeMinActivation = 0;
  DOMNode *attributeMaxActivation = 0;
  DOMNode *attributeXOff          = 0;
  DOMNode *attributeYOff          = 0;
  DOMNode *attributeConstant      = 0;
  DOMNode *attributeExponent      = 0;

  Y_DEBUG("DOMParser::processPWMappingNode");

  // parse the attributes
  attributeType = 
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  attributeMinActivation = 
    attributeNode->getNamedItem(XMLString::transcode(MIN_ACTIVATION_ATTRIBUTE));
  attributeMaxActivation = 
    attributeNode->getNamedItem(XMLString::transcode(MAX_ACTIVATION_ATTRIBUTE));
  attributeXOff = 
    attributeNode->getNamedItem(XMLString::transcode(X_OFF_ATTRIBUTE));
  attributeYOff = 
    attributeNode->getNamedItem(XMLString::transcode(Y_OFF_ATTRIBUTE));
  attributeConstant = 
    attributeNode->getNamedItem(XMLString::transcode(CONSTANT_ATTRIBUTE));
  attributeExponent = 
    attributeNode->getNamedItem(XMLString::transcode(EXPONENT_ATTRIBUTE));

  // type is required, so don't need to check if it exists
  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        NONE) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_NONE;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        CONSTANT) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_CONSTANT;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        LINEAR) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_LINEAR;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        QUADRATIC) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_QUADRATIC;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        POW) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_POW;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        SQRT) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_SQRT;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        LINEAR_INTERPOLATION) == 0)
  {
    pwMapping->type = DEF_MAPPING_TYPE_LINEAR_INTERPOLATION;
  }


  if(attributeMinActivation != NULL)
  {
    pwMapping->minActivation = 
      atof(XMLString::transcode(attributeMinActivation->getNodeValue()));
  }

  if(attributeMaxActivation != NULL)
  {
    pwMapping->maxActivation = 
      atof(XMLString::transcode(attributeMaxActivation->getNodeValue()));
  }

  if(attributeXOff != NULL)
  {
    pwMapping->xOff = 
      atof(XMLString::transcode(attributeXOff->getNodeValue()));
  }

  if(attributeYOff != NULL)
  {
    pwMapping->yOff = 
      atof(XMLString::transcode(attributeYOff->getNodeValue()));
  }

  if(attributeConstant != NULL)
  {
    pwMapping->constant = 
      atof(XMLString::transcode(attributeConstant->getNodeValue()));
  }

  if(attributeExponent != NULL)
  {
    pwMapping->exponent = 
      atof(XMLString::transcode(attributeConstant->getNodeValue()));
  }

  if(pwMapping->type == DEF_MAPPING_TYPE_LINEAR_INTERPOLATION)
  {
    // parse the child nodes
    for(uint i=0; i < children->getLength(); i++)
    {
      node = children->item(i);
      name = XMLString::transcode(node->getNodeName());

      if(strcmp(name, VALUE_PAIR_TAG) == 0)
      {
        processValuePairTag(node, &(pwMapping->interpolationData));
      }
    }

    // calculate the slopes
    postProcessValuePairs(&(pwMapping->interpolationData));
  }
}

void DOMParser::processValuePairTag(DOMNode *valuePairNode, 
    vector<struct InterpolationDataPointSet> *iD)
{
  InterpolationDataPointSet iDPS;

  DOMNamedNodeMap *attributeNode  = valuePairNode->getAttributes();
  DOMNode *attributeX             = 0;
  DOMNode *attributeY             = 0;

  Y_DEBUG("DOMParser::processValuePairTag");

  // parse the attributes
  attributeX = attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeY = attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));

  iDPS.x = atof(XMLString::transcode(attributeX->getNodeValue()));
  iDPS.y = atof(XMLString::transcode(attributeY->getNodeValue()));
  iDPS.slope = 0.0;

  // insert into vector such that x values are sorted in ascending order and
  //   duplicates are eliminated
  if(((*iD).size() < 1) || (iDPS.x > (*iD).back().x))
  {
    (*iD).push_back(iDPS);
  }
  else
  {
    for(int i=(*iD).size()-1; i>=0; i--)
    {
      if(iDPS.x == (*iD)[i].x)
      {
        Y_WARN("Ignoring duplicate value pair (x values have to be unique!)");
        break;
      }
      else if(iDPS.x < (*iD)[i].x)
      {
        (*iD).insert((*iD).begin() + i, iDPS);
      }
    }
  }
}

void DOMParser::postProcessValuePairs( 
    vector<struct InterpolationDataPointSet> *iD)
{
  // calculate slopes
  if((*iD).size() < 1)
  {
    Y_WARN("Interpolation type: no value pairs have been specified!");
  }
  else
  {
    for(int i=0; i<(*iD).size()-1; i++)
    {
      (*iD)[i].slope = ((*iD)[i+1].y - (*iD)[i].y)/((*iD)[i+1].x - (*iD)[i].x);
    }

    // beyond last x value, y value is constant (as before first x value)
    (*iD).back().slope = 0.0;
  }
}

void DOMParser::processAngleVelocitySensorTag(DOMNode *angleSensorNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  SensorStruct s;
  AngleVelSensorStruct aVS;

  initialiseSensorStruct(&s);
  initialiseAngleVelocitySensorStruct(&aVS);

  DOMNodeList *children = angleSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(aVS.startVel), &(aVS.endVel));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(aVS.startValue), &(aVS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

  }

  // if compound == NULL, then we are in compound connectors, so the srcSeg
  // will be filled, when the source tag is parsed
  if(compound != NULL) 
  {
    s.srcSeg   = compound->segments.size();
  }
  s.type     = SD_ANGLE_VEL_SENSOR;

  printSensorStructInformation(s);
  printAngleVelocitySensorStructInformation(aVS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(angleSensorNode, sD);
  sD->setAngleVelSensor(aVS);
  robotDescription->addSensor(sD);

  delete sD;

}


void DOMParser::processAngleSensorTag(DOMNode *angleSensorNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  SensorStruct s;
  AngleSensorStruct aS;

  initialiseSensorStruct(&s);
  initialiseAngleSensorStruct(&aS);

  DOMNodeList *children = angleSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(aS.startAngle), &(aS.endAngle));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(aS.startValue), &(aS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

  }

  //s.srcCompound = robotDescription->getCompoundNumber();
  // if compound == NULL, then we are in compound connectors, so the srcSeg
  // will be filled, when the source tag is parsed
  if(compound != NULL) 
  {
    s.srcSeg   = compound->segments.size();
  }
  s.type     = SD_ANGLE_SENSOR;

  printSensorStructInformation(s);
  printAngleSensorStructInformation(aS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(angleSensorNode, sD);
  sD->setAngleSensor(aS);
  robotDescription->addSensor(sD);

  delete sD;

}

void DOMParser::processAngleFeedbackSensorTag(DOMNode *angleSensorNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  DOMNamedNodeMap *attributeNode  = angleSensorNode->getAttributes();
  DOMNode *attributeType = 0;
  DOMNode *attributeTarget = 0;
  DOMNode *attributeDirection = 0;

  attributeType = 
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  attributeTarget = 
    attributeNode->getNamedItem(XMLString::transcode(TARGET_ATTRIBUTE));
  attributeDirection = 
    attributeNode->getNamedItem(XMLString::transcode(DIRECTION_ATTRIBUTE));

  SensorStruct s;
  AngleFeedbackSensorStruct aFS;

  initialiseSensorStruct(&s);
  initialiseAngleFeedbackSensorStruct(&aFS);

  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        FORCE_ATTRIBUTE) == 0)
  {
    aFS.type = FEEDBACK_SENSOR_FORCE;
  }

  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        TORQUE_ATTRIBUTE) == 0)
  {
    aFS.type = FEEDBACK_SENSOR_TORQUE;
  }

  if(strcmp(XMLString::transcode(attributeTarget->getNodeValue()),
        SOURCE_ATTRIBUTE) == 0)
  {
    aFS.feedbackTarget = FEEDBACK_SENSOR_SOURCE;
  }

  if(strcmp(XMLString::transcode(attributeTarget->getNodeValue()),
        DESTINATION_ATTRIBUTE) == 0)
  {
    aFS.feedbackTarget = FEEDBACK_SENSOR_DESTINATION;
  }

  // maybe we can use a switch command instead
  if(strcmp(XMLString::transcode(attributeDirection->getNodeValue()),
        X_ATTRIBUTE) == 0)
  {
    aFS.direction = FEEDBACK_SENSOR_X;
  }

  if(strcmp(XMLString::transcode(attributeDirection->getNodeValue()),
        Y_ATTRIBUTE) == 0)
  {
    aFS.direction = FEEDBACK_SENSOR_Y;
  }

  if(strcmp(XMLString::transcode(attributeDirection->getNodeValue()),
        Z_ATTRIBUTE) == 0)
  {
    aFS.direction = FEEDBACK_SENSOR_Z;
  }

  if(strcmp(XMLString::transcode(attributeDirection->getNodeValue()),
        HINGE_AXIS_ATTRIBUTE) == 0)
  {
    aFS.direction = FEEDBACK_SENSOR_HINGE_AXIS;
  }

  if(strcmp(XMLString::transcode(attributeDirection->getNodeValue()),
        SLIDER_AXIS_ATTRIBUTE) == 0)
  {
    aFS.direction = FEEDBACK_SENSOR_SLIDER_AXIS;
  }

  DOMNodeList *children = angleSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(aFS.startFeedback), &(aFS.endFeedback));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(aFS.startValue), &(aFS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }
  }

  // if compound == NULL, then we are in compound connectors, so the srcSeg
  // will be filled, when the source tag is parsed
  if(compound != NULL) 
  {
    s.srcSeg   = compound->segments.size();
  }
  s.type     = SD_ANGLE_FEEDBACK_SENSOR;

  printSensorStructInformation(s);
  printAngleFeedbackSensorStructInformation(aFS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(angleSensorNode, sD);
  sD->setAngleFeedbackSensor(aFS);
  robotDescription->addSensor(sD);

  delete sD;
}

void DOMParser::processSliderVelocitySensorTag(DOMNode *sliderSensorNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  SensorStruct s;
  SliderVelSensorStruct sVS;

  initialiseSensorStruct(&s);
  initialiseSliderVelocitySensorStruct(&sVS);

  DOMNodeList *children = sliderSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(sVS.startVel), &(sVS.endVel));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(sVS.startValue), &(sVS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

  }

  // if compound == NULL, then we are in compound connectors, so the srcSeg
  // will be filled, when the source tag is parsed
  if(compound != NULL) 
  {
    s.srcSeg   = compound->segments.size();
  }
  s.type     = SD_SLIDER_VEL_SENSOR;

  printSensorStructInformation(s);
  //printSliderVelocitySensorStructInformation(sVS);

  SensorDescription *sD = new SensorDescription(s);
  sD->setSliderVelSensor(sVS);
  processMinimalSensorInformation(sliderSensorNode, sD);
  robotDescription->addSensor(sD);

  delete sD;

}


void DOMParser::processSliderPositionSensorTag(DOMNode *sliderSensorNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound)
{
  SensorStruct s;
  SliderPositionSensorStruct sPS;

  initialiseSensorStruct(&s);
  initialiseSliderPositionSensorStruct(&sPS);

  DOMNodeList *children = sliderSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(sPS.startPosition), &(sPS.endPosition));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(sPS.startValue), &(sPS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

  }

  //s.srcCompound = robotDescription->getCompoundNumber();
  // if compound == NULL, then we are in compound connectors, so the srcSeg
  // will be filled, when the source tag is parsed
  if(compound != NULL) 
  {
    s.srcSeg   = compound->segments.size();
  }
  s.type     = SD_SLIDER_POSITION_SENSOR;

  printSensorStructInformation(s);
  printSliderPositionSensorStructInformation(sPS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(sliderSensorNode, sD);
  sD->setSliderPositionSensor(sPS);
  robotDescription->addSensor(sD);

  delete sD;

}


void DOMParser::processServoTag(DOMNode *sensorNode, 
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = sensorNode->getAttributes();
  DOMNode *slowDownAttribute = 0;
  DOMNode *stopBiasAttribute = 0;

  slowDownAttribute = 
    attributeNode->getNamedItem(XMLString::transcode(SLOW_DOWN_ATTRIBUTE));

  stopBiasAttribute =
    attributeNode->getNamedItem(XMLString::transcode(STOP_BIAS_ATTRIBUTE));

  connector->slowDownBias = 
    atof(XMLString::transcode(slowDownAttribute->getNodeValue()));

  connector->stopBias = 
    atof(XMLString::transcode(stopBiasAttribute->getNodeValue()));

}

void DOMParser::processRotationAxisTag(DOMNode *rotationAxisTag,
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = rotationAxisTag->getAttributes();
  DOMNode *attributeNodeAlpha = 0;
  DOMNode *attributeNodeBeta = 0;
  DOMNode *attributeNodeGamma = 0;

  attributeNodeAlpha =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeBeta =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeGamma =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  connector->rotationAxis.x =
    atof(XMLString::transcode(attributeNodeAlpha->getNodeValue()));
  connector->rotationAxis.y =
    atof(XMLString::transcode(attributeNodeBeta->getNodeValue()));
  connector->rotationAxis.z =
    atof(XMLString::transcode(attributeNodeGamma->getNodeValue()));

  copyPosition(&(connector->initRotationAxis), connector->rotationAxis);

}

void DOMParser::processSliderAxisTag(DOMNode *sliderAxisTag,
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = sliderAxisTag->getAttributes();
  DOMNode *attributeNodeAlpha = 0;
  DOMNode *attributeNodeBeta = 0;
  DOMNode *attributeNodeGamma = 0;

  attributeNodeAlpha =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeBeta =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeGamma =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  connector->sliderAxis.x =
    atof(XMLString::transcode(attributeNodeAlpha->getNodeValue()));
  connector->sliderAxis.y =
    atof(XMLString::transcode(attributeNodeBeta->getNodeValue()));
  connector->sliderAxis.z =
    atof(XMLString::transcode(attributeNodeGamma->getNodeValue()));

  copyPosition(&(connector->initSliderAxis),connector->sliderAxis);

}

void DOMParser::processDeflectionTag(DOMNode *deflectionTag,
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = deflectionTag->getAttributes();
  DOMNode *attributeNodeMin = 0;
  DOMNode *attributeNodeMax = 0;


  attributeNodeMin =
    attributeNode->getNamedItem(XMLString::transcode(MIN_ATTRIBUTE));
  attributeNodeMax =
    attributeNode->getNamedItem(XMLString::transcode(MAX_ATTRIBUTE));

  connector->min_deflection =
    atof(XMLString::transcode(attributeNodeMin->getNodeValue()));
  connector->max_deflection =
    atof(XMLString::transcode(attributeNodeMax->getNodeValue()));
}

void DOMParser::processPositionExceedableTag(DOMNode *positionExceedableTag,
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = positionExceedableTag->getAttributes();
  DOMNode *attributeNodeMin = 0;
  DOMNode *attributeNodeMax = 0;

  attributeNodeMin =
    attributeNode->getNamedItem(XMLString::transcode(MIN_ATTRIBUTE));
  attributeNodeMax =
    attributeNode->getNamedItem(XMLString::transcode(MAX_ATTRIBUTE));

  connector->minExceedPosition =
    atof(XMLString::transcode(attributeNodeMin->getNodeValue()));
  connector->maxExceedPosition =
    atof(XMLString::transcode(attributeNodeMax->getNodeValue()));
}

void DOMParser::processForceExceedableTag(DOMNode *forceExceedableTag,
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = forceExceedableTag->getAttributes();
  DOMNode *attributeNodeValue = 0;

  attributeNodeValue =
    attributeNode->getNamedItem(XMLString::transcode(VALUE_ATTRIBUTE));

  connector->minExceedForce =
    atof(XMLString::transcode(attributeNodeValue->getNodeValue()));
}

void DOMParser::processAnchorPointTag(DOMNode *anchorPointTag, 
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = anchorPointTag->getAttributes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;
  DOMNode *attributeNodeZ = 0;


  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  connector->anchorInitPos.x =
    atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  connector->anchorInitPos.y =
    atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  connector->anchorInitPos.z =
    atof(XMLString::transcode(attributeNodeZ->getNodeValue()));

  copyPosition(&(connector->anchorPos),connector->anchorInitPos);


}

void DOMParser::processMaxForceTag(DOMNode *maxForceNode, ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = maxForceNode->getAttributes();
  DOMNode *attributeNodeValue = 0;

  attributeNodeValue = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  connector->fMax = atof(
      XMLString::transcode(attributeNodeValue->getNodeValue()));

}

void DOMParser::processMaxVelocity(DOMNode *maxVelocityNode, 
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode  = maxVelocityNode->getAttributes();
  DOMNode *attributeNodeValue = 0;

  attributeNodeValue = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  connector->velMax = atof(
      XMLString::transcode(attributeNodeValue->getNodeValue()));

}


void DOMParser::processObjectConnectorDestinationTag(
    DOMNode *objectConnectorSourceTag,
    ConnectorDescription *connector,
    CompoundDescription *compound)
{
  DOMNamedNodeMap *attributeNode  = objectConnectorSourceTag->getAttributes();
  DOMNode         *nameNode =
    attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  string destinationName = XMLString::transcode(nameNode->getNodeValue());
  SingleSegment *destinationSegment;

  for(uint i = 0; i < compound->segments.size(); i++)
  {
    destinationSegment = &((*compound).segments[i]);

    if(destinationSegment->name == destinationName)
    {
      connector->aimSegNum = i;
      return; // found the destination segment
    }
  }
}

// **************************************************************************
// also adds the coordinates of the source segment to the coordinates of the
// connector, because anchorposition is parsed after source segment position
// **************************************************************************
void DOMParser::processObjectConnectorSourceTag(
    DOMNode *objectConnectorSourceTag,
    ConnectorDescription *connector,
    CompoundDescription *compound)
{
  DOMNamedNodeMap *attributeNode  = objectConnectorSourceTag->getAttributes();
  DOMNode         *nameNode =
    attributeNode->getNamedItem(XMLString::transcode(NAME_ATTRIBUTE));
  string sourceName = XMLString::transcode(nameNode->getNodeValue());
  SingleSegment *sourceSegment;

  for(uint i = 0; i < compound->segments.size(); i++)
  {
    sourceSegment = &((*compound).segments[i]);

    if(sourceSegment->name == sourceName)
    {
      connector->anchorInitPos.x += sourceSegment->init_position.x;
      connector->anchorInitPos.y += sourceSegment->init_position.y;
      connector->anchorInitPos.z += sourceSegment->init_position.z;
      connector->sourceSegNum = i;
      return; // found the source segment
    }
  }
}

void DOMParser::processFixedJointTag(DOMNode *fixedJointNode, 
    ConnectorDescription *connector)
{
  initialiseConnector(connector);
  connector->type = RD_CONN_FIX;
}


void DOMParser::processSimulationTag(DOMNode *simulationTag)
{
  DOMNodeList *simulationParameterNodes = simulationTag->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  for(uint i = 0; i < simulationParameterNodes->getLength(); i++)
  {
    node = simulationParameterNodes->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, CAMERA_POSITION_TAG) == 0)
    {
      extractCameraPosition(node);
    }
    else if(strcmp(name, CAMERA_ORIENTATION_TAG) == 0)
    {
      extractCameraOrientation(node);
    }
    else if(strcmp(name, WINDOW_SIZE_TAG) == 0)
    {
      extractWindowSize(node);
    }
    else if(strcmp(name, UPDATE_FREQUENCY_TAG) == 0)
    {
      extractUpdateFrequency(node);
    }
    else if(strcmp(name, AXES) == 0)
    {
      extractAxesVisualization(node);
    }
  }
}

void DOMParser::extractWindowSize(DOMNode *windowSizeNode)
{
  DOMNamedNodeMap *attributeNode  = windowSizeNode->getAttributes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));

  simulationDescription->setWindowSize(
      atof(XMLString::transcode(attributeNodeX->getNodeValue())),
      atof(XMLString::transcode(attributeNodeY->getNodeValue())));
}

void DOMParser::extractUpdateFrequency(DOMNode *updateFrequencyNode)
{
  DOMNamedNodeMap *attributeNode  = updateFrequencyNode->getAttributes();
  DOMNode *attributeNodePhysSim = 0;
  DOMNode *attributeNodeNet     = 0;

  attributeNodePhysSim =
    attributeNode->getNamedItem(XMLString::transcode(PHYS_SIM_ATTRIBUTE));
  attributeNodeNet =
    attributeNode->getNamedItem(XMLString::transcode(NET_ATTRIBUTE));

  simulationDescription->setUpdateFrequency(
      atof(XMLString::transcode(attributeNodePhysSim->getNodeValue())),
      atof(XMLString::transcode(attributeNodeNet->getNodeValue())));
}

void DOMParser::extractAxesVisualization(DOMNode *axesVisualizationNode)
{
  DOMNamedNodeMap *attributeNode  = axesVisualizationNode->getAttributes();
  DOMNode *attributeNodeVisualization = 0;
  DOMNode *attributeNodeScaling       = 0;
  char    *visualizationString        = 0;

  attributeNodeVisualization =
    attributeNode->getNamedItem(XMLString::transcode(VISUALIZATION_ATTRIBUTE));
  attributeNodeScaling =
    attributeNode->getNamedItem(XMLString::transcode(SCALING_ATTRIBUTE));

  visualizationString = XMLString::transcode(
      attributeNodeVisualization->getNodeValue());
  if(strcmp(visualizationString, TRUE_ATTRIBUTE) == 0)
  {
    simulationDescription->setAxesVisualization(true,
        atof(XMLString::transcode(attributeNodeScaling->getNodeValue())));
  }
  else
  {
    simulationDescription->setAxesVisualization(false,
        atof(XMLString::transcode(attributeNodeScaling->getNodeValue())));
  }
}

void DOMParser::extractCameraOrientation(DOMNode *cameraOrientationNode)
{
  DOMNamedNodeMap *attributeNode  = cameraOrientationNode->getAttributes();
  DOMNode *attributeNodeA = 0;
  DOMNode *attributeNodeB = 0;
  DOMNode *attributeNodeG = 0;

  attributeNodeA =
    attributeNode->getNamedItem(XMLString::transcode(ALPHA_ATTRIBUTE));
  attributeNodeB =
    attributeNode->getNamedItem(XMLString::transcode(BETA_ATTRIBUTE));
  attributeNodeG =
    attributeNode->getNamedItem(XMLString::transcode(GAMMA_ATTRIBUTE));

  simulationDescription->setCameraOrientation(
      RAD_TO_DEG(atof(XMLString::transcode(attributeNodeA->getNodeValue()))) + DP_OFFSET,
      RAD_TO_DEG(atof(XMLString::transcode(attributeNodeB->getNodeValue()))),
      RAD_TO_DEG(atof(XMLString::transcode(attributeNodeG->getNodeValue()))) - DP_OFFSET);

}

void DOMParser::extractCameraPosition(DOMNode *cameraPositionNode)
{
  DOMNamedNodeMap *attributeNode  = cameraPositionNode->getAttributes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;
  DOMNode *attributeNodeZ = 0;


  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  simulationDescription->setCameraPosition(
      atof(XMLString::transcode(attributeNodeX->getNodeValue())),
      atof(XMLString::transcode(attributeNodeY->getNodeValue())),
      atof(XMLString::transcode(attributeNodeZ->getNodeValue())));
}


void DOMParser::processEnvironmentTag(DOMNode *environmentNode)
{
  DOMNodeList *environmentBlocks = environmentNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;
  SingleSegment segment;
  SingleElement element;
  LightSource lightSource;

  for(uint i = 0; i < environmentBlocks->getLength(); i++)
  {
    node = environmentBlocks->item(i);
    name = XMLString::transcode(node->getNodeName());
    initialiseSingleSegment(&segment);
    if(strcmp(name, BOX_TAG) == 0)
    {
      processObjectTag(node, &segment, NULL, NULL);
      segment.type = RD_BOX_GEOM;
      copySegmentToElement(&element, segment);
      environmentDescription->addElement(element);
      printElement(element);
    }
    else if(strcmp(name, SPHERE_TAG) == 0)
    {
      processObjectTag(node, &segment, NULL, NULL);
      segment.type = RD_SPHERE_GEOM;
      copySegmentToElement(&element, segment);
      environmentDescription->addElement(element);
    }
    else if(strcmp(name, CAPPED_CYLINDER_TAG) == 0)
    {
      processObjectTag(node, &segment, NULL, NULL);
      segment.type = RD_CAPPED_CYLINDER_GEOM;
      copySegmentToElement(&element, segment);
      environmentDescription->addElement(element);
    }
    else if(strcmp(name, CYLINDER_TAG) == 0)
    {
      processObjectTag(node, &segment, NULL, NULL);
      segment.type = RD_CYLINDER_GEOM;
      copySegmentToElement(&element, segment);
      environmentDescription->addElement(element);
    }
    else if(strcmp(name, LIGHT_SOURCE_TAG) == 0)
    {
      processLightSourceTag(node, &lightSource);
      environmentDescription->addLight(lightSource);
      printLightSource(lightSource);
    }
    else if(strcmp(name, RANDOM_BARS_TAG) == 0)
    {
      processRandomBars(node); // environmentDescription is available globally
    }
    else if(strcmp(name, AMBIENT_LIGHT_SOURCE_TAG) == 0) { 
      processAmbientLightTag(node);
    }
  }
}

void DOMParser::processRandomBars(DOMNode *randomBarsNode)
{
  Y_DEBUG("DOMParser::processAmbientLightTag start");
  DOMNodeList     *children              = randomBarsNode->getChildNodes();
  DOMNamedNodeMap *attributeNode         = randomBarsNode->getAttributes();
  DOMNode         *attributeNodeQuantity = 0;
  DOMNode         *node                  = 0;
  char            *name                  = 0;

  int              quantity              = 0;

  Position  dimension;
  Position  varianceDimension;
  Position  position;
  Position  distance;
  Position  varianceDistance;
  Position  orientation;
  Position  varianceOrientation;
  Color     color;
  SingleSegment segment;
  SingleElement element;

  initialiseSingleSegment(&segment);

  attributeNodeQuantity = attributeNode->getNamedItem(XMLString::transcode(QUANTITY_ATTRIBUTE));

  quantity = atoi(XMLString::transcode(attributeNodeQuantity->getNodeValue()));

  Y_DEBUG("****************************************");
  Y_DEBUG("************* random bars **************");
  Y_DEBUG("****************************************");

  for(uint i = 0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name,DIMENSION_TAG) == 0)
    {
      Y_DEBUG("--> reading dimension tag");
      extractCoordinate(node, &dimension);
    }

    if(strcmp(name,VARIANCE_DIMENSION_TAG) == 0)
    {
      Y_DEBUG("--> reading dimension variance tag");
      extractCoordinate(node, &varianceDimension);
    }

    if(strcmp(name,INITIAL_POSITION_TAG) == 0)
    {
      Y_DEBUG("--> reading position tag");
      extractCoordinate(node, &position);
    }

    if(strcmp(name,DISTANCE_TAG) == 0)
    {
      Y_DEBUG("--> reading distance tag");
      extractCoordinate(node, &distance);
    }

    if(strcmp(name,VARIANCE_DISTANCE_TAG) == 0)
    {
      Y_DEBUG("--> reading distance variance tag");
      extractCoordinate(node, &varianceDistance);
    }

    if(strcmp(name,ORIENTATION_TAG) == 0)
    {
      Y_DEBUG("--> reading orientation tag");
      extractOrientation(node, &orientation);
    }

    if(strcmp(name,VARIANCE_ORIENTATION_TAG) == 0)
    {
      Y_DEBUG("--> reading orientation variance tag");
      extractOrientation(node, &varianceOrientation);
    }
    if(strcmp(name,GRAPHICAL_REPRESENTATION_TAG) == 0)
    {
      Y_DEBUG("--> reading graphicalRepresentation");
      extractGraphicalRepresentation(node, &segment);
    }
  }

  Y_DEBUG("Quantity              = %d", quantity);

  Y_DEBUG("dimension.x           = %f",dimension.x);
  Y_DEBUG("dimension.y           = %f",dimension.y);
  Y_DEBUG("dimension.z           = %f",dimension.z);

  Y_DEBUG("varianceDimension.x   = %f",varianceDimension.x);
  Y_DEBUG("varianceDimension.y   = %f",varianceDimension.y);
  Y_DEBUG("varianceDimension.z   = %f",varianceDimension.z);

  Y_DEBUG("position.x            = %f",position.x);
  Y_DEBUG("position.y            = %f",position.y);
  Y_DEBUG("position.z            = %f",position.z);

  Y_DEBUG("distance.x            = %f",distance.x);
  Y_DEBUG("distance.y            = %f",distance.y);
  Y_DEBUG("distance.z            = %f",distance.z);

  Y_DEBUG("varianceDistance.x    = %f",varianceDistance.x);
  Y_DEBUG("varianceDistance.y    = %f",varianceDistance.y);
  Y_DEBUG("varianceDistance.z    = %f",varianceDistance.z);

  Y_DEBUG("orientation.x         = %f",orientation.x);
  Y_DEBUG("orientation.y         = %f",orientation.y);
  Y_DEBUG("orientation.z         = %f",orientation.z);

  Y_DEBUG("varianceOrientation.x = %f",varianceOrientation.x);
  Y_DEBUG("varianceOrientation.y = %f",varianceOrientation.y);
  Y_DEBUG("varianceOrientation.z = %f",varianceOrientation.z);

  Y_DEBUG("color.r               = %d",segment.color.r);
  Y_DEBUG("color.g               = %d",segment.color.g);
  Y_DEBUG("color.b               = %d",segment.color.b);


  Y_DEBUG("****************************************");
  Y_DEBUG("******** creating random bars **********");
  Y_DEBUG("****************************************");

  dimension.randomise.enabled       = true;
  dimension.randomise.type          = RD_RANDOMISE_TYPE_INTERVAL;
  dimension.randomise.method        = RD_RANDOMISE_METHOD_GLOBAL;
  dimension.randomise.probability.x = 1;
  dimension.randomise.probability.y = 1;
  dimension.randomise.probability.z = 1;
  dimension.randomise.min.x         = dimension.x - varianceDimension.x;
  dimension.randomise.min.y         = dimension.y - varianceDimension.y;
  dimension.randomise.min.z         = dimension.z - varianceDimension.z;
  dimension.randomise.max.x         = dimension.x + varianceDimension.x;
  dimension.randomise.max.y         = dimension.y + varianceDimension.y;
  dimension.randomise.max.z         = dimension.z + varianceDimension.z;
  printPosition(dimension,"randomBar.dimension.");

  for(int i=0; i < quantity; i++)
  {
    segment.type                     = RD_BOX_GEOM;
    segment.isBumpable               = false;
    position.randomise.enabled       = true;
    position.randomise.type          = RD_RANDOMISE_TYPE_INTERVAL;
    position.randomise.method        = RD_RANDOMISE_METHOD_GLOBAL;
    position.randomise.probability.x = 1;
    position.randomise.probability.y = 1;
    position.randomise.probability.z = 1;
    position.randomise.min.x         = position.x - varianceDistance.x;
    position.randomise.min.y         = position.y - varianceDistance.y;
    position.randomise.min.z         = position.z - varianceDistance.z;
    position.randomise.max.x         = position.x + varianceDistance.x;
    position.randomise.max.y         = position.y + varianceDistance.y;
    position.randomise.max.z         = position.z + varianceDistance.z;
    printPosition(position, "randomBar.position.");
    copyPosition(&(segment.init_position),position);
    position.x += distance.x;
    position.y += distance.y;
    position.z += distance.z;
    copyPosition(&(segment.dimensions),dimension);
    copySegmentToElement(&element, segment);
    environmentDescription->addElement(element);
  }

  //extractGraphicalRepresentation(node, segment);
  //processPhysicalRepresentationTag(node, segment);
}

void DOMParser::processAmbientLightTag(DOMNode *ambientLightNode)
{
  Y_DEBUG("DOMParser::processAmbientLightTag start");
  DOMNodeList     *children           = ambientLightNode->getChildNodes();
  DOMNode         *node               = 0;
  char            *name               = 0;

  RandomiseScalar randomise;
  initialiseRandomiseScalar(&randomise);

  for(uint i = 0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name,RANDOMISE_INTERVAL_TAG) == 0)
    {
      extractRandomiseScalarInterval(node, &(randomise));
    }

    if(strcmp(name,RANDOMISE_LIST_TAG) == 0)
    {
      extractRandomiseScalarList(node, &(randomise));
    }

  }

  environmentDescription->setAmbientLight(
      processIntensityAttribute(ambientLightNode));
  environmentDescription->setAmbientLightRandomise(randomise);
  Y_DEBUG("DOMParser::processAmbientLightTag end");
}

void DOMParser::extractRandomiseScalarList(DOMNode *randomiseListNode,
    RandomiseScalar *randomise)
{
  DOMNode         *node                     = 0;
  char            *name                     = 0;
  DOMNodeList     *children                 = randomiseListNode->getChildNodes();
  DOMNamedNodeMap *attributeNode            = randomiseListNode->getAttributes();
  DOMNode         *attributeNodeMethod      = 0;
  DOMNode         *attributeNodeProbability = 0;
  DOMNode         *attributeNodeValue       = 0;

  Y_DEBUG("DOMParser::extractRandomiseList");
  randomise->type    = RD_RANDOMISE_TYPE_LIST;
  randomise->enabled = true;

  attributeNodeMethod =
    attributeNode->getNamedItem(XMLString::transcode(METHOD_ATTRIBUTE));

  attributeNodeProbability =
    attributeNode->getNamedItem(XMLString::transcode(PROBABILITY_ATTRIBUTE));


  char *method      = XMLString::transcode(attributeNodeMethod->getNodeValue());
  float probability = atof(
      XMLString::transcode(attributeNodeProbability->getNodeValue()));

  Y_DEBUG("DOMParser::extractRandomiseList: setting probability to %f",
      probability);

  randomise->probability = probability;

  if(strcmp(method,RANDOMISE_METHOD_GLOBAL) == 0)
  {
    Y_DEBUG("DOMParser::extractRandomiseList: setting to GLOBAL");
    randomise->method = RD_RANDOMISE_METHOD_GLOBAL;
  }
  if(strcmp(method,RANDOMISE_METHOD_ADDITIVE) == 0)
  {
    Y_DEBUG("DOMParser::extractRandomiseList: setting to ADDITIVE");
    randomise->method = RD_RANDOMISE_METHOD_ADDITIVE;
  }

  randomise->entries.clear();

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, ENTRY_TAG) == 0)
    {
      attributeNode = node->getAttributes();
      attributeNodeValue = 
        attributeNode->getNamedItem(XMLString::transcode(VALUE_ATTRIBUTE));
      double value = atof(
          XMLString::transcode(attributeNodeValue->getNodeValue()));
      randomise->entries.push_back(value);
    }
  }

}

void DOMParser::extractRandomiseScalarInterval(DOMNode *randomiseNode,
    RandomiseScalar *randomise)
{
  DOMNamedNodeMap *attributeNode    = randomiseNode->getAttributes();
  DOMNode *attributeNodeMethod      = 0;
  DOMNode *attributeNodeMin         = 0;
  DOMNode *attributeNodeMax         = 0;
  DOMNode *attributeNodeVariance    = 0;
  DOMNode *attributeNodeProbability = 0;
  char    *attributeString;

  randomise->enabled       = true;
  randomise->type          = RD_RANDOMISE_TYPE_INTERVAL;

  Y_DEBUG("DOMParser::extractRandomiseScalarInterval");

  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: method");
  attributeNodeMethod =
    attributeNode->getNamedItem(XMLString::transcode(METHOD_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: min");
  attributeNodeMin =
    attributeNode->getNamedItem(XMLString::transcode(MIN_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: max");
  attributeNodeMax =
    attributeNode->getNamedItem(XMLString::transcode(MAX_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: probability");
  attributeNodeProbability =
    attributeNode->getNamedItem(XMLString::transcode(PROBABILITY_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: variance");
  attributeNodeVariance =
    attributeNode->getNamedItem(XMLString::transcode(VARIANCE_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractRandomiseScalarInterval: done");

  randomise->min = atof(
      XMLString::transcode(attributeNodeMin->getNodeValue()));
  randomise->max = atof(
      XMLString::transcode(attributeNodeMax->getNodeValue()));
  randomise->probability = atof(
      XMLString::transcode(attributeNodeProbability->getNodeValue()));
  randomise->variance = atof(
      XMLString::transcode(attributeNodeVariance->getNodeValue()));

  attributeString = XMLString::transcode(attributeNodeMethod->getNodeValue());

  if(strcmp(attributeString, RANDOMISE_METHOD_ADDITIVE) == 0)
  {
    randomise->method = RD_RANDOMISE_METHOD_ADDITIVE;
  }
  if(strcmp(attributeString, RANDOMISE_METHOD_GLOBAL) == 0)
  {
    randomise->method = RD_RANDOMISE_METHOD_GLOBAL;
  }
}



void DOMParser::processLightSourceTag(DOMNode *lightSourceNode,
    LightSource *lightSource)
{
  initialseLightSource(lightSource);
  DOMNodeList     *children           = lightSourceNode->getChildNodes();
  DOMNamedNodeMap *attributeNode      = lightSourceNode->getAttributes();
  DOMNode         *drawRangeAttribute = 0;
  DOMNode         *node               = 0;
  char            *name               = 0;
  char            *drawRangeString    = 0;


  drawRangeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(DRAW_RANGE_ATTRIBUTE));

  if(drawRangeAttribute != NULL)
  {
    drawRangeString = XMLString::transcode(drawRangeAttribute->getNodeValue());
    if(strcmp(drawRangeString,TRUE_ATTRIBUTE) == 0)
    {
      lightSource->drawRange = true;
    }
    if(strcmp(drawRangeString,FALSE_ATTRIBUTE) == 0)
    {
      lightSource->drawRange = false;
    }
  }

  lightSource->intensity = processIntensityAttribute(lightSourceNode);

  for(uint i = 0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, COLOR_TAG) == 0)
    {
      extractColor(node, &(lightSource->color));
    }

    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(lightSource->pos));
    }
  }
}

double DOMParser::processIntensityAttribute(DOMNode *node)
{
  DOMNamedNodeMap *attributeNode = node->getAttributes();
  DOMNode *intensityAttribute = 0;

  intensityAttribute = attributeNode->getNamedItem(
      XMLString::transcode(INTENSITY_ATTRIBUTE));

  return atof(XMLString::transcode(intensityAttribute->getNodeValue()));
}

void DOMParser::printElement(SingleElement element)
{
  Y_DEBUG("element.type              = %d",element.type);
  Y_DEBUG("element.dimensions.y      = %f",element.dimensions.y);
  Y_DEBUG("element.dimensions.y      = %f",element.dimensions.y);
  Y_DEBUG("element.dimensions.z      = %f",element.dimensions.z);
  Y_DEBUG("element.dimensions.radius = %f",element.dimensions.radius);
  Y_DEBUG("element.dimensions.length = %f",element.dimensions.length);
  Y_DEBUG("element.color.r           = %f",element.color.r);
  Y_DEBUG("element.color.g           = %f",element.color.g);
  Y_DEBUG("element.color.b           = %f",element.color.b);
  Y_DEBUG("element.color.alpha       = %f",element.color.alpha);
  printPosition(element.position, "element.position.");
  printPosition(element.rotation, "element.rotation.");
  printPosition(element.init_rotation, "element.init_rotation.");

}

void DOMParser::copySegmentToElement(SingleElement *element, 
    SingleSegment segment)
{
  copyPosition(&(element->position),segment.init_position);
  copyPosition(&(element->dimensions),segment.dimensions);
  copyPosition(&(element->init_rotation),segment.init_rotation);
  copyPosition(&(element->rotation),segment.init_rotation);
  element->type                               = segment.type;
  element->color.r                            = segment.color.r;
  element->color.g                            = segment.color.g;
  element->color.b                            = segment.color.b;
  element->color.alpha                        = segment.color.alpha;
}

void DOMParser::processObjectTag(DOMNode *boxNode, SingleSegment *segment,
    RobotDescription *robotDescription, CompoundDescription *compound)
{
  DOMNodeList *children = boxNode->getChildNodes();
  DOMNamedNodeMap *attributeNode  = boxNode->getAttributes();
  DOMNode         *attributeNodeName = 0;
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processObjectTag");

  initialiseSingleSegment(segment);

  attributeNodeName = attributeNode->getNamedItem(
      XMLString::transcode(NAME_ATTRIBUTE));

  segment->name = XMLString::transcode(attributeNodeName->getNodeValue());

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, BUMPABLE_TAG) == 0)
    {
      extractBumpable(node, &(segment->isBumpable));
    }

    if(strcmp(name, TRACE_CONTACT_TAG) == 0)
    {
      extractTraceContact(node, &(segment->traceContact));
    }

    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(segment->init_position));
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(segment->init_rotation));
    }

    if(strcmp(name, GRAPHICAL_REPRESENTATION_TAG) == 0)
    {
      extractGraphicalRepresentation(node, segment);
    }

    if(strcmp(name, PHYSICAL_REPRESENTATION_TAG) == 0)
    {
      processPhysicalRepresentationTag(node, segment);
    }

    if(strcmp(name, DIMENSION_TAG) == 0)
    {
      extractDimension(node, &(segment->dimensions));
    }

    if(strcmp(name, TRACE_POINT_TAG) == 0)
    {
      extractTracePoint(node, &(segment->trace_point));
    }

    if(strcmp(name, TRACE_LINE_TAG) == 0)
    {
      extractTraceLine(node, &(segment->trace_line));
    }

    // if we are in environment tag, then tere is no need for sensors
    if(robotDescription != NULL &&  compound != NULL)
    {
      if(strcmp(name, SHARP_GP2D12_37_TAG) == 0)
      {
        processSharpGP2D12_37(node, robotDescription, compound,
            segment);
      }

      if(strcmp(name, SHARP_DM2Y3A003K0F_TAG) == 0)
      {
        processSharpDM2Y3A003K0F(node, robotDescription, compound,
            segment);
      }

      if(strcmp(name, INFRARED_DISTANCE_SENSOR_TAG) == 0)
      {
        processInfraredDistanceSensorTag(node, robotDescription, compound,
            segment);
      }

      if(strcmp(name, GENERIC_ROTATION_SENSOR_TAG) == 0)
      {
        processGenericRotationSensorTag(node, robotDescription, compound,
            segment);
      }

      if(strcmp(name, LDR_SENSOR_TAG) == 0)
      {
        processLdrSensorTag(node, robotDescription, compound,
            segment);
      }
  if(strcmp(name, DIRECTED_CAMREA_TAG) == 0)
      {
        processDirectedCameraTag(node, robotDescription, compound, segment);
      }

    }
  }
}

void DOMParser::extractTraceLine(DOMNode *traceLineTag,
    TraceLineDescription *trace_line)
{
  Y_DEBUG("DOMNode::extractTraceLine");
  DOMNodeList     *children           = traceLineTag->getChildNodes();
  DOMNamedNodeMap *attributeNode      = traceLineTag->getAttributes();
  DOMNode         *attributeIteration = 0;
  DOMNode         *node               = 0;
  char            *name               = 0;

  numberOfDrawLines++;

  trace_line->enabled = true;

  attributeIteration =
    attributeNode->getNamedItem(XMLString::transcode(ITERATIONS_ATTRIBUTE));

  Y_DEBUG("DOMNode::extractTraceLine: extracted attributeIteration");

  if(attributeIteration != NULL)
  {
    trace_line->iterations = MAX(1,
        atoi(XMLString::transcode(attributeIteration->getNodeValue())));
  }
  else
  {
    Y_DEBUG("DOMNode::extractTraceLine: no iterations attribute found");
    trace_line->iterations = 1;
  }

  Y_DEBUG("DOMNode::extractTraceLine: set trace_line->iterations = %d",
      trace_line->iterations);

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    Y_DEBUG("DOMNode::extractTraceLine: processing child = %s", name);

    if(strcmp(name, COLOR_TAG) == 0)
    {
      extractColor(node, &(trace_line->color));
    }

  }
}


void DOMParser::extractTracePoint(DOMNode *tracePointTag,
    TracePointDescription *trace_point)
{
  Y_DEBUG("DOMNode::extractTracePoint");
  DOMNodeList *children          = tracePointTag->getChildNodes();
  DOMNamedNodeMap *attributeNode = tracePointTag->getAttributes();
  DOMNode *attributeSize         = 0;
  DOMNode *attributeIteration    = 0;
  DOMNode     *node              = 0;
  char        *name              = 0;

  trace_point->enabled = true;

  attributeSize =
    attributeNode->getNamedItem(XMLString::transcode(SIZE_ATTRIBUTE));

  Y_DEBUG("DOMNode::extractTracePoint: extracted attributeSize");

  attributeIteration =
    attributeNode->getNamedItem(XMLString::transcode(ITERATIONS_ATTRIBUTE));

  Y_DEBUG("DOMNode::extractTracePoint: extracted attributeIteration");

  trace_point->size = atof(XMLString::transcode(attributeSize->getNodeValue()));

  Y_DEBUG("DOMNode::extractTracePoint: set trace_point->size = %f",
      trace_point->size);

  if(attributeIteration != NULL)
  {
    trace_point->iterations = MAX(1,
        atoi(XMLString::transcode(attributeIteration->getNodeValue())));
  }
  else
  {
    Y_DEBUG("DOMNode::extractTracePoint: no iterations attribute found");
    trace_point->iterations = 1;
  }

  Y_DEBUG("DOMNode::extractTracePoint: set trace_point->iterations = %d",
      trace_point->iterations);

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    Y_DEBUG("DOMNode::extractTracePoint: processing child = %s", name);

    if(strcmp(name, COLOR_TAG) == 0)
    {
      extractColor(node, &(trace_point->color));
    }

  }
}

void DOMParser::processPhysicalRepresentationTag(
    DOMNode *physicalRepTag,
    SingleSegment *segment)
{
  DOMNodeList *children = physicalRepTag->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MASS_TAG) == 0)
    {
      processMassTag(node, &(segment->mass));
    }

    if(strcmp(name, MASS_GEOM_BOX_TAG) == 0)
    {
      segment->massGeometry = RD_BOX_GEOM;
      processGenericMassGeomTag(node, segment);
    }
    if(strcmp(name, MASS_GEOM_SPHERE_TAG) == 0)
    {
      segment->massGeometry = RD_SPHERE_GEOM;
      processGenericMassGeomTag(node, segment);
    }
    if(strcmp(name, MASS_GEOM_CYLINDER_TAG) == 0)
    {
      segment->massGeometry = RD_CYLINDER_GEOM;
      processGenericMassGeomTag(node, segment);
    }
    if(strcmp(name, MASS_GEOM_CAPPED_CYLINDER_TAG) == 0)
    {
      segment->massGeometry = RD_CAPPED_CYLINDER_GEOM;
      processGenericMassGeomTag(node, segment);
    }

    if(strcmp(name, BOUNCE_TAG) == 0)
    {
      processBounceTag(node, 
          &(segment->cBounce), 
          &(segment->cBounceVel), 
          &(segment->contactMode));
    }

    if(strcmp(name, SLIP_TAG) == 0)
    {
      processSlipTag(node,
          &(segment->cSlip1),
          &(segment->cSlip2),
          &(segment->contactMode));
    }

    if(strcmp(name, CONSTRAINT_FORCE_MIXING_TAG) == 0)
    {
      processConstraintForceMixingTag(node, 
          &(segment->cSoftCFM),
          &(segment->contactMode));
    }

    if(strcmp(name, ERROR_REDUCTION_PARAMETER_TAG) == 0)
    {
      processErrorReductionParameterTag(node,
          &(segment->cSoftErp),
          &(segment->contactMode));
    }

    if(strcmp(name, COULOMB_FRICTION_TAG) == 0)
    {
      processCoulombFriction(node,
          &(segment->cMu),
          &(segment->cMu2),
          &(segment->contactMode));

    }
  }

  //printSegment(*segment);
}

void DOMParser::processGenericMassGeomTag(DOMNode *massGeomNode, 
                                          SingleSegment *segment) 
{
  DOMNodeList *children = massGeomNode->getChildNodes();
  DOMNamedNodeMap *attributeNode  = massGeomNode->getAttributes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processMassGeomTag");

  node = attributeNode->getNamedItem(XMLString::transcode(VISUALIZATION_ATTRIBUTE));

  if(strcmp(TRUE_ATTRIBUTE, XMLString::transcode(node->getNodeValue())) == 0)
  {
    segment->drawRigidBody = true;
  }
  else 
  {
    segment->drawRigidBody = false;
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RELATIVE_ROTATION_TAG) == 0)
    {
      extractOrientation(node, &(segment->massRelativeRotation));
    }
    if(strcmp(name, OFFSET_TAG) == 0)
    {
      extractCoordinate(node, &(segment->massOffset));
    }
    if(strcmp(name, DIMENSION_TAG) == 0)
    {
      extractDimension(node, &(segment->massDimension));
    }
  }
}

void DOMParser::processConstraintForceMixingTag(DOMNode *cfmNode, 
    double *cfm, 
    int *contactMode)
{
  DOMNamedNodeMap *attributeNode = cfmNode->getAttributes();
  DOMNode *cfmAttribute = 0;

  cfmAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  *cfm = atof(XMLString::transcode(cfmAttribute->getNodeValue()));

  if(*cfm > 0.0)
  {
    *contactMode |= dContactSoftCFM;
  }
}

void DOMParser::processErrorReductionParameterTag(DOMNode *erpNode, 
    double *erp, 
    int *contactMode)
{
  DOMNamedNodeMap *attributeNode = erpNode->getAttributes();
  DOMNode *erpAttribute = 0;

  erpAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  *erp = atof(XMLString::transcode(erpAttribute->getNodeValue()));

  if(*erp > 0.0)
  {
    *contactMode |= dContactSoftERP;
  }
}



void DOMParser::processSlipTag(DOMNode *slipNode,
    double *slip1,
    double *slip2,
    int *contactMode)
{
  DOMNamedNodeMap *attributeNode = slipNode->getAttributes();
  DOMNode *slip1Attribute = 0;
  DOMNode *slip2Attribute = 0;

  slip1Attribute = attributeNode->getNamedItem(
      XMLString::transcode(X_ATTRIBUTE));

  slip2Attribute = attributeNode->getNamedItem(
      XMLString::transcode(Y_ATTRIBUTE));

  *slip1 = atof(XMLString::transcode(slip1Attribute->getNodeValue()));
  *slip2 = atof(XMLString::transcode(slip2Attribute->getNodeValue()));

  if(*slip1 > 0.0)
  {
    *contactMode |= dContactSlip1;
  }

  if(*slip2 > 0.0)
  {
    *contactMode |= dContactSlip2;
  }

}

void DOMParser::processCoulombFriction(DOMNode *frictionNode,
    double *friction1,
    double *friction2,
    int *contactMode)
{
  DOMNamedNodeMap *attributeNode = frictionNode->getAttributes();
  DOMNode *friction1Attribute = 0;
  DOMNode *friction2Attribute = 0;

  friction1Attribute = attributeNode->getNamedItem(
      XMLString::transcode(X_ATTRIBUTE));

  friction2Attribute = attributeNode->getNamedItem(
      XMLString::transcode(Y_ATTRIBUTE));

  *friction1 = atof(XMLString::transcode(friction1Attribute->getNodeValue()));
  *friction2 = atof(XMLString::transcode(friction2Attribute->getNodeValue()));

  if(*friction2 > 0.0)
  {
    *contactMode |= dContactMu2;
  }

}

void DOMParser::processBounceTag(DOMNode *bounceNode, 
    double *bounce,
    double *bounceVel,
    int *contactMode)
{
  DOMNamedNodeMap *attributeNode = bounceNode->getAttributes();
  DOMNode *bounceAttribute = 0;
  DOMNode *bounceVelocityAttribute = 0;

  bounceAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  bounceVelocityAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VELOCITY_ATTRIBUTE));

  *bounce = atof(XMLString::transcode(bounceAttribute->getNodeValue()));
  *bounceVel =
    atof(XMLString::transcode(bounceVelocityAttribute->getNodeValue()));

  if(*bounce > 0.0)
  {
    *contactMode |= dContactBounce;
  }

}


void DOMParser::processMassTag(DOMNode *massNode, double *mass)
{
  DOMNamedNodeMap *attributeNode = massNode->getAttributes();
  DOMNode *massAttribute = 0;

  massAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  *mass = atof(XMLString::transcode(massAttribute->getNodeValue()));

}

void DOMParser::processLdrSensorTag(
    DOMNode *ldrNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound,
    SingleSegment *segment)
{
  SensorStruct s;
  LdrSensorStruct lS;

  initialiseSensorStruct(&s);
  initialiseLdrSensorStruct(&lS);

  DOMNodeList *children = ldrNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(lS.init_pos));
      lS.init_pos.x += segment->init_position.x;
      lS.init_pos.y += segment->init_position.y;
      lS.init_pos.z += segment->init_position.z;
    }

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(lS.startIntensity), &(lS.endIntensity));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(lS.startValue), &(lS.endValue));
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(lS.direction));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

    if(strcmp(name, OPENING_ANGLE_TAG) == 0)
    {
      processOpeningAngleTag(node, &(lS.spreadAngleX), &(lS.spreadAngleY));
    }


  }

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_LDR_SENSOR;

  //printSensorStructInformation(s);
  //printLdrSensorInformation(lS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(ldrNode, sD);
  sD->setLdrSensor(lS);
  robotDescription->addSensor(sD);

  delete sD;
}

void DOMParser::processSharpGP2D12_37(
    DOMNode *irDistNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound,
    SingleSegment *segment)
{
  SensorStruct s;
  SharpGP2D12_37_SensorStruct sS;

  initialiseSensorStruct(&s);
  initialiseSharpGP2D12_37Struct(&sS);

  DOMNodeList *children = irDistNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(sS.initStartPos));
      sS.initStartPos.x += segment->init_position.x;
      sS.initStartPos.y += segment->init_position.y;
      sS.initStartPos.z += segment->init_position.z;
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(sS.direction));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(sS.startValue), &(sS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }
  }

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_SHARP_GP2D12_37_SENSOR;

  //  printSensorStructInformation(s);
  //  printInfraredSensorInformation(iS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(irDistNode, sD);
  sD->setSharpGP2D12_37_Sensor(sS);
  robotDescription->addSensor(sD);

  delete sD;
}



void DOMParser::processSharpDM2Y3A003K0F(
    DOMNode *irDistNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound,
    SingleSegment *segment)
{
  SensorStruct s;
  SharpDM2Y3A003K0F_SensorStruct sS;

  initialiseSensorStruct(&s);
  initialiseSharpDM2Y3A003K0FStruct(&sS);

  DOMNodeList *children = irDistNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(sS.initStartPos));
      sS.initStartPos.x += segment->init_position.x;
      sS.initStartPos.y += segment->init_position.y;
      sS.initStartPos.z += segment->init_position.z;
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(sS.direction));
    }

   if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(sS.startValue), &(sS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }
  }

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_SHARP_DM2Y3A003K0F_SENSOR;

  //  printSensorStructInformation(s);
  //  printInfraredSensorInformation(iS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(irDistNode, sD);
  sD->setSharpDM2Y3A003K0F_Sensor(sS);
  robotDescription->addSensor(sD);

  delete sD;
}
void DOMParser::processDirectedCameraTag(DOMNode *cameraSensorNode, RobotDescription *robotDescription, CompoundDescription *compound, SingleSegment *segment)
{
  SensorStruct s;
  DirectedCameraStruct dS;

  initialiseSensorStruct(&s);
  initialiseDirectedCameraStruct(&dS);

  DOMNodeList *children = cameraSensorNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(dS.startValue), &(dS.endValue));
    }
    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }
    if(strcmp(name, OPENING_ANGLE_TAG) == 0)
    {
      processAngleNode(node, &(dS.openingAngle));
    }
    if(strcmp(name, SENSOR_VALUES_TAG) == 0)
    {
      processSensorValuesNode(node, &(dS.xPixel), &(dS.yPixel));
    }
    if(strcmp(name, LIGHT_TYPE_TAG) == 0)
    {
      processLightTypeNode(node, &(dS.light));
    }   
    if(strcmp(name, COLOR_TYPE_TAG) == 0)
    {
      processColorTypeNode(node, &(dS.colorType));
    }   
    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(dS.init_pos));
      dS.init_pos.x += segment->init_position.x;
      dS.init_pos.y += segment->init_position.y;
      dS.init_pos.z += segment->init_position.z;
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(dS.direction));
    }
  }

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_DIRECTED_CAMERA_SENSOR;

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(cameraSensorNode, sD);
  sD->setDirectedCamera(dS);
  robotDescription->addSensor(sD);

  delete sD; 
}



void DOMParser::processInfraredDistanceSensorTag(
    DOMNode *irDistNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound,
    SingleSegment *segment)
{
  SensorStruct s;
  IrSensorStruct iS;

  initialiseSensorStruct(&s);
  initialiseInfraredDistanceSensor(&iS);

  DOMNodeList *children = irDistNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(iS.initStartPos));
      iS.initStartPos.x += segment->init_position.x;
      iS.initStartPos.y += segment->init_position.y;
      iS.initStartPos.z += segment->init_position.z;
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(iS.direction));
    }

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(iS.startRange), &(iS.endRange));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(iS.startValue), &(iS.endValue));
    }

    if(strcmp(name, OPENING_ANGLE_TAG) == 0)
    {
      processOpeningAngleTag(node, &(iS.spreadAngleX), &(iS.spreadAngleY));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }
  }

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_IR_SENSOR;

  //  printSensorStructInformation(s);
  //  printInfraredSensorInformation(iS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(irDistNode, sD);
  sD->setIrSensor(iS);
  robotDescription->addSensor(sD);

  delete sD;
}

void DOMParser::processGenericRotationSensorTag(
    DOMNode *gRotNode,
    RobotDescription *robotDescription,
    CompoundDescription *compound,
    SingleSegment *segment)
{
  SensorStruct s;
  GenericRotationSensorStruct gRS;

  initialiseSensorStruct(&s);
  initialiseGenericRotationSensor(&gRS);

  // attribute stuff
  //
  DOMNamedNodeMap *attributeNode  = gRotNode->getAttributes();
  DOMNode *attributeType = 0;

  attributeType = 
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));

  if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        PITCH_ROLL_YAW_ATTRIBUTE) == 0)
  {
    gRS.type            = G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW;
    gRS.orderDerivative = G_ROT_SENSOR_ABSOLUTE;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        PITCH_ROLL_YAW_VEL_ATTRIBUTE) == 0)
  {
    gRS.type            = G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW;
    gRS.orderDerivative = G_ROT_SENSOR_DIFFERENTIAL;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        PITCH_ROLL_YAW_ACCEL_ATTRIBUTE) == 0)
  {
    gRS.type            = G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW;
    gRS.orderDerivative = G_ROT_SENSOR_ACCELERATION;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        VECTOR_ANGLE_ATTRIBUTE) == 0)
  {
    gRS.type            = G_ROT_SENSOR_TYPE_VECTOR_ANGLE;
    gRS.orderDerivative = G_ROT_SENSOR_ABSOLUTE;
  }
  else if(strcmp(XMLString::transcode(attributeType->getNodeValue()),
        UNIT_SPHERE_XYZ_ATTRIBUTE) == 0)
  {
    gRS.type            = G_ROT_SENSOR_TYPE_UNIT_SPHERE_XYZ;
    gRS.orderDerivative = G_ROT_SENSOR_ABSOLUTE;
  }
  else
  {
    gRS.type            = G_ROT_SENSOR_TYPE_UNDEFINED;
    gRS.orderDerivative = G_ROT_SENSOR_ABSOLUTE;
  }

  // tag stuff
  //
  DOMNodeList *children = gRotNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANGE_TAG) == 0)
    {
      processSensorRangeTag(node, &(gRS.startRange), &(gRS.endRange));
    }

    if(strcmp(name, MAPPING_TAG) == 0)
    {
      processMappingNode(node, &(gRS.startValue), &(gRS.endValue));
    }

    if(strcmp(name, NOISE_TAG) == 0)
    {
      processNoiseTag(node, &(s.noise), &(s.noiseType));
    }

    if(strcmp(name, COORDINATE_TAG) == 0)
    {
      extractCoordinate(node, &(gRS.initStartPos));
      gRS.initStartPos.x += segment->init_position.x;
      gRS.initStartPos.y += segment->init_position.y;
      gRS.initStartPos.z += segment->init_position.z;
    }

    if(strcmp(name, ORIENTATION_TAG) == 0)
    {
      extractOrientation(node, &(gRS.rotation));
    }

    if(strcmp(name, ROTATION_RAY_TAG) == 0)
    {
      processRayPropTag(node, &(gRS.drawRayRot), &(gRS.lengthRayRot),
          &(gRS.rayRotType), &(gRS.rayRotColor));
    }
    if(strcmp(name, REFERENCE_RAY_TAG) == 0)
    {
      processRayPropTag(node, &(gRS.drawRayRef), &(gRS.lengthRayRef),
          &(gRS.rayRefType), &(gRS.rayRefColor));
    }
  }
  // TODO: process genericRotationSEnsor type attribute !!!

  s.srcCompound = robotDescription->getCompoundNumber();
  s.srcSeg   = compound->segments.size();
  s.type     = SD_GEN_ROTATION_SENSOR;

    printSensorStructInformation(s);
    printGenericRotationSensorInformation(gRS);

  SensorDescription *sD = new SensorDescription(s);
  processMinimalSensorInformation(gRotNode, sD);
  sD->setGenericRotationSensor(gRS);
  robotDescription->addSensor(sD);

  delete sD;
}

void DOMParser::processRayPropTag(DOMNode *rayPropNode, bool *drawRay, double
    *lengthRay, int *rayType, Color *rayColor)
{
  // attribute stuff
  //
  DOMNamedNodeMap *attributeNode  = rayPropNode->getAttributes();
  DOMNode *attributeDrawing = 0;

  attributeDrawing = 
    attributeNode->getNamedItem(XMLString::transcode(DRAWING_ATTRIBUTE));

  if(strcmp(XMLString::transcode(attributeDrawing->getNodeValue()),
        TRUE_ATTRIBUTE) == 0)
  {
    *drawRay = true;
  }
  else
  {
    *drawRay = false;
  }

  // tag stuff
  //
  DOMNodeList *children = rayPropNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, LENGTH_TAG) == 0)
    {
      processRayLengthTag(node, lengthRay, rayType);
    }
    else if(strcmp(name, COLOR_TAG) == 0)
    {
      extractColor(node, rayColor);
    }
  }
}

void DOMParser::processRayLengthTag(DOMNode *rayLengthNode, double *lengthRay,
    int *rayType)
{
  // only attributes, no child nodes
  //
  DOMNamedNodeMap *attributeNode  = rayLengthNode->getAttributes();
  DOMNode *attributeValue = 0;
  DOMNode *attributeMode  = 0;

  attributeValue = attributeNode->getNamedItem(
      XMLString::transcode(VALUE_ATTRIBUTE));

  *lengthRay = atof(XMLString::transcode(attributeValue->getNodeValue()));

  attributeMode = 
    attributeNode->getNamedItem(XMLString::transcode(MODE_ATTRIBUTE));

  if(strcmp(XMLString::transcode(attributeMode->getNodeValue()),
        CONSTANT_ATTRIBUTE) == 0)
  {
    *rayType = G_ROT_SENSOR_LENGTH_CONSTANT;
  }
  else if(strcmp(XMLString::transcode(attributeMode->getNodeValue()),
        PROPORTIONAL_ATTRIBUTE) == 0)
  {
    *rayType = G_ROT_SENSOR_LENGTH_PROPORTIONAL;
  }
}

void DOMParser::processNoiseTag(DOMNode *noiseNode,
    double *noise, int *noiseType)
{
  DOMNamedNodeMap *attributeNode = noiseNode->getAttributes();
  DOMNode *noiseRangeAttribute = 0;
  DOMNode *noiseTypeAttribute  = 0;
  char *noiseTypeSting = 0;

  noiseTypeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(TYPE_ATTRIBUTE));

  noiseRangeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(RANGE_ATTRIBUTE));

  *noise = atof(XMLString::transcode(noiseRangeAttribute->getNodeValue()));

  noiseTypeSting = XMLString::transcode(noiseTypeAttribute->getNodeValue());

  if(strcmp(noiseTypeSting, NOISE_TYPE_GAUSS) == 0)
  {
    *noiseType = CN_NOISE_GAUSS;
  }

}

void DOMParser::processInternalFiltersTag(DOMNode *internalFiltersNode,
    ConnectorDescription *connector)
{
  DOMNodeList *children = internalFiltersNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, FORCE_FEEDBACK_FILTER_TAG) == 0)
    {
      processFilterNode(node, &(connector->forceFeedbackFilter));

    }
    else if(strcmp(name, STOP_TORQUE_ACCU_FILTER_TAG) == 0)
    {
      processFilterNode(node, &(connector->stopTorqueAccuFilter));
    }
  }
}

void DOMParser::processFilterNode(DOMNode *filterNode, FilterDescription *fd)
{
  DOMNodeList *children = filterNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MOVING_AVERAGE_FILTER_TAG) == 0)
    {
      processMovingAverageFilterNode(node, fd);

    }
  }
}

void DOMParser::processMovingAverageFilterNode(DOMNode *movingAverageFilterNode,
    FilterDescription *fd)
{
  DOMNamedNodeMap *attributeNode = movingAverageFilterNode->getAttributes();
  DOMNode *windowSizeAttribute = 0;
  DOMNode *initValueAttribute  = 0;
  DOMNode *recursiveAttribute  = 0;
  DOMNode *fullRecalcAttribute = 0;

  fd->type = DEF_FILTER_TYPE_MOVING_AVERAGE;

  windowSizeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(WINDOW_SIZE_ATTRIBUTE));

  initValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(INIT_VALUE_ATTRIBUTE));

  recursiveAttribute = attributeNode->getNamedItem(
      XMLString::transcode(RECURSIVE_ATTRIBUTE));

  fullRecalcAttribute = attributeNode->getNamedItem(
      XMLString::transcode(FULL_RECALC_ATTRIBUTE));

  fd->windowSize = atoi(XMLString::transcode(
        windowSizeAttribute->getNodeValue()));

  if(initValueAttribute != 0)
  {
    fd->initValue = atof(XMLString::transcode(
          initValueAttribute->getNodeValue()));
  }

  if(recursiveAttribute != 0)
  {
    if(strcmp(TRUE_ATTRIBUTE,
          XMLString::transcode(recursiveAttribute->getNodeValue())) == 0)
    {
      fd->recursive = true;
    }
  }

  if(fullRecalcAttribute != 0)
  {
    fd->fullRecalc = atoi(XMLString::transcode(
          fullRecalcAttribute->getNodeValue()));
  }
}

void DOMParser::processDamperNode(DOMNode *damperNode, 
    ConnectorDescription *connector)
{
  DOMNamedNodeMap *attributeNode = damperNode->getAttributes();
  DOMNode *dampingConstantAttribute = 0;

  connector->damper.active = true;

  dampingConstantAttribute = attributeNode->getNamedItem(
      XMLString::transcode(CONSTANT_ATTRIBUTE));

  connector->damper.dampingConstant = atof(XMLString::transcode(
        dampingConstantAttribute->getNodeValue()));
}

void DOMParser::processJointFrictionNode(DOMNode *jointFrictionNode, 
    JointFrictionDescription *jFD)
{
  DOMNodeList *children = jointFrictionNode->getChildNodes();
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::processJointFrictionNode");

  jFD->active = true;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, STATIC_TAG) == 0)
    {
      jFD->staticActive = true;
      processStaticJointFrictionNode(node, jFD);
    }
    else if(strcmp(name, DYNAMIC_TAG) == 0)
    {
      jFD->dynamicActive = true;
      processDynamicJointFrictionNode(node, jFD);
    }
  }
}

void DOMParser::processStaticJointFrictionNode(DOMNode *staticJointFrictionNode,
    JointFrictionDescription *jFD)
{
  DOMNamedNodeMap *attributeNode = staticJointFrictionNode->getAttributes();
  DOMNode *maxForceValueAttribute      = 0;
  DOMNode *velThresholdValueAttribute  = 0;

  maxForceValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(MAX_FORCE_ATTRIBUTE));

  velThresholdValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VEL_THRESHOLD_ATTRIBUTE));

  jFD->staticMaxForce = atof(XMLString::transcode(
        maxForceValueAttribute->getNodeValue()));

  if(velThresholdValueAttribute != NULL)
  {
    jFD->staticVelThres = atof(XMLString::transcode(
          velThresholdValueAttribute->getNodeValue()));
  }
}

void DOMParser::processDynamicJointFrictionNode(DOMNode *dynamicJointFrictionNode, 
    JointFrictionDescription *jFD)
{
  DOMNamedNodeMap *attributeNode       = dynamicJointFrictionNode->getAttributes();
  DOMNode *constantValueAttribute      = 0;
  DOMNode *velDependencyValueAttribute = 0;
  DOMNode *offsetValueAttribute        = 0;
  DOMNode *minVelValueAttribute        = 0;
  const char* tmpString                = NULL;

  constantValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(CONSTANT_ATTRIBUTE));

  velDependencyValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(VEL_DEPENDENCY_ATTRIBUTE));

  offsetValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(OFFSET_ATTRIBUTE));

  minVelValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(MIN_VEL_ATTRIBUTE));

  jFD->dynamicConstant = atof(XMLString::transcode(
        constantValueAttribute->getNodeValue()));

  tmpString = XMLString::transcode(velDependencyValueAttribute->getNodeValue());
  if(strcmp(tmpString, CONSTANT) == 0)
  {
    jFD->dynamicType = DEF_DYNAMIC_FRICTION_TYPE_CONSTANT;
  }
  else if(strcmp(tmpString, LINEAR) == 0)
  {
    jFD->dynamicType = DEF_DYNAMIC_FRICTION_TYPE_LINEAR;
  }
  else if(strcmp(tmpString, QUADRATIC) == 0)
  {
    jFD->dynamicType = DEF_DYNAMIC_FRICTION_TYPE_QUADRATIC;
  }

  if(minVelValueAttribute != NULL)
  {
    jFD->minVelActive  = true;
    jFD->dynamicMinVel = atof(XMLString::transcode(
          minVelValueAttribute->getNodeValue()));
  }

  if(offsetValueAttribute != NULL)
  {
    jFD->dynamicOffset = atof(XMLString::transcode(
          offsetValueAttribute->getNodeValue()));
  }
}


void DOMParser::processMappingNode(DOMNode *mappingNode, 
    double *startValue, double *endValue)
{
  DOMNamedNodeMap *attributeNode = mappingNode->getAttributes();
  DOMNode *startValueAttribute = 0;
  DOMNode *endValueAttribute   = 0;

  startValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(MIN_ATTRIBUTE));

  endValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(MAX_ATTRIBUTE));

  *startValue = 
    atof(XMLString::transcode(startValueAttribute->getNodeValue()));

  *endValue = 
    atof(XMLString::transcode(endValueAttribute->getNodeValue()));
}

void DOMParser::processAngleNode(DOMNode *mappingNode,GLfloat *angle)
{
  DOMNamedNodeMap *attributeNode = mappingNode->getAttributes();
  DOMNode *angleValueAttribute = 0;

  angleValueAttribute = attributeNode->getNamedItem(
      XMLString::transcode(ALPHA_ATTRIBUTE));

  *angle = 
    atof(XMLString::transcode(angleValueAttribute->getNodeValue()));
}

void DOMParser::processSensorValuesNode(DOMNode *sensorValuesNode, int *xPixel, int *yPixel)
{
  DOMNamedNodeMap *attributeNode = sensorValuesNode->getAttributes();
  DOMNode *sensorValueXAttribute = 0;
  DOMNode *sensorValueYAttribute = 0;

  sensorValueXAttribute = attributeNode->getNamedItem(
      XMLString::transcode(X_ATTRIBUTE));

  sensorValueYAttribute = attributeNode->getNamedItem(
      XMLString::transcode(Y_ATTRIBUTE));

  *xPixel =
    atoi(XMLString::transcode(sensorValueXAttribute->getNodeValue()));

  *yPixel =
    atoi(XMLString::transcode(sensorValueYAttribute->getNodeValue()));
}

void DOMParser::processLightTypeNode(DOMNode *imagePortionNode, int *light)
{
  DOMNamedNodeMap *attributeNode = imagePortionNode->getAttributes();
  DOMNode *lightTypeAttribute = 0;

  lightTypeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(TYPE_ATTRIBUTE));

  if(strcmp(XMLString::transcode(lightTypeAttribute->getNodeValue()),"REAL")==0)
  {
    *light = LIGHT_MODE_REAL;
  }
  if(strcmp(XMLString::transcode(lightTypeAttribute->getNodeValue()),"ARTIFICIAL")==0)
  {
    *light = LIGHT_MODE_ARTIFICIAL;
  }
}

void DOMParser::processOpeningAngleTag(DOMNode *openingAngleNode,
    double *spreadAngleX, double *spreadAngleY)
{
  DOMNamedNodeMap *attributeNode = openingAngleNode->getAttributes();
  DOMNode *spreadAngleXAttribute = 0;
  DOMNode *spreadAngleYAttribute = 0;

  spreadAngleXAttribute = attributeNode->getNamedItem(
      XMLString::transcode(X_ATTRIBUTE));

  spreadAngleYAttribute = attributeNode->getNamedItem(
      XMLString::transcode(Y_ATTRIBUTE));

  *spreadAngleX =
    atof(XMLString::transcode(spreadAngleXAttribute->getNodeValue()));

  *spreadAngleY =
    atof(XMLString::transcode(spreadAngleYAttribute->getNodeValue()));
}

void DOMParser::processColorTypeNode(DOMNode *colorTypeNode ,int *colorType)
{
  DOMNamedNodeMap *attributeNode = colorTypeNode->getAttributes();
  DOMNode *colorTypeAttribute = 0;

  colorTypeAttribute = attributeNode->getNamedItem(
      XMLString::transcode(TYPE_ATTRIBUTE));
  if(strcmp(XMLString::transcode(colorTypeAttribute->getNodeValue()),"RGB")==0)
  {
    *colorType = CAMERA_CHANNEL_RGB;
  }
  if(strcmp((XMLString::transcode(colorTypeAttribute->getNodeValue())), "greyscale")==0)
  {
    *colorType = CAMERA_CHANNEL_GREYSCALE;
  }
  if(strcmp(XMLString::transcode(colorTypeAttribute->getNodeValue()),"RED")==0)
  {
    *colorType = CAMERA_CHANNEL_RED;
  }
  if(strcmp(XMLString::transcode(colorTypeAttribute->getNodeValue()),"GREEN")==0)
  {
    *colorType = CAMERA_CHANNEL_GREEN;
  }
  if(strcmp(XMLString::transcode(colorTypeAttribute->getNodeValue()),"BLUE")==0)
  {
    *colorType = CAMERA_CHANNEL_BLUE;
  }
}

void DOMParser::processSensorRangeTag(DOMNode *sensorRangeTag, 
    double *min,
    double *max)
{
  DOMNamedNodeMap *attributeNode  = sensorRangeTag->getAttributes();
  DOMNode *maxAttribute = 0;
  DOMNode *minAttribute = 0;

  if ( min != NULL)
  {
    minAttribute = attributeNode->getNamedItem(
        XMLString::transcode(MIN_ATTRIBUTE));
    *min = atof(XMLString::transcode(minAttribute->getNodeValue()));
    //printf("DMin: %f", *min);
  }

  if ( max != NULL)
  {
    maxAttribute = attributeNode->getNamedItem(
        XMLString::transcode(MAX_ATTRIBUTE));
    *max = atof(XMLString::transcode(maxAttribute->getNodeValue()));
  }
}

void DOMParser::extractDimension(DOMNode *dimensionNode, Position *dimensions)
{
  DOMNamedNodeMap *attributeNode  = dimensionNode->getAttributes();
  DOMNode *attributeNodeH = 0;
  DOMNode *attributeNodeW = 0;
  DOMNode *attributeNodeL = 0;
  DOMNode *attributeNodeR = 0;

  attributeNodeH =
    attributeNode->getNamedItem(XMLString::transcode(DIMENSION_HEIGHT_ATTRIBUTE));
  attributeNodeW =
    attributeNode->getNamedItem(XMLString::transcode(DIMENSION_WIDTH_ATTRIBUTE));
  attributeNodeL =
    attributeNode->getNamedItem(XMLString::transcode(DIMENSION_DEPTH_ATTRIBUTE));
  attributeNodeR =
    attributeNode->getNamedItem(XMLString::transcode(DIMENSION_RADIUS_ATTRIBUTE));

  if(attributeNodeL != 0)
  {
    dimensions->x = atof(XMLString::transcode(attributeNodeL->getNodeValue()));
    dimensions->y = atof(XMLString::transcode(attributeNodeW->getNodeValue()));
    dimensions->z = atof(XMLString::transcode(attributeNodeH->getNodeValue()));
  }

  if(attributeNodeR != 0)
  {
    dimensions->radius = atof(XMLString::transcode(attributeNodeR->getNodeValue()));
  }

  if(attributeNodeH != 0)
  {
    // for capped cylinder
    dimensions->length = atof(XMLString::transcode(attributeNodeH->getNodeValue()));
  }

}

void DOMParser::extractGraphicalRepresentation(DOMNode *grNode,
    SingleSegment *segment)
{
  Y_DEBUG("DOMNode::extractGraphicalRepresentation");
  DOMNodeList *children = grNode->getChildNodes();
  DOMNode *node = 0;
  char    *name = 0;

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());
    if(strcmp(name, COLOR_TAG) == 0)
    {
      extractColor(node, &(segment->color));
    }
  }
  Y_DEBUG("DOMNode::extractGraphicalRepresentation done");
}

void DOMParser::extractColor(DOMNode *colorNode, Color *color)
{
  Y_DEBUG("DOMNode::extractColor start");
  DOMNamedNodeMap *attributeNode  = colorNode->getAttributes();
  DOMNode *attributeNodeR = 0;
  DOMNode *attributeNodeG = 0;
  DOMNode *attributeNodeB = 0;
  DOMNode *alphaAttribute = 0;

  attributeNodeR =
    attributeNode->getNamedItem(XMLString::transcode(COLOR_RGB_R_ATTRIBUTE));
  attributeNodeG =
    attributeNode->getNamedItem(XMLString::transcode(COLOR_RGB_G_ATTRIBUTE));
  attributeNodeB =
    attributeNode->getNamedItem(XMLString::transcode(COLOR_RGB_B_ATTRIBUTE));

  alphaAttribute =
    attributeNode->getNamedItem(XMLString::transcode(COLOR_ALPHA_ATTRIBUTE));

  color->r = atof(XMLString::transcode(attributeNodeR->getNodeValue())) / 255.0;
  color->g = atof(XMLString::transcode(attributeNodeG->getNodeValue())) / 255.0;
  color->b = atof(XMLString::transcode(attributeNodeB->getNodeValue())) / 255.0;

  if(alphaAttribute == NULL)
  {
    color->alpha = 1;
  }
  else
  {
    color->alpha = atof(XMLString::transcode(alphaAttribute->getNodeValue()));
  }
  Y_DEBUG("DOMNode::extractColor end");
}

void DOMParser::extractBumpable(DOMNode *bumpableNode, bool *isBumpable)
{
  *isBumpable = true;
}

void DOMParser::extractTraceContact(DOMNode *traceContactNode, bool *traceContact)
{
  Y_DEBUG("DOMNode::extractTraceContact: found");
  *traceContact = true;
}


void DOMParser::extractCoordinate(DOMNode *coordinateNode, Position *position)
{
  DOMNamedNodeMap *attributeNode  = coordinateNode->getAttributes();
  DOMNodeList *children = coordinateNode->getChildNodes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;
  DOMNode *attributeNodeZ = 0;
  DOMNode     *node = 0;
  char        *name = 0;

  Y_DEBUG("DOMParser::extractCoordinate");

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  position->x = atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  position->y = atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  position->z = atof(XMLString::transcode(attributeNodeZ->getNodeValue()));

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANDOMISE_LIST_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractCoordinate: found RANDOMISE_LIST_TAG");
      extractRandomiseList(node, &(position->randomise));
    }

    if(strcmp(name, RANDOMISE_INTERVAL_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractCoordinate: found RANDOMISE_INTERVAL_TAG");
      extractRandomiseInterval(node, &(position->randomise));
    }
  }

}

void DOMParser::extractRandomiseInterval(DOMNode *randomiseIntervalNode,
    Randomise *randomise)
{
  Y_DEBUG("DOMParser::extractRandomiseInterval");
  DOMNamedNodeMap *attributeNode  = randomiseIntervalNode->getAttributes();
  DOMNode *attributeType    = 0;
  char    *attributeString;
  DOMNode *node             = 0;
  char    *name             = 0;
  DOMNodeList *children = randomiseIntervalNode->getChildNodes();

  attributeType =
    attributeNode->getNamedItem(XMLString::transcode(METHOD_ATTRIBUTE));
  attributeString = XMLString::transcode(attributeType->getNodeValue());

  randomise->type          = RD_RANDOMISE_TYPE_INTERVAL;
  randomise->enabled       = true;

  if(strcmp(attributeString, RANDOMISE_METHOD_ADDITIVE) == 0)
  {
    randomise->method = RD_RANDOMISE_METHOD_ADDITIVE;
  }
  if(strcmp(attributeString, RANDOMISE_METHOD_GLOBAL) == 0)
  {
    randomise->method = RD_RANDOMISE_METHOD_GLOBAL;
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, VARIANCE_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractRandomiseInterval: found VARIANCE_TAG");
      extractP3D(node, &(randomise->variance));
    }

    if(strcmp(name, PROBABILITY_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractRandomiseInterval: found PROBABILITY_TAG");
      extractP3D(node, &(randomise->probability));
    }

    if(strcmp(name, INTERVAL_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractRandomiseInterval: found INTERVAL_TAG");
      extractIntervall(node, &(randomise->min), &(randomise->max));
    }
  }
}

void DOMParser::extractIntervall(DOMNode *intervalNode, P3D *min, P3D *max)
{
  DOMNode     *node     = 0;
  char        *name     = 0;
  DOMNodeList *children = intervalNode->getChildNodes();

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, MIN_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractIntervall: found MIN_TAG");
      extractP3D(node, min);
    }
    if(strcmp(name, MAX_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractIntervall: found MAX_TAG");
      extractP3D(node, max);
    }
  }
}

void DOMParser::extractP3D(DOMNode *p3dNode, P3D *p3d)
{
  DOMNamedNodeMap *attributeNode  = p3dNode->getAttributes();
  DOMNode *attributeNodeX = 0;
  DOMNode *attributeNodeY = 0;
  DOMNode *attributeNodeZ = 0;

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(X_ATTRIBUTE));
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(Y_ATTRIBUTE));
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(Z_ATTRIBUTE));

  p3d->x = atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  p3d->y = atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  p3d->z = atof(XMLString::transcode(attributeNodeZ->getNodeValue()));
}

void DOMParser::extractRandomiseList(DOMNode *randomiseListNode, Randomise
    *randomise)
{
  DOMNode     *node     = 0;
  char        *name     = 0;
  DOMNodeList *children = randomiseListNode->getChildNodes();
  DOMNamedNodeMap *attributeNode  = randomiseListNode->getAttributes();
  DOMNode *attributeNodeMethod = 0;
  DOMNode *attributeNodeProbability = 0;

  Y_DEBUG("DOMParser::extractRandomiseList");
  randomise->type    = RD_RANDOMISE_TYPE_LIST;
  randomise->enabled = true;

  attributeNodeMethod =
    attributeNode->getNamedItem(XMLString::transcode(METHOD_ATTRIBUTE));

  attributeNodeProbability =
    attributeNode->getNamedItem(XMLString::transcode(PROBABILITY_ATTRIBUTE));


  char *method      = XMLString::transcode(attributeNodeMethod->getNodeValue());
  float probability = atof(
      XMLString::transcode(attributeNodeProbability->getNodeValue()));

  Y_DEBUG("DOMParser::extractRandomiseList: setting probability to %f",
      probability);

  randomise->probability.x = probability;

  if(strcmp(method,RANDOMISE_METHOD_GLOBAL) == 0)
  {
    Y_DEBUG("DOMParser::extractRandomiseList: setting to GLOBAL");
    randomise->method = RD_RANDOMISE_METHOD_GLOBAL;
  }
  if(strcmp(method,RANDOMISE_METHOD_ADDITIVE) == 0)
  {
    Y_DEBUG("DOMParser::extractRandomiseList: setting to ADDITIVE");
    randomise->method = RD_RANDOMISE_METHOD_ADDITIVE;
  }

  randomise->entries.clear();

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, ENTRY_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractRandomiseList: found ENTRY_TAG");
      P3D p3d;
      extractP3D(node, &(p3d));
      randomise->entries.push_back(p3d);
    }
  }
}

void DOMParser::extractOrientation(DOMNode *orientationNode, Position
    *rotation)
{
  DOMNamedNodeMap *attributeNode            = orientationNode->getAttributes();
  DOMNode         *attributeNodeX           = 0;
  DOMNode         *attributeNodeY           = 0;
  DOMNode         *attributeNodeZ           = 0;
  DOMNode         *attributeNodeDegreeOrRad = 0;
  DOMNodeList     *children                 = orientationNode->getChildNodes();
  DOMNode         *node                     = 0;
  char            *name                     = 0;

  Y_DEBUG("DOMParser::extractOrientation");

  attributeNodeX =
    attributeNode->getNamedItem(XMLString::transcode(ALPHA_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractOrientation alpha");
  attributeNodeY =
    attributeNode->getNamedItem(XMLString::transcode(BETA_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractOrientation beta");
  attributeNodeZ =
    attributeNode->getNamedItem(XMLString::transcode(GAMMA_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractOrientation gamma");
  attributeNodeDegreeOrRad =
    attributeNode->getNamedItem(XMLString::transcode(TYPE_ATTRIBUTE));
  Y_DEBUG("DOMParser::extractOrientation type");


  rotation->x = atof(XMLString::transcode(attributeNodeX->getNodeValue()));
  Y_DEBUG("DOMParser::extractOrientation x");
  rotation->y = atof(XMLString::transcode(attributeNodeY->getNodeValue()));
  Y_DEBUG("DOMParser::extractOrientation y");
  rotation->z = atof(XMLString::transcode(attributeNodeZ->getNodeValue()));
  Y_DEBUG("DOMParser::extractOrientation z");

  if(attributeNodeDegreeOrRad != NULL)
  {
    if(strcmp(
          XMLString::transcode(attributeNodeDegreeOrRad->getNodeValue()),
          DEGREE_ATTRIBUTE) == 0)
    {
      Y_DEBUG("converting from degree to rad");
      Y_DEBUG("(x,y,z) = (%f,%f,%f)",
          rotation->x, rotation->y, rotation->z);
      rotation->x = DEG_TO_RAD(rotation->x);
      rotation->y = DEG_TO_RAD(rotation->y);
      rotation->z = DEG_TO_RAD(rotation->z);
      Y_DEBUG("(x,y,z) = (%f,%f,%f)",
          rotation->x, rotation->y, rotation->z);
    }
  }

  for(uint i=0; i < children->getLength(); i++)
  {
    node = children->item(i);
    name = XMLString::transcode(node->getNodeName());

    if(strcmp(name, RANDOMISE_LIST_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractOrientation: found RANDOMISE_LIST_TAG");
      extractRandomiseList(node, &(rotation->randomise));
    }

    if(strcmp(name, RANDOMISE_INTERVAL_TAG) == 0)
    {
      Y_DEBUG("DOMParser::extractOrientation: found RANDOMISE_INTERVAL_TAG");
      extractRandomiseInterval(node, &(rotation->randomise));
    }
  }

  Y_DEBUG("DOMParser::extractOrientation done");

}



void DOMParser::printAllNodes(DOMNode *rootNode)
{

  // start with root node
  Y_DEBUG("Root Node");
  Y_DEBUG("Name     :%s", XMLString::transcode(rootNode->getNodeName()));
  Y_DEBUG("NodeType :%d", rootNode->getNodeType());
  Y_DEBUG("Value    :%s", XMLString::transcode(rootNode->getNodeValue()));
  Y_DEBUG("hasChildNodes    :%d", rootNode->hasChildNodes());

  DOMNodeList *children = rootNode->getChildNodes();
  printNodeList(children, 0);

}

void DOMParser::printNodeList(DOMNodeList *nodeList, int treedepth)
{
  DOMNode *node = 0;
  char prefix[1024];
  for(uint i=0; i < nodeList->getLength(); i++)
  {
    node = nodeList->item(i);
    sprintf(prefix,"%s","");
    for(int j=0; j < treedepth; j++)
    {
      sprintf(prefix," %s", prefix);
    }
    Y_DEBUG("%sChild %d: %s", prefix, i, XMLString::transcode(node->getNodeName()));
    if(node->hasAttributes())
    {
      for(uint k=0; k < node->getAttributes()->getLength(); k++)
      {
        DOMNode *attribute = node->getAttributes()->item(k);
        sprintf(prefix,"%s","");
        for(int l=0; l < treedepth; l++)
        {
          sprintf(prefix," %s", prefix);
        }
        Y_DEBUG("%sAttribut %d",prefix,k);
        Y_DEBUG("%sName    : %s",prefix,XMLString::transcode(attribute->getNodeName()));
        Y_DEBUG("%sValue    : %s",prefix,XMLString::transcode(attribute->getNodeValue()));
      }
    }
    if(node->hasChildNodes())
    {
      printNodeList(node->getChildNodes(), treedepth+1);
    }
  }
}



void DOMParser::initialiseSingleSegment(SingleSegment *segment)
{
  segment->type                            = RD_BOX_GEOM;
  initPosition(&(segment->init_position));
  initPosition(&(segment->position));
  initPosition(&(segment->rotation));
  initPosition(&(segment->init_rotation));
  segment->dimensions.x                    = 0;
  segment->dimensions.y                    = 0;
  segment->dimensions.z                    = 0;
  segment->dimensions.radius               = 0;
  segment->dimensions.length               = 0;
  segment->color.alpha                     = 1;
  segment->mass                            = 1;
  segment->massGeometry                    = -1;
  segment->massOffset.x                    = 0;
  segment->massOffset.y                    = 0;
  segment->massOffset.z                    = 0;
  segment->massDimension.x                 = 0;
  segment->massDimension.y                 = 0;
  segment->massDimension.z                 = 0;
  segment->massDimension.radius            = 0;
  segment->massDimension.length            = 0;
  segment->massRelativeRotation.x          = 0;
  segment->massRelativeRotation.y          = 0;
  segment->massRelativeRotation.z          = 0;
  segment->rigidBodyType                   = RD_UNDEFINED_GEOM;
  segment->drawRigidBody                   = false;
  segment->cBounce                         = 0;
  segment->cBounceVel                      = 0;
  segment->cSoftErp                        = 0;
  segment->cSoftCFM                        = 0;
  segment->cSlip1                          = 0;
  segment->cSlip2                          = 0;
  segment->contactMode                     = 0;
  segment->isBumpable                      = false;
  segment->traceContact                    = false;
  initTracePoint(&(segment->trace_point));
  initTraceLine(&(segment->trace_line));
  initColor(&(segment->color));
  while(segment->connectors.size() > 0)
  {
    segment->connectors.pop_back();
  }
}

void DOMParser::initTracePoint(TracePointDescription *trace_position)
{
  trace_position->enabled = false;
  trace_position->iterations = 0;
  trace_position->size = 0.1;
  initColor(&(trace_position->color));
}

void DOMParser::initTraceLine(TraceLineDescription *trace_line)
{
  trace_line->enabled = false;
  trace_line->iterations = 0;
  initColor(&(trace_line->color));
}


void DOMParser::initColor(Color *color)
{
  color->r           = 0;
  color->g           = 0;
  color->b           = 0;
  color->alpha       = 1;
}

void DOMParser::initPosition(Position *pos)
{
  pos->x                       = 0;
  pos->y                       = 0;
  pos->z                       = 0;
  pos->radius                  = 0;
  pos->length                  = 0;
  pos->randomise.enabled       = 0;
  pos->randomise.type          = 0;
  pos->randomise.method        = 0;
  pos->randomise.last_index    = 0;

  pos->randomise.min.x         = 0;
  pos->randomise.min.y         = 0;
  pos->randomise.min.z         = 0;

  pos->randomise.max.x         = 0;
  pos->randomise.max.y         = 0;
  pos->randomise.max.z         = 0;

  pos->randomise.variance.x    = 0;
  pos->randomise.variance.y    = 0;
  pos->randomise.variance.z    = 0;
  pos->randomise.probability.x = 0;
  pos->randomise.probability.y = 0;
  pos->randomise.probability.z = 0;

  pos->randomise.entries.clear();
}

void DOMParser::printSegment(SingleSegment segment)
{
  Y_DEBUG("****************************************");
#ifdef USE_LOG4CPP_OUTPUT
  logger << log4cpp::Priority::DEBUG << 
               "segment.name              = " << segment.name << 
               log4cpp::CategoryStream::ENDLINE;
#endif
  Y_DEBUG("segment.type              = %d ", segment.type);
  printPosition(segment.position, "segment.position.");
  printPosition(segment.init_position, "segment.init_position.");
  printPosition(segment.rotation, "segment.rotation.");
  printPosition(segment.init_rotation, "segment.init_rotation.");
  Y_DEBUG("segment.dimensions.x           = %f ", segment.dimensions.x);
  Y_DEBUG("segment.dimensions.y           = %f ", segment.dimensions.y);
  Y_DEBUG("segment.dimensions.z           = %f ", segment.dimensions.z);
  Y_DEBUG("segment.dimensions.radius      = %f ", segment.dimensions.radius);
  Y_DEBUG("segment.dimensions.length      = %f ", segment.dimensions.length);
  Y_DEBUG("segment.mass                   = %f ", segment.mass);
  Y_DEBUG("segment.massGeometry           = %d ", segment.massGeometry);
  Y_DEBUG("segment.massOffset.x           = %f ", segment.massOffset.x);
  Y_DEBUG("segment.massOffset.y           = %f ", segment.massOffset.y);
  Y_DEBUG("segment.massOffset.z           = %f ", segment.massOffset.z);
  Y_DEBUG("segment.massDimension.x        = %f ", segment.massDimension.x);
  Y_DEBUG("segment.massDimension.y        = %f ", segment.massDimension.y);
  Y_DEBUG("segment.massDimension.z        = %f ", segment.massDimension.z);
  Y_DEBUG("segment.massDimension.radius   = %f ", segment.massDimension.radius);
  Y_DEBUG("segment.massDimension.length   = %f ", segment.massDimension.length);
  Y_DEBUG("segment.massRelativeRotation.x = %f ", segment.massRelativeRotation.x);
  Y_DEBUG("segment.massRelativeRotation.y = %f ", segment.massRelativeRotation.y);
  Y_DEBUG("segment.massRelativeRotation.z = %f ", segment.massRelativeRotation.z);
  Y_DEBUG("segment - size of geoms vector = %d ", segment.geoms.segments.size());
  Y_DEBUG("segment.rigidBodyType          = %d ", segment.rigidBodyType);
  Y_DEBUG("segment.drawRigidBody (bool)   = %d ", segment.drawRigidBody);
#ifdef USE_LOG4CPP_OUTPUT
  logger << log4cpp::Priority::DEBUG << 
    "segment.rigidBodyName          = " << segment.rigidBodyName <<
    log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
  Y_DEBUG("segment.cBounce                = %f ", segment.cBounce);
  Y_DEBUG("segment.cBounceVel             = %f ", segment.cBounceVel);
  Y_DEBUG("segment.cSoftErp               = %f ", segment.cSoftErp);
  Y_DEBUG("segment.cSoftCFM               = %f ", segment.cSoftCFM);
  Y_DEBUG("segment.cSlip1                 = %f ", segment.cSlip1);
  Y_DEBUG("segment.cSlip2                 = %f ", segment.cSlip2);
  Y_DEBUG("segment.cMu                    = %f ", segment.cMu);
  Y_DEBUG("segment.cMu2                   = %f ", segment.cMu2);
  Y_DEBUG("segment.contactMode            = %d ", segment.contactMode);
  Y_DEBUG("segment.isBumpable             = %d ", segment.isBumpable);
  Y_DEBUG("segment.traceContact           = %d ", segment.traceContact);
  Y_DEBUG("segment.dContactBounce         = %d ",
      (segment.contactMode & dContactBounce) == 1 ? 1: 0);
  Y_DEBUG("segment.dContactSlip1          = %d ",
      (segment.contactMode & dContactSlip1) == 1 ? 1: 0);
  Y_DEBUG("segment.dContactSlip2          = %d ",
      (segment.contactMode & dContactSlip2) == 1 ? 1: 0);
  printTracePoint(segment.trace_point, "segment.trace_point.");
  printColor(segment.color,"segment.");

  Y_DEBUG("****************************************");
}

void DOMParser::printTracePoint(TracePointDescription trace_point, char* prefix)
{
  Y_DEBUG("%senabled      = %d ", prefix, trace_point.enabled);
  printColor(trace_point.color, prefix);
}

void DOMParser::printColor(Color color, char *prefix)
{
  Y_DEBUG("%scolor.r           = %f ", prefix, color.r);
  Y_DEBUG("%scolor.g           = %f ", prefix, color.g);
  Y_DEBUG("%scolor.b           = %f ", prefix, color.b);
  Y_DEBUG("%scolor.alpha       = %f ", prefix, color.alpha);
}




void DOMParser::turnAllSegments(RobotDescription *robotDescription,
    double x, double y, double z)
{
  CompoundDescription *compound;
  SingleSegment *segment;
  ConnectorVector *cv;
  ConnectorDescription *c;
  ConnectorVector *cc;
  SensDesVector *sensorVector;
  SensorDescription *sensorDescription;
  IrSensorStruct *irSensor;
  SharpGP2D12_37_SensorStruct *sIrSensor;
  SharpDM2Y3A003K0F_SensorStruct *sIrDM2Y3A003K0FSensor; 

  LdrSensorStruct *ldrSensor;
  DirectedCameraStruct *cameraSensor;

  for(uint i=0; i<robotDescription->getCompoundNumber(); i++)
  {

    compound = robotDescription->getCompound(i);
    for(uint j=0; j < compound->segments.size(); j++)
    {
      segment = &((*compound).segments[j]);
      rotate(&(segment->init_position), segment->rotation);

      }
  }

}

void DOMParser::initialiseConnector(ConnectorDescription *connector)
{
  string s = "test string";
  connector->name = s;
  connector->sourceCompoundNum = -1;
  connector->aimCompoundNum = -1;
  connector->sourceSegNum = -1;
  connector->aimSegNum = -1;
  connector->fMax = -1;
  connector->velMax = -1;


  connector->type = -1;

  initialisePos(&(connector->anchorPos));
  initialisePos(&(connector->anchorInitPos));

  initialisePos((&connector->rotationAxis));
  initialisePos((&connector->rotationAxis2));
  initialisePos((&connector->initRotationAxis));
  initialisePos((&connector->initRotationAxis2));
  initialisePos((&connector->sliderAxis));
  initialisePos((&connector->initSliderAxis));
  connector->min_deflection                = -dInfinity;
  connector->max_deflection                = dInfinity;
  connector->slowDownBias                  = 0;
  connector->stopBias                      = 0;
  connector->noise                         = 0;
  connector->noise_type                    = CN_NOISE_NONE;

  connector->damper.active                 = false;
  connector->damper.dampingConstant        = 0;

  connector->spring.active                 = false;
  connector->spring.springConstant         = 1;
  connector->spring.initialPosition        = 0;
  connector->spring.noise                  = 0;
  connector->spring.noiseType              = CN_NOISE_NONE;

  connector->isPositionExceedable          = false;
  connector->minExceedPosition             = 0.0;
  connector->maxExceedPosition             = 0.0;
  connector->isForceExceedable             = false;
  connector->minExceedForce                = 0.0;
}

void DOMParser::initialiseComplexHingeConnector(ConnectorDescription *connector)
{
  Y_DEBUG("DOMParser::initialiseComplexHingeConnector");
  connector->name = "";
  connector->startValue2 = 0;
  connector->endValue2 = 0;

  connector->motor.stallTorque             = 0;
  connector->motor.maxContinuousTorque     = 0;
  connector->motor.noLoadSpeed             = 0;
  connector->motor.mechanicalTimeConstant  = 0;
  connector->motor.maxEfficiency           = 1;
  connector->motor.brakeEfficiency         = 1;
  connector->motor.brakeFriction.active          = false;
  connector->motor.brakeFriction.staticActive    = false;
  connector->motor.brakeFriction.dynamicActive   = false;
  connector->motor.brakeFriction.minVelActive    = false;
  connector->motor.brakeFriction.staticMaxForce  = 0.0;
  connector->motor.brakeFriction.staticVelThres  = 0.001;
  connector->motor.brakeFriction.dynamicConstant = 0.0;
  connector->motor.brakeFriction.dynamicType     = DEF_DYNAMIC_FRICTION_TYPE_LINEAR;
  connector->motor.brakeFriction.dynamicMinVel   = 0.0;
  connector->motor.brakeFriction.dynamicOffset   = 0.0;
  connector->motor.torqueCurrentConstant   = 1;
  connector->motor.speedVoltageConstant    = 1;
  connector->motor.pwmThreshold            = 0;
  initialisePWMappingDescriptionStruct(&(connector->motor.pwToStallTorque));
  initialisePWMappingDescriptionStruct(&(connector->motor.pwToStallCurrent));
  initialisePWMappingDescriptionStruct(&(connector->motor.pwToNoLoadSpeed));
  initialisePWMappingDescriptionStruct(&(connector->motor.pwToNoLoadCurrent));
  initialisePWMappingDescriptionStruct(&(connector->motor.pwToBrakeEfficiency));
  connector->motor.noise[0]                = 0;
  connector->motor.noiseType[0]            = CN_NOISE_NONE;
  connector->motor.noise[1]                = 0;
  connector->motor.noiseType[1]            = CN_NOISE_NONE;

  connector->gear.active                   = false;
  connector->gear.transmissionRatio        = 1;
  connector->gear.backlash                 = 0;
  connector->gear.efficiency               = 1;
  connector->gear.initialBacklashPosition  = 0;
  connector->gear.noise                    = 0;
  connector->gear.noiseType                = CN_NOISE_NONE;

  connector->springCoupling.active         = false;
  connector->springCoupling.minTorque      = 0;
  connector->springCoupling.springConstant = 1;
  connector->springCoupling.maxDeflection  = 0;
  connector->springCoupling.noise          = 0;
  connector->springCoupling.noiseType      = CN_NOISE_NONE;

  connector->jointFriction.active          = false;
  connector->jointFriction.staticActive    = false;
  connector->jointFriction.dynamicActive   = false;
  connector->jointFriction.minVelActive    = false;
  connector->jointFriction.staticMaxForce  = 0.0;
  connector->jointFriction.staticVelThres  = 0.001;
  connector->jointFriction.dynamicConstant = 0.0;
  connector->jointFriction.dynamicType     = DEF_DYNAMIC_FRICTION_TYPE_LINEAR;
  connector->jointFriction.dynamicMinVel   = 0.0;
  connector->jointFriction.dynamicOffset   = 0.0;

  connector->forceFeedbackFilter.type        = DEF_FILTER_TYPE_MOVING_AVERAGE;
  connector->forceFeedbackFilter.windowSize  = 5;
  connector->forceFeedbackFilter.initValue   = 0.0;
  connector->forceFeedbackFilter.recursive   = true;
  connector->forceFeedbackFilter.fullRecalc  = 1000;
  connector->stopTorqueAccuFilter.type       = DEF_FILTER_TYPE_MOVING_AVERAGE;
  connector->stopTorqueAccuFilter.windowSize = 4;
  connector->stopTorqueAccuFilter.initValue  = 0.0;
  connector->stopTorqueAccuFilter.recursive  = true;
  connector->stopTorqueAccuFilter.fullRecalc = 1000;
}

void DOMParser::printConnectorData(ConnectorDescription connector)
{

  Y_DEBUG("********** ConnectorDescription Data ********************");
#ifdef USE_LOG4CPP_OUTPUT
  logger << log4cpp::Priority::DEBUG << 
    "connector.name            = " << connector.name <<
    log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
  Y_DEBUG("connector.sourceCompoundNum  = %d",connector.sourceCompoundNum);
  Y_DEBUG("connector.aimCompoundNum     = %d",connector.aimCompoundNum);
  Y_DEBUG("connector.sourceSegNum    = %d",connector.sourceSegNum);
  Y_DEBUG("connector.aimSegNum       = %d",connector.aimSegNum);
  Y_DEBUG("connector.type            = %d",connector.type);
  //
  Y_DEBUG("***** SpringDescription: ");
  Y_DEBUG("connector.spring.active          = %d",connector.spring.active);
  Y_DEBUG("connector.spring.springConstant  = %f",connector.spring.springConstant);
  Y_DEBUG("connector.spring.initialPosition = %f",connector.spring.initialPosition);
  Y_DEBUG("connector.spring.noise           = %f",connector.spring.noise);
  Y_DEBUG("connector.spring.noiseType       = %d",connector.spring.noiseType);
  //
  Y_DEBUG("***** DamperDescription: ");
  Y_DEBUG("connector.damper.active          = %d",connector.damper.active);
  Y_DEBUG("connector.damper.dampingConstant = %f",connector.damper.dampingConstant);
  //
  if(connector.type == RD_CONN_COMPLEX_HINGE)
  {
    printComplexHingeData(connector);
  }
  else
  {
    Y_DEBUG("connector.fMax            = %f",connector.fMax);
    Y_DEBUG("connector.velMax          = %f",connector.velMax);
    Y_DEBUG("connector.min_deflection  = %f",connector.min_deflection);
    Y_DEBUG("connector.max_deflection  = %f",connector.max_deflection);
    Y_DEBUG("connector.startValue      = %f",connector.startValue);
    Y_DEBUG("connector.endValue        = %f",connector.endValue);
    Y_DEBUG("connector.noise           = %f",connector.noise);
    Y_DEBUG("connector.noise_type      = %d",connector.noise_type);
    Y_DEBUG("connector.slowDownBias    = %f",connector.slowDownBias);
    Y_DEBUG("connector.stopBias        = %f",connector.stopBias);
    printPosition(connector.anchorPos,        "connector.anchorPos.");
    printPosition(connector.anchorInitPos,    "connector.anchorInitPos.");
    printPosition(connector.rotationAxis,     "connector.rotationAxis.");
    printPosition(connector.initRotationAxis, "connector.initRotationAxis.");
    printPosition(connector.rotationAxis2,    "connector.rotationAxis2.");
    printPosition(connector.initRotationAxis2, "connector.initRotationAxis2.");
    printPosition(connector.sliderAxis,       "connector.sliderAxis.");
    printPosition(connector.initSliderAxis,   "connector.initSliderAxis.");
  }

  Y_DEBUG("**********************************************");
}

void DOMParser::printComplexHingeData(ConnectorDescription connector)
{
  Y_DEBUG("connector.min_deflection  = %f",connector.min_deflection);
  Y_DEBUG("connector.max_deflection  = %f",connector.max_deflection);
  Y_DEBUG("connector.startValue      = %f",connector.startValue);
  Y_DEBUG("connector.endValue        = %f",connector.endValue);
  Y_DEBUG("connector.startValue2     = %f",connector.startValue2);
  Y_DEBUG("connector.endValue2       = %f",connector.endValue2);
  Y_DEBUG("connector.noise           = %f",connector.noise);
  Y_DEBUG("connector.noise_type      = %d",connector.noise_type);
  printPosition(connector.anchorPos,        "connector.anchorPos.");
  printPosition(connector.anchorInitPos,    "connector.anchorInitPos.");
  printPosition(connector.rotationAxis,     "connector.rotationAxis.");
  printPosition(connector.initRotationAxis, "connector.initRotationAxis.");
  Y_DEBUG("connector.isPositionExcd. = %d", connector.isPositionExceedable);
  Y_DEBUG("connector.minExceedPos    = %f", connector.minExceedPosition);
  Y_DEBUG("connector.maxExceedPos    = %f", connector.maxExceedPosition);
  Y_DEBUG("connector.isForceExceed.  = %d", connector.isForceExceedable);
  Y_DEBUG("connector.minExceedForce  = %f", connector.minExceedForce);
  //
  Y_DEBUG("***** MotorDescription: ");
  Y_DEBUG("connector.motor.stallTorque            = %f",
      connector.motor.stallTorque);
  Y_DEBUG("connector.motor.maxContinuousTorque    = %f",
      connector.motor.maxContinuousTorque);
  Y_DEBUG("connector.motor.noLoadSpeed            = %f",
      connector.motor.noLoadSpeed);
  Y_DEBUG("connector.motor.mechanicalTimeConstant = %f",
      connector.motor.mechanicalTimeConstant);
  Y_DEBUG("connector.motor.maxEfficiency          = %f",
      connector.motor.maxEfficiency);
  Y_DEBUG("connector.motor.brakeEfficiency        = %f",
      connector.motor.brakeEfficiency);
  Y_DEBUG("  ** BrakeFrictionDescription: ");
  Y_DEBUG("  connector.motor.brakeFriction.active          = %d",connector.motor.brakeFriction.active);
  Y_DEBUG("  connector.motor.brakeFriction.staticActive    = %d",connector.motor.brakeFriction.staticActive);
  Y_DEBUG("  connector.motor.brakeFriction.dynamicActive   = %d",connector.motor.brakeFriction.dynamicActive);
  Y_DEBUG("  connector.motor.brakeFriction.minVelActive    = %d",connector.motor.brakeFriction.minVelActive);
  Y_DEBUG("  connector.motor.brakeFriction.staticMaxForce  = %f",connector.motor.brakeFriction.staticMaxForce);
  Y_DEBUG("  connector.motor.brakeFriction.staticVelThres  = %f",connector.motor.brakeFriction.staticVelThres);
  Y_DEBUG("  connector.motor.brakeFriction.dynamicConstant = %f",connector.motor.brakeFriction.dynamicConstant);
  Y_DEBUG("  connector.motor.brakeFriction.dynamicType     = %d",connector.motor.brakeFriction.dynamicType);
  Y_DEBUG("  connector.motor.brakeFriction.dynamicMinVel   = %f",connector.motor.brakeFriction.dynamicMinVel);
  Y_DEBUG("  connector.motor.brakeFriction.dynamicOffset   = %f",connector.motor.brakeFriction.dynamicOffset);
  Y_DEBUG("connector.motor.speedVoltageConstant   = %f",
      connector.motor.speedVoltageConstant);
  Y_DEBUG("connector.motor.torqueCurrentConstant  = %f",
      connector.motor.torqueCurrentConstant);
  Y_DEBUG("connector.motor.pwmThreshold    = %f",connector.motor.pwmThreshold);
  Y_DEBUG("  ** PW Mapping Description: ");
  Y_DEBUG("   * PW To Stall Torque: ");
  printPWMappingDescriptionData(&(connector.motor.pwToStallTorque));
  Y_DEBUG("   * PW To Stall Current: ");
  printPWMappingDescriptionData(&(connector.motor.pwToStallCurrent));
  Y_DEBUG("   * PW To No Load Speed: ");
  printPWMappingDescriptionData(&(connector.motor.pwToNoLoadSpeed));
  Y_DEBUG("   * PW To No Load Current: ");
  printPWMappingDescriptionData(&(connector.motor.pwToNoLoadCurrent));
  Y_DEBUG("   * PW To Brake Efficiency: ");
  printPWMappingDescriptionData(&(connector.motor.pwToBrakeEfficiency));
  Y_DEBUG("connector.motor.noise           = %f",connector.motor.noise);
  Y_DEBUG("connector.motor.noiseType       = %d",connector.motor.noiseType);
  //
  Y_DEBUG("***** GearDescription: ");
  Y_DEBUG("connector.gear.active                  = %d",connector.gear.active);
  Y_DEBUG("connector.gear.transmissionRatio       = %f",connector.gear.transmissionRatio);
  Y_DEBUG("connector.gear.backlash                = %f",connector.gear.backlash);
  Y_DEBUG("connector.gear.efficiency              = %f",connector.gear.efficiency);
  Y_DEBUG("connector.gear.initialBacklashPosition = %f",
                                                       connector.gear.initialBacklashPosition);
  Y_DEBUG("connector.gear.noise                   = %f",connector.gear.noise);
  Y_DEBUG("connector.gear.noiseType               = %d",connector.gear.noiseType);
  //
  Y_DEBUG("***** SpringCouplingDescription: ");
  Y_DEBUG("connector.springCoupling.active         = %d",connector.springCoupling.active);
  Y_DEBUG("connector.springCoupling.minTorque      = %f",connector.springCoupling.minTorque);
  Y_DEBUG("connector.springCoupling.springConstant = %f",
                                                         connector.springCoupling.springConstant);
  Y_DEBUG("connector.springCoupling.maxDeflection  = %f",
                                                          connector.springCoupling.maxDeflection);
  Y_DEBUG("connector.springCoupling.noise          = %f",connector.springCoupling.noise);
  Y_DEBUG("connector.springCoupling.noiseType      = %d",connector.springCoupling.noiseType);
  //
  Y_DEBUG("***** JointFrictionDescription: ");
  Y_DEBUG("connector.jointFriction.active         = %d",connector.jointFriction.active);
  Y_DEBUG("connector.jointFriction.staticActive   = %d",connector.jointFriction.staticActive);
  Y_DEBUG("connector.jointFriction.dynamicActive  = %d",connector.jointFriction.dynamicActive);
  Y_DEBUG("connector.jointFriction.minVelActive   = %d",connector.jointFriction.minVelActive);
  Y_DEBUG("connector.jointFriction.staticMaxForce = %f",connector.jointFriction.staticMaxForce);
  Y_DEBUG("connector.jointFriction.staticVelThres = %f",connector.jointFriction.staticVelThres);
  Y_DEBUG("connector.jointFriction.dynamicConstant= %f",connector.jointFriction.dynamicConstant);
  Y_DEBUG("connector.jointFriction.dynamicType    = %d",connector.jointFriction.dynamicType);
  Y_DEBUG("connector.jointFriction.dynamicMinVel  = %f",connector.jointFriction.dynamicMinVel);
  Y_DEBUG("connector.jointFriction.dynamicOffset  = %f",connector.jointFriction.dynamicOffset);
  Y_DEBUG("***** ForceFeedbackFilterDescription: ");
  Y_DEBUG("connector.forceFeedbackFilter.type       = %d",connector.forceFeedbackFilter.type);
  Y_DEBUG("connector.forceFeedbackFilter.windowSize = %d",connector.forceFeedbackFilter.windowSize);
  Y_DEBUG("connector.forceFeedbackFilter.initValue  = %f",connector.forceFeedbackFilter.initValue);
  Y_DEBUG("connector.forceFeedbackFilter.recursive  = %d",connector.forceFeedbackFilter.recursive);
  Y_DEBUG("connector.forceFeedbackFilter.fullRecalc = %d",connector.forceFeedbackFilter.fullRecalc);
  Y_DEBUG("***** StopTorqueAccuFilterDescription: ");
  Y_DEBUG("connector.stopTorqueAccuFilter.type       = %d",connector.stopTorqueAccuFilter.type);
  Y_DEBUG("connector.stopTorqueAccuFilter.windowSize = %d",connector.stopTorqueAccuFilter.windowSize);
  Y_DEBUG("connector.stopTorqueAccuFilter.initValue  = %f",connector.stopTorqueAccuFilter.initValue);
  Y_DEBUG("connector.stopTorqueAccuFilter.recursive  = %d",connector.stopTorqueAccuFilter.recursive);
  Y_DEBUG("connector.stopTorqueAccuFilter.fullRecalc = %d",connector.stopTorqueAccuFilter.fullRecalc);
}

void DOMParser::printPWMappingDescriptionData(PWMappingDescription *pwMapping)
{
  Y_DEBUG("   type          = %d", pwMapping->type);
  Y_DEBUG("   minActivation = %f", pwMapping->minActivation);
  Y_DEBUG("   maxActivation = %f", pwMapping->maxActivation);
  Y_DEBUG("   xOff          = %f", pwMapping->xOff);
  Y_DEBUG("   yOff          = %f", pwMapping->yOff);
  Y_DEBUG("   constant      = %f", pwMapping->constant);
  Y_DEBUG("   exponent      = %f", pwMapping->exponent);
  if(pwMapping->type == DEF_MAPPING_TYPE_LINEAR_INTERPOLATION)
  {
    printInterpolationDataSets(&(pwMapping->interpolationData));
  }
}

void DOMParser::printInterpolationDataSets(
    vector<struct InterpolationDataPointSet> *interpolationData)
{
  if((*interpolationData).size() < 1)
  {
    Y_DEBUG("   - no interpolation value sets.");
  }
  else
  {
    Y_DEBUG("     Interpolation Value Sets: ");
    for (int i=0; i<(*interpolationData).size(); i++) {
      Y_DEBUG("   value set %i: x = %f, y = %f, slope = %f", i,
          (*interpolationData)[i].x, (*interpolationData)[i].y,
          (*interpolationData)[i].slope);
    }
  }
}

void DOMParser::initialiseLdrSensorStruct(LdrSensorStruct *lS)
{
  lS->pos.x = 0.0;
  lS->pos.y = 0.0;
  lS->pos.z = 0.0;

  lS->spreadAngleX = 0.0;
  lS->spreadAngleY = 0.0;

  lS->startIntensity = 0.0;
  lS->endIntensity   = 0.0;

  lS->startValue = 0.0;
  lS->endValue   = 0.0;
}

void DOMParser::initialiseDirectedCameraStruct(DirectedCameraStruct *dS)
{
  dS->pos.x = 0.0;
  dS->pos.y = 0.0;
  dS->pos.z = 0.0;

  dS->init_pos.x = 0.0;
  dS->init_pos.y = 0.0;
  dS->init_pos.z = 0.0;

  dS->openingAngle = 40.0;

  dS->light = 0;

  dS->direction.x = 0.0;
  dS->direction.y = 0.0;
  dS->direction.z = 0.0;

  dS->startValue = 0.0;
  dS->endValue   = 0.0;

  dS->xPixel = 0;
  dS->yPixel = 0;

  dS->colorType= 0;

}

void DOMParser::initialiseInfraredDistanceSensor(IrSensorStruct *iS)
{
  iS->startPos.x = 0.0;
  iS->startPos.y = 0.0;
  iS->startPos.z = 0.0;

  iS->initStartPos.x = 0.0;
  iS->initStartPos.y = 0.0;
  iS->initStartPos.z = 0.0;


  iS->direction.x = 0.0;
  iS->direction.y = 0.0;
  iS->direction.z = 0.0;

  iS->spreadAngleX = 0.0;
  iS->spreadAngleY = 0.0;

  iS->startValue = 0.0;
  iS->endValue   = 0.0;

  iS->startRange = 0.0;
  iS->endRange   = 0.0;
}

void DOMParser::initialiseGenericRotationSensor(GenericRotationSensorStruct *gRS)
{
  gRS->type              = G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW;
  gRS->orderDerivative   = G_ROT_SENSOR_ABSOLUTE;

  gRS->startRange        = 0.0;
  gRS->endRange          = 1.0;
  gRS->startValue        = 0.0;
  gRS->endValue          = 1.0;

  gRS->startPos.x        = 0.0;
  gRS->startPos.y        = 0.0;
  gRS->startPos.z        = 0.0;

  gRS->initStartPos.x    = 0.0;
  gRS->initStartPos.y    = 0.0;
  gRS->initStartPos.z    = 0.0;

  gRS->rotation.x        = 0.0;
  gRS->rotation.y        = 0.0;
  gRS->rotation.z        = 0.0;

  gRS->drawRayRot        = true;
  gRS->drawRayRef        = true;

  gRS->lengthRayRot      = 1.0;
  gRS->lengthRayRef      = 1.0;

  gRS->rayRotType        = G_ROT_SENSOR_LENGTH_CONSTANT;
  gRS->rayRefType        = G_ROT_SENSOR_LENGTH_CONSTANT;

  gRS->rayRotColor.r     = 255.0;
  gRS->rayRotColor.g     = 0.0;
  gRS->rayRotColor.b     = 0.0;
  gRS->rayRotColor.alpha = 0.0;
  gRS->rayRefColor.r     = 255.0;
  gRS->rayRefColor.g     = 255.0;
  gRS->rayRefColor.b     = 255.0;
  gRS->rayRefColor.alpha = 0.0;
}

void DOMParser::printSharpGP2D12_37InfraredSensorInformation(SharpGP2D12_37_SensorStruct sharpS)
{
  Y_DEBUG("********** InfraredSensor ********************");
  Y_DEBUG("sharpS.startPos.x     = %f",sharpS.startPos.x);
  Y_DEBUG("sharpS.startPos.y     = %f",sharpS.startPos.y);
  Y_DEBUG("sharpS.startPos.z     = %f",sharpS.startPos.z);

  Y_DEBUG("sharpS.initStartPos.x = %f",sharpS.initStartPos.x);
  Y_DEBUG("sharpS.initStartPos.y = %f",sharpS.initStartPos.y);
  Y_DEBUG("sharpS.initStartPos.z = %f",sharpS.initStartPos.z);


  Y_DEBUG("sharpS.direction.x    = %f",sharpS.direction.x);
  Y_DEBUG("sharpS.direction.y    = %f",sharpS.direction.y);
  Y_DEBUG("sharpS.direction.z    = %f",sharpS.direction.z);

  Y_DEBUG("sharpS.startValue     = %f",sharpS.startValue);
  Y_DEBUG("sharpS.endValue       = %f",sharpS.endValue  );
  Y_DEBUG("**********************************************");
}

void DOMParser::printInfraredSensorInformation(IrSensorStruct iS)
{
  Y_DEBUG("********** InfraredSensor ********************");
  Y_DEBUG("iS.startPos.x     = %f",iS.startPos.x);
  Y_DEBUG("iS.startPos.y     = %f",iS.startPos.y);
  Y_DEBUG("iS.startPos.z     = %f",iS.startPos.z);

  Y_DEBUG("iS.initStartPos.x = %f",iS.initStartPos.x);
  Y_DEBUG("iS.initStartPos.y = %f",iS.initStartPos.y);
  Y_DEBUG("iS.initStartPos.z = %f",iS.initStartPos.z);

  Y_DEBUG("iS.direction.x    = %f",iS.direction.x);
  Y_DEBUG("iS.direction.y    = %f",iS.direction.y);
  Y_DEBUG("iS.direction.z    = %f",iS.direction.z);

  Y_DEBUG("iS.spreadAngleX   = %f",iS.spreadAngleX);
  Y_DEBUG("iS.spreadAngleY   = %f",iS.spreadAngleY);

  Y_DEBUG("iS.startValue     = %f",iS.startValue);
  Y_DEBUG("iS.endValue       = %f",iS.endValue  );

  Y_DEBUG("iS.startRange     = %f",iS.startRange);
  Y_DEBUG("iS.endRange       = %f",iS.endRange  );
  Y_DEBUG("**********************************************");
}

void DOMParser::printGenericRotationSensorInformation(
    GenericRotationSensorStruct gRS)
{
  Y_DEBUG("********** GenericRotation Sensor ************");
  Y_DEBUG("gRS.type               = %d",gRS.type);
  Y_DEBUG("gRS.orderDerivative    = %d",gRS.orderDerivative);

  Y_DEBUG("gRS.startRange         = %f",gRS.startRange);
  Y_DEBUG("gRS.endRange           = %f",gRS.endRange  );
  Y_DEBUG("gRS.startValue         = %f",gRS.startValue);
  Y_DEBUG("gRS.endValue           = %f",gRS.endValue  );

  Y_DEBUG("gRS.initStartPos.x     = %f",gRS.initStartPos.x);
  Y_DEBUG("gRS.initStartPos.y     = %f",gRS.initStartPos.y);
  Y_DEBUG("gRS.initStartPos.z     = %f",gRS.initStartPos.z);

  Y_DEBUG("gRS.rotation.x         = %f",gRS.rotation.x);
  Y_DEBUG("gRS.rotation.y         = %f",gRS.rotation.y);
  Y_DEBUG("gRS.rotation.z         = %f",gRS.rotation.z);

  Y_DEBUG("gRS.drawRayRot         = %d",gRS.drawRayRot);
  Y_DEBUG("gRS.drawRayRef         = %d",gRS.drawRayRef);
  Y_DEBUG("gRS.lengthRayRot       = %f",gRS.lengthRayRot);
  Y_DEBUG("gRS.lengthRayRef       = %f",gRS.lengthRayRef);
  Y_DEBUG("gRS.rayRotType         = %d",gRS.rayRotType);
  Y_DEBUG("gRS.rayRefType         = %d",gRS.rayRefType);

  Y_DEBUG("gRS.rayRotColor.r      = %f",gRS.rayRotColor.r);
  Y_DEBUG("gRS.rayRotColor.g      = %f",gRS.rayRotColor.g);
  Y_DEBUG("gRS.rayRotColor.b      = %f",gRS.rayRotColor.b);
  Y_DEBUG("gRS.rayRotColor.alpha  = %f",gRS.rayRotColor.alpha);
  Y_DEBUG("gRS.rayRefColor.r      = %f",gRS.rayRefColor.r);
  Y_DEBUG("gRS.rayRefColor.g      = %f",gRS.rayRefColor.g);
  Y_DEBUG("gRS.rayRefColor.b      = %f",gRS.rayRefColor.b);
  Y_DEBUG("gRS.rayRefColor.alpha  = %f",gRS.rayRefColor.alpha);
  Y_DEBUG("**********************************************");
}

void DOMParser::printSensorStructInformation(SensorStruct s)
{
  Y_DEBUG("s.type     = %d",s.type);
  Y_DEBUG("s.srcCompound = %d",s.srcCompound);
  Y_DEBUG("s.srcSeg   = %d",s.srcSeg);
  Y_DEBUG("s.noise    = %f",s.noise);
  Y_DEBUG("s.noise    = %d",s.noiseType);
}


void DOMParser::initialiseSensorStruct(SensorStruct *s)
{
  s->noise = 0.0;
  s->noiseType = CN_NOISE_NONE;
  s->type = -1;
  s->usage = DEF_SENSOR_USED_AS_CONTROL_INPUT;
  s->srcCompound = -1;
  s->srcSeg = -1;
}

void DOMParser::initialisePWMappingDescriptionStruct(PWMappingDescription
    *pwMapping)
{
  pwMapping->type          = DEF_MAPPING_TYPE_NONE;
  pwMapping->minActivation = DBL_MIN;
  pwMapping->maxActivation = DBL_MAX;
  pwMapping->xOff          = 0.0;
  pwMapping->yOff          = 0.0;
  pwMapping->constant      = 1.0;
  pwMapping->exponent      = 1.0;
  pwMapping->interpolationData.clear();
}

void DOMParser::initialseLightSource(LightSource *lightSource)
{
  initialisePos(&(lightSource->pos));
  lightSource->type      = -1; /* is POINT_LIGHT */
  lightSource->color.r   = 0; /*rgb, just for drawing the source */
  lightSource->color.g   = 0; /*rgb, just for drawing the source */
  lightSource->color.b   = 0; /*rgb, just for drawing the source */
  lightSource->intensity = 0.0;
  lightSource->drawRange = false;
}

void DOMParser::initialisePos(Position *pos)
{
  pos->x      = 0;
  pos->y      = 0;
  pos->z      = 0;
  pos->radius = 0;
  pos->length = 0;
  initialseRandomise(&(pos->randomise));
}

void DOMParser::initialseP3D(P3D *p3d)
{
  p3d->x      = 0;
  p3d->y      = 0;
  p3d->z      = 0;
}

void DOMParser::initialseRandomise(Randomise *randomise)
{
  randomise->enabled     = false;
  randomise->type        = -1;
  randomise->last_index  = -1;
  initialseP3D(&(randomise->min));
  initialseP3D(&(randomise->max));
  initialseP3D(&(randomise->variance));
  initialseP3D(&(randomise->probability));
}

void DOMParser::initialiseRandomiseScalar(RandomiseScalar *randomise)
{
  randomise->enabled     = false;
  randomise->type        = -1;
  randomise->last_index  = -1;
  randomise->min         = 0;
  randomise->max         = 0;
  randomise->variance    = 0;
  randomise->probability = 0;
}


void DOMParser::initialiseAngleSensorStruct(AngleSensorStruct *aS)
{
  aS->connectorNum = -1;
  aS->startValue   = -1;
  aS->endValue     = -1;
  aS->startAngle   = -1;
  aS->endAngle     = -1;
}


void DOMParser::printLdrSensorInformation(LdrSensorStruct lS)
{
  Y_DEBUG("********** Ldr Sensor Information *************");
  Y_DEBUG("lS.pos.x          = %f",lS.pos.x);
  Y_DEBUG("lS.pos.y          = %f",lS.pos.y);
  Y_DEBUG("lS.pos.z          = %f",lS.pos.z);

  Y_DEBUG("lS.startIntensity = %f",lS.startIntensity);
  Y_DEBUG("lS.endIntensity   = %f",lS.endIntensity);

  Y_DEBUG("lS.startValue     = %f",lS.startValue);
  Y_DEBUG("lS.endValue       = %f",lS.endValue  );
  Y_DEBUG("**********************************************");
}

void DOMParser::printAngleSensorStructInformation(AngleSensorStruct aS)
{
  Y_DEBUG("********** Angle Sensor Information ***********");
  Y_DEBUG("aS.connectorNum   = %d",aS.connectorNum);
  Y_DEBUG("aS.startValue     = %f",aS.startValue);
  Y_DEBUG("aS.endValue       = %f",aS.endValue  );
  Y_DEBUG("aS.startAngle     = %f",aS.startAngle);
  Y_DEBUG("aS.endAngle       = %f",aS.endAngle  );
  Y_DEBUG("**********************************************");

}

void DOMParser::printAngleVelocitySensorStructInformation(
    AngleVelSensorStruct avs)
{
  Y_DEBUG("********** Angle Velocity Sensor Information *");
  Y_DEBUG("avs.connectorNum  = %d",avs.connectorNum);
  Y_DEBUG("avs.startValue    = %f",avs.startValue);
  Y_DEBUG("avs.endValue      = %f",avs.endValue);
  Y_DEBUG("avs.startVel      = %f",avs.startVel);
  Y_DEBUG("avs.endVel        = %f",avs.endVel);
  Y_DEBUG("**********************************************");
}

void DOMParser::printAngleFeedbackSensorStructInformation(
    AngleFeedbackSensorStruct afs)
{
  Y_DEBUG("********** Angle Feedback Sensor Information *");
  Y_DEBUG("afs.connectorNum   = %d",afs.connectorNum);
  Y_DEBUG("afs.type           = %d",afs.type);
  Y_DEBUG("afs.feedbackTarget = %d",afs.feedbackTarget);
  Y_DEBUG("afs.direction      = %d",afs.direction);
  Y_DEBUG("afs.startValue     = %f",afs.startValue);
  Y_DEBUG("afs.endValue       = %f",afs.endValue);
  Y_DEBUG("afs.startFeedback  = %f",afs.startFeedback);
  Y_DEBUG("afs.endFeedback    = %f",afs.endFeedback);
  Y_DEBUG("**********************************************");
}

void DOMParser::printSliderPositionSensorStructInformation(
    SliderPositionSensorStruct sps)
{
  Y_DEBUG("********** Slider Position Sensor Information *");
  Y_DEBUG("sps.connectorNum   = %d",sps.connectorNum);
  Y_DEBUG("sps.startValue     = %f",sps.startValue);
  Y_DEBUG("sps.endValue       = %f",sps.endValue);
  Y_DEBUG("sps.startPosition  = %f",sps.startPosition);
  Y_DEBUG("sps.endPosition    = %f",sps.endPosition);
  Y_DEBUG("**********************************************");

}

void DOMParser::printSliderVelocitySensorStructInformation(
    AngleVelSensorStruct svs)
{
  Y_DEBUG("********** Slider Velocity Sensor Information *");
  Y_DEBUG("svs.connectorNum  = %d",svs.connectorNum);
  Y_DEBUG("svs.startValue    = %f",svs.startValue);
  Y_DEBUG("svs.endValue      = %f",svs.endValue);
  Y_DEBUG("svs.startVel      = %f",svs.startVel);
  Y_DEBUG("svs.endVel        = %f",svs.endVel);
  Y_DEBUG("**********************************************");
}

void DOMParser::printLightSource(LightSource ls)
{
  Y_DEBUG("********** Light Source **********************");
  Y_DEBUG("ls.type      = %d", ls.type);
  Y_DEBUG("ls.color.r   = %f", ls.color.r);
  Y_DEBUG("ls.color.g   = %f", ls.color.g);
  Y_DEBUG("ls.color.b   = %f", ls.color.b);
  Y_DEBUG("ls.intensity = %f", ls.intensity);
  printPosition(ls.pos, "ls.");
  Y_DEBUG("**********************************************");
}

void DOMParser::printPosition(Position pos, char *prefix)
{
  Y_DEBUG("%spos.x = %f", prefix, pos.x);
  Y_DEBUG("%spos.y = %f", prefix, pos.y);
  Y_DEBUG("%spos.z = %f", prefix, pos.z);
  Y_DEBUG("%spos.radius = %f", prefix, pos.radius);
  Y_DEBUG("%spos.length = %f", prefix, pos.length);
  char new_prefix[1024];
  sprintf(new_prefix,"%spos.",prefix);
  if(pos.randomise.enabled)
  {
    printRandiomise(pos.randomise, new_prefix);
  }
}

void DOMParser::printRandiomise(Randomise randomise, char *prefix)
{
  P3D p3d;
  Y_DEBUG("%srandomise.enable = %d", prefix, randomise.enabled);
  switch(randomise.method)
  {
    case RD_RANDOMISE_METHOD_ADDITIVE:
      Y_DEBUG("%srandomise.method = %d (additive)", 
          prefix, randomise.method);
      break;
    case RD_RANDOMISE_METHOD_GLOBAL:
      Y_DEBUG("%srandomise.method = %d (global)", 
          prefix, randomise.method);
      break;
    default:
      Y_DEBUG("%srandomise.method = ERROR: UNKNOWN METHOD", prefix);
  }
  switch(randomise.type)
  {
    case RD_RANDOMISE_TYPE_INTERVAL:
      Y_DEBUG("%srandomise.type = %d (interval)", prefix, randomise.type);
      break;
    case RD_RANDOMISE_TYPE_LIST:
      Y_DEBUG("%srandomise.type = %d (list)", prefix, randomise.type);
      break;
    default:
      Y_DEBUG("%srandomise.type = ERROR: UNKNOWN TYPE", prefix);
  }

  if(randomise.entries.size() > 0)
  {
    vector<P3D> E = randomise.entries;
    for(unsigned int i=0; i < E.size(); i++)
    {
      p3d = (E[i]);
      Y_DEBUG("%srandomise.entry%d.x = %f ", prefix, i, p3d.x);
      Y_DEBUG("%srandomise.entry%d.y = %f ", prefix, i, p3d.y);
      Y_DEBUG("%srandomise.entry%d.z = %f ", prefix, i, p3d.z);
    }
  }
  //double **entries;
  char new_prefix[1024];
  sprintf(new_prefix,"%srandomise.min",prefix);
  print3D(randomise.min, new_prefix);
  sprintf(new_prefix,"%srandomise.max",prefix);
  print3D(randomise.max, new_prefix);
  sprintf(new_prefix,"%srandomise.variance",prefix);
  print3D(randomise.variance, new_prefix);
  sprintf(new_prefix,"%srandomise.probability",prefix);
  print3D(randomise.probability, new_prefix);
}

void DOMParser::print3D(P3D p3d, char *prefix)
{
  Y_DEBUG("%s.x = %f", prefix, p3d.x);
  Y_DEBUG("%s.y = %f", prefix, p3d.y);
  Y_DEBUG("%s.z = %f", prefix, p3d.z);
}

void DOMParser::initialiseAngleVelocitySensorStruct(AngleVelSensorStruct *avs)
{
  avs->connectorNum = -1;
  avs->startValue   = -1;
  avs->endValue     = -1;
  avs->startVel     = -1;
  avs->endVel       = -1;
}

void DOMParser::initialiseSliderPositionSensorStruct(SliderPositionSensorStruct *sps)
{
  sps->connectorNum  = -1;
  sps->startValue    = -1;
  sps->endValue      = -1;
  sps->startPosition = -1;
  sps->endPosition   = -1;
}

void DOMParser::initialiseSliderVelocitySensorStruct(SliderVelSensorStruct *svs)
{
  svs->connectorNum = -1;
  svs->startValue   = -1;
  svs->endValue     = -1;
  svs->startVel     = -1;
  svs->endVel       = -1;
}

void DOMParser::initialiseAngleFeedbackSensorStruct(
    AngleFeedbackSensorStruct *afs)
{
  afs->connectorNum   = -1;
  afs->type           = -1;
  afs->feedbackTarget = -1;
  afs->direction      = -1;
  afs->startValue     = -1;
  afs->endValue       = -1;
  afs->startFeedback  = -1;
  afs->endFeedback    = -1;
}

void DOMParser::initialiseCoordinateSensor(CoordinateSensorStruct *cS)
{
  cS->coordinate = -1;
  cS->srcCompound   = -1;
  cS->srcSeg     = -1;
}


void DOMParser::initialiseSharpGP2D12_37Struct(SharpGP2D12_37_SensorStruct *sS)
{
  sS->startPos.x = 0.0;
  sS->startPos.y = 0.0;
  sS->startPos.z = 0.0;

  sS->initStartPos.x = 0.0;
  sS->initStartPos.y = 0.0;
  sS->initStartPos.z = 0.0;


  sS->direction.x = 0.0;
  sS->direction.y = 0.0;
  sS->direction.z = 0.0;

  sS->startValue = 0.0;
  sS->endValue   = 0.0;

  sS->startRange = 0.0;
  sS->endRange   = 0.8;
}

void DOMParser::initialiseSharpDM2Y3A003K0FStruct(SharpDM2Y3A003K0F_SensorStruct *sS)
{
  sS->startPos.x = 0.0;
  sS->startPos.y = 0.0;
  sS->startPos.z = 0.0;

  sS->initStartPos.x = 0.0;
  sS->initStartPos.y = 0.0;
  sS->initStartPos.z = 0.0;


  sS->direction.x = 0.0;
  sS->direction.y = 0.0;
  sS->direction.z = 0.0;

  sS->startValue = 0.0;
  sS->endValue   = 0.0;

  sS->startRange = 0.0;
  sS->endRange   = 3.0;
}

void DOMParser::copyPosition(Position *dest, Position source)
{
  dest->x = source.x;
  dest->y = source.y;
  dest->z = source.z;
  dest->radius = source.radius;
  dest->length = source.length;
  dest->randomise.enabled = source.randomise.enabled;
  dest->randomise.type = source.randomise.type;
  dest->randomise.method = source.randomise.method;
  dest->randomise.last_index = source.randomise.last_index;

  dest->randomise.min.x = source.randomise.min.x;
  dest->randomise.min.y = source.randomise.min.y;
  dest->randomise.min.z = source.randomise.min.z;

  dest->randomise.max.x = source.randomise.max.x;
  dest->randomise.max.y = source.randomise.max.y;
  dest->randomise.max.z = source.randomise.max.z;

  dest->randomise.variance.x = source.randomise.variance.x;
  dest->randomise.variance.y = source.randomise.variance.y;
  dest->randomise.variance.z = source.randomise.variance.z;
  dest->randomise.probability.x = source.randomise.probability.x;
  dest->randomise.probability.y = source.randomise.probability.y;
  dest->randomise.probability.z = source.randomise.probability.z;

  dest->randomise.entries = source.randomise.entries;
}

void DOMParser::rotate(Position *pos, Position rotation)
{

  double x = pos->x;
  double y = pos->y;
  double z = pos->z;

  double cx = cos(rotation.x);
  double cy = cos(rotation.y);
  double cz = cos(rotation.z);

  double sx = sin(rotation.x);
  double sy = sin(rotation.y);
  double sz = sin(rotation.z);

  pos->x = cz * ( cy * x + sy * z ) + sz * ( sx * ( cy * z - sy * x ) - cx * y);
  pos->y = sz * ( cy * x + sy * z ) - cz * ( sx * ( cy * z - sy * x ) - cx * y);
  pos->z = cx * ( cy * z - sy * x ) + sx * y;
}
