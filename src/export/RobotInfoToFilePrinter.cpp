/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "RobotInfoToFilePrinter.h"
#include <util/defines.h>

RobotInfoToFilePrinter::RobotInfoToFilePrinter()
{
  string segmentPosOutFile = "SegmentPosOut.txt";
  const char *blubs        = segmentPosOutFile.c_str();

  spOutFile.open(blubs, ios_base::out);
}

RobotInfoToFilePrinter::~RobotInfoToFilePrinter()
{
  spOutFile.close();
}

// prints positions of all Segments
void RobotInfoToFilePrinter::printSegmentPos(RobotHandler *robotHandler)
{
  if (!spOutFile)
  { 
    Y_WARN("File can not be opened for writing.\n"); 
  }
  else
  { 
    // TODO check for xml tag "log"
    for(unsigned int i=0; i < robotHandler->getNumberOfActiveRobots(); ++i)
    {
      for(unsigned int j=0; j < robotHandler->getNumberOfCompounds(i); ++j)
      {
        for(unsigned int k=0; k < robotHandler->getNumberOfSegments(i,j); ++k)
        {
          segmentPos = dBodyGetPosition(*(robotHandler->getSegmentBodyID(i,j,k)));
          spOutFile << segmentPos[0] << ",";
          spOutFile << segmentPos[1] << ",";
          spOutFile << segmentPos[2] << ",";
        }
      }
    }
    spOutFile << endl;
  }
}

void RobotInfoToFilePrinter::printSegmentNames(RobotHandler *robotHandler)
{
  if (!spOutFile)
  {
    Y_WARN("File can not be opened for writing.\n"); 
  }
  else
  {
    string s;
    Y_INFO("\n");
    Y_INFO("PrintSegmentNames(): File could be opened and we're using it ;-)\n");
    Y_INFO("\n");

    // print out Robot Names
    spOutFile << "#";
    for(unsigned int i=0; i < robotHandler->getNumberOfActiveRobots(); ++i)
    { 
      for(unsigned int j=0; j < robotHandler->getNumberOfCompounds(i); ++j)
      { 
        for(unsigned int k=0; k < robotHandler->getNumberOfSegments(i,j); ++k)
        { 
          if (k==0 && j==0) 
          { 
            spOutFile << "Robot No. " << i << ", " << ", " << ", ";
          }
          else
          { 
            spOutFile << ", " << ", " << ", ";
          }
        }
      }
    }
    spOutFile << endl;

    // print out Segment Compound Names
    spOutFile << "#";
    for(unsigned int i=0; i < robotHandler->getNumberOfActiveRobots(); ++i)
    {
      for(unsigned int j=0; j < robotHandler->getNumberOfCompounds(i); ++j)
      {
        for(unsigned int k=0; k < (robotHandler->getNumberOfSegments(i,j)); ++k)
        {
          if (k==0) 
          {
            ((robotHandler->getRobot(i))->getCompound(j))->getCompoundName(s);
            spOutFile << s << ", " << ", " << ", ";
          }
          else
          {
            spOutFile << ", " << ", " << ", ";
          }
        }
      }
    }
    spOutFile << endl;

    // print out Segment Names
    spOutFile << "#";
    for(unsigned int i=0; i < robotHandler->getNumberOfActiveRobots(); ++i)
    {
      for(unsigned int j=0; j < robotHandler->getNumberOfCompounds(i); ++j)
      {
        for(unsigned int k=0; k < robotHandler->getNumberOfSegments(i,j); ++k)
        {
          ((robotHandler->getRobot(i))->getCompound(j))->getCompoundName(s);
          spOutFile << s << ", " << ", " << ", ";
        }
      }
    }
    spOutFile << endl;

    // print out Coordinate Names
    spOutFile << "#";
    for(unsigned int i=0; i < robotHandler->getNumberOfActiveRobots(); ++i)
    {
      for(unsigned int j=0; j < robotHandler->getNumberOfCompounds(i); ++j)
      {
        for(unsigned int k=0; k < robotHandler->getNumberOfSegments(i,j); ++k)
        {
          spOutFile << "x," << "y," << "z,";
        }
      }
    }
    spOutFile << endl;
  }
}
