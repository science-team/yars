/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
//
// Template for GenericContainers, instantiation is as follows:
//   GenericContainers<keyType,valueType> nameOfContainer;
//   or
//   GenericContainers<keyType,valueType,mapType> nameOfContainer;
//
// TODO:
//

#ifndef _GENERIC_CONTAINER_H_
#define _GENERIC_CONTAINER_H_

#include <util/defines.h>
#include <base/Segment.h>
#include <util/RingBuffer.h>

#include <vector>
#include <string>
#include <ext/hash_map>

template<typename _KEY, typename _VALUE, 
  typename _CONT = std::map<_KEY,_VALUE> >
class GenericContainer 
{
  public:
    GenericContainer();
    ~GenericContainer();

    int getSize();
    int getMaxSize();

    bool isEmpty();
    int  count(const _KEY&);

    void clear();
    int  erase(const _KEY&);

    const _VALUE& operator[](const _KEY&);
    const _VALUE&        getValue(const _KEY&);
    const vector<_VALUE> getValueRange(const _KEY&);
    
    bool insertPair(const _KEY&, const _VALUE&);

  private:
    _CONT map;
};


template<typename _KEY, typename _VALUE, typename _CONT>
GenericContainer<_KEY,_VALUE,_CONT>::GenericContainer()
{
  // nothing to do
}

template<typename _KEY, typename _VALUE, typename _CONT>
GenericContainer<_KEY,_VALUE,_CONT>::~GenericContainer()
{
  // nothing to do
}

template<typename _KEY, typename _VALUE, typename _CONT>
int GenericContainer<_KEY,_VALUE,_CONT>::getSize()
{
  return map.size();
}

template<typename _KEY, typename _VALUE, typename _CONT>
int GenericContainer<_KEY,_VALUE,_CONT>::getMaxSize()
{
  return map.max_size();
}

template<typename _KEY, typename _VALUE, typename _CONT>
bool GenericContainer<_KEY,_VALUE,_CONT>::isEmpty()
{
  return map.empty();
}

template<typename _KEY, typename _VALUE, typename _CONT>
int GenericContainer<_KEY,_VALUE,_CONT>::count(const _KEY& key)
{
  return map.count(key);
}

template<typename _KEY, typename _VALUE, typename _CONT>
void GenericContainer<_KEY,_VALUE,_CONT>::clear()
{
  map.clear();
}

template<typename _KEY, typename _VALUE, typename _CONT>
int GenericContainer<_KEY,_VALUE,_CONT>::erase(const _KEY& key)
{
  map.erase(key);
}

template<typename _KEY, typename _VALUE, typename _CONT>
const _VALUE& GenericContainer<_KEY,_VALUE,_CONT>::operator[](const _KEY& key)
{
  return map[key];
}

template<typename _KEY, typename _VALUE, typename _CONT>
const _VALUE& GenericContainer<_KEY,_VALUE,_CONT>::getValue(const _KEY& key)
{
  typename _CONT::iterator iter;
  iter = map.find(key);

  if( iter != map.end() ) 
  {
    return iter->second;
  }
  else
  {
    throw "GenericContainer<_KEY,_VALUE,_CONT>::getValue(): key not found";
  }
}

template<typename _KEY, typename _VALUE, typename _CONT>
const vector<_VALUE> GenericContainer<_KEY,_VALUE,_CONT>::getValueRange(
    const _KEY& key)
{
  pair<typename _CONT::iterator, typename _CONT::iterator> p;
  typename _CONT::iterator iter;

  vector<_VALUE> rangeValueVector;
  
  p = map.equal_range(key);

  if ( (p.first == map.end( )) && (p.second == map.end( )) )
  {
    throw "GenericContainer<_KEY,_VALUE,_CONT>::getValueRange(): no key \
                                  less or equal than requested key";
  }
  else
  {
    // TODO: recheck this part
    for(iter = p.first; iter != p.second; ++iter)
    {
      rangeValueVector.push_back((*iter).second);
    }
    return rangeValueVector;
  }
}

template<typename _KEY, typename _VALUE, typename _CONT>
bool GenericContainer<_KEY,_VALUE,_CONT>::insertPair(
    const _KEY& key, const _VALUE& value)
{
  pair<typename _CONT::iterator, bool> p;
  
  p = map.insert(make_pair(key, value));

  return p.second;
}

#endif
