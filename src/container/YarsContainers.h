/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _YARS_CONTAINERS_H_
#define _YARS_CONTAINERS_H_

#include <base/Segment.h>

#include <util/RingBuffer.h>
#include <util/defines.h>
#include <map>

#include <container/GenericContainer.h>

#include <vector>
#include <ode/ode.h>

// TODO: adapt value types to our needs
typedef GenericContainer< string, vector<SensorData> > SensorContainer;
typedef GenericContainer< string, vector<MotorData> >  MotorContainer;
// TODO: here we should use dGeomID as a key and we shouldn't use a vector
typedef GenericContainer< dGeomID, vector<GeomData> >   GeomContainer;
//typedef GenericContainer<string,int,hash_map<string,int> > GeomContainer;

//
// Class YarsContainer:
// ---------------------
// Provides access to and guarantees single instance of the container classes,
// at this point being
//   - SensorContainer
//   - MotorContainer
//   - GeomContainer
//
// The values of the <key, value> pairs of the containers are structs that are
// defined in util/defines.h:
//   - struct SensorData (raw, mapped)
//   - struct MotorData  (raw, mapped)
//   - struct GeomData   (dGeomID, struct ContactParameter) // TODO: position?
//
// Additionally this class provides acces to the following "global" structs:
//   - simulation parameter struct (where e.g. the sim frequency is stored)
//   - abortCondition struct (which signals if the evaluation should be aborted,
//       e.g. due to bumps, joint torques exceeding a limit ...) and if yes why
//       it should be aborted (source and cause)
//
class YarsContainers
{
  public:
    static SensorContainer*       Sensor();
    static MotorContainer*        Motor();
    static GeomContainer*         Geom();
    static struct SimParams*      SimParams();
    static struct AbortCondition* AbortCondition();

  protected:
    YarsContainers();
    ~YarsContainers();

  private:
    static SensorContainer*       _sensor;
    static MotorContainer*        _motor;
    static GeomContainer*         _geom;
    static struct SimParams*      _simParams;
    static struct AbortCondition* _abortCondition;
};

#endif
