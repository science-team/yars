/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "YarsContainers.h"

YarsContainers::YarsContainers()
{
}

YarsContainers::~YarsContainers()
{
  if(_sensor != NULL)
    delete _sensor;
  if(_motor != NULL)
    delete _motor;
  if(_geom != NULL)
    delete _geom;
  if(_simParams != NULL)
    delete _simParams;
  if(_abortCondition != NULL)
    delete _abortCondition;
}

SensorContainer*       YarsContainers::_sensor         = NULL;
MotorContainer*        YarsContainers::_motor          = NULL;
GeomContainer*         YarsContainers::_geom           = NULL;
struct SimParams*      YarsContainers::_simParams      = NULL;
struct AbortCondition* YarsContainers::_abortCondition = NULL;

SensorContainer* YarsContainers::Sensor()
{
  if(_sensor == NULL)
  {
    _sensor = new SensorContainer();
  }
  return _sensor;
}

MotorContainer* YarsContainers::Motor()
{
  if(_motor == NULL)
  {
    _motor = new MotorContainer();
  }
  return _motor;
}

GeomContainer* YarsContainers::Geom()
{
  if(_geom == NULL)
  {
    _geom = new GeomContainer();
  }
  return _geom;
}

struct SimParams* YarsContainers::SimParams()
{
  if(_simParams == NULL)
  {
    _simParams = new struct SimParams();

    (*_simParams).stepSize = 0.0;
  }
  return _simParams;
}

struct AbortCondition* YarsContainers::AbortCondition()
{
  if(_abortCondition == NULL)
  {
    _abortCondition = new struct AbortCondition();

    (*_abortCondition).abortCondition = false;
    (*_abortCondition).abortSource    = "";
    (*_abortCondition).abortCause     = DEF_ABORT_CAUSE_NONE;
  }
  return _abortCondition;
}
