/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "LightManager.h"
#include <util/RandomiseFunctions.h>
#include <util/defines.h>

LightManager::LightManager(dSpaceID s, double ambient, DrawManager *dM)
  :ambientLight_(ambient), drawManager_(dM)
{
  spaceID_     = dSimpleSpaceCreate(s);
}

LightManager::~LightManager()
{
  for(unsigned int i = 0; i < lights_.size(); ++i)
  {
    dGeomDestroy(lights_[i].geom);
  }
  dSpaceDestroy(spaceID_);
}


void LightManager::addLight(LightSource *elem)
{
  LightObj tmp;
  double radius = 0.1;
  
  tmp.geom = dCreateSphere(0, radius);
  dGeomSetPosition(tmp.geom, elem->pos.x, elem->pos.y, elem->pos.z);
 
  Y_DEBUG("LightManager::addLight");
  drawManager_->addGeom(tmp.geom);
  Color c = elem->color;
  tmp.intensity = elem->intensity;

  drawManager_->changeColor(tmp.geom, 
      float(tmp.intensity)*c.r, 
      float(tmp.intensity)*c.g,
      float(tmp.intensity)*c.b,
      float(tmp.intensity));
 
  lights_.push_back(tmp);

  if(elem->drawRange == true)
  {
    LightObj tmp2;
    float radius = sqrt(elem->intensity/0.1);
    tmp2.geom = dCreateSphere(0, radius);
    Color color;
    color.r   = elem->color.r;
    color.g   = elem->color.g;
    color.b   = elem->color.b;
    color.alpha = 0.25;
    tmp2.intensity = 0.0;
    dGeomSetPosition(tmp2.geom, elem->pos.x, elem->pos.y, elem->pos.z);
    drawManager_->addGeomLast(tmp2.geom);
    drawManager_->changeColor(tmp2.geom,
        color.r,
        color.g,
        color.b,
        color.alpha );

    lights_.push_back(tmp2);

  }
}

unsigned int LightManager::getNumberOfLights()
{
  return lights_.size();
}

double LightManager::getAmbientIntensity()
{
  return ambientLight_;
}

double LightManager::getLightIntensity(unsigned int lightSrc, double distance,
    double angle, double maxAngle)
{
  double itensity;

  /* calculate itensity according to the distance */
  itensity = lights_[lightSrc].intensity / (1.0+ (distance*distance) );

  /* calculate itensity according to the angle*/
  itensity = itensity * sqrt((maxAngle-angle)/maxAngle);
  
  return itensity;
}

Position LightManager::getPosition(unsigned int lightSrc)
{
  Position tmp;
  const dReal *pos = dGeomGetPosition(lights_[lightSrc].geom);

  tmp.x = pos[0];
  tmp.y = pos[1];
  tmp.z = pos[2];

  return tmp;
}

void LightManager::setPosition(unsigned int lightSrc, Position pos)
{
  dGeomSetPosition(lights_[lightSrc].geom, pos.x, pos.y, pos.z);
}

void LightManager::setIntensity(unsigned int lightSrc, double intensity)
{
  double fac = intensity/2.0;
  lights_[lightSrc].intensity = intensity;
  drawManager_->changeColor(lights_[lightSrc].geom, float(fac), float(fac),
      0.0f, 0.6f);
 
}
