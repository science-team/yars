/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_

#include <description/EnvironmentDescription.h>
#include <gui/DrawManager.h>
#include <env/LightManager.h>

/* This class creates a plane as ground */

class Environment
{
  public:
    Environment(dSpaceID s, DrawManager* drawManager);
    ~Environment();

    //!returns the ID of the ground plane
    dGeomID getGroundID();
    //!create function
    void create(EnvironmentDescription *envDes);

    /* draw all objects */
    void draw();

    DrawManager* getDrawManager();

    /* randomise all position */
    void randomisePositions();

    /* returns a pointer to the light manager */
    LightManager* getLightManager();

    bool doesIntersect(Position pos, double radius);

    dSpaceID getSpaceID();

  private:
    /* create a geom described in SingleElement */
    void createGeom(SingleElement *elem);

    /*creates a box */
    void createBox(dGeomID *geom, Position *pos, Position *rot, Position *dim);

    /* creates a sphere */
    void createSphere(dGeomID *geom, Position *pos, Position *rot, Position *dim);

    /* creates a sphere */
    void createCappedCylinder(dGeomID *geom, Position *pos, Position *rot,
        Position *dim);

    /* creates a flat end cylinder*/
    void createCylinder(dGeomID *geom, Position *pos, Position *rot, Position
        *dim);

    /* rotate a geom about the three euler angles in rad */ 
    void setGeomRotation(dGeomID* geom, Position *rotation);

    void copyPosition(Position *dest, Position source);

    EnvironmentDescription *envDes_;
    dSpaceID spaceID_; 
    DrawManager *drawManager_;
    dGeomID ground_;
    LightManager *lightManager_;
};

#endif
