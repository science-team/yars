/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Environment.h"
#include <util/RandomiseFunctions.h>
#include <util/defines.h>

Environment::Environment(dSpaceID s, DrawManager* drawManager)
{
  spaceID_     = dSimpleSpaceCreate(s);
  drawManager_ = drawManager;
}

Environment::~Environment()
{
  delete lightManager_;
  dSpaceDestroy(spaceID_);
}

void Environment::create(EnvironmentDescription *envDes)
{
  envDes_ = envDes;
  ground_ = dCreatePlane(spaceID_,0,0,2,0);

  for(unsigned int i=0; i < envDes_->getNumberOfElements(); ++i)
  {
    this->createGeom(envDes_->getElement(i));
  }

  lightManager_ = new LightManager(spaceID_, envDes->getAmbientLight(),
      drawManager_);

  Y_DEBUG("Environment::create getNumberOfLights = %d", envDes_->getNumberOfLights());
  for(unsigned int i=0; i < envDes_->getNumberOfLights(); ++i)
  {
    lightManager_->addLight(envDes_->getLight(i));
    Y_DEBUG("Environment::create called addLight");
  }

}

LightManager* Environment::getLightManager()
{
  return lightManager_;
}

void Environment::randomisePositions()
{
  for(unsigned int i=0; i < envDes_->getNumberOfElements(); ++i)
  {
    SingleElement *elem = envDes_->getElement(i);
    RandomiseFunctions::randomisePosition(&(elem->position));

    Y_DEBUG("Environment::randomisePosition:rotation %f %f %f ",
        elem->init_rotation.x, 
        elem->init_rotation.y, 
        elem->init_rotation.z);

    RandomiseFunctions::randomisePosition(&(elem->init_rotation));

    Y_DEBUG("--> Environment::randomisePosition:rotation %f %f %f ",
        elem->init_rotation.x, 
        elem->init_rotation.y, 
        elem->init_rotation.z);

    copyPosition(&(elem->rotation), elem->init_rotation);

    Y_DEBUG("-- --> Environment::randomisePosition:rotation %f %f %f ",
        elem->rotation.x, 
        elem->rotation.y, 
        elem->rotation.z);
  }
  for(unsigned int i=0; i < envDes_->getNumberOfLights(); ++i)
  {
    LightSource *elem = envDes_->getLight(i);
    Y_DEBUG("LightManager::addLight:: randomise.enabled = %d",
        elem->pos.randomise.enabled );
    if(elem->pos.randomise.enabled)
    {
      Y_DEBUG("LightManager::addLight:: calling Randomise");
      RandomiseFunctions::randomisePosition(&elem->pos);
    }
  }

}


void Environment::createGeom(SingleElement *elem)
{
  dGeomID geom;
  switch(elem->type)
  {
    case RD_BOX_GEOM:
      {
        this->createBox(&geom, &(elem->position), &(elem->rotation),
            &(elem->dimensions) );
        Color c = elem->color;  
        drawManager_->changeColor(geom, c.r, c.g, c.b, c.alpha);
      }
      break;

    case RD_SPHERE_GEOM:
      {
        this->createSphere(&geom, &(elem->position), &(elem->rotation),
            &(elem->dimensions));
        Color c = elem->color;  
        drawManager_->changeColor(geom, c.r, c.g, c.b, c.alpha);
      }
      break;

    case RD_CAPPED_CYLINDER_GEOM:
      {
        this->createCappedCylinder(&geom, &(elem->position),
            &(elem->rotation), &(elem->dimensions));
        Color c = elem->color;  
        drawManager_->changeColor(geom, c.r, c.g, c.b, c.alpha);
      }
      break;

    case RD_CYLINDER_GEOM:
      {
        this->createCylinder(&geom, &(elem->position),
            &(elem->rotation), &(elem->dimensions));
        Color c = elem->color;  
        drawManager_->changeColor(geom, c.r, c.g, c.b, c.alpha);
      }
      break;

    default:
      break;
  }
 
}

void Environment::createBox(dGeomID *geom, Position *pos, Position *rot,
    Position *dim)
{
  *geom = dCreateBox(spaceID_, dim->x, dim->y, dim->z);

  Y_DEBUG("Environment::createBox dim %f %f %f", dim->x, dim->y, dim->z);

  Y_DEBUG("Environment::createBox");
  drawManager_->addGeom(*geom);

  dGeomSetPosition(*geom, pos->x, pos->y, pos->z);
  this->setGeomRotation(geom, rot);
}

void Environment::createSphere(dGeomID *geom, Position *pos, Position *rot,
    Position *dim)
{
  *geom = dCreateSphere(spaceID_, dim->radius);

  Y_DEBUG("Environment::createSphere");
  drawManager_->addGeom(*geom);

  dGeomSetPosition(*geom, pos->x, pos->y, pos->z);
}

void Environment::createCappedCylinder(dGeomID *geom, Position *pos, Position
    *rot, Position *dim)
{
  *geom = dCreateCCylinder(spaceID_, dim->radius, dim->length);

  Y_DEBUG("Environment::createCappedCylinder");
  drawManager_->addGeom(*geom);

  dGeomSetPosition(*geom, pos->x, pos->y, pos->z);
  this->setGeomRotation(geom, rot);
}

void Environment::createCylinder(dGeomID *geom, Position *pos, Position
    *rot, Position *dim)
{
  *geom = dCreateCylinder(spaceID_, dim->radius, dim->length);

  Y_DEBUG("Environment::createCylinder");
  drawManager_->addGeom(*geom);

  dGeomSetPosition(*geom, pos->x, pos->y, pos->z);
  this->setGeomRotation(geom, rot);
}


void Environment::setGeomRotation(dGeomID* geom, Position *rotation)
{
  dMatrix3 rot;

  dRFromEulerAngles(rot, rotation->x, rotation->y, rotation->z);

  dGeomSetRotation(*geom, rot); 
}

dGeomID Environment::getGroundID()
{
  return ground_;
}

void Environment::draw()
{
  drawManager_->draw();
}

DrawManager* Environment::getDrawManager()
{
  return drawManager_;
}

bool Environment::doesIntersect(Position pos, double radius)
{
  int numOfGeoms = dSpaceGetNumGeoms(spaceID_);
  dGeomID geom;

  for(int i = 0; i < numOfGeoms; ++i)
  {   
    if(dGeomGetClass(dSpaceGetGeom(spaceID_, i)) == dPlaneClass ||
        dGeomIsSpace(dSpaceGetGeom(spaceID_, i)))

    {
      continue;
    }
    
    geom = dSpaceGetGeom(spaceID_, i);
    
    switch(dGeomGetClass(geom))
    {
      case dBoxClass:
        {
          if(dGeomBoxPointDepth(geom, pos.x, pos.y, pos.z) > 0.0-radius)
          {
            return true;
          }
        }
        break;
      case dSphereClass:
        {
          if(dGeomSpherePointDepth(geom, pos.x, pos.y, pos.z) > 0.0-radius)
          {
            return true;
          }
        }
        break;
      case dCCylinderClass:
        {
          if(dGeomCCylinderPointDepth(geom, pos.x, pos.y, pos.z) > 0.0-radius)
          {
            return true;
          }
        }
      default:
        break;                
    }
  }
  return false;

}

dSpaceID Environment::getSpaceID()
{
  return spaceID_;
}

void Environment::copyPosition(Position *dest, Position source)
{
  dest->x = source.x;
  dest->y = source.y;
  dest->z = source.z;
  dest->radius = source.radius;
  dest->length = source.length;
  dest->randomise.enabled = source.randomise.enabled;
  dest->randomise.type = source.randomise.type;
  dest->randomise.method = source.randomise.method;
  dest->randomise.last_index = source.randomise.last_index;

  dest->randomise.min.x = source.randomise.min.x;
  dest->randomise.min.y = source.randomise.min.y;
  dest->randomise.min.z = source.randomise.min.z;
  
  dest->randomise.max.x = source.randomise.max.x;
  dest->randomise.max.y = source.randomise.max.y;
  dest->randomise.max.z = source.randomise.max.z;

  dest->randomise.variance.x = source.randomise.variance.x;
  dest->randomise.variance.y = source.randomise.variance.y;
  dest->randomise.variance.z = source.randomise.variance.z;
  dest->randomise.probability.x = source.randomise.probability.x;
  dest->randomise.probability.y = source.randomise.probability.y;
  dest->randomise.probability.z = source.randomise.probability.z;

  dest->randomise.entries = source.randomise.entries;
}
