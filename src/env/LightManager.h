/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _LIGHTMANAGER_H_
#define _LIGHTMANAGER_H_

#include <gui/DrawManager.h>
#include <description/EnvironmentDescription.h>
#include <util/defines.h>

class LightManager
{
 public:
  LightManager(dSpaceID s, double ambient, DrawManager *drawManager);
  ~LightManager();
  
  /* add a light source as described by light */
  void addLight(LightSource *light);

  unsigned int getNumberOfLights();

  double getAmbientIntensity();

  /* returns the intensity of a light src at a specific distance */
  double getLightIntensity(unsigned int lightSrc, double distance, double angle,
      double maxAngle);

  /* returns the position of a specific light source */
  Position getPosition(unsigned int lightSrc);
 
  void setPosition(unsigned int lightSrc, Position pos);
  void setIntensity(unsigned int lightSrc, double intensity);
 private:

  struct LightObj
  {
    dGeomID geom;
    double intensity;
  };
  
  vector<struct LightObj> lights_;
  double ambientLight_;
 
  dSpaceID spaceID_; 
  DrawManager *drawManager_;

};

#endif
