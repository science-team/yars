/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _DIRECTEDCAMERA_H_
#define _DIRECTEDCAMERA_H_

#include <base/CameraHandler.h>

#include <base/Sensor.h>
#include <env/LightManager.h>
#include <gui/DrawManager.h>
#include <env/Environment.h>
#include <description/DescriptionHelper.h>
#include <base/MappingFunction.h>
#include <sensors/Camera.h>
#include <vector>
#include <iostream>

#define RADIANS 57.295779579
#define PI 3.14159265

using namespace std;

class DirectedCamera: public Sensor 
{
  public: 
    DirectedCamera(DrawManager *dm, dSpaceID s);

    ~DirectedCamera();

    void create(Position position, Position rotation, dBodyID
        *sensorBody, Environment *environment, GLfloat angle, int xWidth, int yHeight, int light, int colorType);


    void updateSensor();

    vector<double> getValues();

    // calculate the euler angles from the given rotation matrix
    Position getAngles(const dReal *mat);

    // max colorValue that can be returned
    int getMaxValue();

    int getMinValue();

    void selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

    Camera* getCamera();

  private:
    DrawManager *drawManager_;

    dSpaceID spaceID_;

    Environment *environment_;

    Position _position;

    Position _direction;

    vector<double> _allValues;

    Camera *_robotView;
    Position newDir;
    
    dBodyID *sensorBody_;

    // number of pixels in x an y axis. _xWidth*_yWidht*colorInformation calculates 
    // the number of sensorValues returned by the camera-sensor
    int _xPix;
    int _yPix;

    // size of the window 
    int _light;

    int _colorType;
};
#endif //_LdrSensor_H_ 
