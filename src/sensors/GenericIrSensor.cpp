/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "GenericIrSensor.h"

GenericIrSensor::GenericIrSensor(DrawManager *drawManager, dSpaceID s)
{ 
  _drawManager = drawManager;
  _spaceID     = s;
  _endRange    = 0;
  _startRange  = 0;
  sensorValues.resize(1);
}

GenericIrSensor::~GenericIrSensor()
{ }

void GenericIrSensor::create(double startRange, double endRange, double spreadAngleX, double
    spreadAngleY, Position rotation, Position startPos, dBodyID *sensorBody)
{

  IrObj irObj;
  dMatrix3 tmpRot, result;
  Position tmpDir1, tmpDir2;
 
  sensorBody_ = sensorBody;
  _endRange = endRange;
  _startRange = startRange;
  
  const dReal *bodyPos = dBodyGetPosition(*sensorBody);

  /* calculate the vector between the centre of sensor body and the startpos of
   * the ir sensor */
  posOffset_.x    = startPos.x-bodyPos[0];
  posOffset_.y    = startPos.y-bodyPos[1];
  posOffset_.z    = startPos.z-bodyPos[2];
 
  /* these values are identical for all rays */
  irObj.pos          = startPos;
  irObj.length       = fabs(_startRange - _endRange);
  irObj.actualLength = irObj.length;
  irObj.isInContact  = false;

  /* transform the given rotation to a matrix */
  dRFromEulerAngles (tmpRot, rotation.x, rotation.y, rotation.z);
 
  /*first create the middle ray */

  tmpDir1.x = tmpRot[2];
  tmpDir1.y = tmpRot[6];
  tmpDir1.z = tmpRot[10];

  this->createRay(&irObj, &tmpDir1);

  /* create the rays lying in the z-y plane */
  
  multiplyMatrix(tmpRot, 0.0, spreadAngleY/2.0, &result);
 
  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];
  
  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, 0.0, -spreadAngleY/2.0, &result);
 
  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];
  
  this->createRay(&irObj, &tmpDir2);

  /* create the rays lying in the z-x plane */
  multiplyMatrix(tmpRot, spreadAngleX/2.0, 0.0, &result);
 
  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];
  
  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, -spreadAngleX/2.0, 0.0, &result);
 
  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];
  
  this->createRay(&irObj, &tmpDir2);

  this->updateSensor();
}

void GenericIrSensor::updateSensor()
{
  Position newDir, rayDir, newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  /* has to calculate only once, because start position is the same for all rays
   * */
  newPosOff.x = bodyRot[0]*posOffset_.x + bodyRot[1]*posOffset_.y +
    bodyRot[2]*posOffset_.z;
  newPosOff.y = bodyRot[4]*posOffset_.x + bodyRot[5]*posOffset_.y +
    bodyRot[6]*posOffset_.z;
  newPosOff.z = bodyRot[8]*posOffset_.x + bodyRot[9]*posOffset_.y +
    bodyRot[10]*posOffset_.z;


  for(unsigned int i = 0; i < irObj_.size(); ++i)
  {
    rayDir = irObj_[i].direction; 

    irObj_[i].pos.x = bodyPos[0] + newPosOff.x;
    irObj_[i].pos.y = bodyPos[1] + newPosOff.y;
    irObj_[i].pos.z = bodyPos[2] + newPosOff.z;

    newDir.x = bodyRot[0]*rayDir.x + bodyRot[1]*rayDir.y + bodyRot[2]*rayDir.z;
    newDir.y = bodyRot[4]*rayDir.x + bodyRot[5]*rayDir.y + bodyRot[6]*rayDir.z;
    newDir.z = bodyRot[8]*rayDir.x + bodyRot[9]*rayDir.y + bodyRot[10]*rayDir.z;

    dGeomRaySet(irObj_[i].ray, irObj_[i].pos.x, irObj_[i].pos.y,
        irObj_[i].pos.z, newDir.x, newDir.y, newDir.z);
    
    /* means was in contact one simulation step before, but now it isn't so
     * set the lenght and color again to normal values*/
    if(irObj_[i].isInContact)
    {
      dGeomRaySetLength(irObj_[i].ray, irObj_[i].length);
      _drawManager->changeColor(irObj_[i].ray, 1.0f, 1.0f, 1.0f);
      irObj_[i].actualLength = irObj_[i].length;
      irObj_[i].isInContact = false;
    }
  }
}

vector<double> GenericIrSensor::getValues()
{
  double length;
 
//  this->updateSensor();
  
  length = irObj_[0].actualLength;
  
  for(unsigned int i = 1; i < irObj_.size(); ++i)
  {
    if(irObj_[i].actualLength < length)
    {
      length = irObj_[i].actualLength;
    }
  }
  
  Y_DEBUG("GenericIrSensor:getValue(): = %f\n", length);
  sensorValues[0] = length;
  return sensorValues;
}

void GenericIrSensor::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{
  Position pos;
  
  if (dGeomGetSpace(geom1) == _spaceID || dGeomGetSpace(geom2) == _spaceID)
  {
    pos.x = contact->geom.pos[0];
    pos.y = contact->geom.pos[1];
    pos.z = contact->geom.pos[2];

    for(unsigned int i = 0; i < irObj_.size(); ++i)
    {
      /* if a ray is collided with something than calculate the new length and
       * set color to red */
      if(irObj_[i].ray == geom1 || irObj_[i].ray == geom2)
      {
        irObj_[i].actualLength = 
          sqrt( (pos.x-irObj_[i].pos.x)*(pos.x-irObj_[i].pos.x) 
            + (pos.y-irObj_[i].pos.y)*(pos.y-irObj_[i].pos.y) 
            + (pos.z-irObj_[i].pos.z)*(pos.z-irObj_[i].pos.z) ); 
          _drawManager->changeColor(irObj_[i].ray, 1.0f, 0.0f, 0.0f);
        irObj_[i].isInContact = true;
      }
    }
  }
}

void GenericIrSensor::createRay(IrObj *irObj, Position *dir)
{
  irObj->ray = dCreateRay(_spaceID, irObj->length);
  
  _drawManager->addGeom(irObj->ray);
  
  irObj->direction.x = dir->x;
  irObj->direction.y = dir->y;
  irObj->direction.z = dir->z;

  dGeomRaySet(irObj->ray, irObj->pos.x, irObj->pos.y, irObj->pos.z,
      irObj->direction.x, irObj->direction.y, irObj->direction.z);

  irObj_.push_back(*irObj);
}

void GenericIrSensor::multiplyMatrix(dMatrix3 m1, double angleX, double angleY,
    dMatrix3 *result)
{
  dQuaternion q1, q2, q3;
  dMatrix3 tmpRot;
  
  dRFromEulerAngles (tmpRot, angleX, angleY, 0.0);
  
  dRtoQ(m1, q1);
  dRtoQ(tmpRot, q2);

  dQMultiply0 (q3, q1, q2);

  dQtoR(q3, *result);
}
