/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "SliderPositionSensor.h"

SliderPositionSensor::SliderPositionSensor()
{ 
  sensorValues.resize(1);
}

SliderPositionSensor::~SliderPositionSensor()
{ }

void SliderPositionSensor::setJointID(dJointID* joint)
{
  joint_ = joint;
}

vector<double> SliderPositionSensor::getValues()
{
  switch(dJointGetType(*joint_))
  {
    case dJointTypeSlider:
      {
        sensorValues[0] = dJointGetSliderPosition(*joint_);
      }
      break;
    default:
      // TODO: default Wert ??
      break;
  }
  return sensorValues;
}

void SliderPositionSensor::updateSensor()
{ }

void SliderPositionSensor::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact)
{ }
