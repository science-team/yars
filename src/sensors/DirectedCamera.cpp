/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/



#include "DirectedCamera.h"


DirectedCamera::DirectedCamera(DrawManager *dm, dSpaceID s)
{
  drawManager_ = dm;
  spaceID_ = s;
}

DirectedCamera::~DirectedCamera()
{
}

void DirectedCamera::create(Position position, Position rotation, dBodyID
    *sensorBody, Environment *environment, GLfloat angle, int xPix, int yPix, int light, int colorType)
{
  _xPix = xPix;
  _yPix = yPix;
  _light = light;
  _colorType = colorType;
  dMatrix3 tmpRot;

  CameraHandler* cameraHandler = CameraHandler::instance();

  _robotView = cameraHandler->getNextCamera(_light, _xPix, _yPix);
  _robotView->create(drawManager_, environment, angle, colorType);

  sensorBody_ = sensorBody;
  environment_ = environment;

  _position.x = position.x;
  _position.y = position.y;
  _position.z = position.z;

  _direction.x  = (rotation.y*180)/PI;
  _direction.y = (rotation.x*180)/PI;
  _direction.z = (rotation.z*180)/PI;

  // calculate number of sensor values
  int colorChannels;
  if(_colorType == RGB_TYPE)
  {
    colorChannels = 3;
  }
  if (_colorType == GREYSCALE_TYPE || _colorType == RED_TYPE || _colorType == GREEN_TYPE || _colorType == BLUE_TYPE)
  {
    colorChannels = 1;
  }

  sensorValues.resize(_xPix*_yPix*colorChannels);
  _allValues.resize(_xPix*_yPix*3);
}

Position DirectedCamera::getAngles(const dReal *mat)
{
  Position fangles;
  GLfloat tr_x, tr_y;
  fangles.y = -asin(mat[2]);

  GLfloat c = cos(fangles.y);
  fangles.y *= RADIANS;

  if(fabs(c) > 0.005)
  {
    tr_x = mat[8]/c;
    tr_y = -mat[5] /c;
    fangles.x = atan2(tr_y, tr_x)*RADIANS;
    tr_x = mat[0]/c;
    tr_y = -mat[1]/c;
    fangles.z = atan2(tr_x, tr_y)*RADIANS;
  }
  else
  {
    fangles.x = 0;
    tr_x = mat[4];
    tr_y = mat[3];

    fangles.z = atan2(tr_y, tr_x)*RADIANS;
  }
  return fangles;
}

void DirectedCamera::updateSensor()
{
  Position newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  newPosOff.x = bodyRot[0]*_position.x + bodyRot[1]*_position.y + bodyRot[2]*_position.z;
  newPosOff.y = bodyRot[4]*_position.x + bodyRot[5]*_position.y + bodyRot[6]*_position.z;
  newPosOff.z = bodyRot[8]*_position.x + bodyRot[9]*_position.y + bodyRot[10]*_position.z;


  newDir.x = bodyPos[0] + newPosOff.x;
  newDir.y = bodyPos[1] + newPosOff.y;
  newDir.z = bodyPos[2] + newPosOff.z;

  Position rot = getAngles(bodyRot);

  rot.x = fabs(rot.x) + _direction.x;
  rot.y += _direction.y;
  rot.z += _direction.z;
  _robotView->drawScene(newDir, rot, bodyRot);
  // get all three color channels from the camera
  _allValues = _robotView->getPixelValues();
}

vector<double> DirectedCamera::getValues()
{
  switch(_colorType)
  {
    int num;
    case RGB_TYPE:
    return _allValues;
    break;
    case RED_TYPE:
    num = 0;
    for(int i = 0; i< _allValues.size(); i+=3)
    {
      sensorValues[num] = _allValues[i];
      num++;
    }
    return sensorValues;
    break;
    case GREEN_TYPE:
    num = 0;
    for(int i = 0; i< _allValues.size(); i+=3)
    {
      sensorValues[num] = _allValues[i+1];
      num++;
    }
    return sensorValues;
    break;
    case BLUE_TYPE:
    num = 0;
    for(int i = 0; i< _allValues.size(); i+=3)
    {
      sensorValues[num] = _allValues[i+2];
      num++;
    }
    return sensorValues;
    break;
    case GREYSCALE_TYPE:
    num = 0;
    for(int i = 0; i< _allValues.size(); i+=4)
    {
      sensorValues[num] = _allValues[i];
      num++;
    }
    return sensorValues;
    break;
  }
}

int DirectedCamera::getMaxValue()
{
  return 255;
}

int DirectedCamera::getMinValue()
{
  return 0;
}

void DirectedCamera::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact)
{
}

Camera* DirectedCamera::getCamera()
{
  return _robotView;
}

