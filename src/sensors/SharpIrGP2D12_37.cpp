/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "SharpIrGP2D12_37.h"

SharpIrGP2D12_37::SharpIrGP2D12_37(DrawManager *drawManager, dSpaceID s)
{ 
  drawManager_ = drawManager;
  //  spaceID_     = dSimpleSpaceCreate(s);
  spaceID_     = s;
  sensorValues.resize(1);
}

SharpIrGP2D12_37::~SharpIrGP2D12_37()
{
  /* destroy all geoms and the space */
  //  dSpaceDestroy(spaceID_);
}


void SharpIrGP2D12_37::create(Position rotation, Position startPos, dBodyID
    *sensorBody, double startRange, double endRange)
{
  IrObj irObj;
  dMatrix3 tmpRot, result;
  Position tmpDir1, tmpDir2;

  startRange_ = startRange;
  endRange_   = endRange;

  sensorBody_ = sensorBody;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody);

  /* calculate the vector between the centre of sensor body and the startpos of
   * the ir sensor */
  posOffset_.x    = startPos.x-bodyPos[0];
  posOffset_.y    = startPos.y-bodyPos[1];
  posOffset_.z    = startPos.z-bodyPos[2];

  /* these values are identical for all rays */
  irObj.pos          = startPos;
  irObj.length       = SHARP_GP2D12_37_LENGTH;
  irObj.actualLength = SHARP_GP2D12_37_LENGTH;
  irObj.isInContact  = false;

  /* transform the given rotation to a matrix */
  dRFromEulerAngles (tmpRot, rotation.x, rotation.y, rotation.z);

  /*first create the middle ray */

  tmpDir1.x = tmpRot[2];
  tmpDir1.y = tmpRot[6];
  tmpDir1.z = tmpRot[10];

  this->createRay(&irObj, &tmpDir1);

  /* create the rays lying in the z-y plane */

  multiplyMatrix(tmpRot, 0.0, SHARP_GP2D12_37_SPREAD_ANGLE_Y/2.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, 0.0, -SHARP_GP2D12_37_SPREAD_ANGLE_Y/2.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  /* create the rays lying in the z-x plane */
  multiplyMatrix(tmpRot, SHARP_GP2D12_37_SPREAD_ANGLE_X/2.0, 0.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, -SHARP_GP2D12_37_SPREAD_ANGLE_X/2.0, 0.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  this->updateSensor();
}

void SharpIrGP2D12_37::extractMinMaxValue()
{
  _minValue = 10;
  _maxValue = 0;
  double x = 0;

  for(int i=0; i < SHARP_GP2D12_37_STEPS; i++)
  {
    double v = distance2Voltage(x);
    Y_DEBUG("SharpIrGP2D12_37::extractMinMaxValue %f = f(%f)",v,x);
    x += SHARP_GP2D12_37_STEPSIZE;
    if ( v > _maxValue) _maxValue = v;
    if ( v < _minValue) _minValue = v;
  }
  Y_DEBUG("min %f max %f", _minValue, _maxValue);
}

void SharpIrGP2D12_37::updateSensor()
{
  Position newDir, rayDir, newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  /* has to calculate only once, because start position is the same for all rays
   * */
  newPosOff.x = bodyRot[0]*posOffset_.x + bodyRot[1]*posOffset_.y + bodyRot[2]*posOffset_.z;
  newPosOff.y = bodyRot[4]*posOffset_.x + bodyRot[5]*posOffset_.y + bodyRot[6]*posOffset_.z;
  newPosOff.z = bodyRot[8]*posOffset_.x + bodyRot[9]*posOffset_.y + bodyRot[10]*posOffset_.z;



  for(unsigned int i = 0; i < irObj_.size(); ++i)
  {
    rayDir = irObj_[i].direction; 

    irObj_[i].pos.x = bodyPos[0] + newPosOff.x;
    irObj_[i].pos.y = bodyPos[1] + newPosOff.y;
    irObj_[i].pos.z = bodyPos[2] + newPosOff.z;

    newDir.x = bodyRot[0]*rayDir.x + bodyRot[1]*rayDir.y + bodyRot[2]*rayDir.z;
    newDir.y = bodyRot[4]*rayDir.x + bodyRot[5]*rayDir.y + bodyRot[6]*rayDir.z;
    newDir.z = bodyRot[8]*rayDir.x + bodyRot[9]*rayDir.y + bodyRot[10]*rayDir.z;

    dGeomRaySet(irObj_[i].ray, irObj_[i].pos.x, irObj_[i].pos.y,
        irObj_[i].pos.z, newDir.x, newDir.y, newDir.z);

    /* means was in contact one simulation step before, but now it isn't so
     * set the lenght and color again to normal values*/
    if(irObj_[i].isInContact)
    {
      dGeomRaySetLength(irObj_[i].ray, irObj_[i].length);
      drawManager_->changeColor(irObj_[i].ray, 1.0f, 1.0f, 1.0f);
      irObj_[i].actualLength = irObj_[i].length;
      irObj_[i].isInContact = false;
    }
  }
}

double SharpIrGP2D12_37::distance2Voltage(double x_in)
{	
  double voltage;
  voltage = 0.0;
  if (x_in < 0.08)
    //if (true)
  {

    double tmp = (double) random();

    voltage = ( tmp / ((double)(RAND_MAX) + (double)1) ) ;
  }
  else
  {
    // this curve approximises the Voltage/Distance-relationship of
    // the real hardware-Sharp
    //voltage = 0.4 + ( 1 / exp(0.2 * distance) ) * pow((0.4 * distance),2);
    // function fittet by ZunZun.com
    // y = (a + b*x + c*x^2 + d*x^3) / (1.0 + e*x + f*x^2 + g*x^3) + h 
    // coefficients
    double a = 6.9150689419777978E-02;
    double b = 1.8839464940157088E+16;
    double c = -2.3998650046308112E+17;
    double d = -5.1523017838895328E+16;
    double e = 1.9349142226118700E+15;
    double f = 3.0785321107083352E+16;
    double g = -7.3075970231840742E+17;
    double h = -6.9150689479670208E-02;

    // Die folgende Zeile gibt durch die Wahl der Funktionsparameter einen Wert zwischen 0.4 und 2.6 entsprechend dem Datenblatt aus
    voltage += (a + b * x_in + c * x_in * x_in + d * x_in * x_in * x_in) / (1.0 + e * x_in + f * x_in * x_in + g * x_in * x_in * x_in) + h;

    // Der berechnete Wert muss auf 0 bis 1 normiert werden, damit er anschliessend auf z.B. -1 bis 1 (wie in der xml-Datei angegeben) gemappt werden kann ?!?
    voltage = (voltage - VOLT_MIN)/(VOLT_MAX - VOLT_MIN);

  }

  return voltage;
}

vector<double> SharpIrGP2D12_37::getValues()
{
  double length;
  double value;

  length = irObj_[0].actualLength;

  for(unsigned int i = 1; i < irObj_.size(); ++i)
  {
    if(irObj_[i].actualLength < length)
    {
      if(irObj_[i].actualLength < startRange_)        
      {
        length = startRange_;
      }
      else if(irObj_[i].actualLength > endRange_)
      {
        length = endRange_;
      }
      else
      {
        length = irObj_[i].actualLength; 
      } 
    }
  }


  value = distance2Voltage(length);
  sensorValues[0] = value;
  return sensorValues;
}

void SharpIrGP2D12_37::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{

  Position pos;

  if (dGeomGetSpace(geom1) == spaceID_ || dGeomGetSpace(geom2) == spaceID_)
  {
    pos.x = contact->geom.pos[0];
    pos.y = contact->geom.pos[1];
    pos.z = contact->geom.pos[2];

    for(unsigned int i = 0; i < irObj_.size(); ++i)
    {
      /* if a ray is collided with something than calculate the new length and
       * set color to red */
      if(irObj_[i].ray == geom1 || irObj_[i].ray == geom2)
      {
        irObj_[i].actualLength = 
          sqrt( (pos.x-irObj_[i].pos.x)*(pos.x-irObj_[i].pos.x) 
              + (pos.y-irObj_[i].pos.y)*(pos.y-irObj_[i].pos.y) 
              + (pos.z-irObj_[i].pos.z)*(pos.z-irObj_[i].pos.z) ); 
        drawManager_->changeColor(irObj_[i].ray, 1.0f, 0.0f, 0.0f);
        irObj_[i].isInContact = true;
      }
    }
  }
}

void SharpIrGP2D12_37::createRay(IrObj *irObj, Position *dir)
{
  irObj->ray = dCreateRay(spaceID_, irObj->length);

  drawManager_->addGeom(irObj->ray);

  irObj->direction.x = dir->x;
  irObj->direction.y = dir->y;
  irObj->direction.z = dir->z;

  dGeomRaySet(irObj->ray, irObj->pos.x, irObj->pos.y, irObj->pos.z,
      irObj->direction.x, irObj->direction.y, irObj->direction.z);

  irObj_.push_back(*irObj);
}

void SharpIrGP2D12_37::multiplyMatrix(dMatrix3 m1, double angleX, double angleY,
    dMatrix3 *result)
{
  dQuaternion q1, q2, q3;
  dMatrix3 tmpRot;

  dRFromEulerAngles (tmpRot, angleX, angleY, 0.0);

  dRtoQ(m1, q1);
  dRtoQ(tmpRot, q2);

  dQMultiply0 (q3, q1, q2);

  dQtoR(q3, *result);
}

double SharpIrGP2D12_37::getMinValue()
{
  return SHARP_GP2D12_37_MIN_SENSOR_VALUE;
}

double SharpIrGP2D12_37::getMaxValue()
{
  return SHARP_GP2D12_37_MAX_SENSOR_VALUE;
}
