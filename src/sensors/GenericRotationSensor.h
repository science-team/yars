/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _GENERIC_ROTATION_SENSOR_H_
#define _GENERIC_ROTATION_SENSOR_H_

#include <base/Sensor.h>
#include <gui/DrawManager.h>
#include <description/DescriptionHelper.h>
#include <description/SensorDescription.h>
#include <base/MappingFunction.h>
#include <vector>

#define RAY_ROT             0
#define RAY_REF             1

class GenericRotationSensor : public Sensor 
{
  public: 
    GenericRotationSensor(DrawManager *drawManager, dSpaceID s);
   ~GenericRotationSensor();

   /* create the rotation sensor with all the geoms */
   void create(int type, int orderDerivative,
               double startRange, double endRange, 
               Position rotation, Position startPos, 
               bool drawRayRot, bool drawRayRef,
               double lengthRayRot, double lengthRayRef,
               int rayRotType, int rayRefType,
               Color rayRotColor, Color rayRefColor,
               dBodyID *sensorBody);
 
   /* set the new position of the rays*/
   void updateSensor();

   vector<double> getValues();

   int getNumberOfValues();

   // unused for this sensor
   void selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

  private:
   void createRay(int rayNum);

   void getBodyRotationAsEuler(double* rx, double* ry, double* rz);

   int      _type;                 // type of sensor
   int      _orderDerivative;      // abs., diff., or accel?
   bool     _drawRay[2];           // shall we draw the ray?
   dGeomID  _ray[2];
   double   _lengthRay[2];         // base length
   double   _actualLengthRay[2];   // actual length
   int      _rayType[2];           // change length with value?
   Color    _rayColor[2];          // color of rays
   Position _posRay[2];            // start position
   Position _directionRay[2];      // direction vector of ray
   dMatrix3 _referenceRayRotation; // rotation of reference vector relative to
                                   //   world coordinate system
   dVector3 _tmpVector;            // used to normalize Position vectors
   
   Position posOffset_;            // vector between centre of 
                                   // the body and position of 
                                   // the ray 

   MappingFunction *map_;
   double          _endRange;
   double          _startRange;

   dBodyID *sensorBody_;
   DrawManager *_drawManager;
   dSpaceID _spaceID;
};
#endif //_GenericIrSensor_H_ 
