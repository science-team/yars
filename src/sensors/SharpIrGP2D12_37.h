/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _SHARP_GP2D12_37_H_
#define _SHARP_GP2D12_37_H_

#include <base/Sensor.h>
#include <gui/DrawManager.h>
#include <description/DescriptionHelper.h>
#include <base/MappingFunction.h>
#include <vector>

// working range 0.1 - 0.8 m
#define SHARP_GP2D12_37_LENGTH 0.8

// spread angle in both planes is 12.5 deg
#define SHARP_GP2D12_37_SPREAD_ANGLE_Y 0.17

// spread angle in both planes is 12.5 deg
#define SHARP_GP2D12_37_SPREAD_ANGLE_X 0.1
#define VOLT_MIN 0.4
#define VOLT_MAX 2.6 
 
#define SHARP_GP2D12_37_MIN_SENSOR_VALUE 0.0006
#define SHARP_GP2D12_37_MAX_SENSOR_VALUE 0.993

// for extractMinMaxValue-method
#define SHARP_GP2D12_37_STEPS 1000
// for extractMinMaxValue-method
#define SHARP_GP2D12_37_STEPSIZE ((double)SHARP_GP2D12_37_LENGTH / (double)SHARP_GP2D12_37_STEPS)


class SharpIrGP2D12_37 : public Sensor 
{
  public: 
    SharpIrGP2D12_37(DrawManager *drawManager, dSpaceID s);
   ~SharpIrGP2D12_37();

   /* create the ir sensor with all the geoms and fills the direction vector for
    * each ray */
   void create(Position rotation, Position startPos, dBodyID *sensorBody, double
       startRange, double endRange);
 
   /* set the new position of the rays,
    * IMPORTANT: this function must be called before the nearcallback function is called.*/
   void updateSensor();

   vector<double> getValues();
   
   int getNumberOfValues();
   
   double getMinValue();
   double getMaxValue();

   void selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

  private:
   struct IrObj
   {
     dGeomID ray;
     double length;
     /* momentary starting position of the ray */
     Position pos;
     /* direction vector of the ray */
     Position direction;   
     /* the original length */
     bool isInContact;
     /* the momentary length */
     double actualLength;
   };
   
   void createRay(IrObj *irObj, Position *dir);
   
   double distance2Voltage(double distance);
   
   void multiplyMatrix(dMatrix3 m1, double angleX, double angleY, dMatrix3
       *result);
  
   DrawManager *drawManager_;
   dSpaceID spaceID_;

   void extractMinMaxValue();
   
   vector<struct IrObj> irObj_;
   dBodyID *sensorBody_;
   MappingFunction *map_;
   /* vector between centre of the body and position of the ray */
   Position posOffset_;

   /* defines the working range, optimal is 0.1 and 0.8 (working range) */
   double startRange_;
   double endRange_;

  double _minValue;
  double _maxValue;
};
#endif //_SharpIrGP2D12_37_H_ 
