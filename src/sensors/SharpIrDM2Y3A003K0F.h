/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _SHARP_DM2Y3A003K0F_H_
#define _SHARP_DM2Y3A003K0F_H_

#include <base/Sensor.h>
#include <gui/DrawManager.h>
#include <description/DescriptionHelper.h>
#include <base/MappingFunction.h>
#include <vector>

// working range 0.1 - 0.8 m
#define SHARP_DM2Y3A003K0F_LENGTH         3.0
// spread angle in both planes is 12.5 deg
#define SHARP_DM2Y3A003K0F_SPREAD_ANGLE_Y 0.17
// spread angle in both planes is 12.5 deg
#define SHARP_DM2Y3A003K0F_SPREAD_ANGLE_X 0.1

// for extractMinMaxValue-method
#define STEPS 1000
// for extractMinMaxValue-method
#define STEPSIZE ((double)SHARP_DM2Y3A003K0F_LENGTH / (double)STEPS)

// calculated by the extractMinMaxValue method, and taken so that they do not be
// recalculated every time
#define SHARP_DM2Y3A003K0F_MIN_SENSOR_VALUE 0.276899
#define SHARP_DM2Y3A003K0F_MAX_SENSOR_VALUE 2.865692

 
class SharpIrDM2Y3A003K0F : public Sensor 
{
  public: 
    SharpIrDM2Y3A003K0F(DrawManager *drawManager, dSpaceID s);
   ~SharpIrDM2Y3A003K0F();

   /* create the ir sensor with all the geoms and fills the direction vector for
    * each ray */
   void create(Position rotation, Position startPos, dBodyID *sensorBody, double
       startRange, double endRange);
 
   /* set the new position of the rays,
    * must be called before nearcallback */
   void updateSensor();

   vector<double> getValues();
   
   int getNumberOfValues();

   double getMinValue();
   double getMaxValue();

   void selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

  private:
   struct IrObj
   {
     dGeomID ray;
     double length;
     /* momentary starting position of the ray */
     Position pos;
     /* direction vector of the ray */
     Position direction;   
     /* the original length */
     bool isInContact;
     /* the momentary length */
     double actualLength;
   };
   
   double valueToVoltage(double x_in);

   void createRay(IrObj *irObj, Position *dir);

   void multiplyMatrix(dMatrix3 m1, double angleX, double angleY, dMatrix3
       *result);
  
   void extractMinMaxValue();

   DrawManager *drawManager_;
   dSpaceID spaceID_;

   vector<struct IrObj> irObj_;
   dBodyID *sensorBody_;
   MappingFunction *map_;
   /* vector between centre of the body and position of the ray */
   Position posOffset_;

   /* defines the working range, optimal is 0.1 and 0.8 (working range) */
   double startRange_;
   double endRange_;
   double _minValue;
   double _maxValue;
};
#endif //_SharpIrDM2Y3A003K0F_H_ 
