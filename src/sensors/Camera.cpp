/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/



#include <sensors/Camera.h>

#include <iostream>
#include <base/CameraHandler.h>


Camera::Camera(int light, int xPix, int yPix)
{   
  if(light)
  {
    _realLight = false;
  }
  else
  {
    _realLight = true;
  }

  glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH | GLUT_DOUBLE);
  _window = 0;
  _renderTexture = NULL;

  maxS = 1;
  maxT = 1;

  iTextureProgram = 0;
  iPassThroughProgram = 0;

  _xPix = xPix;
  _yPix = yPix;

  _windowWidth = 128;
  _windowHeight = 128;
  glutInitWindowPosition(0,0);
  glutInitWindowSize(_windowWidth,_windowHeight);

  _window = glutCreateWindow("Camera");
  glClearColor(1,1,1,1);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);
  glMatrixMode(GL_PROJECTION);
  gluPerspective(60, (GLfloat) _windowWidth/(GLfloat) _windowHeight, 0.1, 400.0);
  glMatrixMode(GL_MODELVIEW);
  int err = glewInit();
  if (GLEW_OK != err)
  {
    // problem: glewInit failed, something is seriously wrong
    fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
    exit(-1);
  }  
  _renderTexture = new RenderTexture(_xPix, _yPix, _windowWidth, _windowHeight);

  // 32-bit float texture.with depth and stencil. (must be POTD)
  _renderTexture->initialize(true, true, true, false, false, 32, 32, 32, 32);

  // 16-bit float texture.with depth and stencil. (must be POTD)
  //    _renderTexture->Initialize(true, true, true, false, false, 16, 16, 16, 16);

  if (_renderTexture->isFloatTexture())
  {
    // rectangle textures need texture coordinates in [0, resolution] rather than [0, 1]
    maxS = _renderTexture->getWidth();
    maxT = _renderTexture->getHeight();
  }

  iTextureProgram = 0;
  iPassThroughProgram = 0;
  if (_renderTexture->isFloatTexture())
  {
    glGenProgramsARB(1, &iTextureProgram);
    glGenProgramsARB(1, &iPassThroughProgram);

    const char* textureProgram = _renderTexture->isRectangleTexture() ?
      "!!ARBfp1.0\n"
      "TEX result.color, fragment.texcoord[0], texture[0], RECT;\n"
      "END\n"
      :
      "!!ARBfp1.0\n"
      "TEX result.color, fragment.texcoord[0], texture[0], 2D;\n"
      "END\n";
    const char* passThroughProgram = "!!ARBfp1.0\n"
      "MOV result.color, fragment.color.primary;\n"
      "END\n";

    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, iTextureProgram);
    glProgramStringARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
        strlen(textureProgram), textureProgram);

    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, iPassThroughProgram);
    glProgramStringARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
        strlen(passThroughProgram), passThroughProgram);
  }
}

Camera::~Camera()
{
  delete _renderTexture;
  glutDestroyWindow(_window);  
}

void Camera::setLightMaterialAndPerspective(GLfloat angle)
{
  GLfloat pos[4] = {0., 0., 10., 1.};
  GLfloat white[4] = {1., 1., 1., 1.};
  GLfloat black[4] = {0., 0., 0., 0.};

  /* Set up light1 */
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT1);

  if(_realLight)
  {
    glLightfv (GL_LIGHT1, GL_POSITION, pos);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, white);
    glLightfv (GL_LIGHT1, GL_SPECULAR, black);
    GLfloat LModelAmbient[4] ={0.5,0.5,0.5,1.0F};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,&LModelAmbient[0]);//Umgebungslichtwerte
  }
  else
  {
    GLfloat LModelAmbient[4] ={1,1,1,1.0F};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,&LModelAmbient[0]);//Umgebungslichtwerte
  }
  /* ambient and diffuse will track glColor */
  glEnable (GL_COLOR_MATERIAL);
  glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glMaterialfv (GL_FRONT, GL_SPECULAR, black);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(angle, (GLfloat) _windowWidth/(GLfloat) _windowHeight, 0.1, 400.0);
  glMatrixMode(GL_MODELVIEW);
}

void Camera::create(DrawManager *drawManager, Environment *environment, GLfloat angle, int colorType)
{
  _renderTexture->setColorType(colorType);
  glutSetWindow(_window);
  _openingAngle = angle;
  _drawManager = drawManager;
  _environment = environment;
  _pixelValues.resize(_xPix*_yPix);
}

vector<double> Camera::getPixelValues()
{
  _pixelValues =_renderTexture->getTexels();
  return _pixelValues;
}

void Camera::drawScene(Position position, Position rotation, const dReal *bodyRot)
{
  GLfloat *floatRot= new GLfloat[12];

  for(int i = 0;  i<12; i++)
  {
    floatRot[i] = bodyRot[i];
  }
  glutSetWindow(_window);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  _renderTexture->beginCapture();
  //   glClearColor(0.2,0.6,1,0.5);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);

  if (_renderTexture->isFloatTexture() && _renderTexture->isRectangleTexture())
  {    
    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, iPassThroughProgram);
    glEnable(GL_FRAGMENT_PROGRAM_ARB);
  } 
  //go to the viewpoint of the camera
  glRotatef(-rotation.x, 1, 0, 0);
  glRotatef(-rotation.y, 0, 1, 0);
  glRotatef(rotation.z, 0, 0, 1);

  glTranslatef(-position.x, -position.y, -position.z);

  setLightMaterialAndPerspective(_openingAngle);
  // objects
  _drawManager->_isCameraView = true;
  _drawManager->draw();
  //environment
  _environment->getDrawManager()->_isCameraView = true;
  _environment->draw();
  _drawManager->_isCameraView = false;
  glLoadIdentity();
  _renderTexture->endCapture();
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  if (_renderTexture->isFloatTexture())
  {    
    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, iTextureProgram);
    glEnable(GL_FRAGMENT_PROGRAM_ARB);
    glActiveTexture(GL_TEXTURE0_ARB);
  }
  _renderTexture->bind();

  GLfloat point = 0;

  if(_xPix == _yPix)
  {
    point = _xPix + _yPix -1;
  }
  else
  {
    if(_xPix < _yPix)
    {
      point = _xPix + _yPix + (_yPix/_xPix);
    }
    else
    {
      point = _yPix + _xPix + (_xPix/_yPix);
    }
  }
  //_renderTexture->enableTextureTarget();
  gluLookAt(0, 0 ,point, 0, 0, 0, 0, 1, 0);

  // render Texture on square
  glBegin(GL_QUADS);
  glTexCoord2f(0,       0); glVertex3f(-1*_xPix, -1*_yPix, -0.5f);
  glTexCoord2f(maxS,    0); glVertex3f( 1*_xPix, -1*_yPix, -0.5f);
  glTexCoord2f(maxS, maxT); glVertex3f( 1*_xPix,  1*_yPix, -0.5f);
  glTexCoord2f(0,    maxT); glVertex3f(-1*_xPix,  1*_yPix, -0.5f);
  glEnd();

  if (_renderTexture->isFloatTexture())
  {
    glDisable(GL_FRAGMENT_PROGRAM_ARB);
  }

  _renderTexture->disableTextureTarget();
  glutSwapBuffers();
  delete[] floatRot;
}

int Camera::getWindowId()
{
  return _window;
}

void Camera::reset()
{
  _drawManager = NULL;
  _environment = NULL;
}
