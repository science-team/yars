/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "AngleFeedbackSensor.h"

AngleFeedbackSensor::AngleFeedbackSensor()
{ 
  sensorValues.resize(1);
}

AngleFeedbackSensor::~AngleFeedbackSensor()
{ }

void AngleFeedbackSensor::setJointID(dJointID* joint)
{
  joint_ = joint;
}

void AngleFeedbackSensor::setType(int type)
{
  type_ = type;
}

void AngleFeedbackSensor::setFeedbackTarget(int feedbackTarget)
{
  feedbackTarget_ = feedbackTarget;
}

// direction 0, 1, 2 correspond to global x, y, z axes
//           3 to torque around hinge axis
//           4 to force along slider axis
void AngleFeedbackSensor::setDirection(int direction)
{
  direction_ = direction;
}

void AngleFeedbackSensor::initialize()
{
  // check if feedback structure already exists, if not
  // allocate it
  feedback_ = dJointGetFeedback(*joint_);
  if (feedback_ == 0)
  {
    dJointSetFeedback(*joint_, new dJointFeedback);
    feedback_ = dJointGetFeedback(*joint_);
  }  
}

vector<double> AngleFeedbackSensor::getValues()
{

  if (type_ == 0)
  {
    if (feedbackTarget_ == 0)
    {
      sensorValues[0] =  getDirValue(direction_, feedback_->f1);
    }  
    else
    {
      sensorValues[0] = getDirValue(direction_, feedback_->f2);
    }  
  }  
  else
  {
    if (feedbackTarget_ == 0)
    {
      sensorValues[0] = getDirValue(direction_, feedback_->t1);
    }  
    else
    {
      sensorValues[0] = getDirValue(direction_, feedback_->t2);
    }  
  }

  Y_DEBUG("AngleFeedbackSensor: %f\n", sensorValues[0]);
  return sensorValues;
}

double AngleFeedbackSensor::getDirValue(int direction, dVector3 vector)
{
  double ret=0.0;
  dVector3 axis;

  // 0, 1, 2 correspond to global x,y,z
  switch(direction)
  {
    case 0: ret = vector[0];
      break;
      
    case 1: ret = vector[1];
      break;

    case 2: ret = vector[2];
      break;

    case 3: dJointGetHingeAxis(*joint_, axis);
            ret = getTransformedVector(0, axis, vector);
      break;

    case 4: dJointGetSliderAxis(*joint_, axis);
            ret = getTransformedVector(0, axis, vector);
      break;

    default: Y_WARN("error occured while processing AngleFeedbackSensor!\n");
      break;
  }  

  return ret;
}  

// project Force/Torque Vector on the Axis of Rotation/Slider
// x-Value of result vector (coordinate == 0) will be the projection value
// on the hinge/slider axis
double AngleFeedbackSensor::getTransformedVector(int coordinate, dVector3 axis, 
    dVector3 feedbackGlobal)
{
  dMatrix3 rot33;
  dVector3 feedbackLocal; 
  dReal ax, ay, az, bx, by, bz;

  ax = axis[0];
  ay = axis[1];
  az = axis[2];

  bx = ax;
  by = ay;
  bz = az + 1;

  // Get Rotation Matrix to transform global to local coordinate
  // system with x-axis of local coordinate system being equal to 
  // the hinge/slider axis
  dRFrom2Axes(rot33, ax, ay, az, bx, by, bz);

  // now get the resulting force/torque vector in the local
  // coordinate system
  dMULTIPLY0_331(feedbackLocal, rot33, feedbackGlobal);

  return feedbackLocal[coordinate];
}

void AngleFeedbackSensor::updateSensor()
{ }
   
void AngleFeedbackSensor::selfCollide(dGeomID geom1, dGeomID geom2, 
    struct dContact* contact)
{ }
