/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "SharpIrDM2Y3A003K0F.h"
#include <iostream>

#define START_POS_X 0.0
#define START_POS_Y 0.0
#define START_POS_Z 0.0

using namespace std;


SharpIrDM2Y3A003K0F::SharpIrDM2Y3A003K0F(DrawManager *drawManager, dSpaceID s)
{ 
  drawManager_ = drawManager;
  spaceID_     = s;
  sensorValues.resize(1);  
}

SharpIrDM2Y3A003K0F::~SharpIrDM2Y3A003K0F()
{ }

void SharpIrDM2Y3A003K0F::create(Position rotation, Position startPos, dBodyID
    *sensorBody, double startRange, double endRange)
{
  IrObj irObj;
  dMatrix3 tmpRot, result;
  Position tmpDir1, tmpDir2;

  startRange_ = startRange;
  endRange_   = endRange;

  sensorBody_ = sensorBody;

  //extractMinMaxValue(); // now constants
  //_maxValue = valueToVoltage(0.29); // peek value taken from test list

  const dReal *bodyPos = dBodyGetPosition(*sensorBody);

  /* calculate the vector between the centre of sensor body and the startpos of
   * the ir sensor */
  posOffset_.x    = startPos.x-bodyPos[0];
  posOffset_.y    = startPos.y-bodyPos[1];
  posOffset_.z    = startPos.z-bodyPos[2];

  /* these values are identical for all rays */
  irObj.pos          = startPos;
  irObj.length       = SHARP_DM2Y3A003K0F_LENGTH;
  irObj.actualLength = SHARP_DM2Y3A003K0F_LENGTH;
  irObj.isInContact  = false;

  /* transform the given rotation to a matrix */
  dRFromEulerAngles (tmpRot, rotation.x, rotation.y, rotation.z);

  /*first create the middle ray */

  tmpDir1.x = tmpRot[2];
  tmpDir1.y = tmpRot[6];
  tmpDir1.z = tmpRot[10];

  this->createRay(&irObj, &tmpDir1);

  /* create the rays lying in the z-y plane */

  multiplyMatrix(tmpRot, 0.0, SHARP_DM2Y3A003K0F_SPREAD_ANGLE_Y/2.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, 0.0, -SHARP_DM2Y3A003K0F_SPREAD_ANGLE_Y/2.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  /* create the rays lying in the z-x plane */
  multiplyMatrix(tmpRot, SHARP_DM2Y3A003K0F_SPREAD_ANGLE_X/2.0, 0.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  multiplyMatrix(tmpRot, -SHARP_DM2Y3A003K0F_SPREAD_ANGLE_X/2.0, 0.0, &result);

  tmpDir2.x = result[2];
  tmpDir2.y = result[6];
  tmpDir2.z = result[10];

  this->createRay(&irObj, &tmpDir2);

  this->updateSensor();
}

void SharpIrDM2Y3A003K0F::extractMinMaxValue()
{
  _minValue = 10;
  _maxValue = 0;
  double x = 0;

  for(int i=0; i < STEPS; i++)
  {
    double v = valueToVoltage(x);
    Y_DEBUG("SharpIrDM2Y3A003K0F::extractMinMaxValue %f = f(%f)",v,x);
    x += STEPSIZE;
    if ( v > _maxValue) _maxValue = v;
    if ( v < _minValue) _minValue = v;
  }
  Y_DEBUG("min %f max %f", _minValue, _maxValue);
}

void SharpIrDM2Y3A003K0F::updateSensor()
{
  Position newDir, rayDir, newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  /* has to calculate only once, because start position is the same for all rays
   * */
  newPosOff.x = bodyRot[0]*posOffset_.x + bodyRot[1]*posOffset_.y +
    bodyRot[2]*posOffset_.z;
  newPosOff.y = bodyRot[4]*posOffset_.x + bodyRot[5]*posOffset_.y +
    bodyRot[6]*posOffset_.z;
  newPosOff.z = bodyRot[8]*posOffset_.x + bodyRot[9]*posOffset_.y +
    bodyRot[10]*posOffset_.z;


  for(unsigned int i = 0; i < irObj_.size(); ++i)
  {
    rayDir = irObj_[i].direction; 

    irObj_[i].pos.x = bodyPos[0] + newPosOff.x;
    irObj_[i].pos.y = bodyPos[1] + newPosOff.y;
    irObj_[i].pos.z = bodyPos[2] + newPosOff.z;

    newDir.x = bodyRot[0]*rayDir.x + bodyRot[1]*rayDir.y + bodyRot[2]*rayDir.z;
    newDir.y = bodyRot[4]*rayDir.x + bodyRot[5]*rayDir.y + bodyRot[6]*rayDir.z;
    newDir.z = bodyRot[8]*rayDir.x + bodyRot[9]*rayDir.y + bodyRot[10]*rayDir.z;

    dGeomRaySet(irObj_[i].ray, irObj_[i].pos.x, irObj_[i].pos.y,
        irObj_[i].pos.z, newDir.x, newDir.y, newDir.z);

    /* means was in contact one simulation step before, but now it isn't so
     * set the lenght and color again to normal values*/
    if(irObj_[i].isInContact)
    {
      dGeomRaySetLength(irObj_[i].ray, irObj_[i].length);
      drawManager_->changeColor(irObj_[i].ray, 1.0f, 1.0f, 1.0f);
      irObj_[i].actualLength = irObj_[i].length;
      irObj_[i].isInContact = false;
    }
  }
}

vector<double> SharpIrDM2Y3A003K0F::getValues()
{
  double length;
  double value;

  //  this->updateSensor();

  length = irObj_[0].actualLength;

  for(unsigned int i = 1; i < irObj_.size(); ++i)
  {
    if(irObj_[i].actualLength < length)
    {
      if(irObj_[i].actualLength < startRange_)        
      {
        length = startRange_;
      }
      else if(irObj_[i].actualLength > endRange_)
      {
        length = endRange_;
      }
      else
      {
        length = irObj_[i].actualLength;
      }
    }
  }

  //  value = exp(-7.0*(length-0.1)) ;
  value = valueToVoltage(length);
  Y_DEBUG("SharpIrDM2Y3A003K0F:getValue: valueToVoltage(length) := %f = f(%f) < %f", value, length, _maxValue);
  Y_DEBUG("SharpIrDM2Y3A003K0F:getValue: resulting %f", value);
  sensorValues[0] = value;
  return sensorValues;
}

double SharpIrDM2Y3A003K0F::valueToVoltage(double x_in)
{
  double temp;
  temp = 0.0;

  // coefficients
  double a = 1.0239641952819603E+00;
  double b = 1.0214759253629424E-01;
  double c = -3.6210296804999246E+01;
  double d = 1.4789371773170392E+03;
  double e = -1.0667498086333941E+04;
  double f = 3.7273092905545200E+04;
  double g = -7.8643785061759234E+04;
  double h = 1.1002096153644304E+05;
  double i = -1.0701466075100192E+05;
  double j = 7.4114076713499817E+04;
  double k = -3.6829557422168262E+04;
  double l = 1.3043813184290941E+04;
  double m = -3.2144973099647768E+03;
  double n = 5.2385290226511984E+02;
  double o = -5.0749204923436430E+01;
  double p = 2.2128071175584232E+00;

  temp = p;
  temp = temp * x_in + o;
  temp = temp * x_in + n;
  temp = temp * x_in + m;
  temp = temp * x_in + l;
  temp = temp * x_in + k;
  temp = temp * x_in + j;
  temp = temp * x_in + i;
  temp = temp * x_in + h;
  temp = temp * x_in + g;
  temp = temp * x_in + f;
  temp = temp * x_in + e;
  temp = temp * x_in + d;
  temp = temp * x_in + c;
  temp = temp * x_in + b;
  temp = temp * x_in + a;
  return temp;
}

void SharpIrDM2Y3A003K0F::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{

  Position pos;

  if (dGeomGetSpace(geom1) == spaceID_ || dGeomGetSpace(geom2) == spaceID_)
  {
    pos.x = contact->geom.pos[0];
    pos.y = contact->geom.pos[1];
    pos.z = contact->geom.pos[2];

    for(unsigned int i = 0; i < irObj_.size(); ++i)
    {
      /* if a ray is collided with something than calculate the new length and
       * set color to red */
      if(irObj_[i].ray == geom1 || irObj_[i].ray == geom2)
      {
        irObj_[i].actualLength = 
          sqrt( (pos.x-irObj_[i].pos.x)*(pos.x-irObj_[i].pos.x) 
              + (pos.y-irObj_[i].pos.y)*(pos.y-irObj_[i].pos.y) 
              + (pos.z-irObj_[i].pos.z)*(pos.z-irObj_[i].pos.z) ); 
        drawManager_->changeColor(irObj_[i].ray, 1.0f, 0.0f, 0.0f);
        irObj_[i].isInContact = true;
      }
    }
  }
}

void SharpIrDM2Y3A003K0F::createRay(IrObj *irObj, Position *dir)
{
  irObj->ray = dCreateRay(spaceID_, irObj->length);

  drawManager_->addGeom(irObj->ray);

  irObj->direction.x = dir->x;
  irObj->direction.y = dir->y;
  irObj->direction.z = dir->z;

  dGeomRaySet(irObj->ray, irObj->pos.x, irObj->pos.y, irObj->pos.z,
      irObj->direction.x, irObj->direction.y, irObj->direction.z);

  irObj_.push_back(*irObj);
}

void SharpIrDM2Y3A003K0F::multiplyMatrix(dMatrix3 m1, double angleX, double angleY,
    dMatrix3 *result)
{
  dQuaternion q1, q2, q3;
  dMatrix3 tmpRot;

  dRFromEulerAngles (tmpRot, angleX, angleY, 0.0);

  dRtoQ(m1, q1);
  dRtoQ(tmpRot, q2);

  dQMultiply0 (q3, q1, q2);

  dQtoR(q3, *result);
}

double SharpIrDM2Y3A003K0F::getMinValue()
{
  return SHARP_DM2Y3A003K0F_MIN_SENSOR_VALUE;
}

double SharpIrDM2Y3A003K0F::getMaxValue()
{
  return SHARP_DM2Y3A003K0F_MAX_SENSOR_VALUE;
}
