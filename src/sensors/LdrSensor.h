/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _LDR_SENSOR_H_
#define _LDR_SENSOR_H_

#include <base/Sensor.h>
#include <env/LightManager.h>
#include <gui/DrawManager.h>
#include <description/DescriptionHelper.h>
#include <base/MappingFunction.h>
#include <vector>

class LdrSensor : public Sensor 
{
  public: 
    LdrSensor(DrawManager *dm, dSpaceID s);
   ~LdrSensor();

   void create(LightManager *lm, Position startPos, dBodyID *sensorBody,
       Position rotation, double spreadAngleX, double spreadAngleY);
 
   void updateSensor();

   vector<double> getValues();
   
   int getNumberOfValues();

   void selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

  private:
   struct LdrRay
   {
     dGeomID ray;
     double length;
     /* momentary starting position of the ray */
     Position pos;
     /* direction vector of the ray */
     Position direction;   
     /* the original length */
     bool isInContact;
     /* the momentary length */
     double actualLength;
     double angle;
   };
   
   double getVectorAngle(Position a, Position b);

   DrawManager *drawManager_;
   dSpaceID spaceID_;

   vector<struct LdrRay> rays_;
   dBodyID *sensorBody_;
   MappingFunction *map_;
   /* vector between centre of the body and position of the ray */
   Position posOffset_;
   /* important for the spread angle */
   Position headingDirection_;
   LightManager *lightManager_;

   /* spread angle of the sensor */
   double spreadAngleX_;

};
#endif //_LdrSensor_H_ 
