/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 

#include "GenericRotationSensor.h"

GenericRotationSensor::GenericRotationSensor(DrawManager *drawManager, dSpaceID s)
{ 
  _drawManager = drawManager;
  _spaceID     = s;
  _endRange    = 0;
  _startRange  = 0;
  sensorValues.resize(1);
}

GenericRotationSensor::~GenericRotationSensor()
{ }

void GenericRotationSensor::create(
            int type, int orderDerivative,
            double startRange, double endRange, 
            Position rotation, Position startPos, 
            bool drawRayRot, bool drawRayRef,
            double lengthRayRot, double lengthRayRef,
            int rayRotType, int rayRefType,
            Color rayRotColor, Color rayRefColor,
            dBodyID *sensorBody)
{
  Position tmpDir1;
 
  sensorBody_ = sensorBody;
  
  const dReal *bodyPos = dBodyGetPosition(*sensorBody);
  
  _type               = type;

  switch(_type)
  {
    case G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW: 
    case G_ROT_SENSOR_TYPE_UNIT_SPHERE_XYZ: 
      sensorValues.resize(3);
      break;
    case G_ROT_SENSOR_TYPE_VECTOR_ANGLE:
      sensorValues.resize(1);
      break;
    default:
      break;
  }

  _orderDerivative    = orderDerivative;
  _endRange           = endRange;
  _startRange         = startRange;
  _posRay[0].x        = startPos.x;
  _posRay[0].y        = startPos.y;
  _posRay[0].z        = startPos.z;
  _posRay[1].x        = startPos.x;
  _posRay[1].y        = startPos.y;
  _posRay[1].z        = startPos.z;
  _directionRay[0].x  = rotation.x;
  _directionRay[0].y  = rotation.y;
  _directionRay[0].z  = rotation.z;
  _directionRay[1].x  = rotation.x;
  _directionRay[1].y  = rotation.y;
  _directionRay[1].z  = rotation.z;
  _drawRay[0]         = drawRayRot;
  _drawRay[1]         = drawRayRef;
  _rayType[0]         = rayRotType;
  _rayType[1]         = rayRefType;
  _lengthRay[0]       = lengthRayRot;
  _lengthRay[1]       = lengthRayRef;
  _actualLengthRay[0] = _lengthRay[0];
  _actualLengthRay[1] = _lengthRay[1];
  _rayColor[0].r      = rayRotColor.r;
  _rayColor[0].g      = rayRotColor.g;
  _rayColor[0].b      = rayRotColor.b;
  _rayColor[0].alpha  = rayRotColor.alpha;
  _rayColor[1].r      = rayRefColor.r;
  _rayColor[1].g      = rayRefColor.g;
  _rayColor[1].b      = rayRefColor.b;
  _rayColor[1].alpha  = rayRefColor.alpha;

  /* calculate the vector between the centre of sensor body and the startpos of
   * the ir sensor */
  posOffset_.x    = startPos.x-bodyPos[0];
  posOffset_.y    = startPos.y-bodyPos[1];
  posOffset_.z    = startPos.z-bodyPos[2];

  /* transform the given rotation to a matrix */
  dRFromEulerAngles (_referenceRayRotation, rotation.x, rotation.y, rotation.z);
 
  /* at startup both rays (ref + rot) point in the same direction */
  _directionRay[0].x = _referenceRayRotation[2];
  _directionRay[0].y = _referenceRayRotation[6];
  _directionRay[0].z = _referenceRayRotation[10];
  _directionRay[1].x = _directionRay[0].x;
  _directionRay[1].y = _directionRay[0].y;
  _directionRay[1].z = _directionRay[0].z;

  this->createRay(0);
  this->createRay(1);

  //this->updateSensor();
}

// update: for the Ref: only position is updated
//         for the Rot: position + rotation is updated
void GenericRotationSensor::updateSensor()
{
  Position newDir, newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  vector<double> tmp;
  tmp.resize(3);

  // has to calculate only once, because start position is the same for all 
  //   rays
  newPosOff.x = bodyRot[0]*posOffset_.x + bodyRot[1]*posOffset_.y +
    bodyRot[2]*posOffset_.z;
  newPosOff.y = bodyRot[4]*posOffset_.x + bodyRot[5]*posOffset_.y +
    bodyRot[6]*posOffset_.z;
  newPosOff.z = bodyRot[8]*posOffset_.x + bodyRot[9]*posOffset_.y +
    bodyRot[10]*posOffset_.z;

  for(unsigned int i = 0; i < 2; ++i)
  {
    // rotation update is only done for rot, ref keeps orig. rotation
    if(i==0)
    {
      newDir.x = bodyRot[0]*_directionRay[1].x + bodyRot[1]*_directionRay[1].y 
        + bodyRot[2]*_directionRay[1].z;
      newDir.y = bodyRot[4]*_directionRay[1].x + bodyRot[5]*_directionRay[1].y 
        + bodyRot[6]*_directionRay[1].z;
      newDir.z = bodyRot[8]*_directionRay[1].x + bodyRot[9]*_directionRay[1].y 
        + bodyRot[10]*_directionRay[1].z;
      
      _directionRay[i].x = newDir.x;
      _directionRay[i].y = newDir.y;
      _directionRay[i].z = newDir.z;
    }

    // position update is done for both rot and ref
    _posRay[i].x = bodyPos[0] + newPosOff.x;
    _posRay[i].y = bodyPos[1] + newPosOff.y;
    _posRay[i].z = bodyPos[2] + newPosOff.z;

    dGeomRaySet(_ray[i], _posRay[i].x, _posRay[i].y, _posRay[i].z, 
                _directionRay[i].x, _directionRay[i].y, _directionRay[i].z);
  }

  //tmp = this->getValues(); // check sensor without client Com
}

vector<double> GenericRotationSensor::getValues()
{
  //  this->updateSensor();
  switch(_type)
  {
    case G_ROT_SENSOR_TYPE_PITCH_ROLL_YAW: 
      getBodyRotationAsEuler(&(sensorValues[0]), &(sensorValues[1]),
          &(sensorValues[2]));
      Y_DEBUG("GenericRotationSensor:getValues(): = rx:%f ry:%f rz:%f\n",
          sensorValues[0], sensorValues[1], sensorValues[2]);
      break;
    case G_ROT_SENSOR_TYPE_UNIT_SPHERE_XYZ: 
      // use the transpose of the rotation matrix that was used to initally
      // rotate the reference vector, this way the sensor values are relative to
      // the reference vector, normalize the vector (length = 1)
      _tmpVector [0] = _referenceRayRotation[0]  * _directionRay[0].x 
        + _referenceRayRotation[4]  * _directionRay[0].y 
        + _referenceRayRotation[8]  * _directionRay[0].z;
      _tmpVector [1] = _referenceRayRotation[1]  * _directionRay[0].x 
        + _referenceRayRotation[5]  * _directionRay[0].y 
        + _referenceRayRotation[9]  * _directionRay[0].z;
      _tmpVector [2] = _referenceRayRotation[2]  * _directionRay[0].x 
        + _referenceRayRotation[6]  * _directionRay[0].y 
        + _referenceRayRotation[10] * _directionRay[0].z;

      dNormalize3(_tmpVector);

      sensorValues[0] = _tmpVector[0];
      sensorValues[1] = _tmpVector[1];
      sensorValues[2] = _tmpVector[2];

      Y_DEBUG("GenericRotationSensor:getValues(): = x:%f y:%f z:%f\n",
          sensorValues[0], sensorValues[1], sensorValues[2]);
      break;
    case G_ROT_SENSOR_TYPE_VECTOR_ANGLE:
      // Calculate the absolute angle between the reference ray and the rotation
      // ray, always return the smaller of the two possible results
      sensorValues[0] = 
        acos( (  _directionRay[0].x * _directionRay[1].x 
               + _directionRay[0].y * _directionRay[1].y 
               + _directionRay[0].z * _directionRay[1].z
              )
              /
              (  sqrt(  _directionRay[0].x * _directionRay[0].x
                      + _directionRay[0].y * _directionRay[0].y
                      + _directionRay[0].z * _directionRay[0].z) 
               * sqrt(  _directionRay[1].x * _directionRay[1].x
                      + _directionRay[1].y * _directionRay[1].y
                      + _directionRay[1].z * _directionRay[1].z)
              )
            ) 
        * (M_PI/360); 
      
      if(sensorValues[0] < 0)
        sensorValues[0] *= -1;

      if(sensorValues[0] > M_PI)
        sensorValues[0] = 2 * M_PI - sensorValues[0];
                         
      Y_DEBUG("GenericRotationSensor:getValues(): = %f\n",
          sensorValues[0]);
      break;
    default:
      break;
  }

  return sensorValues;
}

void GenericRotationSensor::getBodyRotationAsEuler(double* rx, double* ry,
    double* rz)
{
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  // following is copied from ode/src/collision_util.cpp
	*ry = asin(bodyRot[0 * 4 + 2]);
	if (*ry < M_PI /2)
	{
		if (*ry > -M_PI /2)
		{
			*rx = atan2(-bodyRot[1*4 + 2], bodyRot[2*4 + 2]);
			*rz = atan2(-bodyRot[0*4 + 1], bodyRot[0*4 + 0]);
		}
		else
		{
			// not unique
			*rx = -atan2(bodyRot[1*4 + 0], bodyRot[1*4 + 1]);
			*rz = 0.0;
		}
	}
	else
	{
		// not unique
		*rx = atan2(bodyRot[1*4 + 0], bodyRot[1*4 + 1]);
		*rz = 0.0;
	}
}

void GenericRotationSensor::selfCollide(dGeomID geom1, dGeomID geom2, 
                                        struct dContact* contact)
{
  // unused for this sensor type
}

void GenericRotationSensor::createRay(int rayNum)
{
  _ray[rayNum] = dCreateRay(_spaceID, _lengthRay[rayNum]);
  
  if(_drawRay[rayNum]==true)
  {
    _drawManager->addGeom(_ray[rayNum]);
  }
  
  dGeomRaySet(_ray[rayNum], _posRay[rayNum].x, _posRay[rayNum].y,
      _posRay[rayNum].z, _directionRay[rayNum].x, _directionRay[rayNum].y,
      _directionRay[rayNum].z);
  _drawManager->changeColor(_ray[rayNum], _rayColor[rayNum].r,
      _rayColor[rayNum].g, _rayColor[rayNum].b);
}

//
// TODO:
//
// - add the 1st + 2nd derivative option
//   --> we should do this in updateSensor and not in getValues (higher update
//       frequency --> more reliable values)
// - add an option where we return pitch and roll values relative to the
//   longitudinal and transverse body axes and the gravitation vector, yaw
//   should only be given as speed or accel not as absolute value
//


