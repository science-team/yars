/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/



#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <gui/DrawManager.h>
#include <env/Environment.h>

#include <GL/glut.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/gl.h>
#include <math.h>
#include <iostream>


class Camera 
{

  public:

    Camera(int light, int xPix, int yPix);

    ~Camera();

    void create(DrawManager *drawManager, Environment *environment, GLfloat angle, int colorType);

    void setLightMaterialAndPerspective(GLfloat angle);

    // method, only used by CAMERA_TYPE camera-objects.
    // render scene from camera-viewpoint to a texture and visualize the texture 
    void drawScene(Position position, Position rotation, const dReal *bodyRot);

    // return the pixel values of the defined colorType and size.
    vector<double> getPixelValues();

    int getWindowId();

    void reset();

  private:

    int _window;

    int _windowWidth;

    int _windowHeight;

    int _xPix;

    int _yPix;

    GLfloat _openingAngle;

    void *idle_func;

    void *display_func;

    void *mouse_func;

    void *motion_func;

    void *key_func;

    DrawManager *_drawManager;

    Environment *_environment;

    RenderTexture *_renderTexture;

    int maxS;

    int maxT;

    GLuint iTextureProgram;

    GLuint iPassThroughProgram;

    vector<double> _pixelValues;

    bool _realLight;

};

#endif
