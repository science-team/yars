/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "LdrSensor.h"
#include <util/defines.h>

LdrSensor::LdrSensor(DrawManager *drawManager, dSpaceID s)
{ 
  drawManager_ = drawManager;
  spaceID_     = s;
  sensorValues.resize(1);
}

LdrSensor::~LdrSensor()
{ }

void LdrSensor::create(LightManager *lm, Position startPos, dBodyID *sensorBody,
    Position rotation, double spreadAngleX, double spreadAngleY)
{
  LdrRay ldrRay;
  dMatrix3 tmpRot;

  spreadAngleX_ = fabs(spreadAngleX);
  sensorBody_ = sensorBody;
  lightManager_ = lm;
  
  const dReal *bodyPos = dBodyGetPosition(*sensorBody);

  /* calculate the vector between the centre of sensor body and the startpos of
   * the ir sensor */
  posOffset_.x    = startPos.x-bodyPos[0];
  posOffset_.y    = startPos.y-bodyPos[1];
  posOffset_.z    = startPos.z-bodyPos[2];

  /* these values are identical for all rays */
  ldrRay.pos          = startPos;
  ldrRay.isInContact  = false;
 
  /* create the heading ray */
  dRFromEulerAngles(tmpRot, rotation.x, rotation.y, rotation.z);
  
  headingDirection_.x = tmpRot[2];
  headingDirection_.y = tmpRot[6];
  headingDirection_.z = tmpRot[10];

  ldrRay.direction = headingDirection_;  

  ldrRay.length = 0.5;
  
  ldrRay.ray = dCreateRay(spaceID_, ldrRay.length);

  drawManager_->addGeom(ldrRay.ray);
  drawManager_->changeColor(ldrRay.ray, 0.5f, 0.5f, 0.0f);

  dGeomRaySet(ldrRay.ray, ldrRay.pos.x, ldrRay.pos.y, ldrRay.pos.z,
      headingDirection_.x, headingDirection_.y, headingDirection_.z);

  rays_.push_back(ldrRay);

  
  Position tmpDir, lightPos; 
  lightManager_->getNumberOfLights() ;

  Y_DEBUG("LdrSensor.CREATE: number of light = %d",
      lightManager_->getNumberOfLights());
  for(unsigned int i = 0; i < lightManager_->getNumberOfLights(); ++i)
  {
    lightPos = lightManager_->getPosition(i);
    tmpDir.x = lightPos.x - startPos.x;
    tmpDir.y = lightPos.y - startPos.y;
    tmpDir.z = lightPos.z - startPos.z;

    
    ldrRay.length = sqrt(tmpDir.x*tmpDir.x + tmpDir.y*tmpDir.y +
        tmpDir.z*tmpDir.z);

    ldrRay.ray = dCreateRay(spaceID_, ldrRay.length);

    drawManager_->addGeom(ldrRay.ray);
    drawManager_->changeColor(ldrRay.ray, 0.75f, 0.75f, 0.0f);
  
    dGeomRaySet(ldrRay.ray, ldrRay.pos.x, ldrRay.pos.y, ldrRay.pos.z,
        tmpDir.x, tmpDir.y, tmpDir.z);

    rays_.push_back(ldrRay);
  }

  this->updateSensor();
}

void LdrSensor::updateSensor()
{
  Position newDir, lightPos, newPosOff;

  const dReal *bodyPos = dBodyGetPosition(*sensorBody_);
  const dReal *bodyRot = dBodyGetRotation(*sensorBody_);

  /* has to calculate only once, because start position is the same for all rays
   * */
  newPosOff.x = bodyRot[0]*posOffset_.x + bodyRot[1]*posOffset_.y +
    bodyRot[2]*posOffset_.z;
  newPosOff.y = bodyRot[4]*posOffset_.x + bodyRot[5]*posOffset_.y +
    bodyRot[6]*posOffset_.z;
  newPosOff.z = bodyRot[8]*posOffset_.x + bodyRot[9]*posOffset_.y +
    bodyRot[10]*posOffset_.z;

  newDir.x = bodyRot[0]*rays_[0].direction.x + bodyRot[1]*rays_[0].direction.y +
    bodyRot[2]*rays_[0].direction.z;
  newDir.y = bodyRot[4]*rays_[0].direction.x + bodyRot[5]*rays_[0].direction.y +
    bodyRot[6]*rays_[0].direction.z;
  newDir.z = bodyRot[8]*rays_[0].direction.x + bodyRot[9]*rays_[0].direction.y +
    bodyRot[10]*rays_[0].direction.z;

  headingDirection_ = newDir;
  
  rays_[0].pos.x = bodyPos[0] + newPosOff.x;
  rays_[0].pos.y = bodyPos[1] + newPosOff.y;
  rays_[0].pos.z = bodyPos[2] + newPosOff.z;

  dGeomRaySet(rays_[0].ray, rays_[0].pos.x, rays_[0].pos.y, rays_[0].pos.z,
        headingDirection_.x, headingDirection_.y, headingDirection_.z);
 

  for(unsigned int i = 1; i < rays_.size(); ++i)
  {
    lightPos = lightManager_->getPosition(i-1);
    
    rays_[i].pos.x = bodyPos[0] + newPosOff.x;
    rays_[i].pos.y = bodyPos[1] + newPosOff.y;
    rays_[i].pos.z = bodyPos[2] + newPosOff.z;

    newDir.x = lightPos.x - rays_[i].pos.x;
    newDir.y = lightPos.y - rays_[i].pos.y;
    newDir.z = lightPos.z - rays_[i].pos.z;

    rays_[i].length = sqrt(newDir.x*newDir.x + newDir.y*newDir.y +
        newDir.z*newDir.z);

    rays_[i].angle = this->getVectorAngle(headingDirection_, newDir);

    dGeomRaySet(rays_[i].ray, rays_[i].pos.x, rays_[i].pos.y, rays_[i].pos.z,
        newDir.x, newDir.y, newDir.z);
    
    dGeomRaySetLength(rays_[i].ray, rays_[i].length);
    
    /* means was in contact one simulation step before, but now it isn't so
     * draw the geom again*/
    if(rays_[i].isInContact)
    {
      drawManager_->addGeom(rays_[i].ray);
      drawManager_->changeColor(rays_[i].ray, 1.0f, 1.0f, 0.0f);
      rays_[i].isInContact = false;
    }
    /* if the angle between the heading direction and the ray is bigger than
     * the spreadangle, than remove the ray from drawing */
    if(fabs(rays_[i].angle) > spreadAngleX_ )
    {
      drawManager_->removeGeom(rays_[i].ray);
      rays_[i].isInContact = true;
    }
  }
}

vector<double> LdrSensor::getValues()
{
  double val = lightManager_->getAmbientIntensity();

  Y_DEBUG("LdrSensor::getValue().val = %f (size = %d)", val, rays_.size());

  for(unsigned int i = 1; i < rays_.size(); ++i)
  {
    vector<double> sensorValue;
    if(!rays_[i].isInContact)
    {
      val += lightManager_->getLightIntensity(i-1, rays_[i].length,
          rays_[i].angle, spreadAngleX_);
      
      //      old implementation:
      //      val = MAX(val, 
      //          lightManager_->getLightIntensity(
      //            i-1, 
      //            rays_[i].length, 
      //            rays_[i].angle, spreadAngleX_));
      Y_DEBUG("LdrSensor::getValue().getLightIntensity  = %f", 
          lightManager_->getLightIntensity(i-1, rays_[i].length,
            rays_[i].angle, spreadAngleX_));

      Y_DEBUG("LdrSensor::getValue().val (in loop) = %f", val);
    }
  }
  
  Y_DEBUG("LdrSensor::getValue().val (return) = %f", val);
  sensorValues[0] = val;
  return sensorValues;
}

void LdrSensor::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{

  Position pos;

  
  
  if (dGeomGetSpace(geom1) == spaceID_ || dGeomGetSpace(geom2) == spaceID_)
  {
    pos.x = contact->geom.pos[0];
    pos.y = contact->geom.pos[1];
    pos.z = contact->geom.pos[2];

    for(unsigned int i = 1; i < rays_.size(); ++i)
    {
      /* if a ray is collided with something than calculate the new length and
       * and remove it from the drawing vector */
      if( !rays_[i].isInContact && (rays_[i].ray == geom1 || rays_[i].ray ==
            geom2) )
      {
        drawManager_->removeGeom(rays_[i].ray);
        rays_[i].isInContact = true;
      }
    }
  }
}

double LdrSensor::getVectorAngle(Position a, Position b)
{
  double ret;
  ret = (a.x*b.x + a.y*b.y + a.z*b.z) / ( sqrt(a.x*a.x + a.y*a.y + a.z*a.z) *
      sqrt(b.x*b.x + b.y*b.y + b.z*b.z) );
 
  return acos(ret);
}
