/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <sys/time.h>       /* for timer calculations as realtime            */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <boost/program_options.hpp>

#include <ode/ode.h>

#include <base/World.h>
#include <base/RobotHandler.h>
#include <env/Environment.h>

#include <gui/DrawManager.h>
#include <net/StandardCommunication.h>

#include <description/MoveableDescription.h>
#include <description/EnvironmentDescription.h>
#include <description/SimulationDescription.h>
#include <description/SensorDescription.h>
#include <description/RobotDescription.h>

#include <parser/dom/DOMParser.h>

#include <util/defines.h>
#include <util/RandomiseFunctions.h>
#include <util/io/FileSystemOperations.h>

#include <export/RobotInfoToFilePrinter.h>

#include <gui/GlWindow.h>
#include <base/CameraHandler.h>

#include <container/YarsContainers.h>

/* Simulation step size */
#define STEP        0.010
#define UPDATE_FREQ 4
#define SIM_FREQ    100
#define STEPTIMER   10
#define STEP_STEP   1    // Stepsize by which the stepsize is inc'd/dec'd

/*      start       position of the robot */
#define START_POS_X 0.0
#define START_POS_Y 0.0
#define START_POS_Z 0.0

// nearCallback config stuff
#define MAX_COLLISION_POINTS 100

#define RIGHT 0
#define LEFT 1
#define MIDDLE 2

#define PRINTOUT_FPS_PERIOD  1000.0  // printout FPS every x [ms]

#ifdef USE_LOG4CPP_OUTPUT
log4cpp::Appender* app2 = NULL;
#endif

using namespace std;
namespace po = boost::program_options;

string              currPath(".");
string              homePath(getenv("HOME"));
string              etcPath("/etc");
string              yarsStandardInstallPath("/usr/local/share/yars");
string              privateYarsDir(".yars");
string              yarsExamplesDir("examples");
string              dirDelimiter("/");

string*             xmlPath                        = NULL;
string              xmlDirName("xml");
vector<string>      xmlPathCandidates;

string*             yarsRCPath                     = NULL;
string*             texturesPath                   = NULL;
string*             xsdPath                        = NULL;
string*             libPath                        = NULL;
string              yarsRCName(".yarsrc");
string              texturesDirName("textures");
string              xsdDirName("xsd");
string              libDirName("lib");
vector<string>      yarsRCPathCandidates;
vector<string>      texturesPathCandidates;
vector<string>      xsdPathCandidates;
vector<string>      libPathCandidates;

// Search for paths in defined order
void findPaths()
{
  yarsRCPathCandidates.push_back(currPath);
  yarsRCPathCandidates.push_back(homePath);
  yarsRCPathCandidates.push_back(etcPath);
  texturesPathCandidates.push_back(currPath);
  texturesPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir);
  texturesPathCandidates.push_back(yarsStandardInstallPath);
  xsdPathCandidates.push_back(currPath);
  xsdPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir);
  xsdPathCandidates.push_back(yarsStandardInstallPath);
  libPathCandidates.push_back(currPath);
  libPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir);
  libPathCandidates.push_back(yarsStandardInstallPath + dirDelimiter +
      yarsExamplesDir);

  yarsRCPath   = FileSystemOperations::getFirstExistingDirContainingFile(
      yarsRCPathCandidates, &yarsRCName);
  texturesPath = FileSystemOperations::getFirstExistingDirContainingDir(
      texturesPathCandidates, &texturesDirName);
  xsdPath      = FileSystemOperations::getFirstExistingDirContainingDir(
      xsdPathCandidates, &xsdDirName);
  libPath      = FileSystemOperations::getFirstExistingDirContainingDir(
      libPathCandidates, &libDirName);

  FileSystemOperations::checkValidPathFromAlternatives(&yarsRCName, yarsRCPath,
      &yarsRCPathCandidates, false, "yars-config-file");
}


// **************************************************************************
// command line arguments
// **************************************************************************

int simFreqParam;
int behFreqParam;
string xmlFileParam     = "";
string path_to_xsd      = "";
string path_to_textures = "";
string path_to_libs     = "";

po::options_description generic("Generic options");
po::options_description configuration("Configuration");
po::options_description path("Path configurations");
po::options_description cmdline_options;
po::options_description config_file_options;
po::options_description visible("Usage: yars [options]");

po::positional_options_description positional;
po::variables_map vm;

void setupCommandLineOptions()
{
  generic.add_options()
    ("help", "produce help message")
    ("pause,p", "start paused")
    ("version", "print version information and exit")
    ("license", "print license information and exit");

  configuration.add_options()
    ("simfreq,s", po::value<int>(&simFreqParam), 
     "physical simulation frequency (default 100 [Hz])")
    ("ctrlfreq,c", po::value<int>(&behFreqParam),
     "control             frequency (default  25 [Hz], "
     "precondition: simfreq % ctrlfreq == 0)")
    ("nocomm", "do not initialise communication")
    ("notex", "initially disable textures (openGL)")
    ("noshadow", "initially disable shadows (openGL)")
#ifdef USE_LOG4CPP_OUTPUT
    ("info", "print what is being done")
    ("debug", "print debug information")
    ("silent", "be silent, no printouts at all")
#endif
    ("XML_FILE,d", po::value<string>(&xmlFileParam), "the xml description file to be loaded at start-up")
    ("reload,r", "reload the xml description file before every reset")
    ("realtime", "enable realtime")
    ("pFPS", "printout framerate")
    ("noSimVis", "no Visualization, e.g. for cluster mode")
    ("notrace", "deactivate traces on start up (default is on)")
    ("psp", "print Segment Positions to file, default SegmentPosOut.txt");

  path.add_options()
    // TODO change default path in dependence of operating system
    ("xsd,x", po::value<string>(&path_to_xsd), "path to <path>/xsd")
    ("textures,t", po::value<string>(&path_to_textures), "path to <path>/textures")
    ("lib,l", po::value<string>(&path_to_libs), "path to <path>/lib");

  positional.add("XML_FILE", -1); // default option, if no -d is given the last is interpreted as xml file

  cmdline_options.add(generic).add(configuration).add(path);

  config_file_options.add(configuration).add(path);

  visible.add(generic).add(configuration).add(path);
}


// **************************************************************************
// end of command line arguments
// **************************************************************************

StandardCommunication    *standardCommunication   = NULL;
EnvironmentDescription   *envDescription          = NULL;
MoveableDescription      *moveableDescription     = NULL;
SimulationDescription    *simDescription          = NULL;
World                    *world                   = NULL;
RobotHandler             *robotHandler            = NULL;
Environment              *environment             = NULL;
DrawManager              *drawManager             = NULL;
RobotInfoToFilePrinter   *robotInfoToFilePrinter  = NULL;

GlWindow                 *worldWindow             = NULL ;
CameraHandler            *robotViews              = NULL;

double                  stepSize            = STEP;
int                     simFreq             = SIM_FREQ;
int                     updateFreq          = UPDATE_FREQ;
unsigned long           stepTimer           = STEPTIMER;
unsigned long           stepTimerRT         = STEPTIMER;   // realTime stepTimer
int                     drawStep            = 1;           // drawing every drawStep time

unsigned int            simTime             = 0;
unsigned int            oldSimTime          = 0;
//!defines wether communication is on (1) or off (0)
int                     clientCom           = 1;

//!long values for timer calculations
timeval                 getTimeOfDayTime;
unsigned long           t1, t2, t3;
unsigned long           actualLongTime      = 0;
unsigned long           lastLongTime        = 0;
bool                    longTimeOverflow    = false;

//!boolean switches
bool                    draw_mode           = true;        //!drawing        on/off
bool                    realtime_mode       = false;       //!realtime       on/off
bool                    printFPS            = false;       //!print FPS      on/off
bool                    followFlag          = false;       //!camera follow  on/off
bool                    reloadFileOnReset   = false;
bool                    simVisualization    = true;        //!simVis(OpenGL) on/off
bool                    printSegPos         = false;       //!printSegPos    on/off
bool                    paused              = false;
bool                    singleStep          = false;
bool                    singleStepModus     = false;
bool                    useTextures         = true;
bool                    useTraces           = true;
/*  *camera positions */
static float            xyz[3];
static float            hpr[3];
/*  *camera follow stuff */
float                   pos[3], rot[3], oldRoboPos[3];
static dBodyID*         mainSegmentID;
const  dReal*           roboPos; 
const  dReal*           segmentPos; 

/*  *forward declarations */
static void             resetSimulation();
static void             nextTry(unsigned int seed);
static void             reinit(unsigned int seed);

/// the field of view angle, in degrees, in the y direction
GLdouble view_angle_y;
/// the aspect ratio that determines the field of view in the x direction
GLdouble aspect;
/// the distance from the viewer to the near clipping plane
GLdouble z_near;
/// the distance from the viewer to the far clipping plane
GLdouble z_far;

int   pressedButton;
int   mousemotion;
int   mousex;
int   mousey;

float openingAngle;

int    counter;
long   startTime;

void printCopyrightInfo()
{
  Y_INFO("---------------------------------------------------------------");
  Y_INFO("| Yet Another Robot Simulator (YARS).                         |");
  Y_INFO("| Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and |");
  Y_INFO("| Steffen Wischmann. All rights reserved.                     |");
  Y_INFO("| Email: {keyan,twickel,swischmann}@users.sourceforge.net     |");
  Y_INFO("| Web: http://sourceforge.net/projects/yars                   |");
  Y_INFO("| For a list of contributors see the file AUTHORS.            |");
  Y_INFO("| YARS is distributed under the GNU General Public License,   |");
  Y_INFO("| either version 2 of the License, or (at your option) any    |");
  Y_INFO("| later version. The license can be found in the file         |");
  Y_INFO("| COPYING, if not write to the Free Software Foundation.      |");
  Y_INFO("---------------------------------------------------------------");
  Y_INFO("                                                               ");
}

void printStartupText()
{
  // empty so far
}

void printSetupInfo()
{
  Y_INFO("YARS setup is now as follows:");
  Y_INFO("stepSize:   %f", stepSize);
  Y_INFO("simFreq:    %i", simFreq);
  Y_INFO("updateFreq: %i", updateFreq);
  Y_INFO("stepTimer:  %ld", stepTimer);
}

void printKeyCommands()
{
  Y_INFO("---------------------------------------------------------------");
  Y_INFO("| ### KEYS ###                                                |");
  Y_INFO("|   h : help (printout this text)                             |");
  Y_INFO("|   R : Reset                                                 |");
  Y_INFO("|   d : drawing on/off                                        |");
  Y_INFO("|   f : follow camera mode (1st robot)                        |");
  Y_INFO("|   v : restore initial viewpoint                             |");
  Y_INFO("|   r : realtime on/off                                       |");
  Y_INFO("|   + : speed up simulation (realtime has to be on)           |");
  Y_INFO("|   - : slow down simulation (realtime has to be on)          |");
  Y_INFO("|   0 : speed up draw update (default=1=each step)            |");
  Y_INFO("|   9 : slow down draw update (>1 ^= every nth step)          |");
  Y_INFO("|   c : hide camera-windows                                   |");
  Y_INFO("|   C : show camera-windows                                   |");
  Y_INFO("|   p : printout frame rate every %5.2f [ms]                |", PRINTOUT_FPS_PERIOD);
  Y_INFO("|   P : pause    on/off                                       |");
  Y_INFO("|   s : do a single step (in pause mode)                      |");
  Y_INFO("|   T : textures on/off                                       |");
  Y_INFO("|   V : printout camera viewport                              |");
  Y_INFO("|   X : exit simulation                                       |");
  Y_INFO("---------------------------------------------------------------");
}


static unsigned long getLongTime()
{
  gettimeofday(&getTimeOfDayTime, 0);

  actualLongTime = (  (getTimeOfDayTime.tv_sec  % 10000) * 1000 
                     + getTimeOfDayTime.tv_usec          / 1000);
  if(actualLongTime < lastLongTime)
  {
    longTimeOverflow = true;
  }
  lastLongTime   = actualLongTime;

  return actualLongTime;
}

static void init()
{
  Y_DEBUG("init() called");
  envDescription      = new EnvironmentDescription();
  moveableDescription = new MoveableDescription();
  simDescription      = new SimulationDescription();
  world               = new World();
  robotViews          = CameraHandler::instance();
}

static void deinit()
{
  Y_DEBUG("deinit() called");
  delete world;
  delete simDescription;
  delete moveableDescription;
  delete envDescription;
  world               = NULL;
  simDescription      = NULL;
  moveableDescription = NULL;
  envDescription      = NULL;
  //delete robotViews;
}

static void stopSimulation()
{
  robotViews->reset();
  delete robotHandler;
  delete environment;
  delete drawManager;
  robotHandler = NULL;
  environment  = NULL;
  drawManager  = NULL;
}

void closeYars()
{
  if (clientCom)
  {
    delete standardCommunication;
    standardCommunication = NULL;
  }
  stopSimulation();
  deinit();
  delete robotViews;
  robotViews = NULL;
  dCloseODE();
  exit(0);
}

void readXmlDescription()
{
  Y_DEBUG("reading the xml description");
  // TODO path_to_xsd should be in a container

  // search for specified xml file in standard dirs
  xmlPathCandidates.push_back(currPath);
  xmlPathCandidates.push_back(currPath + dirDelimiter + xmlDirName);
  xmlPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir);
  xmlPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir +
      dirDelimiter + xmlDirName);
  xmlPathCandidates.push_back(homePath + dirDelimiter + privateYarsDir +
      dirDelimiter + yarsExamplesDir + dirDelimiter + xmlDirName);
  xmlPathCandidates.push_back(yarsStandardInstallPath + dirDelimiter +
      xmlDirName);
  xmlPathCandidates.push_back(yarsStandardInstallPath + dirDelimiter +
      yarsExamplesDir + dirDelimiter + xmlDirName);

  xmlPath = FileSystemOperations::getFirstExistingDirContainingFile(
      xmlPathCandidates, &xmlFileParam);

  FileSystemOperations::checkValidPathFromAlternatives(&xmlFileParam, xmlPath,
      &xmlPathCandidates, true, "xml-sim-description");
  *xmlPath = *xmlPath + dirDelimiter + xmlFileParam;

  Y_INFO("sending %s to DOMParser",path_to_xsd.c_str());
  DOMParser  *reader = new DOMParser(path_to_xsd);
  reader->read((char*)(*xmlPath).c_str(), moveableDescription, envDescription, simDescription);
  delete reader;
  reader = NULL;
}

/* -- Project specific stuff -------------------------------- begin */
static void nearCallback (void *data, dGeomID o1, dGeomID o2)
{ 
  int i,n;
  int isCollided = 0;

  dBodyID b1;
  dBodyID b2; 

  b1 = dGeomGetBody(o1);
  b2 = dGeomGetBody(o2);

  // connected bodies should not collide
  if (b1 && b2 && dAreConnectedExcluding (b1, b2, dJointTypeContact))
  {
    return;
  }  

  // go through subspaces
  if (dGeomIsSpace (o1) || dGeomIsSpace (o2)) 
  {
    // colliding a space with something
    dSpaceCollide2 (o1, o2, data, &nearCallback);
  }
  else 
  { 
    dContact contact[MAX_COLLISION_POINTS];
    n = dCollide (o1,o2,MAX_COLLISION_POINTS,&contact[0].geom,sizeof(dContact));
    if (n > 0) 
    {
      // for each contact point set its surface properties
      for (i=0; i<n ; i++) 
      {

        isCollided = 0;

        if(dGeomGetClass(o1) == dRayClass || dGeomGetClass(o2) == dRayClass)
        {
          robotHandler->selfCollide(o1, o2, &contact[i]);     
        }
        else
        {
          isCollided = robotHandler->selfCollide(o1, o2, &contact[i]);     
        }

        if (isCollided > 0 )
        {
          dJointID c = dJointCreateContact (world->getWorldID(),
              world->getContactGroupID(), &contact[i]);
          dJointAttach (c,
              dGeomGetBody(contact[i].geom.g1),
              dGeomGetBody(contact[i].geom.g2));
        }
      }  
    }
  }

}

static void simulateStep()
{
  robotHandler->update();
  dSpaceCollide (world->getSpaceID(), 0, &nearCallback);
  world->step(stepSize);
  simTime+=1;
}


static void startSimulation()
{
  if(reloadFileOnReset)
  {
    deinit();
    init();
    readXmlDescription();
  }

  // TODO path_to_textures should be in a container
  drawManager = new DrawManager(path_to_textures, useTraces);
  drawManager->setTextures(useTextures);
  drawManager->setNumberOfDrawLines(
      moveableDescription->getNumberOfDrawLines());
  drawManager->setAxesVisualization(
      simDescription->isAxesVisualization(),
      simDescription->getAxesScaling());

  environment = new Environment(world->getSpaceID(), drawManager);

  environment->create(envDescription);

  if(simVisualization == true)
  {
    environment->getDrawManager()->setTextures(useTextures);
  }

  for(int i = 0; i < 25; ++i)
  {
    world->step(stepSize);
  }

  dWorldID wo = world->getWorldID();
  dSpaceID sp = world->getSpaceID();

  // TODO path_to_libs should be in a container
  robotHandler = new RobotHandler(wo, sp, drawManager, moveableDescription,
      environment, robotViews, path_to_libs);

  // camera following stuff
  // TODO: fix this (probably inside of RobotHandler) to use the first
  // controlled robot if no active robot is available ...
  mainSegmentID = robotHandler->getSegmentBodyID(0,0,0);

  if (clientCom)
  {
    standardCommunication->init(robotHandler, &resetSimulation, &reinit,
        &nextTry);
  }
  
  if(printSegPos)
  {
    robotInfoToFilePrinter->printSegmentNames(robotHandler);
  }

  oldSimTime = simTime = 0;
  t1 = t2 = t3 = getLongTime();
}

static void resetSimulation()
{
  Y_DEBUG("main: resetSimulation         --> start()");
  stopSimulation();
  Y_DEBUG("main: after stopSimulation()");
  startSimulation();
  Y_DEBUG("main: after startSimulation() --> end");
}

static void nextTry(unsigned int seed)
{
  Y_DEBUG("main: CALLING nextTry() with seed=%u",seed);
  reinit(seed);
  resetSimulation();
}


#ifdef USE_LOG4CPP_OUTPUT
static void initLogger()
{
  time_t    time_now;
  struct tm *time_ptr;
  char       string[1024];

  time(&time_now);     /* get time in seconds */

  /* display local time     */
  //pritnf("%s\n\n", asctime(localtime(&time_now)));    

  /* custom build the format of the date/ time */
  time_ptr = gmtime(&time_now);
  strftime(string, 1024, "logs/yars-%F-%H-%M-%S.log", time_ptr);

  /* display new format string */
  //Y_DEBUG("logfile:%s\n\n", string);
  // 1 instantiate an appender object that 
  // will append to a log file
  log4cpp::Appender* app = new 
    log4cpp::FileAppender("FileAppender", string);

//  log4cpp::Appender* app2 = new 
  app2 = new 
    log4cpp::FileAppender("stdout", (fileno(stdout)));

  // 2. Instantiate a layout object
  // Two layouts come already available in log4cpp
  // unless you create your own.
  // BasicLayout includes a time stamp
  
  //  This prints a lot of stuff
  //  log4cpp::Layout* layout = 
  //    new log4cpp::BasicLayout();

  //  This prints a DEBUG/INFO/... prefix
  //  log4cpp::Layout* layout = 
  //    new log4cpp::SimpleLayout();

  //  This prints only the string
  log4cpp::Layout* layout = 
    new log4cpp::PatternLayout();

  // 3. attach the layout object to the 
  // appender object
  //app->setLayout(layout);
  app2->setLayout(layout);

  // 4. Instantiate the category object
  // you may extract the root category, but it is
  // usually more practical to directly instance
  // a child category

  // 5. Step 1 
  // an Appender when added to a category becomes
  // an additional output destination unless 
  // Additivity is set to false when it is false,
  // the appender added to the category replaces
  // all previously existing appenders
  logger.setAdditivity(true);

  // 5. Step 2
  // this appender becomes the only one
  logger.setAppender(app2);
  logger.addAppender(app);

  // 6. Set up the priority for the category
  // and is given INFO priority
  // attempts to log DEBUG messages will fail
  logger.setPriority(log4cpp::Priority::INFO);

}
#endif

static void reinit(unsigned seed)
{
  Y_DEBUG("MAIN:reinit()");
  Y_DEBUG("Using seed %d",seed);
  srand(seed);
  int srand1 = rand();
  int srand2 = rand();
  int srand3 = rand();
  int srand4 = rand();
  srand(srand1);
  robotHandler->randomiseInitialPosition();
  srand(srand2);
  environment->randomisePositions();
  srand(srand3);
  envDescription->randomiseAmbientLight();
  srand(srand4);
}


static void drawObjects()
{
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  // go to the viewpoint, selected with the mouse

  glRotatef(-hpr[0], 1,0,0);
  glRotatef(-hpr[1], 0,1,0);
  glRotatef(-hpr[2], 0 ,0, 1);

  glTranslatef(-xyz[0], -xyz[1], -xyz[2]);

  if(draw_mode)
  {
    // pass viewPoint to drawManager to enable adaptive sky heights
    drawManager->setViewPoint(xyz);
    //environment
    environment->draw();
    // objects
    drawManager->draw();
  }
  else
  {
    // draw only the world
    drawManager->drawWorld();
  }
  glutSwapBuffers();
}


static void restoreInitialViewpoint()
{
  if(simVisualization)
  {
    Position pos  = simDescription->getCameraPos();
    Position rot  = simDescription->getCameraRot();
    xyz[0] = pos.x;
    xyz[1] = pos.y;
    xyz[2] = pos.z;
    hpr[0] = rot.x;
    hpr[1] = rot.y;
    hpr[2] = rot.z;
  }
}

static void command(unsigned char cmd, int x, int y)
{
  // P L E A S E : - keep this in alphabetical order ;-)
  //               - add keys to the printKeyCommands() funtion ;-)
  switch(cmd)
  {
    case '+':
      if(realtime_mode)
      {
        if(stepTimer > 1)
        {
          stepTimer -= STEP_STEP;
          Y_INFO( "increasing simulation speed to %f [Hz] (%fx)",(1000.0 / stepTimer), (1.0 * stepTimerRT / stepTimer));
        }
        else
        {
          Y_INFO("Not possible - already at top speed (1000 [Hz])!");
        }
      }
      else
      {
        Y_INFO("Not possible - Realtime is disabled!");
      }
      break;
    case '-':
      if(realtime_mode)
      {
        stepTimer += STEP_STEP;
        Y_INFO( "decreasing simulation speed to %f [Hz] (%fx)",(1000.0 / stepTimer), (1.0 * stepTimerRT / stepTimer));
      }
      else
      {
        Y_INFO("Not possible - Realtime is disabled!");
      }
      break;
    case '0':
      if(drawStep > 1)
      {
        drawStep--; 
        Y_INFO("increasing draw update frequency to every %d step", drawStep);
      }
      else
      {
        Y_INFO("Not possible - already at max update of 1 !");
      }
      break;
    case '9':
      drawStep++; 
      Y_INFO("decreasing draw update frequency to every %d step", drawStep);
      break;
    case 'c':
      Y_INFO("Hide camera-windows");
      for(int i = 0; i<robotViews->getNumberOfCameras(); i++)
      {
        int camera = robotViews->getCamera(i)->getWindowId();
        glutSetWindow(camera);
        glutHideWindow();
      }
      break;
      //show camera-Windows
    case 'C':
      Y_INFO("Show camera-windows");
      for(int i = 0; i<robotViews->getNumberOfCameras(); i++)
      {
        int camera = robotViews->getCamera(i)->getWindowId();
        glutSetWindow(camera);
        glutShowWindow();
      }
      break;
    case 'd':
      draw_mode=!draw_mode;
      if(draw_mode)
      {
        Y_INFO("Drawing is activated");
      }
      else
      {
        Y_INFO("Drawing is activated");
      }
      break;
    case 'f':
      if(mainSegmentID != NULL)
      {
        followFlag = !followFlag;
        if(followFlag)
        {
          Y_INFO("Camera following is activated");
        }
        else
        {
          Y_INFO("Camera following is deactivated");
        }
        roboPos = dBodyGetPosition(*mainSegmentID);
        oldRoboPos[0] = roboPos[0];
        oldRoboPos[1] = roboPos[1];
        oldRoboPos[2] = roboPos[2];
      }
      else
      {
        Y_INFO("Camera follow mode only supported in UDP Com mode - Sorry");
      }
      break;
      //pause the simulation
    case 'h':
      printKeyCommands();
      break;
      //show camera-Windows
    case 'L':
      reloadFileOnReset = !reloadFileOnReset;
      if(reloadFileOnReset)
      {
        Y_INFO("Reloading activated!\n");
      }
      else
      {
        Y_INFO("Reloading deactivated!\n");
      }
      break;      
    case 'p':
      if(printFPS)
      {
        Y_INFO("Not printing out FPS any more, press p to reenable.");
        printFPS = false;
      }
      else
      {
        Y_INFO("Now printing out FPS every %5.2f [ms], press p to disable.",
            PRINTOUT_FPS_PERIOD);
        printFPS = true;
      }
      break;
    case 'P':
      Y_INFO("Paused");
      if(paused)
        paused = false;
      else
        paused = true;
      break;
      // hide the Camera-Windows
    case 'R':
      if(clientCom == 0)
      {
        Y_DEBUG("RESET");
        reinit(time(NULL));
        resetSimulation();
      }
      else
      {
        Y_INFO("Reset is not possible in Communication Mode!\n");
      }
      break;      
    case 'r':
      realtime_mode =!realtime_mode; 
      if(realtime_mode)
      {
        Y_INFO("Realtime is enabled (%f [Hz]). (To disable press 'r' again!)",(1000.0 / stepTimer));
      }
      else
      {
        stepTimer = stepTimerRT;  // restore original stepTimer value
        Y_INFO("Realtime is disabled! (To enable press 'r' again!)",(1000.0 / stepTimer));
      }
      break;
    case 's':
      if(paused == true)
      {
        singleStep = true;
        Y_INFO("Do a single step.");
      }
      else
      {
        Y_INFO("To do single stepping you have to be in Pause mode \
          (command \"P\").");
      }
      break;
    case 'T':
      useTextures = !useTextures;
      if(useTextures == 0)
      {
        Y_INFO("Textures are off");
      }
      else
      {
        Y_INFO("Textures are on");
      }
      drawManager->setTextures(useTextures);
      environment->getDrawManager()->setTextures(useTextures);
      break;
    case 't':
      useTraces = !useTraces;
      if(useTraces == 0)
      {
        Y_INFO("Traces are off");
      }
      else
      {
        Y_INFO("Traces are on");
      }
      drawManager->setTraces(useTraces);
//      environment->getDrawManager()->setTraces(useTraces);
      break;
    case 'v':
      restoreInitialViewpoint();
      Y_INFO("Restoring original viewpoint!");
      break;
    case 'V':
      Y_INFO("Camera View Port:");
      Y_INFO("XZY = (%f,%f,%f)",xyz[0],xyz[1],xyz[1]);
      Y_INFO("HPR = (%f,%f,%f)",hpr[0],hpr[1],hpr[1]);
      break;
    case 'X':
      Y_INFO("Exiting Simulation due to user request (key 'X')");
      closeYars();
      break;
    default:
      break;
  } 
}

static void communicate()
{
  standardCommunication->communicate(); 
}

void testDescription()
{

  unsigned int robot = 0;
  unsigned int compound = 0;
  unsigned int seg = 1;
  unsigned int motor = 0;
  unsigned int inputNum = 0;
  double value = 1.0;

 
  robotHandler->setMotorValue(robot, compound, seg, motor, inputNum, value);
  robotHandler->setMotorValue(robot, compound, seg+1, motor, inputNum, value-0.1);
  robotHandler->setMotorValue(robot+1, compound, seg, motor, inputNum, value);
  robotHandler->setMotorValue(robot+1, compound, seg+1, motor, inputNum, -value+0.3);

  robotHandler->setMotorValue(robot+2, compound, seg, motor, inputNum, value);
  robotHandler->setMotorValue(robot+2, compound, seg+1, motor, inputNum, value-0.4);

}

void testLights()
{
  LightSource ls;
  ls.pos.x = 0.0;
  ls.pos.y = 0.0;
  ls.pos.z = 11.0;

  ls.type = ED_POINT_LIGHT;

  ls.color.r = 1.0;
  ls.color.g = 1.0;
  ls.color.b = 0.0;

  ls.intensity = 10.0;

  envDescription->addLight(ls);
  envDescription->setAmbientLight(1.0);

  ls.pos.x = 3.0; 
  ls.pos.z = 6.0; 
  envDescription->addLight(ls);

  ls.pos.x = -3.0; 
  ls.pos.y = 2.0; 
  ls.pos.z = 7.0; 
  envDescription->addLight(ls);


}

void testSensors()
{
  SensorStruct s;

  s.type = SD_LDR_SENSOR;
  s.noise = 0.1;
  s.srcCompound = 0;
  s.srcSeg = 0;

  SensorDescription *sD = new SensorDescription(s);

  LdrSensorStruct lS;

  lS.pos.x = 0;
  lS.pos.y = 0;
  lS.pos.z = 14.0;

  lS.startValue    = 0.0;
  lS.endValue      = 255.0;

  lS.startIntensity = 0.2;
  lS.endIntensity   = 5.0;

  sD->setLdrSensor(lS);
  (moveableDescription->getRobotDescription(0))->addSensor(sD);

  delete sD;
  sD = NULL;
}

static void control()
{
  robotHandler->updateControlled(); 
}

static void simLoop (void)
{
  if (realtime_mode)
  {
    t2 = getLongTime();
    t1 += stepTimer;

    if(longTimeOverflow == true)
    {
      // continue and reset flag
      longTimeOverflow = false;
    }
    else
    {
      if (t1 < t2)
      {
        //       Y_WARN(" Warning: one step was not in realtime! (timestamp: %d)\n", 
        //             (int)t2);
      }
      while (t1 > t2)
      {
        // TODO: Verify how long this sleep really takes and 
        //       check if this also works under non-Unix systems ...
        usleep(1000); 
        t2 = getLongTime();    
      } 
    }
  }

  t1 = getLongTime();

  if(printFPS)
  {
    if((t1 - t3) >= PRINTOUT_FPS_PERIOD)
    {
      Y_INFO("%d [frames] in %5.2f [s] = %10.2f [FPS]", 
          (simTime - oldSimTime),
          (PRINTOUT_FPS_PERIOD / 1000.0), 
          ((simTime - oldSimTime) / (PRINTOUT_FPS_PERIOD / 1000.0)));
      oldSimTime = simTime;
      t3 = t1;
    }
  }

  if(!paused || singleStep)
  {
    if(simTime % updateFreq == 0)
    {
      control();
    }

    if((clientCom == 1) && (simTime % updateFreq == 0))
    {
      communicate();
    }

    simulateStep();

    if(singleStep)
    {
      singleStep = false;
    }
  }

  if(mainSegmentID != NULL)
  {
    roboPos = dBodyGetPosition(*mainSegmentID);
  }

  if(simVisualization)
  {
    if(simTime % drawStep == 0)
    {
      if(mainSegmentID != NULL)
      {
        if (followFlag)
        {
          // TODO check for robot index from xml file
          xyz[0] += (roboPos[0] - oldRoboPos[0]);
          xyz[1] += (roboPos[1] - oldRoboPos[1]); 
        }

        oldRoboPos[0] = roboPos[0];
        oldRoboPos[1] = roboPos[1];
        oldRoboPos[2] = roboPos[2];
      }

      worldWindow->setActual();
      drawObjects();
    }
    else
    {
      simLoop();
    }
  }

  if(printSegPos)
  {
    robotInfoToFilePrinter->printSegmentPos(robotHandler);
  }
}


void parseCommandLineArguments(int argc, char **argv)
{
  try 
  {
    int i;

    store(po::command_line_parser(argc, argv).
        options(cmdline_options).positional(positional).run(), vm);

    ostringstream configfile;
    if(yarsRCPath != NULL)
    {
      configfile << *yarsRCPath <<  "/.yarsrc";
    }
    else
    {
      configfile <<  ".yarsrc";
    }

    ifstream ifs2(configfile.str().c_str());
    store(parse_config_file(ifs2, config_file_options), vm);

    notify(vm);

    if (vm.count("help")) {
      stringstream s;
      s << "" << visible;
      Y_INFO("%s", s.str().c_str());
      exit(0);
    }

    if (vm.count("version"))
    {
      Y_INFO("yars version %s (%s)\n", YARS_VERSION, YARS_DATE);
      Y_INFO("For license see: yars --license.\n");
#ifdef USE_LOG4CPP_OUTPUT
      app2->close();
      logger.removeAllAppenders();
#endif
      exit(0);
    }

    if (vm.count("license"))
    {
      printCopyrightInfo();
#ifdef USE_LOG4CPP_OUTPUT
      app2->close();
      logger.removeAllAppenders();
#endif
      exit(0);
    }

    // **************************************************************************
    // checking debug level
    // **************************************************************************
#ifdef USE_LOG4CPP_OUTPUT
    if(vm.count("debug") > 0 && vm.count("silent") == 0)
    {
      logger.setPriority(log4cpp::Priority::DEBUG);
      Y_DEBUG("log level set to DEBUG");
    }

    if(vm.count("info") && vm.count("silent") == 0 && vm.count("debug") == 0)
    {
      logger.setPriority(log4cpp::Priority::INFO);
      Y_INFO("log level set to INFO");
    }

    if(vm.count("silent"))
    {
      logger.setPriority(log4cpp::Priority::FATAL);
      Y_DEBUG("log level set to DEBUG"); // should not show
      Y_INFO("log level set to INFO");   // should not show
    }
#endif

    // **************************************************************************
    // reading xml file
    // **************************************************************************

    if (vm.count("pause"))
    {
      argv[1] = "-pause";
      Y_DEBUG("starting paused");
      paused = true;
    }

    if (vm.count("realtime"))
    {
      realtime_mode = true;
      Y_DEBUG("starting realtime");
    }

    if (vm.count("printFPS"))
    {
      printFPS = true;
      Y_DEBUG("Printing out FPS every %10.0f [ms].", PRINTOUT_FPS_PERIOD);
    }

    if(vm.count("reload"))
    {
      reloadFileOnReset = true;
      Y_DEBUG("setting reloadFileOnReset = true");
    }

    if(vm.count("nocomm"))
    {
      clientCom = 0;
    }

    if(vm.count("notex"))
    {
      useTextures = false;
      // Pass a -notex instead of --notex to dsSimulationLoop
      for(int i=0; i<argc; i++)
      {
        if (strcmp(argv[i],"--notex")==0)
          argv[i] = "-notex";
      }
    }

    if(vm.count("noshadow"))
    {
      // Pass a -noshadow instead of --noshadow to dsSimulationLoop
      for(int i=0; i<argc; i++)
      {
        if (strcmp(argv[i],"--noshadow")==0)
          argv[i] = "-noshadow";
      }
    }

    if(vm.count("noSimVis"))
    {
      simVisualization = false;
    }

    if(vm.count("notrace"))
    {
      useTraces = false;
    }

    if(vm.count("noSimVis"))
    {
      simVisualization = false;
    }

    if(vm.count("pFPS"))
    {
      printFPS = true;
    }

    if(vm.count("psp"))
    {
      printSegPos = true;
      robotInfoToFilePrinter = new RobotInfoToFilePrinter();
    }

    if(vm.count("simfreq"))
    {
      simFreq  = simFreqParam;
      stepSize = 1.0/simFreq;
      Y_DEBUG("setting simFreq (cmdLine)  = %d [Hz] (stepsize = %f)", simFreq,
          stepSize);
    }
    else
    {
      simFreq  = SIM_FREQUENCY_DEFAULT;
      stepSize = 1.0/simFreq;
      Y_DEBUG("setting simFreq (default)  = %d [Hz] (stepsize = %f)", simFreq,
          stepSize);
    }

    if(vm.count("ctrlfreq"))
    {
      updateFreq = behFreqParam;
      Y_DEBUG("setting controlFreq (cmdLine) = %d", updateFreq);
    }
    else
    {
      updateFreq = CONTROL_FREQUENCY_DEFAULT;
      Y_DEBUG("setting controlFreq (default) = %d", updateFreq);
    }

    if(vm.count("xsd"))
    {
      FileSystemOperations::checkValidPath(&path_to_xsd, true, true,
          "xsd-path");
    }
    else
    {
      FileSystemOperations::checkValidPathFromAlternatives(&xsdDirName, xsdPath,
          &xsdPathCandidates, false, "xsd-path");
      if(xsdPath == 0)
      {
        Y_FATAL("No xsd path found. This is a fatal error.\nPlease check your command line arguments and your ~/.yarsrc file.\nExiting");
        exit(-1);
      }
      path_to_xsd = *xsdPath;
    }

    if(vm.count("textures"))
    {
      FileSystemOperations::checkValidPath(&path_to_textures, true, true,
          "textures-path");
    }
    else
    {
      FileSystemOperations::checkValidPathFromAlternatives(&texturesDirName,
          texturesPath, &texturesPathCandidates, false, "textures-path");
      if(texturesPath == 0)
      {
        Y_FATAL("No textures path found. This is a fatal error.\nPlease check your command line arguments and your ~/.yarsrc file.\nExiting");
        exit(-1);
      }
      path_to_textures = *texturesPath;
    }

    if(vm.count("lib"))
    {
      FileSystemOperations::checkValidPath(&path_to_libs, true, true, "lib-path");
    }
    else
    {
      FileSystemOperations::checkValidPathFromAlternatives(&libDirName, libPath,
          &libPathCandidates, false, "lib-path");
      if(libPath == 0)
      {
        Y_FATAL("No libraries path found. This is a fatal error.\nPlease check your command line arguments and your ~/.yarsrc file.\nExiting");
        exit(-1);
      }
      path_to_libs = *libPath;
    }

    if(vm.count("XML_FILE"))
    {
      Y_INFO("IN XML_FILE: path_to_xsd %s", path_to_xsd.c_str());
      readXmlDescription();
    }
    else
    {
      Y_FATAL("Please give an XML file. Use --help for detailed instructions.");
      exit(-1);
    }
  }
  catch(exception& e)
  {
    stringstream s;
    s << "" << visible;
    // Y_INFO("%s", e.what().c_str());
    Y_INFO("Invalid command line parameter. Please check.");
    Y_INFO("%s", s.str().c_str());
    exit(0);

  }
}

void processSimXMLParams()
{
  double simFreqFromXML = simDescription->getPhysSimFrequency();
  double netFreqFromXML = simDescription->getNetFrequency();

  // checking if valid XML data exists 
  if(simFreqFromXML < 0.0 && netFreqFromXML < 0.0)
  {
    Y_INFO("setting simFreq  from XML --> no data");
  }

  // setting the data
  if (simFreqFromXML >= 0.0)
  {
    if(vm.count("simfreq"))
    {
      Y_INFO("WARNING: simulation Frequency settings from XML (= %f [Hz]) overridden by command line (= %d [Hz])!", simFreqFromXML, simFreq);
    }
    else
    {
      simFreq  = (int)simFreqFromXML;
      stepSize = 1.0/simFreq;
      Y_INFO("setting simFreq  from XML = %d [Hz] (stepSize = %f)", simFreq,
          stepSize);
    }
  }

  if (netFreqFromXML >= 0.0)
  {
    if(vm.count("ctrlfreq"))
    {
      Y_INFO("WARNING: control Frequency settings from XML (= %f [Hz]) overridden by command line (= %d [Hz])!", netFreqFromXML, updateFreq);
    }
    else
    {
      updateFreq = (int)netFreqFromXML;
      Y_INFO("setting controlFreq from XML = %d [Hz]", updateFreq);
    }
  }

//  // consistency check and adaptation of associated parameters
  if(vm.count("simfreq") > 0 ||
      vm.count("ctrlfreq") > 0 ||
      simFreqFromXML >= 0.0 ||
      netFreqFromXML >= 0.0)
  {
    if(simFreq % updateFreq != 0)
    {
      Y_FATAL("Inconsistency between simulation frequency (= %d [Hz]) and control frequency (= %d [Hz] (standard, command line and/or XML)) --> not divisible without rest", 
          simFreq, updateFreq);
      exit(-1);
    }
    else
    {
      updateFreq = (simFreq/updateFreq);
      Y_INFO("frequency ratio (physical simulation) : (control) == %d",
          updateFreq);
    }

    stepTimer = (long)(1000.0 * stepSize);
    Y_DEBUG("stepTimer: %f", stepTimer);
    if (stepTimer < 1)
    {
      stepTimer = 1;
      Y_WARN("\nWarning: stepSize is probably too low.\n");
      Y_WARN("--> stepTimer is forced to a minimum value of 1.\n\n");
    }

    stepTimerRT = stepTimer;  // backup because stepTimer might be changed by the user (+, -)
  }

  (*(YarsContainers::SimParams())).stepSize = stepSize;
}


void display( void )
{
  worldWindow->setActual();
  glutPostRedisplay();
}

//notice mouseevents on the simulation-window and save the values for changing the viewport
void mouse(int button, int state, int x, int y)
{
  Y_DEBUG("--> mouse-event caught");
  if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
    mousemotion = 1;
    pressedButton = RIGHT;
    mousex = x;
    mousey = y;
    return;
  }
  if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
    mousemotion = 0;
    return;
  }
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
    pressedButton = LEFT;
    mousemotion = 1;
    mousex = x;
    mousey = y;
    return;
  }
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    mousemotion = 0;
    return;
  }
  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) {
    mousemotion = 1;
    pressedButton = MIDDLE;
    mousex = x;
    mousey = y;
    return;
  }
  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
    mousemotion = 0;
    return;
  }
}

void moveCamera(int mode, int deltax, int deltay)
{
  float side = 0.01f * float(deltax);
  float fwd = (mode==RIGHT) ? (0.01f * float(deltay)) : 0.0f;
  float s = (float) sin (DEG_TO_RAD(hpr[2]));
  float c = (float) cos (DEG_TO_RAD(hpr[2]));

  switch(mode)
  {
    case LEFT:
      hpr[0] += float (deltax) * 0.5f;
      hpr[2] += float (deltay) * 0.5f;
      break;
    case RIGHT:
      xyz[0] += s*side + c*fwd;
      xyz[1] -= c*side + -s*fwd;
      break;
    case MIDDLE:
//       xyz[0] += -s*side + c*fwd;
//       xyz[1] += c*side + s*fwd;
      xyz[2] += 0.01f * float(deltay);
      break;
  }

  for (int i=0; i<3; i++) {
    while (hpr[i] > 180)  hpr[i] -= 360;
    while (hpr[i] < -180) hpr[i] += 360;
  }

}


//Callback Function: reaction to mouse-motion
void motion(int x, int y)
{
  Y_DEBUG("##> motion-event caught");

  if (mousemotion) {
    switch(pressedButton)
    {
      case LEFT:
        // left mouse button rotates the world
        moveCamera(LEFT, y - mousey, x - mousex);
        break;
        // right button changes the viewpoint along the x and y axis (always realtive to actual position)
        //zooming in and out
      case RIGHT:
        //moveCamera(2, x - mousex, y - mousey);
        moveCamera(RIGHT, mousey - y, mousex - x);
        break;
      case MIDDLE:
        //changes the viewpoint along the z-axis
        moveCamera(MIDDLE, x - mousex, y - mousey);
        break;
    }
  }

  glutPostRedisplay();

  mousex = x;
  mousey = y;
}

void reshape(GLsizei w, GLsizei h)
{
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  GLfloat diff;
  if(w>h )
  {
    diff = (GLfloat) w/(GLfloat) h;
    glScalef(diff, diff, 1);
  }
  else 
  {
    diff = (GLfloat) w/(GLfloat) h;
  } 
  gluPerspective((GLfloat) openingAngle, diff, 0.1, 400.0);
  glMatrixMode(GL_MODELVIEW);
}


int main (int argc, char **argv)
{  
  printStartupText();

  startTime = 0;
  counter = 0;

#ifdef USE_LOG4CPP_OUTPUT
  initLogger();
#endif

  init();

  //parse_command_line_parameter(argc, argv);
  findPaths();
  setupCommandLineOptions();
  parseCommandLineArguments(argc,argv);
  processSimXMLParams();

  printSetupInfo();

  if (clientCom)
  {
    Y_INFO("waiting for connection(s) ...");
    standardCommunication = new
      StandardCommunication(moveableDescription->getNumberOfActiveMovables());
  }

  if(simVisualization)
  {
    printKeyCommands();
    Position size = simDescription->getWindowSize();
    //  init the visualisation and updating of the simulation
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH | GLUT_DOUBLE);
    openingAngle = 45.0;
    worldWindow = new GlWindow((GLfloat)openingAngle, (int)size.x, (int)size.y);
    worldWindow->setIdleFunc(simLoop);
    worldWindow->setMotionFunc( motion);
    worldWindow->setMouseFunc( mouse);
    worldWindow->setKeyFunc( command);
    worldWindow->setReshapeFunc(reshape);
    worldWindow->setDisplayFunc(display);
  }

  startSimulation();

  restoreInitialViewpoint();

  if(simVisualization)
  {
    if(clientCom)
    {
      glutMainLoop();
    }
    else
    {
      glutMainLoop();
    }
  }
  else
  {
    while(true)
    {
      // TODO: make dsSimulationLoop optional
      paused = false;
      simLoop();
    }
  }

  closeYars();
  return 0;
}
