/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "RobotHandler.h"

RobotHandler::RobotHandler(dWorldID world, dSpaceID space, DrawManager *drawManager,
    MoveableDescription *moveDes, Environment *environment, CameraHandler *views, string pathToLibs)
{
  _activeRobots.clear();
  _controlledObjects.clear();
  _controlledRobots.clear();
  _passiveRobots.clear();

  _pathToLibs             = pathToLibs;
  worldID_                = world;
  spaceID_                = space;
  drawManager_            = drawManager;
  environment_            = environment;
  _robotViews             = views;
  moveDes_                = moveDes;
  transferFunction_       = RH_TRANSFER_FUNCTION_TANH;
  LightManager *lm        = environment->getLightManager();

  this->create(lm);
}

RobotHandler::~RobotHandler()
{
  //destroy all robots
  for(unsigned int i = 0; i < _activeRobots.size(); ++i)
  {
    delete _activeRobots[i];
  }

  for(unsigned int i = 0; i < _controlledRobots.size(); ++i)
  {
    delete _controlledRobots[i];
  }

  for(unsigned int i = 0; i < _passiveRobots.size(); ++i)
  {
    delete _passiveRobots[i];
  }

  for(unsigned int i = 0; i < _controlledObjects.size(); ++i)
  {
    delete _controlledObjects[i];
  }
  _activeRobots.clear();
  _controlledObjects.clear();
  _controlledRobots.clear();
  _passiveRobots.clear();

  // reset the global AbortCondition stuff
  (*(YarsContainers::AbortCondition())).abortCondition = false;
  (*(YarsContainers::AbortCondition())).abortSource    = "";
  (*(YarsContainers::AbortCondition())).abortCause     = DEF_ABORT_CAUSE_NONE;
}

void RobotHandler::randomiseInitialPosition()
{
  for(unsigned int i = 0; i < _activeRobots.size(); ++i)
  {
    _activeRobots[i]->randomiseInitialPosition();
  }
  
  for(unsigned int i = 0; i < _controlledRobots.size(); ++i)
  {
    _controlledRobots[i]->randomiseInitialPosition();
  }

  for(unsigned int i = 0; i < _controlledObjects.size(); ++i)
  {
    _controlledObjects[i]->randomiseInitialPosition();
  }
  
  for(unsigned int i = 0; i < _passiveRobots.size(); ++i)
  {
    _passiveRobots[i]->randomiseInitialPosition();
  }
}

/* create the robots and store them in one of the 4 vectors.
   The different vectors allow for a different treatment of the 
   robots in dependence of their type */
void RobotHandler::create(LightManager *lm)
{

  for(unsigned int i = 0; i < moveDes_->getNumberOfActiveMovables(); ++i)
  {
    _activeRobots.push_back( new Robot(worldID_, spaceID_, drawManager_,
          moveDes_->getRobotDescription(i), environment_ , _robotViews, _pathToLibs) );
  }
  
  for(unsigned int i = 0; i < moveDes_->getNumberOfControlledMovables(); ++i)
  {
    int activeMov = moveDes_->getNumberOfActiveMovables();
    int index = activeMov+i;
    _controlledRobots.push_back( new Robot(worldID_, spaceID_, drawManager_,
          moveDes_->getRobotDescription(index), environment_ , _robotViews, _pathToLibs) );
    string controller = moveDes_->getRobotDescription(index)->getController();
    _controlledRobots[i]->setController(controller);
  }

 for(unsigned int i = 0; i < moveDes_->getNumberOfMovingMovables(); ++i)
  {
    int activeMov = moveDes_->getNumberOfActiveMovables();
    int controlledMov = moveDes_->getNumberOfControlledMovables();
    int index = activeMov + controlledMov + i;
    _controlledObjects.push_back( new Robot(worldID_, spaceID_, drawManager_,
          moveDes_->getRobotDescription(index), environment_, _robotViews, _pathToLibs ) );
    string controller = moveDes_->getRobotDescription(index)->getController(); 
    _controlledObjects[i]->setObjectController(controller);
  }
  

  int activeMov = moveDes_->getNumberOfActiveMovables();
  int controlledMov = moveDes_->getNumberOfControlledMovables();
  int movingMov = moveDes_->getNumberOfMovingMovables();
  int index = activeMov + controlledMov + movingMov;

  for(unsigned int i = index; i < moveDes_->getNumberOfMovables(); ++i)
  {
    _passiveRobots.push_back( new Robot(worldID_, spaceID_, drawManager_,
          moveDes_->getRobotDescription(i), environment_, _robotViews, _pathToLibs ) );
  }

}

// inputNum should be 1 unless we are using complex motors / muscle models which
//   require several inputs per motor unit
void RobotHandler::setMotorValue(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int motor, unsigned int inputNum, double value)
{
  if(robot >= this->getNumberOfActiveRobots())
  {
    Y_WARN("!!!RobotHandler::setMotorValue ERROR!!!\n");
    Y_WARN("\tRobot Number %d doesn't exist!!!", robot);
   return;
  }
  
  _activeRobots[robot]->setMotorValue(compound, seg, motor, inputNum, value);
}

void RobotHandler::setMotorValues(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int motor, vector<double> values)
{
  if(robot >= this->getNumberOfActiveRobots())
  {
    Y_WARN("!!!RobotHandler::setMotorValue ERROR!!!\n");
    Y_WARN("\tRobot Number %d doesn't exist!!!", robot);
   return;
  }
  
  _activeRobots[robot]->setMotorValues(compound, seg, motor, values);
}

vector<double> RobotHandler::getSensorValues(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int sensor)
{
  if (robot >= this->getNumberOfActiveRobots())
  {
    // TODO
    throw("RobotHandler::getSensorValues robot number does not exist!");
    Y_WARN("!!!RobotHandler::getSensorValue ERROR!!!\n");
    Y_WARN("\tRobot Number %d doesn't exist!!!", robot);
    vector<double> error;
    error.push_back(-1.0);
    return error;
  }

  // get sensor here

  return _activeRobots[robot]->getSensorValues(compound, seg, sensor);
}

dBodyID* RobotHandler::getSegmentBodyID(unsigned robot, unsigned int compound,
    unsigned int seg)
{
  if (robot >= this->getNumberOfActiveRobots())
  {
    Y_DEBUG("!!!RobotHandler::getSegmentBodyID ERROR!!!\n");
    Y_DEBUG("\tRobot Number %d doesn't exist!!!", robot);
    return NULL;
  }

  return _activeRobots[robot]->getSegmentBodyID(compound, seg);
 
}

dSpaceID RobotHandler::getRobotsSpaceID(unsigned int robot)
{
  if (robot >= this->getNumberOfActiveRobots())
  {
    Y_WARN("!!!RobotHandler::getRobotsSpaceID() ERROR!!!\n");
    Y_WARN("\tRobot Number %d doesn't exist!!!", robot);
    return NULL;
  }

  return _activeRobots[robot]->getSpaceID();
}

void RobotHandler::update()
{
  for(unsigned int i=0; i < this->getNumberOfActiveRobots(); ++i)
  {
    _activeRobots[i]->update();
  }

  for(unsigned int i=0; i < this->getNumberOfControlledRobots(); ++i)
  {
    _controlledRobots[i]->update();
  }
 
  for(unsigned int i=0; i < this->getNumberOfControlledObjects(); ++i)
  {
    _controlledObjects[i]->updateForces();
  }

  for(unsigned int i=0; i < this->getNumberOfPassiveRobots(); ++i)
  {
    _passiveRobots[i]->update();
  }
}

unsigned int RobotHandler::getNumberOfActiveRobots()
{
  return _activeRobots.size();  
}


unsigned int RobotHandler::getNumberOfControlledRobots()
{
  return _controlledRobots.size();  
}


unsigned int RobotHandler::getNumberOfControlledObjects()
{
  return _controlledObjects.size();  
}

unsigned int RobotHandler::getNumberOfPassiveRobots()
{
  return _passiveRobots.size();  
}

    
unsigned int RobotHandler::getNumberOfCompounds(unsigned int robot)
{
  return _activeRobots[robot]->getNumberOfCompounds();
}
    
unsigned int RobotHandler::getNumberOfSegments(unsigned int robot, unsigned int
    compound)
{
  return _activeRobots[robot]->getNumberOfSegments(compound);
}

unsigned int RobotHandler::getNumberOfMotors(unsigned int robot, unsigned int
    compound, unsigned int seg)
{
  return _activeRobots[robot]->getNumberOfMotors(compound, seg);
}

unsigned int RobotHandler::getNumberOfMotorInputs(unsigned int robot, unsigned
    int compound, unsigned int seg, unsigned int motor)
{
  return _activeRobots[robot]->getNumberOfMotorInputs(compound, seg, motor);
}

void RobotHandler::getMotorName(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int motor, string &name)
{
  _activeRobots[robot]->getMotorName(compound, seg, motor, name);
}

double RobotHandler::getMotorMinInput(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int motor, unsigned int motorInput)
{
  return _activeRobots[robot]->getMotorMinInput(compound, seg, motor, motorInput);
}

double RobotHandler::getMotorMaxInput(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int motor, unsigned int motorInput)
{
  return _activeRobots[robot]->getMotorMaxInput(compound, seg, motor, motorInput);
}

unsigned int RobotHandler::getNumberOfSensors(unsigned int robot, unsigned int
    compound, unsigned int seg)
{
  return _activeRobots[robot]->getNumberOfSensors(compound, seg);
}

unsigned int RobotHandler::getNumberOfSensorValues(unsigned int robot, unsigned
    int compound, unsigned int seg, unsigned int sensor)
{
  return _activeRobots[robot]->getNumberOfSensorValues(compound, seg, sensor);
}

unsigned int RobotHandler::getNumberOfSensorValues(unsigned int robot, unsigned int
    compound, unsigned int seg)
{
  return _activeRobots[robot]->getNumberOfSensorValues(compound, seg);
}

void RobotHandler::getSensorName(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int sensor, string &name)
{
  _activeRobots[robot]->getSensorName(compound, seg, sensor, name);
}

double RobotHandler::getSensorMinOutput(unsigned int robot, unsigned int
    compound, unsigned int seg, unsigned int sensor, unsigned int sensorOutput)
{
  return _activeRobots[robot]->getSensorMinOutput(compound, seg, sensor, sensorOutput);
}

double RobotHandler::getSensorMaxOutput(unsigned int robot, unsigned int
    compound, unsigned int seg, unsigned int sensor, unsigned int sensorOutput)
{
  return _activeRobots[robot]->getSensorMaxOutput(compound, seg, sensor, sensorOutput);
}

int RobotHandler::getSensorUsage(unsigned int robot, unsigned int compound,
    unsigned int seg, unsigned int sensor)
{
  return _activeRobots[robot]->getSensorUsage(compound, seg, sensor);
}

void  RobotHandler::stopRobot(unsigned int robot)
{
  if (robot >= this->getNumberOfActiveRobots())
  {
    Y_WARN("!!!RobotHandler::stopRobot ERROR!!!\n");
    Y_WARN("\tRobot Number %d doesn't exist!!!\n", robot);
    Y_WARN("!!!RobotHandler::stopRobot ERROR!!!\n");
    return;
  }
  _activeRobots[robot]->stopRobot();
}

int RobotHandler::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{
  int isCollided = 0;

  isCollided = 0;
  
  
  for(unsigned int i = 0; i < _activeRobots.size(); ++i)
  {
    if(_activeRobots[i]->selfCollide(geom1, geom2, contact) == 1)
    {
      isCollided = 1; // do not use return, so that all segments are processed
    }
  }
    
  for(unsigned int i = 0; i < _controlledRobots.size(); ++i)
  {
    if(_controlledRobots[i]->selfCollide(geom1, geom2, contact) == 1)
    {
      isCollided = 1; // do not use return, so that all segments are processed
    }
 }
 
 for(unsigned int i = 0; i < _controlledObjects.size(); ++i)
 {
   if(_controlledObjects[i]->selfCollide(geom1, geom2, contact) == 1)
   {
     isCollided = 1; // do not use return, so that all segments are processed
   }
 }
 

 for(unsigned int i = 0; i < _passiveRobots.size(); ++i)
 {
   if(_passiveRobots[i]->selfCollide(geom1, geom2, contact) == 1)
   {
     isCollided = 1; // do not use return, so that all segments are processed
   }
 }
  
  return isCollided;
}


/* perform the updates for the controlled an moving robots */
void RobotHandler::updateControlled()
{
  for(unsigned int i=0; i< this->getNumberOfControlledRobots(); ++i)
  {

    Y_DEBUG("RobotHandler::Number of controlled robots %d \n",this->getNumberOfControlledRobots());
    Y_DEBUG("RobotHandler::Control Robot %d\n", i);
    _controlledRobots[i]->updateController();
  
  }
  for(unsigned int i=0; i< this->getNumberOfControlledObjects(); ++i)
  {

    Y_DEBUG("RobotHandler::Number of controlled objects %d \n",this->getNumberOfControlledObjects());
    Y_DEBUG("RobotHandler::Control Robot %d\n", i);
    _controlledObjects[i]->updateObjectController();
  
  }

}

Robot* RobotHandler::getRobot(unsigned int robot)
{
  if(this->existRobot(robot)) 
  {
    return _activeRobots[robot];
  }

  return NULL;
}

bool RobotHandler::existRobot(unsigned int robot)
{
  if(robot >= this->getNumberOfActiveRobots())
  {
    Y_WARN("!!!RobotHandler ERROR!!!\n");
    Y_WARN("    Robot Number %d doesn't exist!!!", robot);
    Y_WARN("!!!RobotHandler ERROR!!!\n");
    return false;
  }
  return true;
}

void RobotHandler::setEnvironment(Environment* environment)
{
  environment_ = environment;

}

void RobotHandler::setDrawManager(DrawManager* drawManager)
{
  drawManager_ = drawManager;
}
