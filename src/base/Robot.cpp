/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Robot.h"

#include <sstream>



#define ROBOT_CONTROLLER_PREFIX "libYarsController"
#define OBJECT_CONTROLLER_PREFIX "libYarsObjectController"

#define DYNAMIC_LIB_SUFFIX ".so"

Robot::Robot(dWorldID world, dSpaceID space, DrawManager *drawManager,
    RobotDescription *robotDesc, Environment *environment, CameraHandler *views, string pathToLibs)
{
  _pathToLibs = pathToLibs;
  worldID_         = world;
  spaceID_         = dSimpleSpaceCreate(space);
  drawManager_     = drawManager;
  robotDes_        = robotDesc;
  environment_     = environment;
  robotViews_      = views;
  _lightManager    = environment_->getLightManager();
  jointGroupID_    = dJointGroupCreate(0);
  _robotController = NULL;
  _controller      = NULL;
  _objectController= NULL;
  dSpaceSetCleanup(spaceID_, 1);
  this->create(_lightManager);
  this->init();

}

Robot::~Robot()
{
  /* destroy all joints, which connect the compounds */
  dJointGroupDestroy(jointGroupID_);

  //destroy all compounds 
  for(unsigned int i = 0; i < compound_.size(); ++i)
  {
    delete compound_[i];
  }
  compound_.clear();
  dSpaceDestroy(spaceID_);

  if(_controller!=NULL)
  {
    delete _controller;
  }
  
  if(_objectController!=NULL)
  {
    _objectController->deinit();
    delete _objectController;
    int error =   dlclose(_objectsCo);

  }

  if(_robotController!=NULL)
  {
    dlclose(_robotController);
  }
}

void Robot::init()
{
  _motors=0;
  _sensors=0;
  int i,j,k;
  int compounds = this->getNumberOfCompounds();
  for(i= 0; i<compounds; i++)
  {
    int segs = this->getNumberOfSegments(i);
    for(j=0; j<segs; j++)
    {
      _motors+= this->getNumberOfMotors(i,j);
      _sensors+= this->getNumberOfSensors(i,j);
    }
  }

  for(i= 0; i<compounds; i++)
  {
    int segs = this->getNumberOfSegments(i);
    for(j=0; j<segs; j++)
    {
      int motors = this->getNumberOfMotors( i,j);
      for(k=0; k<motors; k++)
      {
        
        _robotMotors.push_back(i);
        _robotMotors.push_back(j);
        _robotMotors.push_back(k);
        _robotMotors.push_back(this->getNumberOfMotorInputs(i,j,k));
      }
      int sensors = this->getNumberOfSensors( i,j);
      for(k=0; k<sensors; k++)
      {
        _robotSensors.push_back(i);
        _robotSensors.push_back(j);
        _robotSensors.push_back(k);
      }
    }
  }
}

void Robot::randomiseInitialPosition()
{
  robotDes_->randomiseInitialPosition();
}


void Robot::create(LightManager *lm)
{
  //robotDes_->randomiseInitialPosition();
  /* create all segments off all compounds */
  for(unsigned int i = 0; i < robotDes_->getCompoundNumber(); ++i)
  {
    compound_.push_back( new Compound(
          robotDes_->getCompound(i), worldID_, spaceID_, drawManager_, environment_, robotViews_) );
    compound_[i]->create();
    compound_[i]->setLightManager(lm);
  } 

  /* connect all compound together */
  for(unsigned int i = 0; i < robotDes_->getCompoundConnectorNumber(); ++i)
  {
    this->createCompoundConnection(robotDes_->getCompoundConnector(i));
  }

  /* create all sensors */
  for(unsigned int i = 0; i < robotDes_->getSensorNumber(); ++i)
  {
    SensorDescription *s = robotDes_->getSensor(i);
    SensorStruct *sS = s->getSensorStruct();

    if(sS->type == SD_COORDINATE_SENSOR ||
       sS->type == SD_AMBIENT_LIGHT_SENSOR)
    {
      continue; // only process *non-global* sensors, because global sensor must
      // be placed at the end of the sensors list.
    }

    compound_ [sS->srcCompound]->createSensor(s, NULL,0);
  }


  /* create all global sensors, and add them to the last segment */
  for(unsigned int i = 0; i < robotDes_->getSensorNumber(); ++i)
  {
    Y_DEBUG("in global sensor loop");
    SensorDescription *s = robotDes_->getSensor(i);
    SensorStruct *sS = s->getSensorStruct();

    //    Y_DEBUG("sS->type %d == %d\n", sS->type, SD_COORDINATE_SENSOR);

    if(sS->type != SD_COORDINATE_SENSOR &&
       sS->type != SD_AMBIENT_LIGHT_SENSOR)
    {
      //      Y_DEBUG("continue\n");
      continue; // only process *global* sensors, because global sensor must
      // be placed at the end of the sensors list.
    }

    if(sS->type == SD_COORDINATE_SENSOR)
    {
      Y_DEBUG("adding coordinate sensor");
      CoordinateSensorStruct *cSs = s->getCoordinateSensor();
      //    Y_DEBUG("srcCompound %d srcSeg %d\n",cSs->srcCompound, cSs->srcSeg);
      // note that all global sensors are add to the last segment
      sS->srcSeg = 
        compound_[compound_.size()-1]->getNumberOfSegments()-1;
      sS->srcCompound= compound_.size()-1;

      compound_ [compound_.size()-1]->createSensor(s,
          compound_[cSs->srcCompound]->getSegmentGeomID(cSs->srcSeg,0),0);
    }
    if(sS->type == SD_AMBIENT_LIGHT_SENSOR)
    {
      Y_DEBUG("adding ambient light sensor");
      AmbientLightSensorStruct *aSs = s->getAmbientLightSensor();
      sS->srcSeg = 
        compound_[compound_.size()-1]->getNumberOfSegments()-1;
      sS->srcCompound = compound_.size()-1;
      //    Y_DEBUG("srcCompound %d srcSeg %d\n",cSs->srcCompound, cSs->srcSeg);
      // note that all global sensors are add to the last segment
      compound_ [compound_.size()-1]->createSensor(s,
          compound_[sS->srcCompound]->getSegmentGeomID(sS->srcSeg,0),
          _lightManager);
    }
  }

}

void Robot::setMotorValue(unsigned int compound, unsigned int seg, unsigned int
    motor, unsigned int inputNum, double value)
{
  if(this->existCompound(compound))
  {
    compound_[compound]->setMotorValue(seg, motor, inputNum, value);
  }
  else
  {
    return; 
  }
}

void Robot::setMotorValues(unsigned int compound, unsigned int seg, unsigned int
    motor, vector<double> values)
{
  if(this->existCompound(compound))
  {
    compound_[compound]->setMotorValues(seg, motor, values);
  }
  else
  {
    return; 
  }
}

void Robot::setForces(unsigned int compound, double fx, double fy, double fz)
{
  if(this->existCompound(compound))
  {
    
    compound_[compound]->setForces(fx, fy, fz);
  }
  else
  {
    return; 
  }

}

vector<double> Robot::getSensorValues(unsigned int compound, unsigned int seg, unsigned
    int sensor)
{
  if(this->existCompound(compound))
  {
    return compound_[compound]->getSensorValues(seg, sensor);
  }
  else
  {
    // TODO
    throw("Robot::getSensorValues compound does not exist!");
    vector<double> unknown;
    unknown.push_back(-1.0); 
    return unknown;
  }

}

dBodyID* Robot::getSegmentBodyID(unsigned int compound, unsigned int seg)
{
  if(this->existCompound(compound))
  {
    return compound_[compound]->getSegmentBodyID(seg);
  }
  else
  {
    return NULL; 
  }

}

void Robot::update()
{
  for(unsigned int i = 0; i < compound_.size(); ++i)  
  {
    compound_[i]->update();
  }
}

void Robot::updateForces()
{
  for(unsigned int i = 0; i < compound_.size(); ++i)  
  {
    compound_[i]->updateForces();
  }
}

/* creating the actual controller by dynamic loading of the adequate class from the directory /controller */

void Robot::setController(string control)
{
  ostringstream oss;
  oss << _pathToLibs << "/lib/" << ROBOT_CONTROLLER_PREFIX << control << DYNAMIC_LIB_SUFFIX;
  string path = oss.str();
  const char *controller = path.c_str();
  _robotController = dlopen(controller,RTLD_LAZY);
  if(!_robotController)
  {
    Y_DEBUG("Robot::Cannot load library %d\n", dlerror());
    Y_WARN("failed to load %d\n", dlerror());
    return;
  }
  
  dlerror();
  
  create_controller = (create_c*)dlsym(_robotController,"create");
  const char* dlsym_error=dlerror();
  if(!_robotController)
  {
    Y_DEBUG("Robot::Cannot load symbol create");
    return;
  }
  
  _controller = create_controller();

  if((_controller->getNumberOfOutputs()<_motors))
  {
    _controller->setNumberOfOutputs(_motors);
  }

  delete dlsym_error;
}

/* creating the actual controller by dynamic loading of the adequate class from the directory /objectcontroller */
void Robot::setObjectController(string control)
{
  
  ostringstream oss;
  oss << _pathToLibs << "/lib" << OBJECT_CONTROLLER_PREFIX << control << DYNAMIC_LIB_SUFFIX;
  string path = oss.str();
  const char *controller = path.c_str();
  _objectsCo = dlopen(controller,RTLD_LAZY);
  if(!_objectsCo)
  {
    Y_DEBUG("Robot::Cannot load library %d\n", dlerror());
    Y_WARN("failed to load %d\n", dlerror());
    return;
  }
  
  dlerror();
  
  create_objectController = (create_oc*)dlsym(_objectsCo,"createController");
  const char* dlsym_error=dlerror();
  if(!_objectsCo)
  {
    Y_DEBUG("Robot::Cannot load symbol create");
    return;
  }
  
  _objectController = create_objectController();
}

/* changing the motor values of the robot according to the new sensor values and the calculation done in the RobotController */

void Robot::updateController()
{
  int i,j, motorVal;

  vector<double> motorValues(_motors);
  vector<double> sensorValues(_sensors);

  for(int i=0; i<_sensors; i++)
  {
    for(int j= 0; j< getSensorValues( _robotSensors[0 + i*3], _robotSensors[1 + i*3], _robotSensors[2 + i*3]).size(); j++)
    {
      sensorValues[i] = this->getSensorValues( _robotSensors[0 + i*3], _robotSensors[1 + i*3], _robotSensors[2 + i*3])[j];
    }
  }

  _controller->setInputValues(sensorValues);
  _controller->update();
  motorValues = _controller->getOutputValues();
  Y_DEBUG("Robot::updateController()");

  motorVal = 0;

  for(int i=0; i<_motors; i++)
  {
    for(int j=0; j < _robotMotors[3 + i*4]; j++)
    {
      this->setMotorValue( _robotMotors[0 + i*4], _robotMotors[1 + i*4],
          _robotMotors[2 + i*4], j, motorValues[motorVal]);
      motorVal++;
    }
  }
}


/* changing the applied forces for the robot according the calculation done in the ObjectController */
void Robot::updateObjectController()
{
  vector<double> forces;
  _objectController->update();
  forces = _objectController->getForces();
  for(uint i=0; i < compound_.size(); i++)
  {
    this->setForces(i, forces[0], forces[1], forces[2]);
  }
}


void Robot::createCompoundConnection(ConnectorDescription *connStruct)
{
  int      srcCompound = (*connStruct).sourceCompoundNum;
  int      srcSeg   = (*connStruct).sourceSegNum;
  int      aimCompound = (*connStruct).aimCompoundNum;
  int      aimSeg   = (*connStruct).aimSegNum;
  Position anchor   = (*connStruct).anchorPos;

  if(aimSeg == -1)
  {
    Y_DEBUG("Robot::createCompoundConnection() Found aimSeg == -1");
    compound_[srcCompound]->addGlobalConnector(connStruct, srcSeg, NULL);
  }
  else
  {
    dBodyID *aimBody = compound_[aimCompound]->getSegmentBodyID(aimSeg);

   compound_[srcCompound]->addGlobalConnector(connStruct, srcSeg, aimBody);
  }
}

void Robot::connectFix(ConnObj *conn, Position *pos)
{
  /* axis doesn't matter, because it is a fixed hinge */
  Position axis;
  axis.x = 1.0;
  axis.y = 0.0;
  axis.z = 0.0;

  this->connectHinge( conn, pos, &axis);
  this->setJointStops( &(conn->joint), 0.0, 0.0 );
}

void Robot::connectHinge(ConnObj *conn, const Position *pos, const Position *axis)
{
  (*conn).joint = dJointCreateHinge(worldID_, jointGroupID_);

  dJointAttach((*conn).joint, *(conn->body1), *(conn->body2));

  dJointSetHingeAxis((*conn).joint, axis->x, axis->y, axis->z);

  dJointSetHingeAnchor( (*conn).joint, pos->x, pos->y, pos->z);
}


void Robot::connectSlider(ConnObj *conn, const Position *axis)
{

  (*conn).joint = dJointCreateSlider(worldID_, jointGroupID_);

  dJointAttach((*conn).joint, *(conn->body1), *(conn->body2));

  dJointSetSliderAxis((*conn).joint, axis->x, axis->y, axis->z);
}


void Robot::setJointStops(dJointID *joint, const double loStop, const double
    hiStop)
{
  switch ( dJointGetType(*joint) )
  {
    case dJointTypeHinge:
      {
        dJointSetHingeParam(*joint, dParamHiStop, hiStop);
        dJointSetHingeParam(*joint, dParamLoStop, loStop);  
      }
      break;
    case dJointTypeSlider:
      {
        dJointSetSliderParam(*joint, dParamHiStop, hiStop);
        dJointSetSliderParam(*joint, dParamLoStop, loStop);  
      }
      break;
    default:
      break;
  }
}

unsigned int Robot::getNumberOfCompounds()
{
  return compound_.size();
}

unsigned int Robot::getNumberOfSegments(unsigned int compound)
{
  return compound_[compound]->getNumberOfSegments();
}

unsigned int Robot::getNumberOfMotors(unsigned int compound, unsigned int seg)
{
  return compound_[compound]->getNumberOfMotors(seg);
}

unsigned int Robot::getNumberOfMotorInputs(unsigned int compound, unsigned int seg, unsigned int motor)
{
  return compound_[compound]->getNumberOfMotorInputs(seg, motor);
}

void Robot::getMotorName(unsigned int compound, unsigned int seg, unsigned int
    motor, string &name)
{
  compound_[compound]->getMotorName(seg, motor, name);
}

double Robot::getMotorMinInput(unsigned int compound, unsigned int seg, unsigned
    int motor, unsigned int motorInput)
{
  return compound_[compound]->getMotorMinInput(seg, motor, motorInput);
}

double Robot::getMotorMaxInput(unsigned int compound, unsigned int seg, unsigned
    int motor, unsigned int motorInput)
{
  return compound_[compound]->getMotorMaxInput(seg, motor, motorInput);
}

unsigned int Robot::getNumberOfSensors(unsigned int compound, unsigned int seg)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getNumberOfSensors(seg);
  }
  else
  {
    return 0;
  }
}

unsigned int Robot::getNumberOfSensorValues(unsigned int compound, unsigned int
    seg, unsigned int sensor)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getNumberOfSensorValues(seg, sensor);
  }
  else
  {
    return 0;
  }
}

unsigned int Robot::getNumberOfSensorValues(unsigned int compound, unsigned int seg)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getNumberOfSensorValues(seg);
  }
  else
  {
    return 0;
  }
}

void Robot::getSensorName(unsigned int compound, unsigned int seg, unsigned int
    sensor, string &name)
{
  if( this->existCompound(compound) )
  {
    compound_[compound]->getSensorName(seg, sensor, name);
  }
  else
  {
    // TODO error handling
    name = "SensorDoesNotExist";
    return;
  }
}

double Robot::getSensorMinOutput(unsigned int compound, unsigned int seg,
    unsigned int sensor, unsigned int sensorOutput)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getSensorMinOutput(seg, sensor, sensorOutput);
  }
  else
  {
    // TODO error handling
    return 0.0;
  }
}

double Robot::getSensorMaxOutput(unsigned int compound, unsigned int seg,
    unsigned int sensor, unsigned int sensorOutput)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getSensorMaxOutput(seg, sensor, sensorOutput);
  }
  else
  {
    // TODO error handling
    return 0.0;
  }
}

int Robot::getSensorUsage(unsigned int compound, unsigned int seg, unsigned int
    sensor)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound]->getSensorUsage(seg, sensor);
  }
  else
  {
    // TODO error handling
    return DEF_SENSOR_USED_AS_CONTROL_INPUT; // default
  }
}

void Robot::stopRobot()
{
  for(unsigned int i = 0; i < compound_.size(); ++i)
  {
    compound_[i]->stopMotors();
  }
}

dSpaceID Robot::getSpaceID()
{
  return spaceID_;
}

int Robot::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact)
{
  int isCollided = 0;

  isCollided = 0;

  for(unsigned int i = 0; i < compound_.size(); ++i)
  {
    // only if a collision in one of each compound is detected then return
    // value is set to 1
    if(compound_[i]->selfCollide(geom1, geom2, contact) == 1)
    {
      // do not use return, so that all segments are processed
      isCollided = 1;
    }
  }

  return isCollided;
}

Compound* Robot::getCompound(unsigned int compound)
{
  if( this->existCompound(compound) )
  {
    return compound_[compound];
  }

  return NULL;
}

bool Robot::existCompound(unsigned int compound)
{
  if(compound >= this->getNumberOfCompounds() )
  {
    Y_WARN("!!!Robot:: ERROR!!!\n");
    Y_WARN("   Compound Number %d doesn't exist!!!\n", compound);
    Y_WARN("!!!Robot:: ERROR!!!\n");
    return false;
  }
  else
  {
    return true;
  }
}
