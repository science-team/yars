/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef __ROBOTCONTROLLER_HPP__
#define __ROBOTCONTROLLER_HPP__


#include <vector>

using namespace std;

/**
  Abstract definition of the RobotControllers. Implementations of this class
  might be used for defining "controlled" robots. A controlled robot uses an
  exported network (designed or evolved in an external neural network or
  artificial evolution program or an otherwise defined controller. The advantage
  is that for moving the robot no connection to a client is necessary.
 **/
class RobotController 
{

  public:

    // Methode for defining the calcuation of the output values.
    virtual void update() = 0;
    // intialisation of the _inputs and _outputs vectors and additional local-Variables
    virtual void init()= 0;
        
    // gives access to the vector _outputs
    vector<double> getOutputValues()
    {
      return _outputs;
    }


    int getNumberOfOutputs()
    {
      return _outputs.size();
    }
    // change the value of _inputs, e.g. in case of updated/changed sensor-values
    void setInputValues(vector<double> inputs)
    {
      _inputs = inputs; 
    }

    // define the number of Output values that are necessary for the given robot
    void setNumberOfOutputs( int number)
    {
      _outputs.resize(number); 
    }
    
  protected:
    
    // field of output values. e.g. for controlling all motors
    vector<double> _outputs;

    // field of input values e.g. using all/parts of the sensors 
    vector<double> _inputs;

};

// the types of the class factory for the concrete implementations of RobotController's
typedef RobotController* create_c();

#endif // __ROBOTCONTROLLER_HPP__

