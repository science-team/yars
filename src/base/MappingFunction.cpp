/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <base/MappingFunction.h>

MappingFunction::MappingFunction(double inStartVal, double inEndVal, double
    outStartVal, double outEndVal)
{
  this->addMap(inStartVal, inEndVal, outStartVal, outEndVal);
}

MappingFunction::~MappingFunction()
{
}

double MappingFunction::map(double value)
{
  // this->checkValueRange(&value);

  if(value < mapData_[0].inMin)
    value = mapData_[0].inMin;
  else if(value > mapData_[0].inMax)
    value = mapData_[0].inMax;

  value = (mapData_[0].a * value) + mapData_[0].b;

  if(value < mapData_[0].outMin)
    value = mapData_[0].outMin;
  else if(value > mapData_[0].outMax)
    value = mapData_[0].outMax;

  return value;
}

double MappingFunction::map(int mapNum, double value)
{
  if(this->doesMapNumExist(mapNum))
  {
    // this->checkValueRange(mapNum, &value);
    
    if(value < mapData_[mapNum].inMin)
      value = mapData_[mapNum].inMin;
    else if(value > mapData_[mapNum].inMax)
      value = mapData_[mapNum].inMax;

    value = (mapData_[mapNum].a * value) + mapData_[mapNum].b;

    if(value < mapData_[mapNum].outMin)
      value = mapData_[mapNum].outMin;
    else if(value > mapData_[mapNum].outMax)
      value = mapData_[mapNum].outMax;

    return value;
  }
  else
  {
    // TODO: error handling
    throw("MappingFunction::map(int, double) mapNum %d does not exist.", mapNum);
  }
}

void MappingFunction::map(vector<double> *values)
{
  if(mapData_.size() < (*values).size())
  {
    throw("MappingFunction::map(vector<double>) mapData.size(%d) < values.size(%d", 
        mapData_.size(), (*values).size());
  }
  else
  {
    for(int i=0; i < (*values).size(); i++)
    {
      //this->checkValueRange(i, &((*values)[i]));

      if((*values)[i] < mapData_[i].inMin)
        (*values)[i] = mapData_[i].inMin;
      else if((*values)[i] > mapData_[i].inMax)
        (*values)[i] = mapData_[i].inMax;

      (*values)[i] = (mapData_[i].a * (*values)[i]) + mapData_[i].b;

      if((*values)[i] < mapData_[i].outMin)
        (*values)[i] = mapData_[i].outMin;
      else if((*values)[i] > mapData_[i].outMax)
        (*values)[i] = mapData_[i].outMax;
    }
  }
}

void MappingFunction::addMap(double inStartVal, double inEndVal, 
    double outStartVal, double outEndVal)
{
  int newMapNum = mapData_.size();

  MapStruct map;
  mapData_.push_back(map);

  this->setInputOutputRange(newMapNum, inStartVal, inEndVal, outStartVal,
      outEndVal);
}

void MappingFunction::setInputOutputRange(double inStartVal, double inEndVal,
    double outStartVal, double outEndVal)
{
  this->setInputOutputRange(0, inStartVal, inEndVal, outStartVal, outEndVal);
}

void MappingFunction::setInputOutputRange(int mapNum, double inStartVal, 
    double inEndVal, double outStartVal, double outEndVal)
{
  if(this->doesMapNumExist(mapNum))
  {
    mapData_[mapNum].inStartVal  = inStartVal;
    mapData_[mapNum].inEndVal    = inEndVal;
    mapData_[mapNum].outStartVal = outStartVal;
    mapData_[mapNum].outEndVal   = outEndVal;

    mapData_[mapNum].inMin       = MIN(inStartVal, inEndVal);
    mapData_[mapNum].inMax       = MAX(inStartVal, inEndVal);
    mapData_[mapNum].outMin      = MIN(outStartVal, outEndVal);
    mapData_[mapNum].outMax      = MAX(outStartVal, outEndVal);

    this->calculateConstants(mapNum);
  }
  else
  {
    // TODO: error handling
  }
}
 
void MappingFunction::calculateConstants()
{
  for(int i=0; i < mapData_.size(); i++)
  {
    this->calculateConstants(i);
  }
}

void MappingFunction::calculateConstants(int mapNum)
{
  if(! (this->doesMapNumExist(mapNum)))
  {
    // TODO: error message /handling
    return;
  }

  // TODO: error handling if NaN or infinity occurs ...
  mapData_[mapNum].a = (mapData_[mapNum].outStartVal -
                        mapData_[mapNum].outEndVal) 
                      /(mapData_[mapNum].inStartVal  -
                        mapData_[mapNum].inEndVal);
  mapData_[mapNum].b = mapData_[mapNum].outStartVal - 
                      ((mapData_[mapNum].inStartVal 
                        /(mapData_[mapNum].inStartVal - 
                          mapData_[mapNum].inEndVal)
                       )
                       * (mapData_[mapNum].outStartVal - 
                          mapData_[mapNum].outEndVal)
                      );
}

double MappingFunction::getInStartVal()
{
  return mapData_[0].inStartVal;
}

double MappingFunction::getInStartVal(int mapNum)
{
  if(! (this->doesMapNumExist(mapNum)))
  {
    // TODO: error message /handling
    return 0.0;
  }

  return mapData_[mapNum].inStartVal;
}

double MappingFunction::getInEndVal()
{
  return mapData_[0].inEndVal;
}

double MappingFunction::getInEndVal(int mapNum)
{
  if(! (this->doesMapNumExist(mapNum)))
  {
    // TODO: error message /handling
    return 0.0;
  }

  return mapData_[mapNum].inEndVal;
}

double MappingFunction::getOutStartVal()
{
  return mapData_[0].outStartVal;
}

double MappingFunction::getOutStartVal(int mapNum)
{
  if(! (this->doesMapNumExist(mapNum)))
  {
    // TODO: error message /handling
    return 0.0;
  }

  return mapData_[mapNum].outStartVal;
}

double MappingFunction::getOutEndVal()
{
  return mapData_[0].outEndVal;
}

double MappingFunction::getOutEndVal(int mapNum)
{
  if(! (this->doesMapNumExist(mapNum)))
  {
    // TODO: error message /handling
    return 0.0;
  }

  return mapData_[mapNum].outEndVal;
}

// depreceated
void MappingFunction::checkValueRange(double *value)
{
  if(mapData_[0].inStartVal < mapData_[0].inEndVal)
  {
    if(*value < mapData_[0].inStartVal)
    {
      *value = mapData_[0].inStartVal;
    }
    else if(*value > mapData_[0].inEndVal)
    {
      *value = mapData_[0].inEndVal;
    }
  }
  else
  {
    if(*value > mapData_[0].inStartVal)
    {
      *value = mapData_[0].inStartVal;
    }
    else if(*value < mapData_[0].inEndVal)
    {
      *value = mapData_[0].inEndVal;
    }
  }
}

// depreceated
void MappingFunction::checkValueRange(int mapNum, double *value)
{
  if(mapData_[mapNum].inStartVal < mapData_[mapNum].inEndVal)
  {
    if(*value < mapData_[mapNum].inStartVal)
    {
      *value = mapData_[mapNum].inStartVal;
    }
    else if(*value > mapData_[mapNum].inEndVal)
    {
      *value = mapData_[mapNum].inEndVal;
    }
  }
  else
  {
    if(*value > mapData_[mapNum].inStartVal)
    {
      *value = mapData_[mapNum].inStartVal;
    }
    else if(*value < mapData_[mapNum].inEndVal)
    {
      *value = mapData_[mapNum].inEndVal;
    }
  }
}

bool MappingFunction::doesMapNumExist(int mapNum)
{
  if(mapNum < mapData_.size())
  {
    return true;
  }
  else
  {
    return false;
  }
}

int MappingFunction::getNumberOfMaps()
{
  return mapData_.size();
}
