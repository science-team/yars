/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "World.h"

World::World()
{ 
  world_        = dWorldCreate();
  space_        = dSimpleSpaceCreate(0);
  contactgroup_ = dJointGroupCreate(0);

  dWorldSetGravity (world_,0,0,GRAVITY); 
}

World::~World()
{
  dJointGroupDestroy (contactgroup_);
  dSpaceDestroy (space_);
  dWorldDestroy (world_);
}

dWorldID World::getWorldID()
{  
  return world_; 
}

dSpaceID World::getSpaceID()
{   
  return space_; 
}

dJointGroupID World::getContactGroupID()
{   
  return contactgroup_; 
}

void World::step(dReal stepSize)
{
  dWorldStep (world_, stepSize);
  dJointGroupEmpty (contactgroup_);
}
