/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef OBJECTCONTROLLER_HPP
#define OBJECTCONTROLLER_HPP


#include <cmath>
#include <string> 
#include <iostream>
#include <vector>
#include <math.h>
#include <stdio.h>

#include <description/DescriptionHelper.h>

/**
  Abstract definition of ObjectControllers. This class enables to define
  controllers for objects without motors. The movements of the object are
  controlled by changing the applied forces.
 **/
class ObjectController 
{

  public:
    // set the forces for controlling the movements
    virtual void update()=0;
    
    // setting the start forces
    virtual void init()
    {
      _forces.resize(3);
      _forces[0] = 0;
      _forces[1] = 0;
      _forces[2] = 0;
    }
    
    virtual void deinit()
    {
      _forces[0] = 0;
      _forces[1] = 0;
      _forces[2] = 0;
    }
   
    vector<double> getForces()
    {
      return _forces; 
    }

    void setForces(double fx, double fy, double fz)
    {
      _forces[0] = fx;
      _forces[1] = fy;
      _forces[2] = fz;
    }


  protected:
  
    vector<double> _forces;
};

typedef ObjectController* create_oc();

#endif
