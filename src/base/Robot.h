/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _ROBOT_H_
#define _ROBOT_H_

#include <gui/DrawManager.h>
#include <base/CameraHandler.h>
#include <description/RobotDescription.h>
#include <description/SensorDescription.h>

#include <env/Environment.h>
#include <base/Compound.h>
#include <env/LightManager.h>
#include <ode/ode.h>
#include <base/RobotController.hpp>

#include <base/ObjectController.hpp>
#include <vector>
#include <dlfcn.h>
#include <string>

using namespace std;

class Robot
{
  public:
    Robot(dWorldID world, dSpaceID space, DrawManager *drawManager,
        RobotDescription *robotDesc, Environment *environment, CameraHandler *views, string pathToLibs);
    ~Robot();

    void create(LightManager *lm);
   /*    get the motors and sensors of the robot and put them in a vector 
    for "controlled"-type-update*/  
    void init();
 
    /* set the velocity of a spefic motor 
     * value is either a velocity or a angle, depends on the motorType
     * (see RobotDescription.h) */
    void setMotorValue(unsigned int segCompound, unsigned int seg, unsigned int
        motor, unsigned int inputNum, double value);

    void setMotorValues(unsigned int segCompound, unsigned int seg, unsigned int
        motor, vector<double> values);
    
    /* set the forces applied to a "moving" robot
     * */
    void setForces(unsigned int segCompound, double fx, double fy, double fz);
    
    vector<double> getSensorValues(unsigned int segCompound, unsigned int seg, unsigned int
        sensor);
 
    dBodyID* getSegmentBodyID(unsigned int segCompound, unsigned int seg);

    void update();
 
    void updateForces();

    unsigned int getNumberOfCompounds();
    
    unsigned int getNumberOfSegments(unsigned int segCompound);

    // motor functions
    unsigned int getNumberOfMotors(unsigned int segCompound, unsigned int seg);

    unsigned int getNumberOfMotorInputs(unsigned int segCompound, unsigned int seg, unsigned int motor);

    void getMotorName(unsigned int segCompound, unsigned int seg, unsigned int motor, string &name);

    double getMotorMinInput(unsigned int segCompound, unsigned int seg, unsigned
        int motor, unsigned int motorInput);

    double getMotorMaxInput(unsigned int segCompound, unsigned int seg, unsigned
        int motor, unsigned int motorInput);

    // sensor functions
    unsigned int getNumberOfSensors(unsigned int segCompound, unsigned int seg);

    unsigned int getNumberOfSensorValues(unsigned int segCompound, unsigned int
        seg, unsigned int sensor);

    unsigned int getNumberOfSensorValues(unsigned int segCompound, unsigned int seg);

    void getSensorName(unsigned int segCompound, unsigned int seg, unsigned int
        sensor, string &name);

    double getSensorMinOutput(unsigned int segCompound, unsigned int seg, unsigned
        int sensor, unsigned int sensorOutput);

    double getSensorMaxOutput(unsigned int segCompound, unsigned int seg, unsigned
        int sensor, unsigned int sensorOutput);

    int getSensorUsage(unsigned int compound, unsigned int seg, unsigned int
        sensor);

    void stopRobot();

    /* Method for controlling/driving the "controlled"-robots.*/

    void updateController();
    
    /* Updating the forces applied to the robot*/
    void updateObjectController();
    
    void setController(string control);
    
    void setObjectController(string control);

    void randomiseInitialPosition();

    /* returns the space id of this robot */
    dSpaceID getSpaceID();
    
    int selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

    Compound* getCompound(unsigned int segCompound);

  private:

    bool existCompound(unsigned int segCompound);
  
    /* Compound ConnectorDescription Object, contains all relevant date */
    struct ConnObj
    {
      dJointID joint;
      dBodyID *body1;
      dBodyID *body2;
    };
    
    /* connect two compounds together, how to is described in the ConnectorDescription struct
     * */
    void createCompoundConnection(ConnectorDescription *connStruct);
      
    /* connect two compounds via a fixed hinge joint */
    void connectFix(ConnObj *conn, Position *pos);;
    
    /* connect the bodies of two compounds with a hinge */
    void connectHinge(ConnObj *conn, const Position *pos, const Position *axis);
    
    /* connect the bodies of two compounds with a slider joint */
    void connectSlider(ConnObj *conn,  const Position *axis);
    
    /* set the lo and hi stops for a specific joint, has to be a hinge, values in      * rad */
    void setJointStops(dJointID *joint, const double loStop, const double
        hiStop);

    dWorldID worldID_;
    dSpaceID spaceID_;
 
    DrawManager *drawManager_;
    LightManager *_lightManager;
    Environment *environment_;

    CameraHandler *robotViews_;

    RobotDescription *robotDes_;

    vector<Compound*> compound_;
    vector<struct ConnObj> connectors_;
    dJointGroupID jointGroupID_;

    vector<int> _robotMotors;
    vector<int> _robotSensors;
    
    int _motors;
    int _sensors;
    
    void *_robotController;
     
    void *_objectsCo;
    
    /* Controller that manipulates the robot in case of type="controlled" */

    RobotController *_controller;
       
    /* Controller that manipulates the robot in case of type="controlled" */
    ObjectController *_objectController;
    
    create_c* create_controller;    
    
    create_oc* create_objectController;

    string _pathToLibs;

};

#endif
