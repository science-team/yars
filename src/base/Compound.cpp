/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <base/Compound.h>


Compound::Compound(CompoundDescription *compound, dWorldID w, dSpaceID s,
    DrawManager *d, Environment *e, CameraHandler *views)
{
  _compound    = compound;
  worldID_     = w;
  spaceID_     = s;
  /* all geoms in this space will be destroyed, if the space is destroyed */
  drawManager_ = d;
  environment_ = e;
  robotViews_ = views;
}

Compound::~Compound()
{
  for(unsigned int i = 0; i < segments_.size(); ++i)
  {
    delete segments_[i];
  }

}

void Compound::create()
{

  Y_DEBUG("Compound::create() start");
  /* first create all geoms and bodies without out any connection */ 
  for(unsigned int i = 0; i < _compound->segments.size(); ++i)
  {
    segments_.push_back( new Segment( &((*_compound).segments[i]), worldID_,
          spaceID_, drawManager_, environment_, robotViews_) );    
  }
  
   /* go through all segments */
  for(unsigned int i = 0; i < _compound->segments.size(); ++i)
  {
    /* go through all connectors of each segment */
    for(unsigned int j = 0; j < (*_compound).segments[i].connectors.size(); ++j)
    {
      int aimSegNum   = (*_compound).segments[i].connectors[j].aimSegNum; 

      segments_[i]->createConnector( segments_[aimSegNum]->getBodyID(),
          &((*_compound).segments[i].connectors[j]) );
    }
  }
}

void Compound::createSensor(SensorDescription *sensDes, dGeomID *targetGeom,
    LightManager *lm)
{
  SensorStruct *sS = sensDes->getSensorStruct(); 

  if(this->existSegment(sS->srcSeg))
  {
    segments_[sS->srcSeg]->createSensor(sensDes, targetGeom, lm);
  }
}


void Compound::addGlobalConnector(ConnectorDescription *connStruct, int srcSegNum,
    dBodyID *aimBody)
{
  Y_DEBUG("Compound::addGlobalConnector start");
  if(aimBody == NULL || *aimBody == NULL  )
  {
    Y_DEBUG("Compound::addGlobalConnector found *aimBody == 0");
    segments_[srcSegNum]->createConnector(0, connStruct);
  }
  else
  {
    Y_DEBUG("Compound::addGlobalConnector *aimBody != 0");
    segments_[srcSegNum]->createConnector(aimBody, connStruct);
  }
}

void Compound::setMotorValues(unsigned int seg, unsigned int motor,
    vector<double> values)
{
  if(this->existSegment(seg))
  {
    segments_[seg]->setMotorValues(motor, values);
  }
  else
  {
    return;
  }
}
    
void Compound::setMotorValue(unsigned int seg, unsigned int motor, unsigned
    int inputNum, double value)
{
  if(this->existSegment(seg))
  {
    segments_[seg]->setMotorValue(motor, inputNum, value);
  }
  else
  {
    return;
  }
}

void Compound::setForces(double fx, double fy, double fz)
{
  for(unsigned int i = 0; i < segments_.size(); ++i) 
  {
    segments_[i]->setForces(fx, fy, fz);
  }

}


vector<double> Compound::getSensorValues(unsigned int seg, unsigned int sensor)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getSensorValues(sensor);
  }
  else
  {
    throw("Compound.cpp::getSensorValues segment does not exist");
    vector<double> unknown;
    unknown.push_back(-1.0);
    return unknown;
  }
}

dBodyID* Compound::getSegmentBodyID(unsigned int seg)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getSegmentBodyID();
  }
  else
  {
    return NULL;
  }

}

void Compound::update()
{
  for(unsigned int i = 0; i < segments_.size(); ++i)
  {
    segments_[i]->update();
  }
}

void Compound::updateForces()
{
  for(unsigned int i = 0; i < segments_.size(); ++i)
  {
    segments_[i]->updateForces();
  }
}

unsigned int Compound::getNumberOfSegments()
{
  return segments_.size();
}

unsigned int Compound::getNumberOfMotors(unsigned int seg)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getNumberOfMotors();
  }
  else
  {
    return 0;
  }
}

unsigned int Compound::getNumberOfMotorInputs(unsigned int seg, unsigned int motor)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getNumberOfMotorInputs(motor);
  }
  else
  {
    return 0;
  }
}

void Compound::getMotorName(unsigned int seg, unsigned int motor, string &name)
{
  if(this->existSegment(seg))
  {
    segments_[seg]->getMotorName(motor, name);
  }
  else
  {
    // TODO: error handling
    name = "MotorDoesNotExist";
    return;
  }
}

double Compound::getMotorMinInput(unsigned int seg, unsigned int motor, unsigned
    int motorInput)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getMotorMinInput(motor, motorInput);
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

double Compound::getMotorMaxInput(unsigned int seg, unsigned int motor, unsigned
    int motorInput)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getMotorMaxInput(motor, motorInput);
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

unsigned int Compound::getNumberOfSensors(unsigned int seg)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getNumberOfSensors();
  }
  else
  {
    return 0;
  }
}

unsigned int Compound::getNumberOfSensorValues(unsigned int seg, unsigned int
    sensor)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getNumberOfSensorValues(sensor);
  }
  else
  {
    return 0;
  }
}

unsigned int Compound::getNumberOfSensorValues(unsigned int seg)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getNumberOfSensorValues();
  }
  else
  {
    return 0;
  }
}

void Compound::getSensorName(unsigned int seg, unsigned int sensor, string &name)
{
  if(this->existSegment(seg))
  {
    segments_[seg]->getSensorName(sensor, name);
  }
  else
  {
    // TODO: error handling
    name = "SensorDoesNotExist";
    return;
  }
}

double Compound::getSensorMinOutput(unsigned int seg, unsigned int sensor,
    unsigned int sensorOutput)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getSensorMinOutput(sensor, sensorOutput);
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

double Compound::getSensorMaxOutput(unsigned int seg, unsigned int sensor,
    unsigned int sensorOutput)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getSensorMaxOutput(sensor, sensorOutput);
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

int Compound::getSensorUsage(unsigned int seg, unsigned int sensor)
{
  if(this->existSegment(seg))
  {
    return segments_[seg]->getSensorUsage(sensor);
  }
  else
  {
    // TODO: error handling
    return DEF_SENSOR_USED_AS_CONTROL_INPUT; // default
  }
}

void Compound::stopMotors()
{
  for(unsigned int i = 0; i < segments_.size(); ++i)
  {
    segments_[i]->stopMotors();
  }
}


void Compound::setLightManager(LightManager *lm)
{
  for(unsigned int i = 0; i < segments_.size(); ++i) 
  {
    segments_[i]->setLightManager(lm);
  }
}


Segment* Compound::getSegment(unsigned int seg)
{
  if(this->existSegment(seg))
  {
    return segments_[seg];
  }
  
  return NULL;
}

void Compound::getCompoundName(string &name)
{
  name = _name;
}

void Compound::setCompoundName(string name)
{
  _name = name;
}


int Compound::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{
  int isCollided = 0;

  isCollided = 0;
  for(unsigned int i = 0; i < segments_.size(); ++i)
  {
    // only if a collision in one of each segment is detected then return
    // value is set to 1
    if(segments_[i]->selfCollide(geom1, geom2, contact) == 1)
    {
      // do not use return, so that all segments are processed
      isCollided = 1;
    }
  }
 
  return isCollided;
}

int Compound::isCompound(dGeomID geom)
{
  return (dGeomGetSpace(geom) == spaceID_);
}

dBodyID* Compound::getSegmentBodyID(int seg)
{
  return segments_[seg]->getBodyID();
}

dGeomID* Compound::getSegmentGeomID(int seg, int geom)
{
  return segments_[seg]->getGeomID(geom);
}

bool Compound::existSegment(unsigned int seg)
{
  if(seg >= this->getNumberOfSegments())
  {
    Y_FATAL("Compound ERROR");
    Y_FATAL("    Segment Number %d doesn't exist.", seg);
    return false;
  }
  return true;
}
