/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _ROBOT_HANDLER_H_
#define _ROBOT_HANDLER_H_
#include <gui/DrawManager.h>

#include <ode/ode.h>
#include <description/RobotDescription.h>
#include <base/Robot.h>
#include <description/MoveableDescription.h>
#include <container/YarsContainers.h>

#include <env/Environment.h>
#include <env/LightManager.h>
#include <base/CameraHandler.h>

#define RH_TRANSFER_FUNCTION_TANH 0
#define RH_TRANSFER_FUNCTION_SIGM 1

using namespace std;

class RobotHandler
{
  public:
    RobotHandler(dWorldID world, dSpaceID space, DrawManager *drawManager,
        MoveableDescription *moveDes, Environment *environment, CameraHandler *views, string pathToLibs);
    ~RobotHandler();

    void create(LightManager *lm);

    /* set the velocity of a spefic motor 
     * value is either a velocity or a angle, depends on the motorType
     * see RobotDescription.h) */
    void setMotorValue(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int motor, unsigned int inputNum, double value);

    void setMotorValues(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int motor, vector<double> values);

    /* returns sensor values of a specific sensor of a specific segment of a specific compound of a specific robot
       , range depends on the mapping*/
    vector<double> getSensorValues(unsigned int robot, unsigned int compound,
        unsigned int seg, unsigned int sensor);

    dBodyID* getSegmentBodyID(unsigned robot, unsigned int compound, unsigned
        int seg);

    /* returns the space id where all geoms of a distinct robot are placed */
    dSpaceID getRobotsSpaceID(unsigned int robot);

    /* updates the motor and sensor signals, has to be executed in every time
     * step (see main.cpp) */
    void update();
    
    // returns the number of moveables that are declared as "active"
    unsigned int getNumberOfActiveRobots();
    
    // returns the number of moveables that are declared as "controlled"
    unsigned int getNumberOfControlledRobots();
   
    // returns the number of moveables that are declared as "moving"
    unsigned int getNumberOfControlledObjects();
    
    // returns the number of moveables that are declared as "passive"
    unsigned int getNumberOfPassiveRobots();
      
    unsigned int getNumberOfCompounds(unsigned int robot);
    
    unsigned int getNumberOfSegments(unsigned int robot, unsigned int compound);

    // motor functions
    unsigned int getNumberOfMotors(unsigned int robot, unsigned int compound,
        unsigned int seg);

    unsigned int getNumberOfMotorInputs(unsigned int robot, unsigned int compound,
        unsigned int seg, unsigned int motor);

    void getMotorName(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int motor, string &name);

    double getMotorMinInput(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int motor, unsigned int motorInput);

    double getMotorMaxInput(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int motor, unsigned int motorInput);

    // sensor functions
    unsigned int getNumberOfSensors(unsigned int robot, unsigned int compound,
        unsigned int seg);

    unsigned int getNumberOfSensorValues(unsigned int robot, unsigned int
        compound, unsigned int seg, unsigned int sensor);

    unsigned int getNumberOfSensorValues(unsigned int robot, unsigned int compound,
        unsigned int seg);

    void getSensorName(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int sensor, string &name);

    double getSensorMinOutput(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int sensor, unsigned int sensorOutput);

    double getSensorMaxOutput(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int sensor, unsigned int sensorOutput);

    int getSensorUsage(unsigned int robot, unsigned int compound, unsigned int
        seg, unsigned int sensor);

    void  stopRobot(unsigned int robot);

    int selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);
    
    void updateControlled();

    void randomiseInitialPosition();
  
    Robot* getRobot(unsigned int robot);

    void setEnvironment(Environment* environment);

    void setDrawManager(DrawManager* drawManager);

  private:
    bool existRobot(unsigned int robot);
      
    dWorldID worldID_;
    dSpaceID spaceID_;
 
    DrawManager *drawManager_;

    Environment *environment_;

    MoveableDescription *moveDes_;

    CameraHandler *_robotViews;

    vector<Robot*> _activeRobots;
    
    vector<Robot*> _controlledRobots;

    vector<Robot*> _passiveRobots;
    
    vector<Robot*> _controlledObjects;

    int transferFunction_;

    string _pathToLibs;

};

#endif
