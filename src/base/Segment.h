/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _SEGMENT_H_
#define _SEGMENT_H_

#include <gui/DrawManager.h>
#include <description/RobotDescription.h>
#include <description/SensorDescription.h>
#include <ode/ode.h>
#include <motors/ComplexHinge.h>

#include <env/Environment.h>
#include <base/MappingFunction.h>
#include <base/Sensor.h>
#include <env/LightManager.h>
#include <sensors/CoordinateSensor.h>
#include <sensors/AngleSensor.h>
#include <sensors/AngleVelSensor.h>
#include <sensors/SliderPositionSensor.h>
#include <sensors/SliderVelSensor.h>
#include <sensors/AngleFeedbackSensor.h>
#include <sensors/GenericIrSensor.h>
#include <sensors/GenericRotationSensor.h>
#include <sensors/SharpIrGP2D12_37.h>
#include <sensors/SharpIrDM2Y3A003K0F.h>
#include <sensors/LdrSensor.h>
#include <sensors/AmbientLightSensor.h>
#include <sensors/DirectedCamera.h>
#include <util/Gauss.h>
#include <util/defines.h>
#include <container/YarsContainers.h>

#include <string>
#include <vector>
#include <values.h>

#include <util/math/Matrix.h>
#include <util/filter/MovingAverage.h>
//#include <container/YarsContainers.h>

#include <base/CameraHandler.h>

/* for hyperbolic tangent */
#define MAP_TRANS_MIN       -1.0
#define MAP_TRANS_MAX        1.0

using namespace std;

class Segment
{
  public:
    Segment(SingleSegment *singleSeg, dWorldID w, dSpaceID s, DrawManager *d, Environment *e, CameraHandler *views);
    ~Segment();

    //create all parts of the segment without connections;
    void createSegment();

    /* create all connections */
    /* IMPORTANT: call this function after you have created all segments and
     * segment compounds in the robot class */
    void createConnector(dBodyID *aimBody, ConnectorDescription* connStruct);
    
    /* create a sensor, as defined in the description */
    void createSensor(SensorDescription *sensDes, dGeomID *targetGeom,
        LightManager *lm);
    
    // is this geom part of the segment compound? (for collision)
    int isSegment(dGeomID geom);

    int selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

    /* returns the body id of the segment */
    dBodyID* getBodyID();

    /* returns the geom id of the segment */
    dGeomID* getGeomID(int geom);

    /* set the velocity of a spefic motor 
     * value is either a velocity or a angle, depends on the motorType
     * (see RobotDescription.h) */
    void setMotorValue(unsigned int motor, unsigned int inputNum, double value);

    void setMotorValues(unsigned int motor, vector<double> values);

    void updatePassiveJoint(unsigned int passiveJoint);

    void updateMotorActivation(unsigned int motor);
    
    /* set the forces of the segment/ object */
    void setForces(double fx, double fy, double fz);

    vector<double> getSensorValues(unsigned int sensor);

    dBodyID* getSegmentBodyID();
      
    void update();
  
    void updateForces();
    /* set for all motors vel to zero, and fMax to 1000, to stop all motors */
    void stopMotors();

    void setLightManager(LightManager *lm);
    
    // motor functions
    unsigned int getNumberOfMotors();

    unsigned int getNumberOfMotorInputs(unsigned int motor);

    void getMotorName(unsigned int motor, string &name);

    double getMotorMinInput(unsigned int motor, unsigned int motorInput);

    double getMotorMaxInput(unsigned int motor, unsigned int motorInput);

    // sensor functions
    unsigned int getNumberOfSensors();

    unsigned int getNumberOfSensorValues(unsigned int sensor);

    unsigned int getNumberOfSensorValues();

    void getSensorName(unsigned int sensor, string &name);

    double getSensorMinOutput(unsigned int sensor, unsigned int sensorOutput);

    double getSensorMaxOutput(unsigned int sensor, unsigned int sensorOutput);

    int getSensorUsage(int sensor);

    void getSegName(string &name);

    void setIsBumpable(bool isBumpable);
    void setTraceContact(bool traceContact);

  private:
    /* stores all necessary information of controllable joints and
     * non-controllable joints with special properties (spring, damper) of a
     * segment object */
    struct MotorJoint
    {
      int    jointNum;
      int    jointType;
      int    motorType;

      // name of this joint
      string name;

      // spring
      bool   springEnabled;
      double springInitialPosition;
      double springConstant;

      // damper
      bool   damperEnabled;
      double dampingConstant;
      MovingAverage *mAD;     // moving average filter for the damper

      // from here an only needed by controllable joints
      vector<double> inputs;
      vector<double> mappedInputs;

      double vMax;
      double fMax;

      // servos
      double loStop;
      double hiStop;
      double slowDownBias;
      double stopBias;

      MappingFunction *mapMotor;

      // might also be used by a passive complex hinge joint
      ComplexHinge *complexHinge;

      Gauss *gauss;
    };

    struct SensorObj
    {
      int type;
      int usage;   // controlInput, aux or internal
      string name; // name of this sensor
      Sensor* sensor;
      double startValue;
      double endValue;
      MappingFunction* mapSensor;
      Gauss *gauss;
      int numberOfValues;
    };

    struct SegmentObject
    {
      dBodyID body;
      //dGeomID geom;
      /* we can have multiple geoms per body */
      vector<dGeomID> geoms;
      /* all joint ids (fix and others) */
      vector<dJointID> joints;
      /* information storage of active joints */
      vector<struct MotorJoint> motors;
      /* information storage of passive joints with special props */
      vector<struct MotorJoint> passiveJoints;
      /* information storage of sensors */
      vector<struct SensorObj> sensors;

      int numberOfSensorValues;

      double fx, fy, fz;
    };

    /* creates a composite object */
    void createCompositeObject(SegmentObject *segment, 
        SingleSegment *singleSeg);

    /* creates a box */
    void createBox(dBodyID *body, dGeomID *geom, Position *pos,
        Position *rot, Position *dim, double *mass, int *massGeometry, 
        Position *massOffset, Position *massDimension, 
        Position *massRelativeRotation, bool visualizeMass);

    /* creates a sphere */
    void createSphere(dBodyID *body, dGeomID *geom, Position *pos, 
        Position *rot, Position *dim, double *mass, int *massGeometry, 
        Position *massOffset, Position *massDimension, 
        Position *massRelativeRotation, bool visualizeMass);
 

    /* creates a capped cylinder */
    void createCappedCylinder(dBodyID *body, dGeomID *geom, Position *pos,
        Position *rot, Position *dim, double *mass, int *massGeometry, 
        Position *massOffset, Position *massDimension, 
        Position *massRelativeRotation, bool visualizeMass);

    /* creates a cylinder */
    void createCylinder(dBodyID *body, dGeomID *geom, Position *pos,
        Position *rot, Position *dim, double *mass, int *massGeometry, 
        Position *massOffset, Position *massDimension, 
        Position *massRelativeRotation, bool visualizeMass);


    /* create custom rigidBody and do geom offsets */
    void createRigidBody(dBodyID *body, dGeomID *geom, Position *pos,
        Position *rot, double *mass, int *massGeometry, Position *massOffset,
        Position *massDimension, Position *massRelativeRotation, bool visualizeMass);


    /* rotate a geom about the three euler angles in rad */ 
    void setGeomRotation(dGeomID* geom, Position *rotation);
    /* connect the segment to a body via a fixed hinge joint, this joint is
     * related to this segment */
    void connectFix(dBodyID *aimBody, Position *pos);
    
    /* connect one segment to a body with a hinge, this joint is
     * related to srcSeg */
    void connectHinge(dBodyID *aimBody, const Position
        *pos, const Position *axis);
 
    void connectHinge2(dBodyID *aimBody, const Position *pos, const Position
        *axis, Position *axis2);
    
    /* connect one segment to a body with a slider, this joint is
     * related to srcSeg */
    void connectSlider(dBodyID *aimBody, const Position *axis);

    /* set the lo and hi stops for a specific joint, has to be a hinge, values in
     * rad */
    void setJointStops(dJointID *joint, const double loStop, const double
        hiStop);
    
    /* returns false, if the sensor not exist within this segment */
    bool existSensor(unsigned int sensor);

    /* returns false, if the motor not exist within this segment */
    bool existMotor(unsigned int motor);

    /* creates an angle sensor */
    void createAngleSensor(SensorDescription *sensDes);
 
    /* creates an angle vel sensor */
    void createAngleVelSensor(SensorDescription *sensDes);
    
    /* creates an slider position sensor */
    void createSliderPositionSensor(SensorDescription *sensDes);
 
    /* creates an slider vel sensor */
    void createSliderVelSensor(SensorDescription *sensDes);
    
    /* creates an angle feedback sensor */
    void createAngleFeedbackSensor(SensorDescription *sensDes);
    
    /* creates an infra red  sensor */
    void createIrSensor(SensorDescription *sensDes);

    void createGenericRotationSensor(SensorDescription *sensDes);

    void createSharp_GP2D12_37_Sensor(SensorDescription *sensDes);

    void createSharp_DM2Y3A003K0F_Sensor(SensorDescription *sensDes);

    /* create a camera sensor, without zooming, pan and tilt*/
    void createDirectedCameraSensor(SensorDescription *sensDes);

    void createLdrSensor(SensorDescription *sensDes);

    void createAmbientLightSensor(SensorDescription *sensDes, LightManager *lm);

    /* creates a coordinate sensor*/
    void createCoordinateSensor(SensorDescription *sensDes, 
        dGeomID *targetGeom);

    /* calculates absolute standard derivation for gaussian noise out of relative
     * standard derivation */
    double calcAbsNoiseRange(const double sensorRange, 
        const double relNoiseRange);

    void setupMechanicalJoint(ConnectorDescription *connStruct, 
        unsigned int jointNum);
    void setupSimpleActiveJoint(ConnectorDescription *connStruct, 
        unsigned int jointNum);
    void setupPassiveJoint(ConnectorDescription *connStruct, 
        unsigned int jointNum);
    void setupPassiveProperties(MotorJoint *joint, 
        ConnectorDescription *connStruct);
    void setupComplexHingePassiveJoint(ConnectorDescription *connStruct, 
        unsigned int jointNum, dBodyID *aimBody);
    void setupComplexHingeActiveJoint(ConnectorDescription *connStruct, 
        unsigned int jointNum, dBodyID *aimBody);
    void calcDesiredPassiveForceAndVel(double currPos, double currVel, 
        double *desVel, double *desForce, MotorJoint *passiveJoint);

    long getLongTime();

    void addTraceGeom();
    void addTraceLineGeom();

    /* this comes from the robot description */
    SingleSegment *singleSeg_;
    dWorldID worldID_;
    dSpaceID spaceID_;
    DrawManager *drawManager_;
    dJointGroupID jointGroupID_;
    Environment *environment_;
    CameraHandler *robotViews_;

    /* This struct contains all relevant data for the segment object */
    SegmentObject segment_;
    LightManager *lightManager_;

    vector<struct ContactParameter> contactInfo_;
    bool isBumpedExceptWithPlane_;
    bool isBumpable_; // with ground plane
    bool traceContact_; // with ground plane
    int _iterations;
    vector<dGeomID*> _geoms;
    dGeomID _massGeom;

    FILE *traceFile;
};

#endif
