/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Segment.h"
#include <iostream>
#include <sstream>

using namespace std;

Segment::Segment(SingleSegment *singleSeg, dWorldID w, dSpaceID s, 
    DrawManager *d, Environment *e, CameraHandler *views)
{
  singleSeg_   = singleSeg;
  worldID_     = w;
  /* all geoms of one segment compound are in the same space, so this class has the
   * same space like in segment compound class */
  spaceID_     = s;
  drawManager_ = d;
  environment_ = e;
  robotViews_  = views;
  dGeomID* newGeomPointer = NULL;
  _geoms.push_back(newGeomPointer);
  _massGeom = NULL;
  jointGroupID_ = dJointGroupCreate(0);
  this->createSegment();
  _iterations = -1;

  ContactParameter newContactInfo;
  newContactInfo.mode        = singleSeg_->contactMode;
  newContactInfo.bounce      = singleSeg_->cBounce;
  newContactInfo.bounceVel   = singleSeg_->cBounceVel;
  newContactInfo.slip1       = singleSeg_->cSlip1;
  newContactInfo.slip2       = singleSeg_->cSlip2;
  newContactInfo.softCFM     = singleSeg_->cSoftCFM;
  newContactInfo.softERP     = singleSeg_->cSoftErp;
  newContactInfo.mu          = singleSeg_->cMu;
  newContactInfo.mu2         = singleSeg_->cMu2;
  contactInfo_.push_back(newContactInfo);

  isBumpedExceptWithPlane_ = false;
  isBumpable_              = singleSeg_->isBumpable;
  traceContact_            = singleSeg_->traceContact;
  Y_DEBUG("Segment::Segment: traceContact_ = %d", traceContact_);

  if(traceContact_ == true)
  {
    std::ostringstream oss;
    oss << "trace_" << singleSeg_->name << ".txt";
    string s = oss.str();
    Y_DEBUG("starting trace file %s\n", s.c_str());
    traceFile = fopen(s.c_str(),"w");
  }
}

Segment::~Segment()
{
  /* destroy all sensors */
  for(unsigned int i = 0; i < segment_.sensors.size(); ++i)
  {
    if(segment_.sensors[i].type = SD_DIRECTED_CAMERA_SENSOR)
    {
      delete segment_.sensors[i].sensor;
      delete segment_.sensors[i].mapSensor;

    }
    else
    {
      delete segment_.sensors[i].mapSensor;
      delete segment_.sensors[i].gauss;
      delete segment_.sensors[i].sensor;
    }
  }

  for(unsigned int i = 0; i < segment_.motors.size(); ++i)
  {
    delete segment_.motors[i].mapMotor;
    delete segment_.motors[i].gauss;

    if(segment_.motors[i].complexHinge != NULL)
    {
      delete segment_.motors[i].complexHinge;
    }

    if(segment_.motors[i].mAD != NULL)
    {
      delete segment_.motors[i].mAD;
    }
  }

  for(unsigned int i = 0; i < segment_.passiveJoints.size(); ++i)
  {
    if(segment_.passiveJoints[i].complexHinge != NULL)
    {
      delete segment_.passiveJoints[i].complexHinge;
    }

    if(segment_.passiveJoints[i].mAD != NULL)
    {
      delete segment_.passiveJoints[i].mAD;
    }
  }

  /* all joints will be destroyed */
  dJointGroupDestroy(jointGroupID_);

  /* all geoms will be destroyed  in */
  for(int i = (segment_.geoms.size() - 1); i >= 0; --i)
  {
    dGeomDestroy(segment_.geoms[i]);
  }

  if(_massGeom != NULL)
  {
    dGeomDestroy(_massGeom);
  }

  /* destroy all bodies */
  dBodyDestroy(segment_.body);

  if(traceContact_)
  {
    fclose(traceFile);
  }
  segment_.sensors.clear();
}

void Segment::createSegment()
{
  dGeomID newGeom;

  switch(singleSeg_->type)
  {
    case RD_BOX_GEOM:
      {
        this->createBox( &segment_.body, &newGeom,
            &(singleSeg_->position), &(singleSeg_->rotation),
            &(singleSeg_->dimensions), &(singleSeg_->mass),
            &(singleSeg_->massGeometry) , &(singleSeg_->massOffset),
            &(singleSeg_->massDimension), &(singleSeg_->massRelativeRotation),
            singleSeg_->drawRigidBody);
        Color c = singleSeg_->color;  
        drawManager_->changeColor(newGeom, c.r, c.g, c.b, c.alpha);
        segment_.geoms.push_back(newGeom);
      }
      break;

    case RD_SPHERE_GEOM:
      {
        this->createSphere( &segment_.body, &newGeom,
            &(singleSeg_->position), &(singleSeg_->rotation),
            &(singleSeg_->dimensions), &(singleSeg_->mass),
            &(singleSeg_->massGeometry), &(singleSeg_->massOffset),
            &(singleSeg_->massDimension), &(singleSeg_->massRelativeRotation),
            singleSeg_->drawRigidBody );
        Color c = singleSeg_->color;  
        drawManager_->changeColor(newGeom, c.r, c.g, c.b, c.alpha);
        segment_.geoms.push_back(newGeom);
      }
      break;

    case RD_CAPPED_CYLINDER_GEOM:
      {
        this->createCappedCylinder( &segment_.body, &newGeom,
            &(singleSeg_->position), &(singleSeg_->rotation),
            &(singleSeg_->dimensions), &(singleSeg_->mass),
            &(singleSeg_->massGeometry), &(singleSeg_->massOffset),
            &(singleSeg_->massDimension), &(singleSeg_->massRelativeRotation),
            singleSeg_->drawRigidBody);
        Color c = singleSeg_->color;  
        drawManager_->changeColor(newGeom, c.r, c.g, c.b, c.alpha);
        segment_.geoms.push_back(newGeom);
      }
      break;
    case RD_CYLINDER_GEOM:
      {
        this->createCylinder( &segment_.body, &newGeom,
            &(singleSeg_->position), &(singleSeg_->rotation),
            &(singleSeg_->dimensions), &(singleSeg_->mass),
            &(singleSeg_->massGeometry), &(singleSeg_->massOffset),
            &(singleSeg_->massDimension), &(singleSeg_->massRelativeRotation),
            singleSeg_->drawRigidBody);
        Color c = singleSeg_->color;  
        drawManager_->changeColor(newGeom, c.r, c.g, c.b, c.alpha);
        segment_.geoms.push_back(newGeom);
      }
      break;
    case RD_COMPOSITE_GEOM:
      {
        this->createCompositeObject(&segment_, singleSeg_);
      }
      break;


    default:
      break;
  }

}

void Segment::createConnector(dBodyID *aimBody, ConnectorDescription *connStruct)
{
  Y_DEBUG("Segment::createConnector start");
  Position anchorPos = connStruct->anchorPos;


  switch( connStruct->type )
  {
    case RD_CONN_FIX:
      {
        this->connectFix(aimBody, &anchorPos);
      }
      break;
    case RD_CONN_HINGE:
      {
        Y_DEBUG("Segment->createConnector - case RD_CONN_HINGE");

        Position axis = connStruct->rotationAxis;
        /* create a hinge */
        this->connectHinge(aimBody, &anchorPos, &axis);

        unsigned int jointNum = segment_.joints.size()-1;
        setupMechanicalJoint(connStruct, jointNum);

        int motorType = connStruct->motorType;

        if( motorType == RD_PASSIVE_JOINT)
        {
          setupPassiveJoint(connStruct, jointNum);
        }
        else // active joint (setMotorValue(s), updateMotor ...)
        {
          setupSimpleActiveJoint(connStruct, jointNum);
        }
      }
      break;
    case RD_CONN_HINGE_2:
      {
        /* create a hinge 2*/
        Y_DEBUG("Connecting Hinge 2");
        this->connectHinge2( aimBody, &anchorPos, &(connStruct->rotationAxis),
            &(connStruct->rotationAxis2) );
      }
      break;
    case RD_CONN_SLIDER:
      {
        Y_DEBUG("Segment->createConnector - case RD_CONN_SLIDER");
        Position axis = connStruct->sliderAxis;
        /* create a slider */
        this->connectSlider(aimBody, &axis);

        unsigned int jointNum = segment_.joints.size()-1;
        setupMechanicalJoint(connStruct, jointNum);

        int motorType = connStruct->motorType;

        if( motorType == RD_PASSIVE_JOINT)
        {
          setupPassiveJoint(connStruct, jointNum);
        }
        else // active joint (setMotorValue(s), updateMotor ...)
        {
          setupSimpleActiveJoint(connStruct, jointNum);
        }
      }
      break;
    case RD_CONN_COMPLEX_HINGE:
      {
        Y_DEBUG("Segment->createConnector - case RD_CONN_COMPLEX_HINGE");

        Position axis = connStruct->rotationAxis;
        Position anchorPos = connStruct->anchorPos;

        this->connectHinge(aimBody, &anchorPos, &axis);

        unsigned int jointNum = segment_.joints.size()-1;
        setupMechanicalJoint(connStruct, jointNum);

        int motorType = connStruct->motorType;

        if( motorType == RD_PASSIVE_JOINT)
        {
          setupComplexHingePassiveJoint(connStruct, jointNum, aimBody);
        }
        else
        {
          setupComplexHingeActiveJoint(connStruct, jointNum, aimBody);
        }
      }
      break;
    default:
      break;
  }
}

void Segment::setupMechanicalJoint(ConnectorDescription *connStruct, unsigned int jointNum)
{
  /* set the stops */
  double loStop = connStruct->min_deflection;
  double hiStop = connStruct->max_deflection;

  this->setJointStops( &(segment_.joints[jointNum]), loStop, hiStop );
}

void Segment::setupSimpleActiveJoint(ConnectorDescription *connStruct, 
    unsigned int jointNum)
{
  MotorJoint motor;
  motor.jointNum  = jointNum;
  motor.name      = connStruct->name;
  motor.vMax      = connStruct->velMax;
  motor.fMax      = connStruct->fMax;
  motor.jointType = connStruct->type;
  motor.motorType = connStruct->motorType;
  motor.inputs.resize(1,0);        // we need 1 input + initialize it with 0
  motor.mappedInputs.resize(1,0);

  setupPassiveProperties(&motor, connStruct); // spring, damper ...

  if(motor.motorType == RD_ANGLE_MOTOR)
  {
    motor.loStop       = connStruct->min_deflection;
    motor.hiStop       = connStruct->max_deflection;
    motor.slowDownBias = connStruct->slowDownBias;
    motor.stopBias     = connStruct->stopBias;

    motor.mapMotor  = new MappingFunction(connStruct->startValue,
        connStruct->endValue, motor.loStop, motor.hiStop);
  }
  else if(motor.motorType == RD_VEL_MOTOR)
  {
    motor.mapMotor = new MappingFunction(connStruct->startValue,
        connStruct->endValue, -motor.vMax, motor.vMax);
  }

  motor.gauss = new Gauss(this->calcAbsNoiseRange(fabs(connStruct->startValue -
          connStruct->endValue), connStruct->noise));

  segment_.motors.push_back(motor);
}

void Segment::setupPassiveJoint(ConnectorDescription *connStruct, 
    unsigned int jointNum)
{
  if(connStruct->damper.active || connStruct->spring.active)
  {
    MotorJoint passiveJoint;

    passiveJoint.name      = connStruct->name;

    passiveJoint.jointNum  = jointNum;
    passiveJoint.jointType = connStruct->type;
    passiveJoint.motorType = connStruct->motorType;

    setupPassiveProperties(&passiveJoint, connStruct);

    segment_.passiveJoints.push_back(passiveJoint);
  }
  else
  {
    // we don't need to add it to the passiveJoint vector cause ODE will
    //   take care of everything / no interaction necessary
  }
}

void Segment::setupPassiveProperties(MotorJoint *joint, ConnectorDescription *connStruct)
{
  // TODO: we should have a MotorJointInitialise function 
  joint->damperEnabled   = false;
  joint->springEnabled   = false;

  joint->complexHinge    = NULL;

  if(connStruct->damper.active)
  {
    joint->damperEnabled   = connStruct->damper.active;
    joint->dampingConstant = connStruct->damper.dampingConstant;
    joint->mAD             = new MovingAverage(4, 0.0, true, 1000);
  }
  else
  {
    joint->mAD             = NULL;
  }

  if(connStruct->spring.active)
  {
    joint->springEnabled         = connStruct->spring.active;
    joint->springConstant        = connStruct->spring.springConstant;
    joint->springInitialPosition = connStruct->spring.initialPosition;
  }
}

void Segment::setupComplexHingePassiveJoint(ConnectorDescription *connStruct, 
    unsigned int jointNum, dBodyID *aimBody)
{
  MotorJoint passiveJoint;

  passiveJoint.name      = connStruct->name;

  passiveJoint.jointNum  = jointNum;
  passiveJoint.jointType = connStruct->type;
  passiveJoint.motorType = connStruct->motorType;
  passiveJoint.mAD       = NULL;

  /* create complexHinge object */
  passiveJoint.complexHinge = new ComplexHinge(connStruct,
      &(segment_.joints[jointNum]), &(segment_.body), aimBody);

  segment_.passiveJoints.push_back(passiveJoint);
}

void Segment::setupComplexHingeActiveJoint(ConnectorDescription *connStruct, 
    unsigned int jointNum, dBodyID *aimBody)
{
  MotorJoint motor;

  motor.name      = connStruct->name;

  motor.jointNum     = jointNum;
  motor.jointType    = connStruct->type;
  motor.motorType    = connStruct->motorType;
  motor.mAD          = NULL;

  motor.inputs.resize(2,0); // we need 2 inputs, initialize them with 0
  motor.mappedInputs.resize(2,0);

  motor.mapMotor = new MappingFunction(connStruct->startValue,
      connStruct->endValue, 0, 1);
  (motor.mapMotor)->addMap(connStruct->startValue2, connStruct->endValue2, 0, 1);

  // TODO: how to combine the noise of the subcomponents
  motor.gauss = new Gauss(this->calcAbsNoiseRange(fabs(
          connStruct->startValue - connStruct->endValue),
        connStruct->motor.noise[0]));
  motor.gauss->add(this->calcAbsNoiseRange(fabs(
          connStruct->startValue - connStruct->endValue),
        connStruct->motor.noise[1]));

  /* create complexHinge object */
  motor.complexHinge = new ComplexHinge(connStruct,
      &(segment_.joints[jointNum]), &(segment_.body), aimBody);

  segment_.motors.push_back(motor);
}


void Segment::createSensor(SensorDescription *sensDes, 
    dGeomID* targetGeom, LightManager *lm)
{
  switch(sensDes->getSensorStruct()->type)
  {
    case SD_ANGLE_SENSOR:
      {
        this->createAngleSensor(sensDes);
      }
      break;
    case SD_ANGLE_VEL_SENSOR:
      {
        this->createAngleVelSensor(sensDes);
      }
      break;
    case SD_SLIDER_POSITION_SENSOR:
      {
        this->createSliderPositionSensor(sensDes);
      }
      break;
    case SD_SLIDER_VEL_SENSOR:
      {
        this->createSliderVelSensor(sensDes);
      }
      break;
    case SD_ANGLE_FEEDBACK_SENSOR:
      {
        this->createAngleFeedbackSensor(sensDes);
      }
      break;
    case SD_IR_SENSOR:
      {
        this->createIrSensor(sensDes);
      }
      break;
    case SD_GEN_ROTATION_SENSOR:
      {
        this->createGenericRotationSensor(sensDes);
      }
      break;
    case SD_SHARP_GP2D12_37_SENSOR:
      {
        this->createSharp_GP2D12_37_Sensor(sensDes);
      }
      break;
    case SD_SHARP_DM2Y3A003K0F_SENSOR:
      {
        this->createSharp_DM2Y3A003K0F_Sensor(sensDes);
      }
      break;

    case SD_LDR_SENSOR:
      {
        this->createLdrSensor(sensDes);
      }
      break;
    case SD_COORDINATE_SENSOR:
      {
        this->createCoordinateSensor(sensDes, targetGeom);
      }
      break;
    case SD_AMBIENT_LIGHT_SENSOR:
      {
        this->createAmbientLightSensor(sensDes, lm);
      }
      break;
    case SD_DIRECTED_CAMERA_SENSOR:
      {
        this->createDirectedCameraSensor(sensDes);
      }
    default:
      break;
  }
}

void Segment::setMotorValues(unsigned int motor, vector<double> values)
{
  if(this->existMotor(motor) && (values.size() == segment_.motors[motor].inputs.size()))
  {
    segment_.motors[motor].inputs = values;
  }
  else
  {
    Y_FATAL("!!!setMotorValues -> vector sizes do NOT match --> values not set!!!");
  }
}

void Segment::setMotorValue(unsigned int motor, unsigned int inputNum, double
    value)
{
  if((! this->existMotor(motor)) || (inputNum >= segment_.motors[motor].inputs.size()))
  {
    return;
  }
  else
  {
    segment_.motors[motor].inputs[inputNum] = value;
  }
}

void Segment::calcDesiredPassiveForceAndVel(double currPos, double currVel,
    double *desVel, double *desForce, MotorJoint *passiveJoint)
{
  *desVel   = 0.0;
  *desForce = 0.0;

  // damper
  if(passiveJoint->damperEnabled == 1)
  {
    // we need to filter the damping Force due to the discrete stepping,
    // otherwise we could get ugly oscillations
    *desForce = passiveJoint->mAD->getFilteredOutput(-1 * currVel *
      passiveJoint->dampingConstant);
  }

  // spring
  if(passiveJoint->springEnabled == 1)
  {
    *desForce += -1 * (currPos - passiveJoint->springInitialPosition)
                 * passiveJoint->springConstant;

    if(*desForce > 0.0)
    {
      *desVel = 1000.0;
    }
    else
    {
      *desVel    = -1000.0;
      *desForce *= -1;
    }
  }
  else
  {
    if(*desForce < 0.0)
    {
      *desForce *= -1;
    }
  }

}

void Segment::updatePassiveJoint(unsigned int passiveJoint)
{
  double currPos, currVel, desVel, desForce;
  // TODO: maybe we should store the jointID in the passiveJoints struct ?!
  dJointID joint =
    segment_.joints[segment_.passiveJoints[passiveJoint].jointNum];

  switch(segment_.passiveJoints[passiveJoint].jointType)
  {
    case RD_CONN_HINGE:
      {
        currPos  = dJointGetHingeAngle(joint);
        currVel  = dJointGetHingeAngleRate(joint);

        calcDesiredPassiveForceAndVel(currPos, currVel, &desVel, &desForce,
            &(segment_.passiveJoints[passiveJoint]));

        dJointSetHingeParam(joint, dParamVel, desVel);
        dJointSetHingeParam(joint, dParamFMax, desForce);
      }
      break;
    case RD_CONN_HINGE_2:
      {
        // TODO: not implemented yet
      }
      break;
    case RD_CONN_BALL:
      {
        // TODO: not implemented yet
      }
      break;
    case RD_CONN_SLIDER:
      {
        currPos  = dJointGetSliderPosition(joint);
        currVel  = dJointGetSliderPositionRate(joint);

        calcDesiredPassiveForceAndVel(currPos, currVel, &desVel, &desForce,
            &(segment_.passiveJoints[passiveJoint]));

        dJointSetSliderParam(joint, dParamVel, desVel);
        dJointSetSliderParam(joint, dParamFMax, desForce);
      }
      break;
    case RD_CONN_COMPLEX_HINGE:
      {
        segment_.passiveJoints[passiveJoint].complexHinge->updatePassiveComplexHinge();
      }
      break;
    default:
      break;
  }
}

void Segment::updateMotorActivation(unsigned int motor)
{
  if( !(this->existMotor(motor)) )
  {
    return;
  }

  int i, type, jointNum;

  vector<double>* inputs       = &(segment_.motors[motor].inputs);
  vector<double>* mappedInputs = &(segment_.motors[motor].mappedInputs);

  jointNum = segment_.motors[motor].jointNum;

  type = segment_.motors[motor].motorType;

  if((*inputs).size() == 1)
  {
    (*mappedInputs)[0] = segment_.motors[motor].mapMotor->map(
        segment_.motors[motor].gauss->next((*inputs)[0]));
  }
  else
  {
    for(int i=0; i < (*inputs).size(); i++)
    {
      (*mappedInputs)[i] = (*inputs)[i];
    }
    segment_.motors[motor].gauss->next(mappedInputs);
    segment_.motors[motor].mapMotor->map(mappedInputs);
  }

  switch(segment_.motors[motor].jointType)
  {

    case RD_CONN_HINGE:
      {
        // should definitely make a function out of this ;-)
        switch(type)
        {
          case RD_VEL_MOTOR:
            {
              dJointSetHingeParam(segment_.joints[jointNum], dParamVel,(*mappedInputs)[0]);
              dJointSetHingeParam(segment_.joints[jointNum], dParamFMax,
                  segment_.motors[motor].fMax);

            }
            break;
          case RD_ANGLE_MOTOR:
            {
              double vel, diff, slowDownBias, stopBias;

              diff = (*mappedInputs)[0] - dJointGetHingeAngle(segment_.joints[jointNum]);

              slowDownBias = segment_.motors[motor].slowDownBias;
              stopBias = segment_.motors[motor].stopBias;

              diff = diff / slowDownBias; //normalized

              if (fabs(diff) > slowDownBias)
              {
                vel = segment_.motors[motor].vMax *(diff/fabs(diff));
              }
              else
              {
                vel = segment_.motors[motor].vMax * diff;
              }

              if(fabs(diff) < stopBias)
              {
                dJointSetHingeParam(segment_.joints[jointNum], dParamVel, 0.0);
                dJointSetHingeParam(segment_.joints[jointNum], dParamFMax,
                    segment_.motors[motor].fMax);
              }
              else
              {  
                dJointSetHingeParam(segment_.joints[jointNum], dParamVel, vel);
                dJointSetHingeParam(segment_.joints[jointNum], dParamFMax,
                    segment_.motors[motor].fMax);
              }

            }
            break;
          default:
            break;
        }
      }
      break;

    case RD_CONN_SLIDER:
      {
        // should make a function out of this ;-)
        switch(type)
        {
          case RD_VEL_MOTOR:
            {
              dJointSetSliderParam(segment_.joints[jointNum], dParamVel, (*mappedInputs)[0]);
              dJointSetSliderParam(segment_.joints[jointNum], dParamFMax,
                  segment_.motors[motor].fMax);
            }
            break;
            //should change the attribute name to account for other joint types
          case RD_ANGLE_MOTOR:
            {
              double vel, diff, slowDownBias, stopBias;

              diff = (*mappedInputs)[0] - dJointGetSliderPosition(segment_.joints[jointNum]);

              slowDownBias = segment_.motors[motor].slowDownBias;
              stopBias = segment_.motors[motor].stopBias;

              diff = diff / slowDownBias; //normalized

              if (fabs(diff) > slowDownBias)
              {
                vel = segment_.motors[motor].vMax *(diff/fabs(diff));
              }
              else
              {
                vel = segment_.motors[motor].vMax * diff;
              }

              if(fabs(diff) < stopBias)
              {
                dJointSetSliderParam(segment_.joints[jointNum], dParamVel, 0.0);
                dJointSetSliderParam(segment_.joints[jointNum], dParamFMax,
                    segment_.motors[motor].fMax);
              }
              else
              {  
                dJointSetSliderParam(segment_.joints[jointNum], dParamVel, vel);
                dJointSetSliderParam(segment_.joints[jointNum], dParamFMax,
                    segment_.motors[motor].fMax);
              }
            }
            break;
          default:
            break;
        }
      }
      break;
    case RD_CONN_HINGE_2:
      {
        // TODO: not implemented yet / instead RD_CONN_HINGE is used ...
      }
      break;
    case RD_CONN_BALL:
      {
        // TODO: not implemented yet
      }
      break;
    case RD_CONN_COMPLEX_HINGE:
      {
        // should definitely make a function out of this ;-)
        switch(type)
        {
          case RD_VEL_MOTOR:

            // TODO: switch to mapping function
            segment_.motors[motor].complexHinge->updateActiveComplexHinge((*mappedInputs)[0],
                (*mappedInputs)[1]);
            break;
          default:
            break;
        }
      }
      break;
    default:
      break;
  }
}

vector<double> Segment::getSensorValues(unsigned int sensor)
{
  vector<double> sensorValues;
  double ret;

  // get all return-values of the sensor
  for(int i = 0; i< segment_.sensors[sensor].sensor->getValues().size(); i++)
  {
    if(this->existSensor(sensor))
    {
      ret = segment_.sensors[sensor].sensor->getValues()[i];
      Y_DEBUG("Segment:getSensorValue.getValue %f",ret);
      ret = segment_.sensors[sensor].gauss->next(ret);
      Y_DEBUG("Segment:getSensorValue.next %f",ret);
      ret = segment_.sensors[sensor].mapSensor->map(ret);
      Y_DEBUG("Segment:getSensorValue.map %f",ret);
      sensorValues.push_back(ret);
    }
    else
    {
      sensorValues.push_back(-1.0);
    }
  }
  return sensorValues;
}

void Segment::getSegName(string &name)
{
  name = (*singleSeg_).name;
}


dBodyID* Segment::getSegmentBodyID()
{
  return &segment_.body;
}

void Segment::update()
{
  _iterations++;
  for(unsigned int i = 0; i < segment_.sensors.size(); ++i)
  {
    segment_.sensors[i].sensor->updateSensor();
  }

  for(unsigned int i = 0; i < segment_.passiveJoints.size(); ++i)
  {
    this->updatePassiveJoint(i);
  }

  /* important especially for servo motors when communication with client */
  for(unsigned int i = 0; i < segment_.motors.size(); ++i)
  {
    this->updateMotorActivation(i);
  }

  //  Y_DEBUG("Segment::update->trace_point %d with iter %d",
  //      singleSeg_->trace_point.enabled,
  //      singleSeg_->trace_point.iterations);
  if(singleSeg_->trace_point.enabled && 
      (_iterations % singleSeg_->trace_point.iterations == 0))
  {
    Y_DEBUG("Segment::add trace point");
    addTraceGeom();
  }

  //  Y_DEBUG("Segment::update->trace_line %d with iter %d",
  //      singleSeg_->trace_line.enabled,
  //      singleSeg_->trace_line.iterations);
  if(singleSeg_->trace_line.enabled && 
      (_iterations % singleSeg_->trace_line.iterations == 0))
  {
    Y_DEBUG("Segment::add trace point");
    addTraceLineGeom();
  }
}

// forces for manipulating "moving"-moveables
void Segment::setForces(double fx, double fy, double fz)
{
  segment_.fx = fx;
  segment_.fy = fy;
  segment_.fz = fz; 
}

void Segment::updateForces()
{
  _iterations++;
  /* Question to decide on: AddRelForce -> force vectors that are relative to
     the body's own frame of reference (good for Boxes)
     SetForce -> useful to zero the force for deactivated
     bodies before they are reactivated, in the case where
     the force-adding functions were called on them
     while they were deactivated. (better in case of
     spheres*/
  
  dBodySetForce(segment_.body, segment_.fx, segment_.fy, segment_.fz);
  
  if(singleSeg_->trace_point.enabled && 
     (_iterations % singleSeg_->trace_point.iterations == 0))
  {
    Y_DEBUG("Segment::add trace point");
    addTraceGeom();
  }
  if(singleSeg_->trace_line.enabled && 
     (_iterations % singleSeg_->trace_line.iterations == 0))
  {
    Y_DEBUG("Segment::add trace point");
    addTraceLineGeom();
  }
}






void Segment::addTraceLineGeom()

{
  if(_geoms[0] == NULL)
  {
    return;
  }
  
  // TODO: for compound objects, get the center of the objects here
  const dReal *pos = dGeomGetPosition(segment_.geoms[0]);
  P3DColor p;
  p.x = pos[0];
  p.y = pos[1];
  p.z = pos[2];
  p.r = singleSeg_->trace_line.color.r;
  p.g = singleSeg_->trace_line.color.g;
  p.b = singleSeg_->trace_line.color.b;

  drawManager_->addTraceLineGeom(p);
}

void Segment::addTraceGeom()
{
  if(_geoms[0] == NULL)
  {
    return;
  }

  // TODO: for compound objects, get the center of the objects here
  const dReal *pos = dGeomGetPosition(segment_.geoms[0]);
  dGeomID trace_geom = dCreateSphere(spaceID_, singleSeg_->trace_point.size);
  dGeomSetPosition(trace_geom, pos[0], pos[1], pos[2]);

  drawManager_->addTraceGeom(trace_geom, 
      singleSeg_->trace_point.color.r,
      singleSeg_->trace_point.color.g,
      singleSeg_->trace_point.color.b);

}

unsigned int Segment::getNumberOfMotors()
{
  return segment_.motors.size();
}

unsigned int Segment::getNumberOfMotorInputs(unsigned int motor)
{
  if(this->existMotor(motor))
  {
    return segment_.motors[motor].inputs.size();
  }
  else 
  {
    return 0;
  }
}

void Segment::getMotorName(unsigned int motor, string &name)
{
  if(this->existMotor(motor))
  {
    name = segment_.motors[motor].name;
  }
  else 
  {
    // TODO: error handling
    name = "MotorDoesNotExist";
  }
}

double Segment::getMotorMinInput(unsigned int motor, unsigned int motorInput)
{
  if(this->existMotor(motor))
  {
    if(segment_.motors[motor].mapMotor != NULL)
    {
      return segment_.motors[motor].mapMotor->getInStartVal(motorInput);
    }
    else
    {
      // TODO: error handling
      return 0.0;
    }
  }
  else 
  {
    // TODO: error handling
    return 0.0;
  }
}

double Segment::getMotorMaxInput(unsigned int motor, unsigned int motorInput)
{
  if(this->existMotor(motor))
  {
    if(segment_.motors[motor].mapMotor != NULL)
    {
      return segment_.motors[motor].mapMotor->getInEndVal(motorInput);
    }
    else
    {
      // TODO: error handling
      return 0.0;
    }
  }
  else 
  {
    // TODO: error handling
    return 0.0;
  }
}

unsigned int Segment::getNumberOfSensors()
{
  return segment_.sensors.size();
}

unsigned int Segment::getNumberOfSensorValues(unsigned int sensor)
{
  if(segment_.sensors.size() > sensor)
  {
    return segment_.sensors[sensor].sensor->getNumberOfValues();
  }
  else
  {
    // TODO: error handling
    return 0;
  }
}

unsigned int Segment::getNumberOfSensorValues()
{
  int count=0;
  for(int i=0; i< segment_.sensors.size(); i++)
  {
    count += segment_.sensors[i].sensor->getNumberOfValues();
  }
  return count;
}

void Segment::getSensorName(unsigned int sensor, string &name)
{
  if(segment_.sensors.size() > sensor)
  {
    name = segment_.sensors[sensor].name;
  }
  else
  {
    // TODO: error handling
    name = "SensorDoesNotExist";
  }
}

double Segment::getSensorMinOutput(unsigned int sensor, unsigned int sensorOutput)
{
  if(segment_.sensors.size() > sensor)
  {
    if(segment_.sensors[sensor].mapSensor != NULL)
    {
      return segment_.sensors[sensor].mapSensor->getOutStartVal(sensorOutput);
    }
    else
    {
      // TODO: error handling
      return 0.0;
    }
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

double Segment::getSensorMaxOutput(unsigned int sensor, unsigned int sensorOutput)
{
  if(segment_.sensors.size() > sensor)
  {
    if(segment_.sensors[sensor].mapSensor != NULL)
    {
      return segment_.sensors[sensor].mapSensor->getOutEndVal(sensorOutput);
    }
    else
    {
      // TODO: error handling
      return 0.0;
    }
  }
  else
  {
    // TODO: error handling
    return 0.0;
  }
}

int Segment::getSensorUsage(int sensor)
{
  if(segment_.sensors.size() > sensor)
  {
    return segment_.sensors[sensor].usage; 
  }
  else
  {
    // TODO: error handling
    return DEF_SENSOR_USED_AS_CONTROL_INPUT; // default 
  }
}

void Segment::createCompositeObject(SegmentObject *segment, 
    SingleSegment *singleSegment)
{
  int dir = 3; // alignment for cylinders along the z axis initially
  dMass    m;
  dGeomID  massGeom;
  dMatrix3 tmpRot;  // temporary rotation matrix converted from Euler Angles

  // create the mass "geoms" and adjust the mass object accordingly
  switch(singleSegment->rigidBodyType)
  {
    case RD_BOX_GEOM:
      dMassSetBox(&m, 1.0, singleSegment->dimensions.x, 
                           singleSegment->dimensions.y,
                           singleSegment->dimensions.z);
      if(singleSegment->drawRigidBody == true)
      {
        massGeom = dCreateBox(spaceID_, singleSegment->dimensions.x, 
                                        singleSegment->dimensions.y,
                                        singleSegment->dimensions.z);
      }
      break;
    case RD_SPHERE_GEOM:
      dMassSetSphere (&m, 1.0, singleSegment->dimensions.radius);
      if(singleSegment->drawRigidBody == true)
      {
        massGeom = dCreateSphere(spaceID_, singleSegment->dimensions.radius);
      }
      break;
    case RD_CAPPED_CYLINDER_GEOM:
      dMassSetCapsule (&m, 1.0, dir, singleSegment->dimensions.radius,
                                     singleSegment->dimensions.length);
      if(singleSegment->drawRigidBody == true)
      {
        massGeom = dCreateCylinder(spaceID_, singleSegment->dimensions.radius,
                                             singleSegment->dimensions.length);
      }
      break;
    case RD_CYLINDER_GEOM:
      dMassSetCylinder (&m, 1.0, dir, singleSegment->dimensions.radius,
                                      singleSegment->dimensions.length);
      if(singleSegment->drawRigidBody == true)
      {
        massGeom = dCreateCCylinder(spaceID_, singleSegment->dimensions.radius,
                                              singleSegment->dimensions.length);
      }
      break;
    default:
      ;
  }
  dMassAdjust(&m, singleSegment->mass);

  // create body 
  segment->body = dBodyCreate(worldID_);
  // and if requested visualize it (no collision detection on it)
  if(singleSegment->drawRigidBody == true)
  {
    drawManager_->addGeom(massGeom);
    drawManager_->changeColor(massGeom, singleSegment->color.r,
                                        singleSegment->color.g,
                                        singleSegment->color.b,
                                        singleSegment->color.alpha);
    dGeomSetBody(massGeom, segment->body);
    _massGeom = massGeom;
  }

  // create and connect geoms and contact structures
  segment->geoms.resize(singleSegment->geoms.segments.size());
  _geoms.resize(singleSegment->geoms.segments.size());
  contactInfo_.resize(singleSegment->geoms.segments.size());
  //
  for(int i=0; i<singleSegment->geoms.segments.size(); i++)
  {
    switch(singleSegment->geoms.segments[i].type)
    {
      case RD_BOX_GEOM:
        {
          segment->geoms[i] = dCreateBox(spaceID_, 
                                         singleSegment->geoms.segments[i].dimensions.x,
                                         singleSegment->geoms.segments[i].dimensions.y,
                                         singleSegment->geoms.segments[i].dimensions.z);
        }
        break;

      case RD_SPHERE_GEOM:
        {
          segment->geoms[i] = dCreateSphere(spaceID_, 
                                         singleSegment->geoms.segments[i].dimensions.radius);
        }
        break;

      case RD_CAPPED_CYLINDER_GEOM:
        {
          segment->geoms[i] = dCreateCCylinder(spaceID_, 
                                         singleSegment->geoms.segments[i].dimensions.radius,
                                         singleSegment->geoms.segments[i].dimensions.length);
        }
        break;

      case RD_CYLINDER_GEOM:
        {
          segment->geoms[i] = dCreateCCylinder(spaceID_, 
                                         singleSegment->geoms.segments[i].dimensions.radius,
                                         singleSegment->geoms.segments[i].dimensions.length);
        }
        break;

      default:
        break;
    }
    drawManager_->addGeom(segment->geoms[i]);
    drawManager_->changeColor(segment->geoms[i], 
                                singleSegment->geoms.segments[i].color.r,
                                singleSegment->geoms.segments[i].color.g,
                                singleSegment->geoms.segments[i].color.b,
                                singleSegment->geoms.segments[i].color.alpha);
    _geoms[i] = &(segment->geoms[i]);

    // update the contact struct for each geom
    contactInfo_[i].mode      = singleSegment->geoms.segments[i].contactMode;
    contactInfo_[i].bounce    = singleSegment->geoms.segments[i].cBounce;
    contactInfo_[i].bounceVel = singleSegment->geoms.segments[i].cBounceVel;
    contactInfo_[i].slip1     = singleSegment->geoms.segments[i].cSlip1;
    contactInfo_[i].slip2     = singleSegment->geoms.segments[i].cSlip2;
    contactInfo_[i].softCFM   = singleSegment->geoms.segments[i].cSoftCFM;
    contactInfo_[i].softERP   = singleSegment->geoms.segments[i].cSoftErp;
    contactInfo_[i].mu        = singleSegment->geoms.segments[i].cMu;
    contactInfo_[i].mu2       = singleSegment->geoms.segments[i].cMu2;

    // attach geom to rigid body
    dGeomSetBody(segment->geoms[i], segment->body);
  }

  // assign mass object to the rigid body 
  dBodySetMass (segment->body, &m);

  // set rigid body and geom positions and rotations (it doesn't matter if we
  //   call the functions of one or the other because changes are mutually
  //   propagated)
  dBodySetPosition(segment->body, singleSegment->position.x,
                                  singleSegment->position.y,
                                  singleSegment->position.z);
  dRFromEulerAngles(tmpRot, singleSegment->rotation.x, 
                            singleSegment->rotation.y,
                            singleSegment->rotation.z);
  dGeomSetRotation(segment->geoms[0], tmpRot); 

  // now set the position and rotation offsets for all geoms
  for(int i=0; i < segment->geoms.size(); i++)
  {
    if(i > singleSegment->geoms.segments.size())
    {
      Y_DEBUG("Segment.cpp: Error: segment->geoms.size() > singleSegment->geoms.segments.size()");
      break;
    }
    dGeomSetOffsetPosition(segment->geoms[i], 
                            singleSegment->geoms.segments[i].init_position.x,
                            singleSegment->geoms.segments[i].init_position.y,
                            singleSegment->geoms.segments[i].init_position.z);
    dRFromEulerAngles(tmpRot, singleSegment->geoms.segments[i].init_rotation.x, 
                              singleSegment->geoms.segments[i].init_rotation.y,
                              singleSegment->geoms.segments[i].init_rotation.z);
    dGeomSetOffsetRotation(segment->geoms[i], tmpRot);
  }
}

void Segment::createBox(dBodyID *body, dGeomID *geom, Position *pos,
    Position *rot, Position *dim, double *mass, int *massGeometry, 
    Position *massOffset, Position *massDimension, 
    Position *massRelativeRotation, bool visualizeMass)
{
  *geom = dCreateBox(spaceID_, dim->x, dim->y, dim->z);
  (this->_geoms[0]) = geom;

  drawManager_->addGeom(*geom);

  if(*massGeometry == RD_UNDEFINED_GEOM)
  {
    // mass geometry will be the same as collision geom
    *massGeometry = RD_BOX_GEOM;
    (*massDimension).x = dim->x;
    (*massDimension).y = dim->y;
    (*massDimension).z = dim->z;
  }

  this->createRigidBody(body, geom, pos, rot, mass, massGeometry, massOffset,
      massDimension, massRelativeRotation, visualizeMass);
}



void Segment::createSphere(dBodyID *body, dGeomID *geom, Position *pos,
    Position *rot, Position *dim, double *mass, int *massGeometry, 
    Position *massOffset, Position *massDimension, 
    Position *massRelativeRotation, bool visualizeMass)
{
  *geom = dCreateSphere(spaceID_, dim->radius);
  (this->_geoms[0]) = geom;

  drawManager_->addGeom(*geom);

  if(*massGeometry == RD_UNDEFINED_GEOM)
  {
    // mass geometry will be the same as collision geom
    *massGeometry = RD_SPHERE_GEOM;
    (*massDimension).radius = dim->radius;
  }

  this->createRigidBody(body, geom, pos, rot, mass, massGeometry, massOffset,
      massDimension, massRelativeRotation, visualizeMass);
}

void Segment::createCylinder(dBodyID *body, dGeomID *geom, Position
    *pos, Position *rot, Position *dim, double *mass, int *massGeometry, 
    Position *massOffset, Position *massDimension, 
    Position *massRelativeRotation, bool visualizeMass)
{
  *geom = dCreateCylinder(spaceID_, dim->radius, dim->length);
  (this->_geoms[0]) = geom;

  drawManager_->addGeom(*geom);

  if(*massGeometry == RD_UNDEFINED_GEOM)
  {
    // mass geometry will be the same as collision geom
    *massGeometry = RD_CYLINDER_GEOM;
    (*massDimension).radius = dim->radius;
    (*massDimension).length = dim->length;
  }

  this->createRigidBody(body, geom, pos, rot, mass, massGeometry, massOffset,
      massDimension, massRelativeRotation, visualizeMass);
}

void Segment::createCappedCylinder(dBodyID *body, dGeomID *geom, Position
    *pos, Position *rot, Position *dim, double *mass, int *massGeometry, 
    Position *massOffset, Position *massDimension, 
    Position *massRelativeRotation, bool visualizeMass)
{
  *geom = dCreateCCylinder(spaceID_, dim->radius, dim->length);
  (this->_geoms[0]) = geom;

  drawManager_->addGeom(*geom);

  if(*massGeometry == RD_UNDEFINED_GEOM)
  {
    // mass geometry will be the same as collision geom
    *massGeometry = RD_CAPPED_CYLINDER_GEOM;
    (*massDimension).radius = dim->radius;
    (*massDimension).length = dim->length;
  }

  this->createRigidBody(body, geom, pos, rot, mass, massGeometry, massOffset,
      massDimension, massRelativeRotation, visualizeMass);
}

// create the rigid bodies to which the geoms will be attached to
// - hide the fact that geoms positions and rotations are relative to their
//     corresponding rigid body, instead invert this behaviour
// - this means that geom positions and rotations are specified and those of the
//     rigid body are relative to these from the users point of view
void Segment::createRigidBody(dBodyID *body, dGeomID *geom, Position *pos,
    Position *rot, double *mass, int *massGeometry, Position *massOffset,
    Position *massDimension, Position *massRelativeRotation, bool visualizeMass)
{
  dMass m;
  dGeomID massGeom;

  /* at the momtent all ccylinders are aligned along the z-axis */
  int dir = 3;
  dVector3 tmp, tmp2;

  dMatrix3       gRot;         // rotation matrix for world -> geom rotation
  dMatrix3       gRotTransp;   // rotation matrix for geom -> world rotation
  dMatrix3       rBRotOffset;  // rotation matrix for geom -> rigid body rot
  dMatrix3       rBRotTransp;  // rot matrix rigid body -> world rot
 
  dVector3       rBPos;        // rigid body position
  dMatrix3       rBRot;        // rigid body rotation
  dVector3       gPosOffset;   // geom position offset
  dMatrix3       gRotOffset;   // geom rotation offset

  // calculate rotations and rotation offsets
  dRFromEulerAngles(gRot, rot->x, rot->y, rot->z);
  dRFromEulerAngles(rBRotOffset, massRelativeRotation->x, 
      massRelativeRotation->y, massRelativeRotation->z);
  // rbRot is rot multiplied by massRelativeRotation
  dMULTIPLY0_333(rBRot, gRot, rBRotOffset); 
  // gRotOffset is the inverse/transpose of massRelativeRotation
  Matrix::getTransposedMatrix3(gRotOffset, rBRotOffset);

  // Calculate positions and position offsets
  rBPos[0] = pos->x + massOffset->x;
  rBPos[1] = pos->y + massOffset->y;
  rBPos[2] = pos->z + massOffset->z;

  gPosOffset[0] = -1 * massOffset->x; //pos->x;
  gPosOffset[1] = -1 * massOffset->y; //pos->y;
  gPosOffset[2] = -1 * massOffset->z; //pos->z;

  // Now revert the position shift of the geom due to rB rotations
  Matrix::getTransposedMatrix3(rBRotTransp, rBRot);
  tmp[0] = gPosOffset[0];
  tmp[1] = gPosOffset[1];
  tmp[2] = gPosOffset[2];
  dMULTIPLY0_331(gPosOffset, rBRotTransp, tmp);
  // ... and add the position shift due to gRot to the rigid body
  //Matrix::getTransposedMatrix3(gRotTransp, gRot);
  tmp[0] = massOffset->x;
  tmp[1] = massOffset->y;
  tmp[2] = massOffset->z;
  dMULTIPLY0_331(tmp2, gRot, tmp);
  tmp2[0]  -= tmp[0];
  tmp2[1]  -= tmp[1];
  tmp2[2]  -= tmp[2];
  rBPos[0] += tmp2[0];
  rBPos[1] += tmp2[1];
  rBPos[2] += tmp2[2];
  // ... and finally substract this from the geom
  dMULTIPLY0_331(tmp, rBRotTransp, tmp2);
  gPosOffset[0] -= tmp[0];
  gPosOffset[1] -= tmp[1];
  gPosOffset[2] -= tmp[2];

  // take care of the mass stuff (mass geometries ...)
  switch(*massGeometry)
  {
    case RD_BOX_GEOM:
      dMassSetBox(&m, 1.0, massDimension->x, 
                           massDimension->y,
                           massDimension->z);
      if(visualizeMass)
      {
        massGeom = dCreateBox(spaceID_, massDimension->x, 
            massDimension->y,
            massDimension->z);
      }
      break;
    case RD_SPHERE_GEOM:
      dMassSetSphere (&m, 1.0, massDimension->radius);
      if(visualizeMass)
      {
        massGeom = dCreateSphere(spaceID_, massDimension->radius);
      }
      break;
    case RD_CAPPED_CYLINDER_GEOM:
      dMassSetCapsule (&m, 1.0, dir, massDimension->radius,
                                     massDimension->length);
      if(visualizeMass)
      {
        massGeom = dCreateCylinder(spaceID_, massDimension->radius,
            massDimension->length);
      }
      break;
    case RD_CYLINDER_GEOM:
      dMassSetCylinder (&m, 1.0, dir, massDimension->radius,
                                      massDimension->length);
      if(visualizeMass)
      {
        massGeom = dCreateCCylinder(spaceID_, massDimension->radius,
            massDimension->length);
      }
      break;
    default:
      ;
  }
  dMassAdjust(&m, *mass);

  // create body and connect geom, assign mass property
  *body = dBodyCreate(worldID_);
  dGeomSetBody (*geom, *body);
  dBodySetMass (*body, &m);

  // set rigid body and geom positions and rotations (it doesn't matter if we
  //   call the functions of one or the other because changes are mutually
  //   propagated)
  dBodySetPosition(*body, rBPos[0], rBPos[1], rBPos[2]);
  dGeomSetRotation(*geom, rBRot); 
                                  
  if(visualizeMass)
  {
    drawManager_->addGeom(massGeom);
    drawManager_->changeColor(massGeom, 255, 0, 0, 150);
    dGeomSetBody (massGeom, *body);
  }

  dGeomSetOffsetPosition(*geom, gPosOffset[0], gPosOffset[1], gPosOffset[2]);
  dGeomSetOffsetRotation(*geom, gRotOffset);
}



void Segment::setGeomRotation(dGeomID* geom, Position *rotation)
{
  dMatrix3 rot;

  dRFromEulerAngles(rot, rotation->x, rotation->y, rotation->z);

  dGeomSetRotation(*geom, rot); 
}


void Segment::connectFix(dBodyID *aimBody, Position *pos)
{
  /* axis orientation doesn't matter, because it is a fixed hinge, but is
   * necessary to set */
  Position axis;
  axis.x = 1.0;
  axis.y = 0.0;
  axis.z = 0.0;

  /* create a hinge and push it back in the joints vector of the segment object */
  this->connectHinge(aimBody, pos, &axis);

  /* and now set the stops to zero for the new joint */
  int jointNum = segment_.joints.size() - 1;
  this->setJointStops( &(segment_.joints[jointNum]), 0.0, 0.0 );
}

void Segment::connectHinge(dBodyID *aimBody, const Position *pos, const Position
    *axis)
{
  Y_DEBUG("Segment::connectHinge start");
  dJointID joint;
  joint =  dJointCreateHinge(worldID_, jointGroupID_);

  if(aimBody == NULL)
  {
    Y_DEBUG("Segment::connectHinge aimBody == NULL");
    dJointAttach(joint, segment_.body, 0);
  }
  else
  {
    Y_DEBUG("Segment::connectHinge aimBody != NULL");
    dJointAttach(joint, segment_.body, *aimBody);
  }

  dJointSetHingeAxis(joint, axis->x, axis->y, axis->z);

  dJointSetHingeAnchor(joint, pos->x, pos->y, pos->z);
  segment_.joints.push_back(joint);
  Y_DEBUG("Segment::connectHinge end");
}

void Segment::connectHinge2(dBodyID *aimBody, const Position *pos, const
    Position *axis, Position *axis2)
{
  dJointID joint;
  joint =  dJointCreateHinge2(worldID_, jointGroupID_);

  dJointAttach(joint, segment_.body, *aimBody);

  dJointSetHinge2Axis1(joint, axis->x, axis->y, axis->z);
  dJointSetHinge2Axis2(joint, axis2->x, axis2->y, axis2->z);

  dJointSetHinge2Anchor(joint, pos->x, pos->y, pos->z);
  segment_.joints.push_back(joint);
}

void Segment::connectSlider(dBodyID *aimBody, const Position *axis)
{
  Y_DEBUG("Segment::connectSlider start");
  dJointID joint;
  joint =  dJointCreateSlider(worldID_, jointGroupID_);

  if(aimBody == NULL)
  {
    Y_DEBUG("Segment::connectSlider aimBody == NULL");
    dJointAttach(joint, segment_.body, 0);
  }
  else
  {
    Y_DEBUG("Segment::connectSlider aimBody != NULL");
    dJointAttach(joint, segment_.body, *aimBody);
  }

  dJointSetSliderAxis(joint, axis->x, axis->y, axis->z);

  segment_.joints.push_back(joint);

  Y_DEBUG("Segment::connectSlider end");
}


void Segment::setJointStops(dJointID *joint, const double loStop, const double
    hiStop)
{
  switch( dJointGetType(*joint) )
  {
    case dJointTypeHinge:
      {
        dJointSetHingeParam(*joint, dParamHiStop, hiStop);
        dJointSetHingeParam(*joint, dParamLoStop, loStop);  
      }
      break;
    case dJointTypeSlider:
      {
        dJointSetSliderParam(*joint, dParamHiStop, hiStop);
        dJointSetSliderParam(*joint, dParamLoStop, loStop);
      }
      break;
    default:
      break;
  }
}

void Segment::stopMotors()
{
  int jointNum;
  double vel, fMax;

  vel = 0.0;
  fMax = 1000.0;

  for(unsigned int i = 0; i < segment_.motors.size(); ++i)
  {
    jointNum =  segment_.motors[i].jointNum;
    switch( dJointGetType(segment_.joints[jointNum]) )
    {
      case dJointTypeHinge2:
        {
          dJointSetHinge2Param(segment_.joints[jointNum], dParamVel, vel);
          dJointSetHinge2Param(segment_.joints[jointNum], dParamFMax, fMax);
          dJointSetHinge2Param(segment_.joints[jointNum], dParamVel2, vel);
          dJointSetHinge2Param(segment_.joints[jointNum], dParamFMax2, fMax);
        }
        break;

      case dJointTypeHinge:
        {
          dJointSetHingeParam(segment_.joints[jointNum], dParamVel, vel);
          dJointSetHingeParam(segment_.joints[jointNum], dParamFMax, fMax);
        }
        break;
      case dJointTypeSlider:
        {
          dJointSetSliderParam(segment_.joints[jointNum], dParamVel, vel);
          dJointSetSliderParam(segment_.joints[jointNum], dParamFMax, fMax);
        }
        break;
      default:
        break;
    }
  }
}

void Segment::setLightManager(LightManager *lm)
{

  lightManager_ = lm;
}

int Segment::selfCollide(dGeomID geom1, dGeomID geom2, struct dContact*
    contact)
{
  int isCollided = 0;
  int geomNum    = 0;

  isCollided = 0;
  //bumpedWithOtherObject_ = false; // do not do, because this function can be
  //called more than once for an object

  for(int i=0; i<segment_.geoms.size(); i++)
  {
    if(segment_.geoms[i] == geom1 || segment_.geoms[i] == geom2)
    {
      isCollided = 1;
      geomNum = i;
      break;
    }
  }

  if(isCollided == 1)
  {
    contact->surface.mode       = contactInfo_[geomNum].mode;
    contact->surface.bounce     = contactInfo_[geomNum].bounce;
    contact->surface.bounce_vel = contactInfo_[geomNum].bounceVel;
    contact->surface.slip1      = contactInfo_[geomNum].slip1;
    contact->surface.slip2      = contactInfo_[geomNum].slip2;
    contact->surface.mu         = contactInfo_[geomNum].mu;
    contact->surface.mu2        = contactInfo_[geomNum].mu2;
    contact->surface.soft_erp   = contactInfo_[geomNum].softERP;
    contact->surface.soft_cfm   = contactInfo_[geomNum].softCFM;
  }

  bool checkForBumped = (isBumpable_ == true) && (isCollided == 1);

  bool bumpedWithOtherSpace = (dGeomGetSpace(geom1) != dGeomGetSpace(geom2)) &&
    (dGeomGetClass(geom1) != dRayClass && 
     dGeomGetClass(geom2) != dRayClass);

  if(checkForBumped) 
  {
    if(bumpedWithOtherSpace) // to make sure that first checkForBumped is checked
    {
      drawManager_->changeColor(segment_.geoms[geomNum], 1.0, 0.0, 0.0, 1.0);
      (*(YarsContainers::AbortCondition())).abortCondition = true;
      (*(YarsContainers::AbortCondition())).abortSource    = (*singleSeg_).name;
      (*(YarsContainers::AbortCondition())).abortCause     = DEF_ABORT_CAUSE_BUMPED;
    }
  }

  if(traceContact_ == true)
  {
    if(bumpedWithOtherSpace == true && isCollided == 1)
    {
      fprintf(traceFile,"%ld 1\n", getLongTime());
    }
    else
    {
      fprintf(traceFile,"%ld 0\n", getLongTime());
    }
    fflush(traceFile);
  }



  /* if sensors collide, isCollided doesn't become 1, because there should no
   * contact joint created for this */
  for(unsigned int i = 0; i < segment_.sensors.size(); ++i)
  {
    segment_.sensors[i].sensor->selfCollide(geom1, geom2, contact);
  }

  return isCollided;
}

long Segment::getLongTime()
{
  timeval time;

  gettimeofday(&time, 0);   

  return ((time.tv_sec%10000) * 1000 + time.tv_usec / 1000);
}


int Segment::isSegment(dGeomID geom)
{
  return (dGeomGetSpace(geom) == spaceID_);
}

dBodyID* Segment::getBodyID()
{
  return &(segment_.body);
}

dGeomID* Segment::getGeomID(int geom)
{
  return &(segment_.geoms[geom]);
}

bool Segment::existSensor(unsigned int sensor)
{
  if(sensor >= this->getNumberOfSensors())
  {
    Y_FATAL("!!!Segment ERROR!!!");
    Y_FATAL("   Sensor Number %d doesn't exist!!!", sensor);
    Y_FATAL("!!!Segment ERROR!!!");
    return false;
  }
  else
  {
    return true;
  }
}

bool Segment::existMotor(unsigned int motor)
{
  if(motor >= this->getNumberOfMotors())
  {
    Y_FATAL("!!!Segment ERROR!!!");
    Y_FATAL("   Motor Number %d doesn't exist!!!", motor);
    Y_FATAL("!!!Segment ERROR!!!");
    return false;
  }
  else
  {
    return true;
  }
}

void Segment::createAngleSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int jointNum, num;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();

  AngleSensorStruct *aSS = sensDes->getAngleSensor();

  jointNum = aSS->connectorNum;

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new AngleSensor();

  tmp.startValue = aSS->startValue;
  tmp.endValue   = aSS->endValue;

  tmp.mapSensor  = new MappingFunction(aSS->startAngle, aSS->endAngle,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(aSS->startAngle-aSS->endAngle), 
          sS->noise));

  segment_.sensors.push_back(tmp);

  num = segment_.sensors.size()-1;

  AngleSensor *aS = (AngleSensor*)segment_.sensors[num].sensor;

  aS->setJointID( &(segment_.joints[jointNum]) );
}

void Segment::createAngleVelSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int jointNum, num;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();

  AngleVelSensorStruct *aVsS = sensDes->getAngleVelSensor();

  jointNum = aVsS->connectorNum;

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new AngleVelSensor();

  tmp.startValue = aVsS->startValue;
  tmp.endValue   = aVsS->endValue;

  tmp.mapSensor  = new MappingFunction(aVsS->startVel, aVsS->endVel,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(aVsS->startVel-aVsS->endVel), 
          sS->noise));

  segment_.sensors.push_back(tmp);

  num = segment_.sensors.size()-1;

  AngleVelSensor *aVS = (AngleVelSensor*)segment_.sensors[num].sensor;

  aVS->setJointID( &(segment_.joints[jointNum]) );
}

void Segment::createSliderPositionSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int jointNum, num;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();

  SliderPositionSensorStruct *sPSS = sensDes->getSliderPositionSensor();

  jointNum = sPSS->connectorNum;

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new SliderPositionSensor();

  tmp.startValue = sPSS->startValue;
  tmp.endValue   = sPSS->endValue;

  tmp.mapSensor  = new MappingFunction(sPSS->startPosition, sPSS->endPosition,
      tmp.startValue, tmp.endValue);

  tmp.gauss = new Gauss(
      this->calcAbsNoiseRange(fabs(sPSS->startPosition-sPSS->endPosition), 
        sS->noise));

  segment_.sensors.push_back(tmp);

  num = segment_.sensors.size()-1;

  SliderPositionSensor *sPS = (SliderPositionSensor*)segment_.sensors[num].sensor;

  sPS->setJointID( &(segment_.joints[jointNum]) );
}

void Segment::createSliderVelSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int jointNum, num;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();

  SliderVelSensorStruct *sVsS = sensDes->getSliderVelSensor();

  jointNum = sVsS->connectorNum;

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new SliderVelSensor();

  tmp.startValue = sVsS->startValue;
  tmp.endValue   = sVsS->endValue;

  tmp.mapSensor  = new MappingFunction(sVsS->startVel, sVsS->endVel,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(sVsS->startVel-sVsS->endVel), 
          sS->noise));

  segment_.sensors.push_back(tmp);

  num = segment_.sensors.size()-1;

  SliderVelSensor *sVS = (SliderVelSensor*)segment_.sensors[num].sensor;

  sVS->setJointID( &(segment_.joints[jointNum]) );
}

void Segment::createAngleFeedbackSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int jointNum, num;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();

  AngleFeedbackSensorStruct *aFsS = sensDes->getAngleFeedbackSensor();

  jointNum = aFsS->connectorNum;

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new AngleFeedbackSensor();

  tmp.startValue = aFsS->startValue;
  tmp.endValue   = aFsS->endValue;

  tmp.mapSensor  = new MappingFunction(aFsS->startFeedback, aFsS->endFeedback,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(aFsS->startFeedback-aFsS->endFeedback), 
          sS->noise));

  segment_.sensors.push_back(tmp);

  num = segment_.sensors.size()-1;

  AngleFeedbackSensor *aFS = (AngleFeedbackSensor*)segment_.sensors[num].sensor;

  aFS->setJointID( &(segment_.joints[jointNum]) );

  aFS->setType(aFsS->type);
  aFS->setFeedbackTarget(aFsS->feedbackTarget);
  aFS->setDirection(aFsS->direction);

  aFS->initialize();
}

void Segment::createIrSensor(SensorDescription *sensDes)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  IrSensorStruct *iSs = sensDes->getIrSensor();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new GenericIrSensor(drawManager_, spaceID_);

  tmp.startValue = iSs->startValue;
  tmp.endValue   = iSs->endValue;

  tmp.mapSensor  = new MappingFunction(iSs->startRange, iSs->endRange,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(iSs->endRange-iSs->startRange), sS->noise));

  GenericIrSensor *geIrSens = (GenericIrSensor*)tmp.sensor;

  geIrSens->create(iSs->startRange, iSs->endRange, iSs->spreadAngleX,
      iSs->spreadAngleY, iSs->direction, iSs->startPos, &segment_.body);

  segment_.sensors.push_back(tmp);
}

void Segment::createGenericRotationSensor(SensorDescription *sensDes)
{
  SensorObj tmp;
  int i;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  GenericRotationSensorStruct *gRSs = sensDes->getGenericRotationSensor();

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new GenericRotationSensor(drawManager_, spaceID_);

  tmp.startValue = gRSs->startValue;
  tmp.endValue   = gRSs->endValue;

  tmp.mapSensor  = new MappingFunction(gRSs->startRange, gRSs->endRange,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(gRSs->endRange-gRSs->startRange), sS->noise));

  GenericRotationSensor *geRotationSens = (GenericRotationSensor*)tmp.sensor;

  geRotationSens->create(gRSs->type, gRSs->orderDerivative, 
                         gRSs->startRange, gRSs->endRange,
                         gRSs->rotation, gRSs->startPos,
                         gRSs->drawRayRot, gRSs->drawRayRef,
                         gRSs->lengthRayRot, gRSs->lengthRayRef,
                         gRSs->rayRotType, gRSs->rayRefType,
                         gRSs->rayRotColor, gRSs->rayRefColor,
                         &segment_.body);
      
  segment_.sensors.push_back(tmp);
}

void Segment::createSharp_GP2D12_37_Sensor(SensorDescription *sensDes)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  SharpGP2D12_37_SensorStruct* iSs = sensDes->getSharpGP2D12_37_Sensor();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new SharpIrGP2D12_37(drawManager_, spaceID_);

  tmp.startValue = iSs->startValue;
  tmp.endValue   = iSs->endValue;

  tmp.mapSensor  = new MappingFunction(0.0, 1.0,
      tmp.startValue, tmp.endValue);

  tmp.gauss = new Gauss(sS->noise);

  SharpIrGP2D12_37 *geIrSens = (SharpIrGP2D12_37*)tmp.sensor;

  geIrSens->create(iSs->direction, iSs->startPos, &segment_.body,
      iSs->startRange, iSs->endRange);

  segment_.sensors.push_back(tmp);

}


void Segment::createSharp_DM2Y3A003K0F_Sensor(SensorDescription *sensDes)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  SharpDM2Y3A003K0F_SensorStruct* iSs = sensDes->getSharpDM2Y3A003K0F_Sensor();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new SharpIrDM2Y3A003K0F(drawManager_, spaceID_);
  SharpIrDM2Y3A003K0F *tmpSensor = (SharpIrDM2Y3A003K0F*)tmp.sensor;

  tmp.startValue = iSs->startValue;
  tmp.endValue   = iSs->endValue;

  tmp.mapSensor  = new MappingFunction(
      tmpSensor->getMaxValue(), // because max < min
      tmpSensor->getMinValue(),
      tmp.startValue, tmp.endValue);

  Y_DEBUG("Segment::createSharp_DM2Y3A003K0F_Sensor: [%f,%f]", tmp.startValue, tmp.endValue);

  tmp.gauss = new Gauss(sS->noise);

  SharpIrDM2Y3A003K0F *geIrSens = (SharpIrDM2Y3A003K0F*)tmp.sensor;

  geIrSens->create(iSs->direction, iSs->startPos, &segment_.body,
      iSs->startRange, iSs->endRange);

  segment_.sensors.push_back(tmp);
}

void Segment::createDirectedCameraSensor(SensorDescription *sensDes)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  DirectedCameraStruct* dCs = sensDes->getDirectedCamera();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new DirectedCamera(drawManager_, spaceID_);
  DirectedCamera *tmpSensor = (DirectedCamera*)tmp.sensor;

  tmp.startValue = dCs->startValue;
  tmp.endValue   = dCs->endValue;

  tmp.mapSensor  = new MappingFunction(
      tmpSensor->getMinValue(),
      tmpSensor->getMaxValue(),
      tmp.startValue, tmp.endValue);
  
  tmp.gauss = new Gauss(sS->noise);

  Y_DEBUG("Segment::createDirecetdCameraSensor: [%f,%f]", tmp.startValue, tmp.endValue);

  DirectedCamera *camSens = (DirectedCamera*)tmp.sensor;

  camSens->create(dCs->init_pos, dCs->direction, &segment_.body, environment_, dCs->openingAngle, dCs->xPixel, dCs->yPixel, dCs->light, dCs->colorType);

  segment_.sensors.push_back(tmp);

}

void Segment::createLdrSensor(SensorDescription *sensDes)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  LdrSensorStruct *lSs = sensDes->getLdrSensor();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new LdrSensor(drawManager_, spaceID_);


  tmp.startValue = lSs->startValue;
  tmp.endValue   = lSs->endValue;

  tmp.mapSensor  = new MappingFunction(lSs->startIntensity, lSs->endIntensity,
      tmp.startValue, tmp.endValue);

  tmp.gauss = 
    new Gauss(this->calcAbsNoiseRange(fabs(lSs->startIntensity-lSs->endIntensity),
          sS->noise));


  LdrSensor *geLdrSens = (LdrSensor*)tmp.sensor;

  geLdrSens->create(lightManager_, lSs->pos, &segment_.body, lSs->direction,
      lSs->spreadAngleX, lSs->spreadAngleY);

  segment_.sensors.push_back(tmp);
}

void Segment::createAmbientLightSensor(SensorDescription *sensDes,
    LightManager *lm)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  AmbientLightSensorStruct *aSs = sensDes->getAmbientLightSensor();

  tmp.type       = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new AmbientLightSensor();

  tmp.mapSensor  = new MappingFunction(-DBL_MAX/2, DBL_MAX/2,
      -DBL_MAX/2, DBL_MAX/2);

  tmp.gauss = new Gauss(0.0);

  AmbientLightSensor *ambientLightSensor = (AmbientLightSensor*)tmp.sensor;
  ambientLightSensor->setLightManager(lm);

  segment_.sensors.push_back(tmp);
}

void Segment::createCoordinateSensor(SensorDescription *sensDes, dGeomID
    *targetGeom)
{
  SensorObj tmp;

  sensDes->getName(tmp.name);

  SensorStruct *sS = sensDes->getSensorStruct();
  CoordinateSensorStruct *cSs = sensDes->getCoordinateSensor();

  tmp.type = sS->type;
  tmp.usage      = sS->usage;
  tmp.sensor     = new CoordinateSensor();

  tmp.mapSensor  = new MappingFunction(-DBL_MAX/2, DBL_MAX/2,
      -DBL_MAX/2, DBL_MAX/2);

  tmp.gauss = new Gauss(0.0);

  CoordinateSensor *coordinateSensor = (CoordinateSensor*)tmp.sensor;

  coordinateSensor->setCoordinate(cSs->coordinate);
  coordinateSensor->setGeomID(targetGeom);

  segment_.sensors.push_back(tmp);
}

double Segment::calcAbsNoiseRange(const double sensorRange, 
    const double relNoiseRange)
{
  return fabs(relNoiseRange*sensorRange);
}

void Segment::setIsBumpable(bool isBumpable)
{
  isBumpable_ = isBumpable;
}

void Segment::setTraceContact(bool traceContact)
{
  traceContact_ = traceContact;
}
