/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _COMPOUND_H_
#define _COMPOUND_H_

#include <base/CameraHandler.h>
#include <base/Segment.h>
#include <description/RobotDescription.h>
#include <description/SensorDescription.h>
#include <env/LightManager.h>
#include <env/Environment.h>
#include <env/LightManager.h>
#include <gui/DrawManager.h>
#include <base/MappingFunction.h>

#include <ode/ode.h>


using namespace std;

class Compound
{
  public:
    Compound(CompoundDescription *segVec, dWorldID w, dSpaceID s, DrawManager *d, Environment *e, CameraHandler *views);
    ~Compound();

    /*create all parts of the segment compound and connect them in the right manner
    */
    void create();

    /* create a sensor, as defined in the description */
    void createSensor(SensorDescription *sensDes, dGeomID *targetGeom,
        LightManager *lm);

    // is this geom part of the segment compound? (for collision)
    int isCompound(dGeomID geom);

    int selfCollide(dGeomID geom1, dGeomID geom2, struct dContact* contact);

    /* returns the body id of a distinct segment */
    dBodyID* getSegmentBodyID(int seg);

    /* set the velocity of a specific motor 
     * value is either a velocity or a angle, depends on the motorType
     * (see RobotDescription.h) */
    void setMotorValue(unsigned int seg, unsigned int motor, unsigned int
        inputNum, double value);
    
    void setMotorValues(unsigned int seg, unsigned int motor, vector<double>
        values);

    void setForces(double fx, double fy, double fz);

    /* returns the sensor values of a specific sensor of a specifc segment */
    vector<double> getSensorValues(unsigned int seg, unsigned int sensor);

    dBodyID* getSegmentBodyID(unsigned int seg);
    dGeomID* getSegmentGeomID(int seg, int geom);
      
    void update();
 
    void updateForces();

    unsigned int getNumberOfSegments();
    
    // motor functions
    unsigned int getNumberOfMotors(unsigned int seg);

    unsigned int getNumberOfMotorInputs(unsigned int seg, unsigned int motor);

    void getMotorName(unsigned int seg, unsigned int motor, string &name);

    double getMotorMinInput(unsigned int seg, unsigned int motor, unsigned int
        motorInput);

    double getMotorMaxInput(unsigned int seg, unsigned int motor, unsigned int
        motorInput);

    // sensor functions
    //returns the number of sensors of the segment seg. 
    unsigned int getNumberOfSensors(unsigned int seg);
      
    unsigned int getNumberOfSensorValues(unsigned int seg, unsigned int sensor);

    // returns the sum of sensor-values that the sensors attached to segment seg return
    unsigned int getNumberOfSensorValues(unsigned int seg);

    void getSensorName(unsigned int seg, unsigned int sensor, string &name);

    double getSensorMinOutput(unsigned int seg, unsigned int sensor, unsigned
        int sensorOutput);

    double getSensorMaxOutput(unsigned int seg, unsigned int sensor, unsigned
        int sensorOutput);

    int getSensorUsage(unsigned int seg, unsigned int sensor);

    /* add a additional joint to the source Segment, which is connected to a body
     * of a different segment compound, this aim bodyID is give by the Robot Class
     * the first motor indices of a segment are compound internal motors, and last
     * ones are motors, which are connections to different compounds.
     * */
    void addGlobalConnector(ConnectorDescription *connStruct, int srcSegNum, dBodyID
        *aimBody);
 
    /* set for all motors vel to zero, and fMax to 1000, to stop all motors */
    void stopMotors();

    void setLightManager(LightManager *lm);
    
    Segment* getSegment(unsigned int seg);
    
    void setCompoundName(string name);
    void getCompoundName(string &name);

  private:
    /* check if the segment exists */  
    bool existSegment(unsigned int seg);

  
    /* comes from the robot description */
    CompoundDescription *_compound;
    dWorldID worldID_;
    dSpaceID spaceID_;
    DrawManager *drawManager_;
    Environment *environment_;
    CameraHandler *robotViews_;

    vector<Segment*> segments_;

    string _name;
};

#endif
