/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <net/StandardCommunication.h>
#include <util/defines.h>


/**
 * Constructor creates StandardCommunication object and initializes number
 * "clients" UDP server. This should be the amount of robots in the simulation
 * so that each STANDARD manages one simulated robot. Each UDP server waits for
 * one client (socket ports are automatically increased).
 */
StandardCommunication::StandardCommunication(unsigned int clients)
{
  unsigned int i;

  udpInitConnection(_STANDARD_COMPORT, clients); 

  _clientCount               = clients;

  _clientState               = new int[_clientCount];
  _seed                      = new unsigned int[_clientCount];
  _isParallelModeEnabled     = new bool[_clientCount];

  for (i=0; i<_clientCount; i++)
  {
    _clientState[i]           = _STANDARD_STATEREADY;
    _seed[i]                  = 0;
    _isParallelModeEnabled[i] = true;
  }

  _robotMotorCount           = new unsigned int[0];
  _robotMotorInputCount      = new unsigned int[0];
  _robotSensorCICount        = new unsigned int[0];
  _robotSensorAUXCount       = new unsigned int[0];
  _robotSensorCIValuesCount  = new unsigned int[0];
  _robotSensorAUXValuesCount = new unsigned int[0];
  _robotMotors               = new unsigned int[0];
  _robotSensorsCI            = new unsigned int[0];
  _robotSensorsAUX           = new unsigned int[0];
}

/**
 * Destructor deinits recources (but NOT upd server)
 */
StandardCommunication::~StandardCommunication()
{
  // no udp-deinit implemented yet. socket will be destroyed with program
  // termination 
  deinit();
  delete[] _clientState;
  delete[] _seed;
  delete[] _isParallelModeEnabled;
}

/**
 * Sets the RobotHandler and all neccesary functions for communication (e.g.
 * reset, init, simstep etc.).
 */
void StandardCommunication::init(RobotHandler *robHandler, FuncPointer *fnReset,
    InitFuncPointer *fnInit, NextTryFuncPointer* fnNextTry)
{
  int i, j, k, l;
  int robots, compounds, segs, sensors, motors;
  unsigned int robotMotors, robotMotorInputs;
  unsigned int robotSensorsCI,  robotSensorCIValues;
  unsigned int robotSensorsAUX, robotSensorAUXValues;

  _robotHandler = robHandler;
  _fnReset = fnReset;
  _fnInit = fnInit;
  _fnNextTry = fnNextTry;

  // count robot motors and sensors
  robots = _robotHandler->getNumberOfActiveRobots();
  _robots = robots;
  _maxRobotMotorCount     = 0;
  _maxRobotSensorCICount  = 0;
  _maxRobotSensorAUXCount = 0;

  deinit();

  _robotMotorCount           = new unsigned int[robots];
  _robotMotorInputCount      = new unsigned int[robots];
  _robotSensorCICount        = new unsigned int[robots];
  _robotSensorAUXCount       = new unsigned int[robots];
  _robotSensorCIValuesCount  = new unsigned int[robots];
  _robotSensorAUXValuesCount = new unsigned int[robots];

  for (i=0; i<robots; i++)
  {
    robotMotors          = 0;
    robotMotorInputs     = 0;
    robotSensorsCI       = 0;
    robotSensorsAUX      = 0;
    robotSensorCIValues  = 0;
    robotSensorAUXValues = 0;
    compounds            = _robotHandler->getNumberOfCompounds(i);

    for (j=0; j<compounds; j++)
    {
      segs = _robotHandler->getNumberOfSegments(i, j);
      for (k=0; k<segs; k++)
      {
        motors = _robotHandler->getNumberOfMotors(i, j, k); 
        for(l=0; l<motors; l++)
        {
          robotMotorInputs += _robotHandler->getNumberOfMotorInputs(i,j,k,l); 
        }
        robotMotors  += motors;

        sensors = _robotHandler->getNumberOfSensors(i, j, k);
        for(l=0; l<sensors; l++)
        {
          switch(_robotHandler->getSensorUsage(i,j,k,l))
          {
            case DEF_SENSOR_USED_AS_CONTROL_INPUT:
              robotSensorsCI++;
              robotSensorCIValues  += _robotHandler->getNumberOfSensorValues(i, j, k, l); 
              break;
            case DEF_SENSOR_USED_AS_AUX:
              robotSensorsAUX++;
              robotSensorAUXValues += _robotHandler->getNumberOfSensorValues(i, j, k, l); 
              break;
            case DEF_SENSOR_USED_INTERNALLY:
              // nothing to do, nothing to send to the clients
              break;
            default:
              ;
          }
        }
      }
    }
    _robotMotorCount[i]           = robotMotors;
    _robotMotorInputCount[i]      = robotMotorInputs;
    _robotSensorCICount[i]        = robotSensorsCI;
    _robotSensorAUXCount[i]       = robotSensorsAUX;
    _robotSensorCIValuesCount[i]  = robotSensorCIValues;
    _robotSensorAUXValuesCount[i] = robotSensorAUXValues;

    // store maximum robot motors and sensors
    if (robotMotors > _maxRobotMotorCount)
    {
      _maxRobotMotorCount = robotMotors;
    }
    if (robotSensorsCI > _maxRobotSensorCICount)
    {
      _maxRobotSensorCICount = robotSensorsCI;
    }
    if (robotSensorsAUX > _maxRobotSensorAUXCount)
    {
      _maxRobotSensorAUXCount = robotSensorsAUX;
    }
  }

  _robotMotors     = new unsigned int[4 * _robots * _maxRobotMotorCount];  
  _robotSensorsCI  = new unsigned int[4 * _robots * _maxRobotSensorCICount];  
  _robotSensorsAUX = new unsigned int[4 * _robots * _maxRobotSensorAUXCount];  

  // build sensors-motors-table
  robots = _robotHandler->getNumberOfActiveRobots();
  for (i=0; i<robots; i++)
  {    
    compounds = _robotHandler->getNumberOfCompounds(i);

    robotMotors     = 0;
    robotSensorsCI  = 0;
    robotSensorsAUX = 0;
    for (j=0; j<compounds; j++)
    {
      segs = _robotHandler->getNumberOfSegments(i, j);
      for (k=0; k<segs; k++)
      {
        motors  = _robotHandler->getNumberOfMotors(i, j, k); 
        sensors = _robotHandler->getNumberOfSensors(i, j, k); 
        for (l=0; l<motors; l++)
        {
          _robotMotors[0 + robotMotors*4 + i*4*_maxRobotMotorCount] = j;
          _robotMotors[1 + robotMotors*4 + i*4*_maxRobotMotorCount] = k;
          _robotMotors[2 + robotMotors*4 + i*4*_maxRobotMotorCount] = l;
          _robotMotors[3 + robotMotors*4 + i*4*_maxRobotMotorCount] = 
            _robotHandler->getNumberOfMotorInputs(i, j, k, l);

          robotMotors++;
        }

        for (l=0; l<sensors; l++)
        {
          if(_robotHandler->getSensorUsage(i,j,k,l) == DEF_SENSOR_USED_AS_CONTROL_INPUT)
          {
            _robotSensorsCI[0 + robotSensorsCI*4 + i*4*_maxRobotSensorCICount] = j;
            _robotSensorsCI[1 + robotSensorsCI*4 + i*4*_maxRobotSensorCICount] = k;
            _robotSensorsCI[2 + robotSensorsCI*4 + i*4*_maxRobotSensorCICount] = l;
            _robotSensorsCI[3 + robotSensorsCI*4 + i*4*_maxRobotSensorCICount] = 
              _robotHandler->getNumberOfSensorValues(i, j, k, l);

            robotSensorsCI++;
          }
          else if(_robotHandler->getSensorUsage(i,j,k,l) == DEF_SENSOR_USED_AS_AUX)
          {
            _robotSensorsAUX[0 + robotSensorsAUX*4 + i*4*_maxRobotSensorAUXCount] = j;
            _robotSensorsAUX[1 + robotSensorsAUX*4 + i*4*_maxRobotSensorAUXCount] = k;
            _robotSensorsAUX[2 + robotSensorsAUX*4 + i*4*_maxRobotSensorAUXCount] = l;
            _robotSensorsAUX[3 + robotSensorsAUX*4 + i*4*_maxRobotSensorAUXCount] = 
              _robotHandler->getNumberOfSensorValues(i, j, k, l);

            robotSensorsAUX++;
          }
        }
      }
    }
  }
}

/**
 * Deinits alls pointer recources and is called by destructor.
 */
void StandardCommunication::deinit()
{
  delete[] _robotSensorCICount;
  delete[] _robotSensorAUXCount;
  delete[] _robotSensorCIValuesCount;
  delete[] _robotSensorAUXValuesCount;
  delete[] _robotMotorCount;
  delete[] _robotMotorInputCount;
  delete[] _robotSensorsCI;
  delete[] _robotSensorsAUX;
  delete[] _robotMotors;

  _robotSensorCICount        = NULL;
  _robotSensorAUXCount       = NULL;
  _robotSensorCIValuesCount  = NULL;
  _robotSensorAUXValuesCount = NULL;
  _robotMotorCount           = NULL;
  _robotMotorInputCount      = NULL;
  _robotSensorsCI            = NULL;
  _robotSensorsAUX           = NULL;
  _robotMotors               = NULL;
}

void StandardCommunication::doHandshake(unsigned int client, int mode)
{
  unsigned int i, j, k, l, m;
  unsigned int robots, compounds, segs, motors, sensors, sensorValues;
  int          robot, compound, segment, sensor;
  string       tmpString;
  stringstream tmpStringStream;
  int          tmpValue;

  if(mode == _COM_MODE_SIMPLE)
  {
    Y_INFO("handshake request (%d), mode SIMPLE ...", client);

    pbReset(&_pbuf);
    // acknowledge handshake
    pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKEACK);
  }
  else if(mode == _COM_MODE_EXTENDED)
  {
    Y_INFO("handshake request (%d), mode EXTENDED ...", client);

    // receive tag that decides if net + physSim will run in parall or serial
    //   mode
    pbReadInt(&_pbuf, &tmpValue);
    if(tmpValue == _STANDARD_COM_ID_PARALLEL_REQ)
    {
      _isParallelModeEnabled[client] = true;
      Y_INFO("  Parallel net-physSim mode enabled");
    }
    else if(tmpValue == _STANDARD_COM_ID_SERIAL_REQ)
    {
      if(_robots > 1)
      {
        _isParallelModeEnabled[client] = true;
        Y_INFO("  Serial net-physSim mode not possible (at the moment) with multiple robots");
      }
      else
      {
        _isParallelModeEnabled[client] = false;
        Y_INFO("  Serial net-physSim mode enabled");
      }
    }
    else
    {
      Y_FATAL("wrong TAG received (should have been PARALLEL or SERIAL) ... ABORT");
      exit(-1);
    }
    
    pbReset(&_pbuf);
    // acknowledge extended handshake
    pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_ACK);
    // acknowledge parallel/serial mode
    if(_isParallelModeEnabled[client] == true)
    {
      pbWriteInt(&_pbuf, _STANDARD_COM_ID_PARALLEL_ACK);
    }
    else
    {
      pbWriteInt(&_pbuf, _STANDARD_COM_ID_SERIAL_ACK);
    }
  }

  // build handshake 
  robots = _robotHandler->getNumberOfActiveRobots();
  for (i=0; i<robots; i++)
  {    
    pbWriteInt(&_pbuf, _STANDARD_COM_TAGROBOT);
    compounds = _robotHandler->getNumberOfCompounds(i);

    // full robot description only for the correct client
    if (i == client)
    {
      for (j=0; j<compounds; j++)
      {
        pbWriteInt(&_pbuf, _STANDARD_COM_TAGCOMPOUND);
        segs = _robotHandler->getNumberOfSegments(i, j);

        for (k=0; k<segs; k++)
        {
          pbWriteInt(&_pbuf, _STANDARD_COM_TAGSEGMENT);
          motors  = _robotHandler->getNumberOfMotors(i, j, k); 
          sensors = _robotHandler->getNumberOfSensors(i, j, k); 
          sensorValues = _robotHandler->getNumberOfSensorValues(i, j, k);
          Y_INFO("Compound: %d Seg: %d Motors: %d Sensors: %d SensorValues: %d",j, k,  motors, sensors, sensorValues);

          for (l=0; l<motors; l++)
          {
            tmpString = "";
            _robotHandler->getMotorName(i,j,k,l, tmpString);

            for(m=0; m < _robotHandler->getNumberOfMotorInputs(i, j, k, l); m++)
            {
              pbWriteInt(&_pbuf, _STANDARD_COM_TAGMOTOR);

              if(mode == _COM_MODE_EXTENDED)
              {
                tmpStringStream.str("");
                tmpStringStream << tmpString << "-In" << m;

                sendExtendedMotorSensorInfo(client, 
                    _robotHandler->getMotorMinInput(i, j, k, l, m),
                    _robotHandler->getMotorMaxInput(i, j, k, l, m),
                    &tmpStringStream);
              }
            }
          }

          for (l=0; l<sensors; l++)
          {
            if(_robotHandler->getSensorUsage(i,j,k,l) == DEF_SENSOR_USED_AS_CONTROL_INPUT)
            {
              tmpString = "";
              _robotHandler->getSensorName(i,j,k,l, tmpString);

              for(m=0; m < _robotHandler->getNumberOfSensorValues(i, j, k, l); m++)
              {
                pbWriteInt(&_pbuf, _STANDARD_COM_TAGSENSOR);

                if(mode == _COM_MODE_EXTENDED)
                {
                  tmpStringStream.str("");
                  tmpStringStream << tmpString << "-Out" << m;

                  sendExtendedMotorSensorInfo(client, 
                      _robotHandler->getSensorMinOutput(i, j, k, l, m),
                      _robotHandler->getSensorMaxOutput(i, j, k, l, m),
                      &tmpStringStream);
                }
              }
            }
            else if((mode == _COM_MODE_EXTENDED) &&
                (_robotHandler->getSensorUsage(i,j,k,l) == DEF_SENSOR_USED_AS_AUX))
            {
              // in extended mode the aux sensors are send at their original
              // place in the robot struct
              tmpString = "";
              _robotHandler->getSensorName(i,j,k,l, tmpString);

              for(m=0; m < _robotHandler->getNumberOfSensorValues(i, j, k, l); m++)
              {
                pbWriteInt(&_pbuf, _STANDARD_COM_TAGSENSOR_AUX);

                tmpStringStream.str("");
                tmpStringStream << tmpString << "-Aux" << m;

                sendExtendedMotorSensorInfo(client, 
                    _robotHandler->getSensorMinOutput(i, j, k, l, m),
                    _robotHandler->getSensorMaxOutput(i, j, k, l, m),
                    &tmpStringStream);
              }
            }
            else
            {
              // sensor is only used internally and if appendMode == true then
              // auxiliary ones are send after the last segment ...
            }
          }
        }
      }
      
      if(mode == _COM_MODE_SIMPLE)
      {
        // in simple mode the aux sensors are send at the end of the last
        // segment of the robot
        for (j=0; j < (int)_robotSensorAUXCount[i]; j++)
        {
          robot    = i;
          compound = _robotSensorsAUX[0 + j*4 + robot*4*_maxRobotSensorAUXCount]; 
          segment  = _robotSensorsAUX[1 + j*4 + robot*4*_maxRobotSensorAUXCount];
          sensor   = _robotSensorsAUX[2 + j*4 + robot*4*_maxRobotSensorAUXCount];

          tmpString = "";
          _robotHandler->getSensorName(robot, compound, segment, sensor, tmpString);

          for(int k=0; k< _robotSensorsAUX[3 + j*4 + robot*4*_maxRobotSensorAUXCount]; k++)
          {
            pbWriteInt(&_pbuf, _STANDARD_COM_TAGSENSOR);
          }
        }
      }
    }
  }

  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);
 
  Y_INFO("...handshake sent.");
}

void StandardCommunication::sendExtendedMotorSensorInfo(int client, double
    minVal, double maxVal, stringstream *tmpStringStream)
{
  int   n;
  int   stringLength;
  int   tmpValue;
  float tmpFloatValue;

  // send min/max motor/sensor mapping input/output values
  tmpFloatValue = (float) minVal;
  pbWriteFloat(&_pbuf, tmpFloatValue);
  tmpFloatValue = (float) maxVal;
  pbWriteFloat(&_pbuf, tmpFloatValue);

  // send packet
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);

  // reset buffer and wait for another handshake ext req
  pbReset(&_pbuf);
  udpReadPbuf(&_pbuf, client); 
  pbReadInt(&_pbuf, &tmpValue);
  if(tmpValue != _STANDARD_COM_ID_HANDSHAKE_EXT_REQ)
  {
    Y_FATAL("wrong TAG received (should have been HANDSHAKE EXT REQ) ... ABORT");
    exit(-1);
  }

  // send handshake ext ack, string length, string + comend
  pbReset(&_pbuf);
  // acknowledge extended handshake
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_ACK);
  // send string lenght of motor/sensor name
  stringLength = MIN((PBUF_SIZE - 3*4), (*tmpStringStream).str().length());
  pbWriteInt(&_pbuf, stringLength);
  // send string with max length stringLength
  for(n=0; n < stringLength; n++)
  {
    pbWriteByte(&_pbuf, (*tmpStringStream).str()[n]);
  }
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);

  // reset buffer and wait for another handshake ext req
  pbReset(&_pbuf);
  udpReadPbuf(&_pbuf, client); 
  pbReadInt(&_pbuf, &tmpValue);
  if(tmpValue != _STANDARD_COM_ID_HANDSHAKE_EXT_REQ)
  {
    Y_FATAL("wrong TAG received (should have been HANDSHAKE EXT REQ) ... ABORT");
    exit(-1);
  }

  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_ACK);
}

void StandardCommunication::receiveMotorData(unsigned int client)
{
  int i,j;
  int robot, motors;
  float valuef;
  
  pbReadInt(&_pbuf, &robot); 
  if(robot != client)
  {
    Y_FATAL("\nreceiveMotorData: wrong robot number requested (%d instead of %d)! ABORT!\n\n",
        robot, client);
    exit(-1);
  }
  pbReadInt(&_pbuf, &motors);
  if (motors != (int)_robotMotorInputCount[robot])
  {
    Y_FATAL("\nFatal error: wrong motor amount requested (%d instead of %d)! ABORT!\n\n",
        motors, _robotMotorInputCount[robot]);
    exit(-1);
  }

  for (i=0; i<_robotMotorCount[robot]; i++)
  {
    // Each motor may be activated with more than 1 value (e.g. antagonistic
    // muscles)
    for (j=0; j<_robotMotors[3 + i*4 + robot*4*_maxRobotMotorCount]; j++)
    {
      pbReadFloat(&_pbuf, &valuef);

      _robotHandler->setMotorValue(robot, 
          _robotMotors[0 + i*4 + robot*4*_maxRobotMotorCount], 
          _robotMotors[1 + i*4 + robot*4*_maxRobotMotorCount],
          _robotMotors[2 + i*4 + robot*4*_maxRobotMotorCount], j, (double) valuef);
    }
  }
}

void StandardCommunication::sendSensorData(unsigned int client)
{
  int i,j;
  int robot;
  double valued;

  robot = client;

  // send sensors
  pbReset(&_pbuf);  
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_DATAACK);
  pbWriteInt(&_pbuf, robot);           

  if((*(YarsContainers::AbortCondition())).abortCondition == true)
  {
#ifdef USE_LOG4CPP_OUTPUT
    logger << log4cpp::Priority::INFO << "AbortCondition is true. Source = "
      << (*(YarsContainers::AbortCondition())).abortSource
      << ", Cause = " << (*(YarsContainers::AbortCondition())).abortCause
      << log4cpp::CategoryStream::ENDLINE;
#endif // USE_LOG4CPP_OUTPUT
    pbWriteInt(&_pbuf, _STANDARD_ROBOT_STATE_ABORT);
  }
  else
  {
    pbWriteInt(&_pbuf, _STANDARD_ROBOT_STATE_OK);
  }

  // send a total of CISensorvalues + AUXSEnsorValues values to the client
  pbWriteInt(&_pbuf, _robotSensorCIValuesCount[robot]
                   + _robotSensorAUXValuesCount[robot]);  

  // first write all control input sensor values
  for (i=0; i<(int)_robotSensorCICount[robot]; i++)
  {
    for(int j=0; j< _robotSensorsCI[3 + i*4 + robot*4*_maxRobotSensorCICount]; j++)
    {
      valued = _robotHandler->getSensorValues(robot, 
          _robotSensorsCI[0 + i*4 + robot*4*_maxRobotSensorCICount], 
          _robotSensorsCI[1 + i*4 + robot*4*_maxRobotSensorCICount],
          _robotSensorsCI[2 + i*4 + robot*4*_maxRobotSensorCICount])[j];

      pbWriteFloat(&_pbuf, (float) valued);
    }
  }
  // then all auxiliary sensor values
  for (i=0; i<(int)_robotSensorAUXCount[robot]; i++)
  {
    for(int j=0; j< _robotSensorsAUX[3 + i*4 + robot*4*_maxRobotSensorAUXCount]; j++)
    {
      valued = _robotHandler->getSensorValues(robot, 
          _robotSensorsAUX[0 + i*4 + robot*4*_maxRobotSensorAUXCount], 
          _robotSensorsAUX[1 + i*4 + robot*4*_maxRobotSensorAUXCount],
          _robotSensorsAUX[2 + i*4 + robot*4*_maxRobotSensorAUXCount])[j];

      pbWriteFloat(&_pbuf, (float) valued);
    }
  }

  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);
}

void StandardCommunication::doData(unsigned int client)
{
  receiveMotorData(client);

  if(_isParallelModeEnabled[client] == true)
  {
    // parallel mode
    // directly send sensor data after having received the motor data (i.e.
    // before the sim update(s)) so that controller and simulation may be
    // processed in parallel
    sendSensorData(client);
  }
  else
  {
    // serial mode
    // only receive the motor data now and set the client state so that the
    // sensor data will be directly send AFTER the sim update(s) (see
    // communicate() function)
    _clientState[client] = _STANDARD_STATE_WAITING_FOR_SENSOR;
  }
}

void StandardCommunication::doInit(unsigned int client)
{
  unsigned int i;
  int state, robot;
    
  _clientState[client] = _STANDARD_STATEINIT;
  
  pbReadInt(&_pbuf, &robot); 
  pbReadInt(&_pbuf, &state); // state used as tmp var
  _seed[robot] = (unsigned int)state;
  Y_DEBUG("StandardCommunication::doInit: seed = %d", _seed[robot]);

  _robotHandler->stopRobot(robot);

  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_INITACK);
  pbWriteInt(&_pbuf, robot);  
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);

  //if all clients are ready to init -> init
  state = _STANDARD_STATEINIT;
  for (i=0; i<_clientCount; i++)
  {
    state = state & _clientState[i];
  }
  if (state == _STANDARD_STATEINIT)
  {
    _fnInit(_seed[robot]);
    for (i=0; i<_clientCount; i++)
    {
      _clientState[i] = _STANDARD_STATEREADY;
    }
  }
}

void StandardCommunication::doNextTry(unsigned int client)
{
  unsigned int i;
  int state, robot;
  _clientState[client] = _STANDARD_STATE_NEXTTRY;

  pbReadInt(&_pbuf, &robot); 
  pbReadInt(&_pbuf, &state); // state used as tmp var
  _seed[robot] = (unsigned int)state;

  _robotHandler->stopRobot(robot);

  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_NEXT_TRYACK);
  pbWriteInt(&_pbuf, robot); 
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);

  //if all clients are ready to reset -> reset
  state = _STANDARD_STATE_NEXTTRY;
  for (i=0; i<_clientCount; i++)
  {
    state = state & _clientState[i];
  }
  if (state == _STANDARD_STATE_NEXTTRY)
  {
    Y_DEBUG("StandardCommunication::doNextTry: seed = %d", _seed[robot]);
    _fnNextTry(_seed[robot]);
    for (i=0; i<_clientCount; i++)
    {
      _clientState[i] = _STANDARD_STATEREADY;
    }
  }
}


void StandardCommunication::doReset(unsigned int client)
{
  unsigned int i;
  int state, robot;

  _clientState[client] = _STANDARD_STATERESET;

  pbReadInt(&_pbuf, &robot); 

  _robotHandler->stopRobot(robot);

  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_RESETACK);
  pbWriteInt(&_pbuf, robot); 
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf, client);

  //if all clients are ready to reset -> reset
  state = _STANDARD_STATERESET;
  for (i=0; i<_clientCount; i++)
  {
    state = state & _clientState[i];
  }
  if (state == _STANDARD_STATERESET)
  {
    _fnReset();
    for (i=0; i<_clientCount; i++)
    {
      _clientState[i] = _STANDARD_STATEREADY;
    }
  }
}

void StandardCommunication::doUndefined(unsigned int client, int id)
{
  Y_DEBUG("UNDEFINED PACKET ID RECIEVED from client %d (%d). Maybe communication is not stable anymore!\n", client, id);
}

void StandardCommunication::communicate()
{
  unsigned int i;
  int id;

  Y_DEBUG("StandardCommunication::communicate");

  for (i=0; i<_clientCount; i++)
  {

    if(_clientState[i] == _STANDARD_STATE_WAITING_FOR_SENSOR)
    {
      // serial mode, i.e. we send sensor data after sim update(s)
      Y_DEBUG("StandardCommunication::communicate: send sensor data after simSteps");
      sendSensorData(i);
      _clientState[i] = _STANDARD_STATEREADY;
    }
    else if (_clientState[i] != _STANDARD_STATEREADY)
    {
      //skip not ready clients (e.g. clients waiting for reset)
      continue;
    }

    pbReset(&_pbuf);
    id = udpReadPbuf(&_pbuf, i); 

    pbReadInt(&_pbuf, &id);

    switch(id)
    {
      // handshake: send robot information
      case _STANDARD_COM_ID_HANDSHAKEREQ:
        Y_DEBUG("StandardCommunication::communicate: HANDSHAKE");
        doHandshake(i, _COM_MODE_SIMPLE);
        break;

      // extended handshake: send robot information
      case _STANDARD_COM_ID_HANDSHAKE_EXT_REQ:
        Y_DEBUG("StandardCommunication::communicate: HANDSHAKE EXT");
        doHandshake(i, _COM_MODE_EXTENDED);
        break;

        // data exchange
      case _STANDARD_COM_ID_DATAREQ:
        Y_DEBUG("StandardCommunication::communicate: DATA");
        doData(i);
        break;

        // robot init
      case _STANDARD_COM_ID_INITREQ:
        Y_DEBUG("StandardCommunication::communicate: INIT");
        doInit(i);
        break;

        // robot reset
      case _STANDARD_COM_ID_RESETREQ:
        Y_DEBUG("StandardCommunication::communicate: RESET");
        doReset(i);
        break;
      case _STANDARD_COM_ID_NEXT_TRYREQ:
        Y_DEBUG("StandardCommunication::communicate: NEXT TRY");
        doNextTry(i);
        break;
      default:
        Y_DEBUG("StandardCommunication::communicate: UNDEFINED COM REQUEST");
        doUndefined(i, id);
        break;
    }
  }
}


