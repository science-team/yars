/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef __STANDARD_COMMUNICATION_H
#define __STANDARD_COMMUNICATION_H


#include <net/UDPCommunication.h>
#include <net/PacketBuf.h>
#include <base/RobotHandler.h>

#include <sstream>
#include <string>

#define _STANDARD_COMPORT                4500

#define _STANDARD_COMEND                   -1

#define _STANDARD_COM_ID_PINGREQ            0
#define _STANDARD_COM_ID_PINGACK            1
#define _STANDARD_COM_ID_HANDSHAKEREQ      10
#define _STANDARD_COM_ID_HANDSHAKEACK      11
#define _STANDARD_COM_ID_HANDSHAKE_EXT_REQ 12
#define _STANDARD_COM_ID_HANDSHAKE_EXT_ACK 13
#define _STANDARD_COM_ID_SENSORREQ         20
#define _STANDARD_COM_ID_SENSORACK         21
#define _STANDARD_COM_ID_MOTORREQ          30
#define _STANDARD_COM_ID_MOTORACK          31
#define _STANDARD_COM_ID_DATAREQ           40
#define _STANDARD_COM_ID_DATAACK           41
#define _STANDARD_COM_ID_RESETREQ          50
#define _STANDARD_COM_ID_RESETACK          51
#define _STANDARD_COM_ID_INITREQ           60
#define _STANDARD_COM_ID_INITACK           61
#define _STANDARD_COM_ID_NEXT_TRYREQ       70
#define _STANDARD_COM_ID_NEXT_TRYACK       71

#define _STANDARD_COM_ID_PARALLEL_REQ      80
#define _STANDARD_COM_ID_PARALLEL_ACK      81
#define _STANDARD_COM_ID_SERIAL_REQ        82
#define _STANDARD_COM_ID_SERIAL_ACK        83

#define _STANDARD_COM_TAGROBOT              0
#define _STANDARD_COM_TAGCOMPOUND         100
#define _STANDARD_COM_TAGSEGMENT          200
#define _STANDARD_COM_TAGMOTOR            300
#define _STANDARD_COM_TAGSENSOR           400
#define _STANDARD_COM_TAGSENSOR_AUX       500

#define _STANDARD_STATEREADY                0
#define _STANDARD_STATERESET                1
#define _STANDARD_STATEINIT                 2
#define _STANDARD_STATE_NEXTTRY             3
#define _STANDARD_STATE_WAITING_FOR_SENSOR  4

#define _STANDARD_ROBOT_STATE_OK            0
#define _STANDARD_ROBOT_STATE_ABORT         1

#define _COM_MODE_SIMPLE                    0
#define _COM_MODE_EXTENDED                  1

typedef void FuncPointer (void);
typedef void ResetFuncPointer (unsigned int seed);
typedef void NextTryFuncPointer (unsigned int seed);
typedef void InitFuncPointer (unsigned int seed);

class StandardCommunication
{
 public:
  StandardCommunication(unsigned int clients);
  ~StandardCommunication();

  void init(RobotHandler *robHandler, FuncPointer *reset, InitFuncPointer *init,
      NextTryFuncPointer *nextTry);

  void communicate();

 private:

  void doInit(unsigned int client);
  void doHandshake(unsigned int client, int mode);
  void doReset(unsigned int client);
  void doNextTry(unsigned int client);
  void receiveMotorData(unsigned int client);
  void sendSensorData(unsigned int client);
  void doData(unsigned int client);
  void doSimulate(unsigned int client);
  void doUndefined(unsigned int client, int id);
  
  void deinit();

  void sendExtendedMotorSensorInfo(int client, double minVal, double maxVal,
      stringstream *tmpStringStream);

  RobotHandler *_robotHandler;
  FuncPointer *_fnReset;
  NextTryFuncPointer *_fnNextTry;
  InitFuncPointer *_fnInit;

  /** how many robots */
  unsigned int _robots;
  
  /** complicated: store compound, seg, motor and motor input for each robot
   * motor input. Example: one robot has 5 motor inputs in different compounds
   * and segments. This information should NOT be parsed in each simulation
   * step.  Therefor there will be created a 4-D-data array, wich 'knows' for
   * all N motor inputs (0..N) its compounds, segments, motor number and motor
   * input number. The same holds for the control input sensors and the
   * auxiliary sensors.
   */
  unsigned int *_robotMotors;
  /** the same as above */    
  unsigned int *_robotSensorsCI;
  unsigned int *_robotSensorsAUX;

  /** the highest dimension of motors for the 4-d-array */
  unsigned int _maxRobotMotorCount;
  /** the highest dimension of sensors for the 4-d-array */
  unsigned int _maxRobotSensorCICount;
  unsigned int _maxRobotSensorAUXCount;

  /** how many motors has ONE robot */
  unsigned int *_robotMotorCount;
  /** how many motors inputs has ONE robot */
  unsigned int *_robotMotorInputCount;
  /** how many sensors has ONE robot */
  unsigned int *_robotSensorCICount;
  unsigned int *_robotSensorAUXCount;
  /** how many sensorValues has ONE robot */
  unsigned int *_robotSensorCIValuesCount;
  unsigned int *_robotSensorAUXValuesCount;
  
  /** how many clients are recently connected */
  unsigned int _clientCount;

  /** seed for reset function, received from client */
  unsigned int *_seed;

  /** which state has each client? */
  int          *_clientState;

  /** state variables */
  bool         *_isParallelModeEnabled; // true or false
  
  PacketBuffer _pbuf;

};

#endif
