/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "UDPCommunication.h"

/**
 * C-implementation of UDP communication
 */
struct    sockaddr_in udpServer[UDP_MAX_SERVER];
int       udpSock[UDP_MAX_SERVER];
struct    sockaddr_in udpClient[UDP_MAX_SERVER];
struct    sockaddr_in udpTmpClient;
socklen_t udpLength[UDP_MAX_SERVER];
int       udpServers = 0;

/**
 * Inits the UDP Connection with the specified port. This method should be
 * called at simulator startup
 */
void udpInitConnection(int port, int servers)
{
  int    rval = 0;
  char   buf[1024];  
  struct hostent *udpCname;
  int i;

  udpServers = servers;

  // register clients
  for (i=0; i<servers; i++)
  {
    udpServer[i].sin_family = AF_INET;
    udpServer[i].sin_addr.s_addr = INADDR_ANY;
    udpServer[i].sin_port = htons(port);

    Y_INFO("\nIniting UDP-Socket-Connection...\n");

    if((udpSock[i] = socket(PF_INET, SOCK_DGRAM,0))<0)
    {
      perror("opening udpSocket");
      exit(1);
    }

    if ( (udpSock[i] = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {	
      perror("opening udpSocket");
      exit(1);
    }

    while (bind(udpSock[i], (struct sockaddr *)&udpServer[i], sizeof(udpServer[i])) < 0)
    {
      port++;
      udpServer[i].sin_port = htons(port);    
    }

    udpLength[i] = sizeof(udpServer[i]);
    if (getsockname(udpSock[i], (struct sockaddr *)&udpServer[i], &udpLength[i]) < 0)
    {	
      perror("getting udpSocket name");
      exit(1);
    }

    Y_INFO("Socket port %d assigned, ", ntohs(udpServer[i].sin_port));

    Y_INFO("\nWaiting for udpClient %d ...\n", i);

    udpLength[i] = sizeof(udpClient[i]);
    rval = recvfrom(udpSock[i], buf, sizeof(buf), 0,
        (struct sockaddr *) &udpClient[i], &udpLength[i]);

    udpCname = gethostbyaddr((char *) &udpClient[i].sin_addr, 4, AF_INET);
    if (udpCname == NULL)
    {
      Y_INFO("Connected to unknown machine %s, ",
          inet_ntoa(udpClient[i].sin_addr));
    }
    else
    {
      Y_INFO("Connected to %s (%s), ", udpCname->h_name,
          inet_ntoa(udpClient[i].sin_addr));
    }

    Y_INFO("port %d\n", ntohs(udpClient[i].sin_port));
    buf[rval] = 0;
  }
  
  Y_INFO("...init done\n");
}


int udpReadPbuf(struct PacketBuffer *pb, int server)
{
  int ret;

  ret = udpReadByteArray(pb->buf, PBUF_SIZE-1, server);
  pb->pointer = 0;

  return ret;
}

int udpSendPbuf(struct PacketBuffer *pb, int server)
{
  int ret;

  ret =  udpSendByteArray(pb->buf, pb->pointer, server);
  pb->pointer = 0;

  return ret;
}

int udpReadByteArray(char *b, int size, int server)
{
  int i;

  do
  {
    i = recvfrom(udpSock[server], b, size, 0,
        (struct sockaddr *) &udpTmpClient, &udpLength[server]);

    if (udpTmpClient.sin_port != udpClient[server].sin_port)
    {
      Y_DEBUG("Got UDP Datagramm from unregistered client. No problem: packet dropped!\n");  
      sendto(udpSock[server], b, 1, 0, (struct sockaddr *) &udpTmpClient, 
          sizeof(udpTmpClient));
      i = -1;
    }
  } 
  while (i<0);

  return i;
}

int udpSendByteArray(char *b, int size, int server)
{
  int i;
  
  do
  {
    i = sendto(udpSock[server], b, size, 0,
        (struct sockaddr *) &udpClient[server], sizeof(udpClient[server]));
  } 
  while (i<0);
  return 0; // TODO
}
