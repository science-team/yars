/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Braitenberg.h"

using namespace std;

void Braitenberg::update(){
  double oldOutput1 = _outputs[0];
  double oldOutput2 = _outputs[1];
  _outputs[0] = tanh((-1.4)*(_inputs[1]+0.3) + 1.3*oldOutput1);
  _outputs[1] = tanh((-1.4)*(_inputs[0]+0.3) + 1.4*oldOutput2);
}

void Braitenberg::init(){
}

// the class factories
extern "C" RobotController* create() {
  return new Braitenberg;
}

extern "C" void destroy(RobotController* controller) {
  delete controller;
}
