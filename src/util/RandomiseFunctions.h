/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 

#ifndef __RANDOMISE_FUNCTIONS_H__
#define __RANDOMISE_FUNCTIONS_H__

#include <description/DescriptionHelper.h>
#include <util/defines.h>

class RandomiseFunctions 
{
public:
  static void randomisePosition(Position *position);
  static void randomiseAdditive(Position *position);
  static void randomiseGlobal(Position *position);
  static void randomiseGlobalList(Position *position);
  static void randomiseGlobalInterval(Position *position);
  static double nextDouble();
  static void getGlobalInterval(double *value, double min, double max);
  static void getAdditiveInterval(double *value, double min, double max,
      double variance);
  static void randomiseAdditiveList(Position *position);
  static void randomiseAdditiveInterval(Position *position);

  static void randomiseScalar(double *scalar,
      RandomiseScalar *randomise);

  static void randomiseScalarGlobal(double *scalar,
      RandomiseScalar *randomise);

  static void randomiseScalarAdditive(double *scalar,
      RandomiseScalar *randomise);

  static void randomiseScalarGlobalList(double *scalar,
      RandomiseScalar *randomise);

  static void randomiseScalarGlobalInterval(double *scalar,
      RandomiseScalar *randomise);

  static void randomiseScalarAdditiveInterval(double *scalar, 
      RandomiseScalar *randomise);

  static void randomiseScalarAdditiveList(double *scalar, 
      RandomiseScalar *randomise);
};

#endif // __RANDOMISE_FUNCTIONS_H__
