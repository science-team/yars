/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _RING_BUFFER_H_
#define _RING_BUFFER_H_

template<typename T, int MAXSIZE>
class RingBuffer
{
  public:
    RingBuffer();
    ~RingBuffer();
    int getMaxSize();
    int getSize();
    void addValue(const T&);
    T getValue(int);
    void clearBuffer();

  private:
    T rB[MAXSIZE];
    int last;
    int size;
};


template <typename T, int MAXSIZE>
RingBuffer<T,MAXSIZE>::RingBuffer()
  : size(0), last(-1)
{
  // nothing to do
}

template <typename T, int MAXSIZE>
RingBuffer<T,MAXSIZE>::~RingBuffer()
{
  // nothing to do
}

template <typename T, int MAXSIZE>
int RingBuffer<T,MAXSIZE>::getMaxSize()
{
  return MAXSIZE;
}

template <typename T, int MAXSIZE>
int RingBuffer<T,MAXSIZE>::getSize()
{
  return size;
}
  
template <typename T, int MAXSIZE>
void RingBuffer<T,MAXSIZE>::addValue(const T& elem)
{
  if(last >= MAXSIZE - 1)
  {
    last = -1;
  }
  if(size < MAXSIZE)
  {
    size++;
  }

  rB[++last] = elem;
}

// Position is counted backwards from last
// so getValue(0) will get most recent value
//    getValue(maxSize) will get oldest value
template <typename T, int MAXSIZE>
T RingBuffer<T,MAXSIZE>::getValue(int pos)
{
  if(pos < size)
  {
    // valid position
    if(last - pos < 0)
    {
      return rB[MAXSIZE + last - pos];
    }
    else
    {
      return rB[last - pos];
    }
  }
  else
  {
    // TODO: non valid position, exception handling?
    throw "RingBuffer<>::getValue(): non valid position";
  }
}

template <typename T, int MAXSIZE>
void RingBuffer<T,MAXSIZE>::clearBuffer()
{
  last = 0;
  size = 0;
}

#endif

//
// Template for RingBuffers, instantiation is as follows:
//   RingBuffer<type,maxSize> nameOfRingBuffer;
//
// TODO:
// - if maxSize is chosen as a power of two we might use modulo arithmetic
//     instead of the if/else stuff ...
// - not thread safe yet
//


