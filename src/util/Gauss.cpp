/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Gauss.h"
#include <values.h>

/**
 * This class calculates gaussian noise. It is directly copied from java class
 * Gauss.java of the EvoSun-package.
 */
Gauss::Gauss(double sigma)
{
  timeval time;

  gettimeofday(&time, 0);
  srand(time.tv_usec);
  
  eps_ = DBL_MIN;

  init(sigma);
}

Gauss::~Gauss()
{ 
}

void Gauss::init(double sigma)
{
  if(sigma_.size() == 0)
  {
    this->add(sigma);
  }
  else
  {
    this->init(0, sigma);
  }
}

void Gauss::init(int gaussNum, double sigma)
{
  if(gaussNum > (sigma_.size() - 1))
  {
    throw("Gauss::init(int, double) gaussNum > sigma_.size() (requested gauss does not exist)");
  }
  else
  {
    sigma_[gaussNum] = sigma;
  }
}

void Gauss::add(double sigma)
{
  sigma_.push_back(sigma);
}

int Gauss::getSize()
{
  return sigma_.size();
}

double Gauss::next(double mean)
{
  double rnd1, rnd2, x, y, z, radius;

  radius = 1.0;
  do
  {
    rnd1 = rand();
    rnd1 = rnd1/RAND_MAX;
    rnd2 = rand();
    rnd2 = rnd2/RAND_MAX;

    x = 2.0 * rnd1 - 1.0;
    y = 2.0 * rnd2 - 1.0;
    radius = x*x + y*y;
  } 
  while ((radius >= 1.0) || (radius < eps_));
 
  z = mean + sigma_[0] * x * sqrt(-2.0 * log(radius) / radius);
  
  return z; 
}

double Gauss::next(int gaussNum, double mean)
{
  double rnd1, rnd2, x, y, z, radius;

  radius = 1.0;
  do
  {
    rnd1 = rand();
    rnd1 = rnd1/RAND_MAX;
    rnd2 = rand();
    rnd2 = rnd2/RAND_MAX;

    x = 2.0 * rnd1 - 1.0;
    y = 2.0 * rnd2 - 1.0;
    radius = x*x + y*y;
  } 
  while ((radius >= 1.0) || (radius < eps_));
 
  z = mean + sigma_[gaussNum] * x * sqrt(-2.0 * log(radius) / radius);
  
  return z; 
}

void Gauss::next(vector<double> *means)
{
  double rnd1, rnd2, x, y, radius;

  if(sigma_.size() < (*means).size())
  {
    throw("Gauss::next(vector<double>) sigma_size() < means.size()");
  }

  for(int i=0; i < (*means).size(); i++)
  {
    radius = 1.0;
    do
    {
      rnd1 = rand();
      rnd1 = rnd1/RAND_MAX;
      rnd2 = rand();
      rnd2 = rnd2/RAND_MAX;

      x = 2.0 * rnd1 - 1.0;
      y = 2.0 * rnd2 - 1.0;
      radius = x*x + y*y;
    } 
    while ((radius >= 1.0) || (radius < eps_));

    (*means)[i] = (*means)[i] + sigma_[i] * x * sqrt(-2.0 * log(radius) / radius);
  }
}
