/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "Matrix.h"

/**
 * This class provides some routines concerned with matrices
 */

Matrix::Matrix()
{
}

Matrix::~Matrix()
{
}

void Matrix::transposeMatrix3(dMatrix3 m)
{
  dReal tmp;

  tmp  = m[1];
  m[1] = m[4];
  m[4] = tmp;

  tmp  = m[2];
  m[2] = m[8];
  m[8] = tmp;

  tmp  = m[6];
  m[6] = m[9];
  m[9] = tmp;
}

void Matrix::getTransposedMatrix3(dMatrix3 resultMatrix, dMatrix3 inputMatrix)
{
  resultMatrix[0]  = inputMatrix[0];
  resultMatrix[1]  = inputMatrix[4];
  resultMatrix[2]  = inputMatrix[8];
  resultMatrix[4]  = inputMatrix[1];
  resultMatrix[5]  = inputMatrix[5];
  resultMatrix[6]  = inputMatrix[9];
  resultMatrix[8]  = inputMatrix[2];
  resultMatrix[9]  = inputMatrix[6];
  resultMatrix[10] = inputMatrix[10];
}
