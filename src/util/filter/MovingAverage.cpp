/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "MovingAverage.h"


/**
 * This class constitutes a simple moving average filter wich may be constructed
 * with the following parameters
 * 
 * - numberOfPoints 
 * - initial value of points not yet filled
 * - recursive calculation on / off
 * - iterations after wich full calculation is redone (0 means never)
 */
MovingAverage::MovingAverage(int numberOfPoints, double initialValue, 
    bool recursive, unsigned int itersBeforeFullRecalc)
{
  numPoints_     = numberOfPoints;
  if(numPoints_ < 1)
    numPoints_ = 1;
  recursive_     = recursive;
  maxIters_      = itersBeforeFullRecalc;
  if(maxIters_ == 0)
    maxIters_ == 65535;

  points_.resize(numPoints_, initialValue);

  iters_         = 0;
  pointPos_      = 0;

  accumulator_ = numPoints_ * initialValue;
}

MovingAverage::~MovingAverage()
{ 
}

double MovingAverage::getFilteredOutput(double unfilteredInput)
{
  if(!recursive_ || iters_ >= maxIters_)
  {
    iters_       = 0;
    accumulator_ = 0;

    points_[pointPos_] = unfilteredInput; // replace oldest value

    for(int i = 0; i < numPoints_; i++)
    {
      accumulator_ += points_[i];
    }
  }
  else // recursive case
  {
    accumulator_      -= points_[pointPos_];
    points_[pointPos_] = unfilteredInput; // replace oldest value
    accumulator_      += points_[pointPos_];

    iters_++;
  }

  // move on
  pointPos_++;
  if(pointPos_ >= numPoints_)
    pointPos_ = 0;

  return (accumulator_ / numPoints_);
}
