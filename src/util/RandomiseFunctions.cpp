/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "RandomiseFunctions.h"
#include <sys/time.h>
#include <stdlib.h>


void RandomiseFunctions::randomiseScalar(double *scalar,
    RandomiseScalar *randomise)
{
  if(randomise->enabled == false)
  {
    Y_DEBUG("RandomiseFunctions::randomiseScalar: not enabled");
    return;
  }

  Y_DEBUG("RandomiseFunctions::randomiseScalar: *** ENABLED ***");

  if(nextDouble() > randomise->probability)
  {
    Y_DEBUG(
        "RandomiseFunctions::randomiseScalar: probability fail -> exit");
    return;
  }


  switch(randomise->method)
  {
    case RD_RANDOMISE_METHOD_GLOBAL:
      RandomiseFunctions::randomiseScalarGlobal(scalar, randomise);
      break;
    case RD_RANDOMISE_METHOD_ADDITIVE:
      RandomiseFunctions::randomiseScalarAdditive(scalar, randomise);
      break;
    default:
      Y_DEBUG("RandomiseFunctions::SCALAR::SERIOUS PROBLEM method = %d",
          randomise->method);
      exit(-1);
  }
}

void RandomiseFunctions::randomiseScalarGlobal(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobal");

  switch(randomise->type)
  {
    case RD_RANDOMISE_TYPE_INTERVAL:
      randomiseScalarGlobalInterval(scalar, randomise);
      break;
    case RD_RANDOMISE_TYPE_LIST:
      randomiseScalarGlobalList(scalar, randomise);
      break;
  }
};

void RandomiseFunctions::randomiseScalarAdditive(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarAdditive");

  switch(randomise->type)
  {
    case RD_RANDOMISE_TYPE_INTERVAL:
      randomiseScalarAdditiveInterval(scalar, randomise);
      break;
    case RD_RANDOMISE_TYPE_LIST:
      randomiseScalarAdditiveList(scalar, randomise);
      break;
  }
};


void RandomiseFunctions::randomiseScalarGlobalList(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobalList start");
  int size = randomise->entries.size();
  int index = (int)(RandomiseFunctions::nextDouble()*size);
  *scalar = (randomise->entries)[index];
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobalList new value %f",
      scalar);
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobalList end");
}

void RandomiseFunctions::randomiseScalarGlobalInterval(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobalInterval start");
  getGlobalInterval(scalar, randomise->min, randomise->max);
  Y_DEBUG("RandomiseFunctions::randomiseScalarGlobalInterval end");
}

void RandomiseFunctions::randomiseScalarAdditiveList(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarAdditiveList start");
  double new_scalar = 0;
  int index = randomise->last_index;

  if(index == -1)
  {
    index = (int)(RandomiseFunctions::nextDouble() 
        * randomise->entries.size());
  }
  else
  {
    if(nextDouble() < 0.5) 
    {
      Y_DEBUG("RandomiseFunctions::increasing index");
      index += 1;
    }
    else
    {
      Y_DEBUG("RandomiseFunctions::decreasing index");
      index -= 1;
    }

    int size = randomise->entries.size();
    int max_index = size - 1;
    if(index < 0)
    {
      Y_DEBUG("RandomiseFunctions::index %d < 0 -> %d", index, max_index);
      index = max_index;
    }
    if(index > max_index)
    {
      Y_DEBUG("RandomiseFunctions::index %d > %d -> 0", index ,max_index);
      index = 0;
    }
  }

  Y_DEBUG("new_scalar index %d last_index %d",
      index, randomise->last_index);
  new_scalar = (randomise->entries)[index];
  Y_DEBUG("new_scalar index %d = %f", index, new_scalar);

  *scalar = new_scalar;
  randomise->last_index = index;

  Y_DEBUG("RandomiseFunctions::randomiseScalarAdditiveList end");
}

void RandomiseFunctions::randomiseScalarAdditiveInterval(double *scalar, 
    RandomiseScalar *randomise)
{
  Y_DEBUG("RandomiseFunctions::randomiseScalarAdditiveInterval start");
  getAdditiveInterval(scalar,
      randomise->min,
      randomise->max,
      randomise->variance);
  Y_DEBUG("RandomiseFunctions::randomiseScalarAdditiveInterval end");
}




void RandomiseFunctions::randomisePosition(Position *position)
{

  if(position->randomise.enabled == false)
  {
    Y_DEBUG("RandomiseFunctions::randomisePosition: not enabled");
    Y_DEBUG("RandomiseFunctions::randomisePosition: %d",
        position->randomise.enabled);
    // to randomisation
    return;
  }

  Y_DEBUG("RandomiseFunctions::randomisePosition: *** ENABLED ***");

  switch(position->randomise.method)
  {
    case RD_RANDOMISE_METHOD_GLOBAL:
      RandomiseFunctions::randomiseGlobal(position);
      break;
    case RD_RANDOMISE_METHOD_ADDITIVE:
      RandomiseFunctions::randomiseAdditive(position);
      break;
    default:
      Y_DEBUG("RandomiseFunctions::SERIOUS PROBLEM method = %d",
          position->randomise.method);
  }
};


void RandomiseFunctions::randomiseGlobal(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseGlobal");

  switch(position->randomise.type)
  {
    case RD_RANDOMISE_TYPE_INTERVAL:
      randomiseGlobalInterval(position);
      break;
    case RD_RANDOMISE_TYPE_LIST:
      randomiseGlobalList(position);
      break;
  }
};

void RandomiseFunctions::randomiseAdditive(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseAdditive");

  switch(position->randomise.type)
  {
    case RD_RANDOMISE_TYPE_INTERVAL:
      randomiseAdditiveInterval(position);
      break;
    case RD_RANDOMISE_TYPE_LIST:
      randomiseAdditiveList(position);
      break;
    default:
      Y_DEBUG("RandomiseFunctions::SERIOUS PROBLEM type = %d", position->randomise.type);
      break;
  }
};

void RandomiseFunctions::randomiseGlobalInterval(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseGlobalInterval");

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.x)
  {
    RandomiseFunctions::getGlobalInterval(
        &(position->x), position->randomise.min.x, position->randomise.max.x);
  }

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.y)
  {
    RandomiseFunctions::getGlobalInterval(
        &(position->y), position->randomise.min.y, position->randomise.max.y);
  }

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.z)
  {
    RandomiseFunctions::getGlobalInterval(
        &(position->z), position->randomise.min.z, position->randomise.max.z);
  }
};

void RandomiseFunctions::randomiseAdditiveInterval(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseAdditiveInterval");

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.x)
  {
    RandomiseFunctions::getAdditiveInterval(
        &(position->x),
        position->randomise.min.x,
        position->randomise.max.x,
        position->randomise.variance.x);
  }

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.y)
  {
    RandomiseFunctions::getAdditiveInterval(
        &(position->y),
        position->randomise.min.y,
        position->randomise.max.y,
        position->randomise.variance.y);
  }

  if(RandomiseFunctions::nextDouble() <= position->randomise.probability.z)
  {
    RandomiseFunctions::getAdditiveInterval(
        &(position->z),
        position->randomise.min.z,
        position->randomise.max.z,
        position->randomise.variance.z);
  }

};



void RandomiseFunctions::getGlobalInterval(double *value,
    double min, double max)
{
  *value = min + nextDouble() * (max - min);
}

void RandomiseFunctions::getAdditiveInterval(double *value,
    double min, double max, double variance)
{
  Y_DEBUG("old value: %f", *value);
  double add = (2.0 * nextDouble() - 1.0) * variance;
  Y_DEBUG("old value: %f + %f", *value, add);
  *value = *value + add;
  if(*value > max)
  {
    *value = max;
  }
  if(*value < min)
  {
    *value = min;
  }
  Y_DEBUG("new value: %f", *value);
}


void RandomiseFunctions::randomiseGlobalList(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseGlobalList");

  if(RandomiseFunctions::nextDouble() > position->randomise.probability.x)
  {
    Y_DEBUG("RandomiseFunctions::probability check, no change");
    return;
  }
  P3D new_position;

  int size = position->randomise.entries.size();
  int index = (int)(RandomiseFunctions::nextDouble()*size);
  new_position = (position->randomise.entries)[index];
  Y_DEBUG("new_position index %d = %f, %f, %f",
      index, new_position.x, new_position.y, new_position.z);

  position->x = new_position.x;
  position->y = new_position.y;
  position->z = new_position.z;


  position->randomise.last_index = index;

};

void RandomiseFunctions::randomiseAdditiveList(Position *position)
{
  Y_DEBUG("RandomiseFunctions::randomiseAdditiveList");

  if(RandomiseFunctions::nextDouble() > position->randomise.probability.x)
  {
    Y_DEBUG("RandomiseFunctions::probability check, no change");
    return;
  }
  P3D new_position;
  int index = position->randomise.last_index;

  if(index == -1)
  {
    index = (int)(RandomiseFunctions::nextDouble() 
        * position->randomise.entries.size());
  }
  else
  {
    if(nextDouble() < 0.5) 
    {
      Y_DEBUG("RandomiseFunctions::increasing index");
      index += 1;
    }
    else
    {
      Y_DEBUG("RandomiseFunctions::decreasing index");
      index -= 1;
    }

    int size = position->randomise.entries.size();
    int max_index = size - 1;
    if(index < 0)
    {
      Y_DEBUG("RandomiseFunctions::index %d < 0 -> %d", index, max_index);
      index = max_index;
    }
    if(index > max_index)
    {
      Y_DEBUG("RandomiseFunctions::index %d > %d -> 0", index ,max_index);
      index = 0;
    }
  }

  Y_DEBUG("new_position index %d last_index %d",
      index, position->randomise.last_index);
  new_position = (position->randomise.entries)[index];
  Y_DEBUG("new_position index %d = %f, %f, %f",
      index, new_position.x, new_position.y, new_position.z);

  position->x = new_position.x;
  position->y = new_position.y;
  position->z = new_position.z;

  position->randomise.last_index = index;
};


double RandomiseFunctions::nextDouble()
{
  double next = (double)((double)rand()/(double)RAND_MAX);
  Y_DEBUG("RandomiseFunctions::nextDouble() = %f",next);
  return next;
}
