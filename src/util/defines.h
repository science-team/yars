/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
#ifndef __DEFINES_H__
#define __DEFINES_H__

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/glxew.h>

#include <string>
#include <vector>
#include <math.h>
#include <ode/ode.h>
#include <config.h>


#define SIM_FREQUENCY_DEFAULT      100
#define CONTROL_FREQUENCY_DEFAULT   25

#define RAD_TO_DEG(x) (( (double)(x) / (M_PI)) * 180)
#define DEG_TO_RAD(x) (( (double)(x) / 180.0) * (M_PI))

#define MIN(a,b) (((a)>(b))?(b):(a))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define CAMERA_CHANNEL_RGB                           0
#define CAMERA_CHANNEL_GREYSCALE                     1
#define CAMERA_CHANNEL_RED                           2
#define CAMERA_CHANNEL_GREEN                         3
#define CAMERA_CHANNEL_BLUE                          4

#define LIGHT_MODE_REAL                              0
#define LIGHT_MODE_ARTIFICIAL                        1

#define MAX_NAME_LENGTH                              1024

#define DEF_UNAMED                                   ("#UNAMED#")
#define DEF_NAME_SEPERATOR                           (":")
#define DEF_NAME_DELIMITER                           ("/")

#define DEF_RD_CONN_FIX_STRING                       ("FIXED JOINT")
#define DEF_RD_CONN_HINGE_STRING                     ("HINGE JOINT")
#define DEF_RD_CONN_HINGE_2_STRING                   ("HINGE 2 JOINT")
#define DEF_RD_CONN_BALL_STRING                      ("BALL JOINT")
#define DEF_RD_ANGLE_MOTOR_STRING                    ("ANGLE MOTOR")
#define DEF_RD_VEL_MOTOR_STRING                      ("VELOCITY MOTOR")
#define DEF_RD_PASSIVE_JOINT_STRING                  ("PASSIVE JOINT")
#define DEF_RD_CONN_SLIDER_STRING                    ("SLIDER JOINT")
#define DEF_RD_CONN_COMPLEX_HINGE_STRING             ("COMPLEX HINGE")

#define DEF_COMPOUND_ALTERNATIVE_STRING              ("COMPOUND")
#define DEF_SEGMENT_ALTERNATIVE_STRING               ("SEGMENT")

#define DEF_SD_ANGLE_SENSOR_STRING                   ("ANGLE SENSOR")
#define DEF_SD_ANGLE_VEL_SENSOR_STRING               ("ANGLE VELOCITY SENSOR")
#define DEF_SD_IR_SENSOR_STRING                      ("IR SENSOR")
#define DEF_SD_LDR_SENSOR_STRING                     ("LDR SENSOR")
#define DEF_SD_SHARP_GP2D12_37_SENSOR_STRING         ("SHARP GP2D12 37 SENSOR")
#define DEF_SD_COORDINATE_SENSOR_STRING              ("COORDINATE SENSOR")
#define DEF_SD_ANGLE_FEEDBACK_SENSOR_STRING          ("ANGLE FEEDBACK SENSOR")
#define DEF_SD_SLIDER_POSITION_SENSOR_STRING         ("SLIDER POSITION SENSOR")
#define DEF_SD_SLIDER_VEL_SENSOR_STRING              ("SLIDER VELOCITY SENSOR")
#define DEF_SD_AMBIENT_LIGHT_SENSOR_STRING           ("AMBIENT LIGHT SENSOR")
#define DEF_SD_SHARP_DM2Y3A003K0F_SENSOR_STRING      ("SHARP DM2Y3A003K0F SENSOR")
#define DEF_SD_DIRECTED_CAMERA_SENSOR_STRING         ("DIRECTED CAMERA SENSOR")
#define DEF_SD_GEN_ROTATION_SENSOR_STRING            ("GENERIC ROTATION SENSOR")

#define DEF_ABORT_CAUSE_NONE                         0
#define DEF_ABORT_CAUSE_BUMPED                       1
#define DEF_ABORT_CAUSE_JOINT_POSITION_EXCEEDED      2
#define DEF_ABORT_CAUSE_JOINT_FORCE_EXCEEDED         3

#define DEF_DYNAMIC_FRICTION_TYPE_CONSTANT           0
#define DEF_DYNAMIC_FRICTION_TYPE_LINEAR             1
#define DEF_DYNAMIC_FRICTION_TYPE_QUADRATIC          2

#define DEF_FILTER_TYPE_NONE                         0
#define DEF_FILTER_TYPE_MOVING_AVERAGE               1

#define DEF_MAPPING_TYPE_NONE                        0
#define DEF_MAPPING_TYPE_CONSTANT                    1
#define DEF_MAPPING_TYPE_LINEAR                      2
#define DEF_MAPPING_TYPE_QUADRATIC                   3
#define DEF_MAPPING_TYPE_POW                         4
#define DEF_MAPPING_TYPE_SQRT                        5
#define DEF_MAPPING_TYPE_LINEAR_INTERPOLATION        6

#ifdef SUPPRESS_ALL_OUTPUT
#  undef USE_LOG4CPP_OUTPUT
#endif

#ifdef USE_LOG4CPP_OUTPUT 
#  include <util/Logger.h>
#  define LOG4CPP_PRE(level) logger << log4cpp::Priority::level << __FILE__ << ":" << __LINE__ << "  "
// TODO: add Y_LOG4CPP_PRE to all Y_* as a prefix --> figure out how to do it
// without a line break
#  define Y_DEBUG(...)   logger.debug(__VA_ARGS__) 
#  define Y_INFO(...)    logger.info(__VA_ARGS__) 
#  define Y_FATAL(...)   logger.fatal(__VA_ARGS__) 
#  define Y_WARN(...)    logger.warn(__VA_ARGS__) 
#  define Y_MESSAGE(...) logger.message(__VA_ARGS__) 
#else
#  define Y_DEBUG(...)   // not debugging: nothing
#  define Y_INFO(...)    printf(__VA_ARGS__); printf("\n") 
#  define Y_FATAL(...)   printf(__VA_ARGS__); printf("\n");
#  define Y_WARN(...)    printf(__VA_ARGS__); printf("\n")
#  define Y_MESSAGE(...) // not debugging: nothing
#endif

#ifdef SUPPRESS_ALL_OUTPUT
#  undef  Y_INFO
#  define Y_INFO(...)    
#  undef  Y_FATAL
#  define Y_FATAL(...)
#  undef  Y_WARN
#  define Y_WARN(...)    
#endif

//
// structs used as values in the <key,value> pairs of the yarsContainers
//
// 0. Basic Simulation parameters
//
struct SimParams
{
  double stepSize;
};
// 1. SensorData
//
struct SensorData
{
  double raw;
  double mapped;
};
//
// 2. MotorData
//
struct MotorData
{
  double raw;
  double mapped;
};
//
// 3. GeomData
//
struct ContactParameter
{
  int mode;
  double bounce;
  double bounceVel;
  double slip1;
  double slip2;
  double softCFM;
  double softERP;
  double mu;
  double mu2;
};
struct GeomData
{
  std::string      name;                      // name of the geom
  bool             createContactJoints;       // shall contact joints be created
  bool             calcRayNearestContact;     // calculate ray dist to nearest coll. point?
  bool             drawIfCollided;            // shall geom be drawn if collision is true
  bool             logging;                   // shall geom data (position etc.) be logged
  bool             isCollided;                // has collision occured in last timestep
  bool             isCollidedWithOtherSpace;  //  |--> if yes - with other spaces?
  double           rayNearestContactDistance; // ray distance to nearest collision point
  ContactParameter contactParameter;          // Contact Parameters (friction etc., s.a.)
};
//
// 4. AbortCondition
//
struct AbortCondition
{
  bool        abortCondition;
  std::string abortSource;
  int         abortCause;
};;
//
// end yarsContainers structs
//

// interpolation data
struct InterpolationDataPointSet
{
  double x;
  double y;
  double slope;
};

#endif // __DEFINES_H__
