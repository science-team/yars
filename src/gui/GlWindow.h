/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef __GLWINDOW_H__
#define __GLWINDOW_H__

#include <GL/glut.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <GL/gl.h>
#include <math.h>
#include <iostream>

class GlWindow
{

  public:

    GlWindow(GLfloat angle, int width, int height);


    ~GlWindow();

    void setLightMaterialAndPerspective(GLfloat angle);

    void setIdleFunc(void (*func) (void));

    void setDisplayFunc(void (*func) (void));

    void setMotionFunc(void (*func) (int x, int y));

    void setMouseFunc(void (*func) (int w, int x, int y, int z));

    void setKeyFunc(void (*func) (unsigned char key, int x, int y));

    void setReshapeFunc(void (*func) (GLsizei w, GLsizei h));
    
    // makes this camera-window the current one (for rendering to this window...)
    void setActual();

  private:

    int _window;

    int _windowWidth;

    int _windowHeight;

    int _xPix;

    int _yPix;

    GLfloat _openingAngle;

    void *idle_func;

    void *display_func;

    void *mouse_func;

    void *motion_func;

    void *key_func;
    
    void *reshape_func;

    int maxS;

    int maxT;

};

#endif
