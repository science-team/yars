/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
//------------------------------------------------------------------------------
// File : RenderTexture.cpp
//------------------------------------------------------------------------------
// Copyright 2002 Mark J. Harris and
// The University of North Carolina at Chapel Hill
//------------------------------------------------------------------------------
// Permission to use, copy, modify, distribute and sell this software and its 
// documentation for any purpose is hereby granted without fee, provided that 
// the above copyright notice appear in all copies and that both that copyright 
// notice and this permission notice appear in supporting documentation. 
// Binaries may be compiled with this software without any royalties or 
// restrictions. 
//
// The author(s) and The University of North Carolina at Chapel Hill make no 
// representations about the suitability of this software for any purpose. 
// It is provided "as is" without express or implied warranty.
//
// -----------------------------------------------------------------------------
// Credits:
// Original RenderTexture class: Mark J. Harris
// Original Render-to-depth-texture support: Thorsten Scheuermann
// Linux Copy-to-texture: Eric Werness
// Various Bug Fixes: Daniel (Redge) Sperl 
//                    Bill Baxter
//
// -----------------------------------------------------------------------------
/**
 * @file RenderTexture.cpp
 * 
 * Implementation of class RenderTexture.  A multi-format render to 
 * texture wrapper.
 */
#include "RenderTexture.h"



//------------------------------------------------------------------------------
// Function     	  : IsPowerOfTwo
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn IsPowerOfTwo(int n)
 * @brief Returns true if /param n is an integer power of 2.
 * 
 * Taken from Steve Baker's Cute Code Collection.
 */ 
bool isPowerOfTwo(int n)
{
  return ((n&(n-1))==0);
}

//------------------------------------------------------------------------------
// Function      : RenderTexture::RenderTexture
// Description	 : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::RenderTexture()
 * @brief Constructor.
 */ 
RenderTexture::RenderTexture(int iWidth, int iHeight, int imageWidth, int imageHeight, bool bIsTexture /* = true */,
    bool bIsDepthTexture /* = false */)
: _iWidth(iWidth), 
  _iHeight(iHeight), 
  _imageWidth(imageWidth),
  _imageHeight(imageHeight),
  _bIsTexture(bIsTexture),
  _bIsDepthTexture(bIsDepthTexture),
  _bHasArbDepthTexture(true),            // [Redge]
  _eUpdateMode(RT_RENDER_TO_TEXTURE),
  _bInitialized(false),
  _bFloat(false),
  _bRectangle(false),
  _bHasDepth(false),
  _bHasStencil(false),
  _bMipmap(false),
  _bAnisoFilter(false),
#ifdef _WIN32
  _hDC(NULL), 
  _hGLContext(NULL), 
  _hPBuffer(NULL),
  _hPreviousDC(0),
  _hPreviousContext(0),
#else
  _pDpy(NULL),
  _hGLContext(NULL),
  _hPBuffer(0),
  _hPreviousContext(0),
  _hPreviousDrawable(0),
#endif
  _iTextureTarget(GL_NONE),
  _iTextureID(0),
  _pPoorDepthTexture(0) // [Redge]
{
  assert(iWidth > 0 && iHeight > 0);
  _iBits[0] = _iBits[1] = _iBits[2] = _iBits[3] = 0;
  _bRectangle = !(isPowerOfTwo(iWidth) && isPowerOfTwo(iHeight));
  _bufferSize = _iWidth*_iHeight*3;
}


//------------------------------------------------------------------------------
// Function     	  : RenderTexture::~RenderTexture
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::~RenderTexture()
 * @brief Destructor.
 */ 
RenderTexture::~RenderTexture()
{
  _invalidate();
}


//------------------------------------------------------------------------------
// Function     	  : wglGetLastError
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn wglGetLastError()
 * @brief Returns the last windows error generated.
 */ 
//------------------------------------------------------------------------------
// Function     	  : printExtensionError
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn printExtensionError( char* strMsg, ... )
 * @brief Prints an error about missing OpenGL extensions.
 */ 
void printExtensionError( char* strMsg, ... )
{
  fprintf(stderr, "Error: RenderTexture requires the following unsupported "
      "OpenGL extensions: \n");
  char strBuffer[512];
  va_list args;
  va_start(args, strMsg);
   vsnprintf( strBuffer, 512, strMsg, args );
  va_end(args);

  fprintf(stderr, strMsg);

  exit(1);
}


//------------------------------------------------------------------------------
// Function     	  : RenderTexture::Initialize
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::Initialize(bool bShare, bool bDepth, bool bStencil, bool bMipmap, bool bAnisoFilter, unsigned int iRBits, unsigned int iGBits, unsigned int iBBits, unsigned int iABits);
 * @brief Initializes the RenderTexture, sharing display lists and textures if specified.
 * 
 * This function actually does the creation of the p-buffer.  It can only be called 
 * once a GL context has already been created.  Note that if the texture is not
 * power of two dimensioned, or has more than 8 bits per channel, enabling mipmapping
 * or aniso filtering will cause an error.
 */ 
bool RenderTexture::initialize(bool         bShare       /* = true */, 
    bool         bDepth       /* = false */, 
    bool         bStencil     /* = false */, 
    bool         bMipmap      /* = false */, 
    bool         bAnisoFilter /* = false */,
    unsigned int iRBits       /* = 8 */, 
    unsigned int iGBits       /* = 8 */, 
    unsigned int iBBits       /* = 8 */, 
    unsigned int iABits       /* = 8 */,
    UpdateMode   updateMode   /* = RT_RENDER_TO_TEXTURE */)
{
 
  if (!GLXEW_SGIX_pbuffer)
  {
    printExtensionError("GLX_SGIX_pbuffer");
    return false;
  }
  if (!GLXEW_SGIX_fbconfig)
  {
    printExtensionError("GLX_SGIX_fbconfig");
    return false;
  }
  if (_bIsDepthTexture)
  {
    printExtensionError("I don't know");
    return false;
  }
  if (updateMode == RT_RENDER_TO_TEXTURE)
  {
    printExtensionError("Some GLX render texture extension");
  }

  if (_bInitialized)
    _invalidate();

  _bFloat = (iRBits > 8 || iGBits > 8 || iBBits > 8 || iABits > 8);

  bool bNVIDIA = true;
 
  if (_bFloat && _bIsTexture && !GLXEW_NV_float_buffer)
  {
    printExtensionError("GLX_NV_float_buffer");
    return false;
  }
  if (!_bFloat && !GLEW_NV_texture_rectangle)
  {
    bNVIDIA = false;
  }

  _bRectangle = _bRectangle || (_bFloat && bNVIDIA);

    _bHasDepth  = bDepth;

  _bHasStencil  = bStencil;
  _bMipmap      = false;   // until it is enabled.
  _bAnisoFilter = false;   // until it is enabled.
  _eUpdateMode  = updateMode;

  GLuint iWGLTextureTarget = 0;
  GLuint iBindTarget = 0;
  GLuint iTextureFormat = 0;
  GLuint iDepthBindTarget = 0;
  GLuint iDepthTextureFormat = 0;

  if (_bIsTexture)
  {
    glGenTextures(1, &_iTextureID);
  }
  if (_bIsDepthTexture)
  {
    glGenTextures(1, &_iDepthTextureID);
  }
  // Determine the appropriate texture formats and filtering modes.
  if (_bIsTexture)
  {
    if (_bFloat)
    {      
      if (!bNVIDIA && _bRectangle)
      {
        fprintf(stderr, 
            "RenderTexture Error: ATI textures must be power-of-two-dimensioned.\n");
        return false;
      }

      if (bNVIDIA)
      {
        _iTextureTarget = GL_TEXTURE_RECTANGLE_NV;
      }
      else
      {
        _iTextureTarget = GL_TEXTURE_2D;
      }
      glBindTexture(_iTextureTarget, _iTextureID);  

      // We'll use clamp to edge as the default texture wrap mode for all tex types
      glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
      glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//       glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//       glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//       if (bMipmap)
//       {
// 
//         fprintf(stderr, 
//             "RenderTexture Error: float textures do not support mipmaps\n");
//         return false;
//       }

      if (RT_COPY_TO_TEXTURE == _eUpdateMode)
      {
        GLuint iInternalFormat;
        GLuint iFormat;
        if (iABits > 0)
        {
          if (bNVIDIA)
            iInternalFormat = (iABits > 16) ? GL_FLOAT_RGBA32_NV : GL_FLOAT_RGBA16_NV;
          else
            iInternalFormat = (iABits > 16) ? GL_RGBA_FLOAT32_ATI : GL_RGBA_FLOAT16_ATI;
          iFormat = GL_RGBA;
        }
        else if (iBBits > 0)
        {
          if (bNVIDIA)
            iInternalFormat = (iBBits > 16) ? GL_FLOAT_RGB32_NV : GL_FLOAT_RGB16_NV;
          else
            iInternalFormat = (iBBits > 16) ? GL_RGB_FLOAT32_ATI : GL_RGB_FLOAT16_ATI;
          iFormat = GL_RGB;
        }
        else if (iGBits > 0)
        {
          if (bNVIDIA)
            iInternalFormat = (iGBits > 16) ? GL_FLOAT_RG32_NV : GL_FLOAT_RG16_NV;
          else
            iInternalFormat = (iGBits > 16) ? GL_LUMINANCE_ALPHA_FLOAT32_ATI : 
              GL_LUMINANCE_ALPHA_FLOAT16_ATI;
          iFormat = GL_LUMINANCE_ALPHA;
        }
        else 
        {
          if (bNVIDIA)
            iInternalFormat = (iRBits > 16) ? GL_FLOAT_R32_NV : GL_FLOAT_R16_NV;
          else
            iInternalFormat = (iRBits > 16) ? GL_LUMINANCE_FLOAT32_ATI : 
              GL_LUMINANCE_FLOAT16_ATI;
          iFormat = GL_LUMINANCE;
        }
        // Allocate the texture image (but pass it no data for now).
        glTexImage2D(_iTextureTarget, 0, iInternalFormat, _iWidth, _iHeight, 
            0, iFormat, GL_FLOAT, NULL);
      }

    }
    else
    {
      if (!_bRectangle)
      {
        _iTextureTarget = GL_TEXTURE_2D;
        glBindTexture(_iTextureTarget, _iTextureID);

        // We'll use clamp to edge as the default texture wrap mode for all tex types
        glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
        glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (bMipmap)
        {
          _bMipmap = true;
          glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

          // Generate mipmap automatically if supported
          if (GLEW_SGIS_generate_mipmap)
          {
            glTexParameteri( _iTextureTarget, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
          }
          else
          {
            printExtensionError("GL_SGIS_generate_mipmap");
          }
        }
        else
        {
          glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }

        // Set anisotropic filter to the max ratio
        if (bAnisoFilter)
        {
          if (GLEW_EXT_texture_filter_anisotropic)
          {
            _bAnisoFilter = true;
            float rMaxAniso;
            glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &rMaxAniso);
            glTexParameterf( _iTextureTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, rMaxAniso);
          }
        }

        if (RT_COPY_TO_TEXTURE == _eUpdateMode)
        {
          GLuint iInternalFormat;
          GLuint iFormat;
          if (iABits > 0)
          {
            iInternalFormat = GL_RGBA8;
            iFormat = GL_RGBA;
          }
          else 
          {
            iInternalFormat = GL_RGB8;
            iFormat = GL_RGB;
          }
          // Allocate the texture image (but pass it no data for now).
          glTexImage2D(_iTextureTarget, 0, iInternalFormat, _iWidth, _iHeight, 
              0, iFormat, GL_FLOAT, NULL);
        }

      } 
      else
      {
        if (!bNVIDIA)
        {
          fprintf(stderr, 
              "RenderTexture Error: ATI textures must be power-of-two-dimensioned.\n");
          return false;
        }

        _iTextureTarget = GL_TEXTURE_RECTANGLE_NV;

        glBindTexture(_iTextureTarget, _iTextureID);

        glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
      glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//         glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//         glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (bMipmap) 
        { 

          fprintf(stderr, 
              "RenderTexture Error: rectangle textures do not support mipmaps\n"); 
          return false; 
        } 

        if (RT_COPY_TO_TEXTURE == _eUpdateMode)
        {
          GLuint iInternalFormat;
          GLuint iFormat;
          if (iABits > 0)
          {
            iInternalFormat = GL_RGBA8;
            iFormat = GL_RGBA;
          }
          else 
          {
            iInternalFormat = GL_RGB8;
            iFormat = GL_RGB;
          }
          // Allocate the texture image (but pass it no data for now).
          glTexImage2D(_iTextureTarget, 0, iInternalFormat, _iWidth, _iHeight, 
              0, iFormat, GL_FLOAT, NULL);
        }

      } 
    }
  }

//   if (_bIsDepthTexture)
//   {
// 
//     if (!bNVIDIA && _bRectangle)
//     {
//       fprintf(stderr, 
//           "RenderTexture Error: ATI textures must be power-of-two-dimensioned.\n");
//       return false;
//     }
// 
//     if (!_iTextureTarget)
//       _iTextureTarget = _bRectangle ? GL_TEXTURE_RECTANGLE_NV : GL_TEXTURE_2D;
// 
//     glBindTexture(_iTextureTarget, _iDepthTextureID);  
// 
//     // We'll use clamp to edge as the default texture wrap mode for all tex types
//     glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//     glTexParameteri( _iTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
//       glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//       glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
// //     glTexParameteri( _iTextureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
// //     glTexParameteri( _iTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);;
// 
//     // user will need to set up hardware shadow mapping himself (using ARB_shadow)
// 
//     if (RT_COPY_TO_TEXTURE == _eUpdateMode)
//     {
//       // [Redge]
//       if (_bHasArbDepthTexture) 
//       {
//         // Allocate the texture image (but pass it no data for now).
//         glTexImage2D(_iTextureTarget, 0, GL_DEPTH_COMPONENT, _iWidth, _iHeight, 
//             0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
//       } 
//       else 
//       {
//         // allocate memory for depth texture
//         // Since this is slow, we warn the user in debug mode. (above)
//         _pPoorDepthTexture = new unsigned short[_iWidth * _iHeight];
//         glTexImage2D(_iTextureTarget, 0, GL_LUMINANCE16, _iWidth, _iHeight, 
//             0, GL_LUMINANCE, GL_UNSIGNED_SHORT, _pPoorDepthTexture);
//       }
//       // [/Redge]
//     }
//     
//   }

  _pDpy = glXGetCurrentDisplay();
  GLXContext context = glXGetCurrentContext();
  int screen = DefaultScreen(_pDpy);
  XVisualInfo *visInfo;

  int iFormat = 0;
  int iNumFormats;
  int fbAttribs[50];
  int attrib = 0;

  fbAttribs[attrib++] = GLX_RENDER_TYPE_SGIX;
  fbAttribs[attrib++] = GLX_RGBA_BIT_SGIX;
  fbAttribs[attrib++] = GLX_DRAWABLE_TYPE_SGIX;
  fbAttribs[attrib++] = GLX_PBUFFER_BIT_SGIX;
  fbAttribs[attrib++] = GLX_STENCIL_SIZE;
  fbAttribs[attrib++] = (bStencil) ? 8 : 0;
  fbAttribs[attrib++] = GLX_DEPTH_SIZE;
  fbAttribs[attrib++] = (bDepth) ? 24 : 0;
  if (_bFloat)
  {
    fbAttribs[attrib++] = GLX_RED_SIZE;
    fbAttribs[attrib++] = iRBits;
    fbAttribs[attrib++] = GLX_GREEN_SIZE;
    fbAttribs[attrib++] = iGBits;
    fbAttribs[attrib++] = GLX_BLUE_SIZE;
    fbAttribs[attrib++] = iBBits;
    fbAttribs[attrib++] = GLX_ALPHA_SIZE;
    fbAttribs[attrib++] = iABits;
    fbAttribs[attrib++] = GLX_FLOAT_COMPONENTS_NV;
    fbAttribs[attrib++] = 1;    
  }
  fbAttribs[attrib++] = None;

  GLXFBConfigSGIX *fbConfigs;
  int nConfigs;

  fbConfigs = glXChooseFBConfigSGIX(_pDpy, screen, fbAttribs, &nConfigs);

  if (nConfigs == 0 || !fbConfigs) {
    fprintf(stderr,
        "RenderTexture::Initialize() creation error: Couldn't find a suitable pixel format\n");
    return false;
  }

  // Pick the first returned format that will return a pbuffer
  for (int i=0;i<nConfigs;i++)
  {
    _hPBuffer = glXCreateGLXPbufferSGIX(_pDpy, fbConfigs[i], _iWidth, _iHeight, NULL);
    if (_hPBuffer) {
      _hGLContext = glXCreateContextWithConfigSGIX(_pDpy, fbConfigs[i], GLX_RGBA_TYPE, bShare?context:NULL, True);
      break;
    }
  }

  if (!_hPBuffer)
  {
    fprintf(stderr, "RenderTexture::Initialize() pbuffer creation error: glXCreateGLXPbufferSGIX() failed\n");
    return false;
  }

  if(!_hGLContext)
  {
    // Try indirect
    _hGLContext = glXCreateContext(_pDpy, visInfo, bShare?context:NULL, False);
    if ( !_hGLContext )
    {
      fprintf(stderr, "RenderTexture::Initialize() creation error:  glXCreateContext() failed\n");
      return false;
    }
  }

  glXQueryGLXPbufferSGIX(_pDpy, _hPBuffer, GLX_WIDTH_SGIX, (GLuint*)&_iWidth);
  glXQueryGLXPbufferSGIX(_pDpy, _hPBuffer, GLX_HEIGHT_SGIX, (GLuint*)&_iHeight);

  _bInitialized = true;

  // XXX Query the color format

  // #endif

  return true;
}

//------------------------------------------------------------------------------
// Function     	  : RenderTexture::_Invalidate
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::_Invalidate()
 * @brief Returns the pbuffer memory to the graphics device.
 * 
 */ 
bool RenderTexture::_invalidate()
{
  _bFloat       = false;
  _bRectangle   = false;
  _bInitialized = false;
  _bHasDepth    = false;
  _bHasStencil  = false;
  _bMipmap      = false;
  _bAnisoFilter = false;
  _iBits[0] = _iBits[1] = _iBits[2] = _iBits[3] = 0;

  if (_bIsTexture)
    glDeleteTextures(1, &_iTextureID);
  if (_bIsDepthTexture) {
    // [Redge]
    if (!_bHasArbDepthTexture) delete[] _pPoorDepthTexture;
    // [/Redge]
    glDeleteTextures(1, &_iDepthTextureID);
  }

  if ( _hPBuffer )
  {
    if(glXGetCurrentContext() == _hGLContext)
      // XXX I don't know if this is right at all
      glXMakeCurrent(_pDpy, _hPBuffer, 0);
    glXDestroyGLXPbufferSGIX(_pDpy, _hPBuffer);
    _hPBuffer = 0;
    return true;
  }
  //#endif
  return false;
}


//------------------------------------------------------------------------------
// Function     	  : RenderTexture::BeginCapture
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::BeginCapture()
 * @brief Activates rendering to the RenderTexture.
 */ 
bool RenderTexture::beginCapture()
{
  if (!_bInitialized)
  {
    fprintf(stderr, "RenderTexture::BeginCapture(): Texture is not initialized!\n");
    exit(1);
    return false;
  }

  _hPreviousContext = glXGetCurrentContext();
  _hPreviousDrawable = glXGetCurrentDrawable();

  if (False == glXMakeCurrent(_pDpy, _hPBuffer, _hGLContext)) {
    return false;
  }
  //#endif

  return true;
}


//------------------------------------------------------------------------------
// Function     	  : RenderTexture::EndCapture
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::EndCapture()
 * @brief Ends rendering to the RenderTexture.
 */ 
bool RenderTexture::endCapture()
{
  bool bContextReset = false;

  if (!_bInitialized)
  {
    fprintf(stderr, "RenderTexture::EndCapture(): Texture is not initialized!\n");
    return false;
  }
assert(_bIsTexture);
glBindTexture(_iTextureTarget, _iTextureID);
  GLubyte pBuf[_bufferSize];
 if(_colorType == GREYSCALE_TYPE)
 {
   // glCopyTexSubImage2D(_iTextureTarget, 0, 0, 0, 0, 0, _iWidth, _iHeight);
   glCopyTexImage2D(_iTextureTarget, 0, GL_LUMINANCE, 0, 0, _iWidth, _iHeight, 0);
 
// // save image in Buffer, for giving back the Pixel-values
 
   pixelBuffer = pBuf;
   glGetTexImage(_iTextureTarget, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, pBuf);
 }
 else
 { 
// glCopyTexSubImage2D(_iTextureTarget, 0, 0, 0, 0, 0, _iWidth, _iHeight);
glCopyTexImage2D(_iTextureTarget, 0, GL_RGB, 0, 0, _iWidth, _iHeight, 0);

// save image in Buffer, for giving back the Pixel-values
pixelBuffer = pBuf;
glGetTexImage(_iTextureTarget, 0, GL_RGB, GL_UNSIGNED_BYTE, pBuf);
 }
_texels.resize(_bufferSize);
for(int k=0; k<_bufferSize; k++)
{
  _texels[k] = (double)pBuf[k];
}
if(!bContextReset)
{
  if (False == glXMakeCurrent(_pDpy, _hPreviousDrawable, _hPreviousContext))
  {
    return false;
  }
}
return true;
}



//------------------------------------------------------------------------------
// Function     	  : RenderTexture::Bind
// Description	    : 
//------------------------------------------------------------------------------
/**
 * @fn RenderTexture::Bind()
 * @brief Binds RGB texture.
 */ 
void RenderTexture::bind() const 
{ 
  if (_bInitialized) {
    glBindTexture(_iTextureTarget, _iTextureID);   
  }    
}


vector<double> RenderTexture::getTexels()
{

  return _texels;
}


void RenderTexture::setColorType(int type)
{
  _colorType = type;
}
