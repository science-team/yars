/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <gui/GlWindow.h>

#include <iostream>

GlWindow::GlWindow(GLfloat angle, int width, int height)
{  
  glutInitDisplayMode(GLUT_RGB| GLUT_DEPTH | GLUT_DOUBLE);
  _window = 0;

  maxS = 1;
  maxT = 1;
  _openingAngle = angle;

  _windowWidth = width;
  _windowHeight = height;
  glutInitWindowPosition(0,0);
  glutInitWindowSize(_windowWidth, _windowHeight);

  _window = glutCreateWindow("YARS-Simulation");
  setLightMaterialAndPerspective(_openingAngle);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);
  // just changed for the star-sky
  //   glClearColor(0.5,0.7,1,0.5);
  //   glClearColor(0,0,0,0);
  glViewport(0, 0, _windowWidth,_windowHeight);
}


GlWindow::~GlWindow()
{
  glutDestroyWindow(_window);  
}

void GlWindow::setLightMaterialAndPerspective(GLfloat angle)
{
  GLfloat pos[4] = {0., 0., 10., 1.};
  GLfloat white[4] = {1., 1., 1., 1.};
  GLfloat black[4] = {0., 0., 0., 0.};
  GLfloat LSpotCutOff = 180.0F; 
  /* Set up light1 */
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT1);
  glLightfv (GL_LIGHT1, GL_POSITION, pos);
  glLightfv (GL_LIGHT1, GL_DIFFUSE, white);
  glLightfv (GL_LIGHT1, GL_SPECULAR, black);
  
  GLfloat LModelAmbient[4] ={0.5,0.5,0.5,1.0F};
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,&LModelAmbient[0]);//Umgebungslichtwert
  
  glEnable (GL_COLOR_MATERIAL);
  glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glMaterialfv (GL_FRONT, GL_SPECULAR, black);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
}

void GlWindow::setIdleFunc(void (*func) (void))
{
  idle_func = (void *)func;
  glutIdleFunc(func);
}

void GlWindow::setDisplayFunc(void (*func) (void))
{
  display_func = (void *)func;
  glutDisplayFunc(func);
}

void GlWindow::setMouseFunc(void (*func) (int w, int x, int y, int z))
{
  mouse_func = (void *)func;
  glutMouseFunc(func);
}

void GlWindow::setMotionFunc(void (*func) (int x, int y))
{
  motion_func = (void *)func;
  glutMotionFunc(func);
}

void GlWindow::setKeyFunc(void (*func) (unsigned char key, int x, int y))
{
  key_func = (void *)func;
  glutKeyboardFunc(func);
}

void GlWindow::setReshapeFunc(void (*func) (GLsizei w, GLsizei h))
{
  reshape_func = (void*)func;
  glutReshapeFunc(func);
}

void GlWindow::setActual()
{
  glutSetWindow(_window);
}
