/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef _DRAWMANAGER_H_
#define _DRAWMANAGER_H_

#include <gui/DrawObject.h>

#include <vector>
#include <list>
#include <iterator>

#include <description/DescriptionHelper.h>

#include <ode/ode.h>

/*      start       position of the robot */
#define START_POS_X 0.0
#define START_POS_Y 0.0
#define START_POS_Z 0.0

#define DEFAULT_PATH_TO_TEXTURES "../textures/"

using namespace std;

class DrawManager
{
  public:
    struct BMPImage 
    {
      int   width;
      int   height;
      char *data;
    };

    DrawManager(string pathToTextures, bool useTraces = false);
    ~DrawManager();

    //! set the current viewpoint
    void setViewPoint(float xyz[3]);

    //!add a geom to the list of drawable geoms, with default color
    void addGeom(dGeomID geom);
    void addGeomLast(dGeomID geom);

    //!add a geom to the list of drawable geoms, with specific color
    void addGeom(dGeomID geom, const float red, const float green,
        const float blue);

    //!remove a geom from the list
    void removeGeom(dGeomID geom);

    //!set the default color, alpha is set to 1 (non-transparent)
    void setDefaultColor(const float red, const float green,
        const float blue);

    //!set the default color
    void setDefaultColor(const float red, const float green,
        const float blue, const float alpha);

    //!change the color of an specific object, which is in the list, alpha is set
    // to  1 (non-transparent)
    void changeColor(dGeomID geom, const float red, const float green,
        const float blue);

    //!change the color of an specific object, which is in the list
    void changeColor(dGeomID geom, const float red, const float green,
        const float blue, const float alpha);

    
    void setTransform (const dReal pos[3], const dReal R[12]);

    
    //!draw all geoms in the list
    void draw();

    //! draw ground and "orientation"-lines
    void drawWorld();

    // add trace geom
    void addTraceGeom(dGeomID geom, const float red, const float green,
        const float blue);

    void addTraceLineGeom(P3DColor pos);

    // clear all trace geom
    void clearTraceGeoms();

    void setNumberOfDrawLines(int nr);

    // if camera view is rendered, no "orientation" lines and sensor-ray are drawn
    bool _isCameraView;

    void setTextures(bool useTextures);
    
    void setTraces( bool useTraces);

    void setUpTextures();

    void getBitmapImageData( char *pFileName, BMPImage *pImage );

    void setAxesVisualization(bool visualize, double scaling);

  private:



    struct Color
    {
      float red;
      float green;
      float blue;
      float alpha;
    };

    struct Item
    {
      dGeomID geom;
      Color color;
    };

    void drawGeom(dGeomID geom, const dReal *pos, const dReal *rot,
        const float *red, const float *green, const float *blue, const
        float *alpha);

    Color                _defaultColor;
    list<Item>           _items;
    list<Item>::iterator _itemsIterator;
    vector<Item>         _trace_items;
    vector<P3DColor>     _trace_line;
    Color                _trace_line_color;
    int                  _numberOfDrawLines;
    bool                 _useTextures;
    bool                 _drawAxes;
    bool                 _useTraces;
    double               _axesScaling;

    DrawObject *_drawHelper;
    
    GLuint ground_texture;

    GLuint sky_texture;

    BMPImage groundTextureImage;

    BMPImage skyTextureImage;

    string _pathToTextures;
    

};

#endif
