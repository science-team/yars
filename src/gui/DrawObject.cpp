/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <gui/DrawObject.h>


DrawObject::DrawObject()
{
  _capped_Cylinder_Quality = 3;
  _sphere_quality = 1;

  quadSphere = gluNewQuadric();

  matrix = new GLfloat[16];
}

DrawObject::~DrawObject()
{
  delete[] matrix;
  gluDeleteQuadric(quadSphere);
}

void DrawObject::drawBox (
    const dReal sides[3])
{
  glShadeModel (GL_SMOOTH);

  float lx = sides[0]*0.5f;
  float ly = sides[1]*0.5f;
  float lz = sides[2]*0.5f;
  // sides
  glBegin (GL_TRIANGLE_STRIP);
  glNormal3f (-1,0,0);
  glVertex3f (-lx,-ly,-lz);
  glVertex3f (-lx,-ly,lz);
  glVertex3f (-lx,ly,-lz);
  glVertex3f (-lx,ly,lz);
  glNormal3f (0,1,0);
  glVertex3f (lx,ly,-lz);
  glVertex3f (lx,ly,lz);
  glNormal3f (1,0,0);
  glVertex3f (lx,-ly,-lz);
  glVertex3f (lx,-ly,lz);
  glNormal3f (0,-1,0);
  glVertex3f (-lx,-ly,-lz);
  glVertex3f (-lx,-ly,lz);
  glEnd();
  // top face
  glBegin (GL_TRIANGLE_FAN);
  glNormal3f (0,0,1);
  glVertex3f (-lx,-ly,lz);
  glVertex3f (lx,-ly,lz);
  glVertex3f (lx,ly,lz);
  glVertex3f (-lx,ly,lz);
  glEnd();
  // bottom face
  glBegin (GL_TRIANGLE_FAN);
  glNormal3f (0,0,-1);
  glVertex3f (-lx,-ly,-lz);
  glVertex3f (-lx,ly,-lz);
  glVertex3f (lx,ly,-lz);
  glVertex3f (lx,-ly,-lz);
  glEnd();

  glPopMatrix();
}


void DrawObject::drawCappedCylinder (
    float length, float radius)
{
  int i,j;
  float tmp,nx,ny,nz,start_nx,start_ny,a,ca,sa;
  // number of sides to the cylinder (divisible by 4):
  const int n = _capped_Cylinder_Quality*4;

  length *= 0.5;
  a = float(M_PI*2.0)/float(n);
  sa = (float) sin(a);
  ca = (float) cos(a);

  // draw cylinder body
  ny=1; nz=0;		  // normal vector = (0,ny,nz)
  glBegin (GL_TRIANGLE_STRIP);
  for (i=0; i<=n; i++) 
  {
    glNormal3d (ny,nz,0);
    glVertex3d (ny*radius,nz*radius,length);
    glNormal3d (ny,nz,0);
    glVertex3d (ny*radius,nz*radius,-length);
    // rotate ny,nz
    tmp = ca*ny - sa*nz;
    nz = sa*ny + ca*nz;
    ny = tmp;
  }
  glEnd();

  // draw first cylinder cap
  start_nx = 0;
  start_ny = 1;
  for (j=0; j<(n/4); j++) 
  {
    // get start_n2 = rotated start_n
    float start_nx2 =  ca*start_nx + sa*start_ny;
    float start_ny2 = -sa*start_nx + ca*start_ny;
    // get n=start_n and n2=start_n2
    nx = start_nx; ny = start_ny; nz = 0;
    float nx2 = start_nx2, ny2 = start_ny2, nz2 = 0;
    glBegin (GL_TRIANGLE_STRIP);
    for (i=0; i<=n; i++) 
    {
      glNormal3d (ny2,nz2,nx2);
      glVertex3d (ny2*radius,nz2*radius,length+nx2*radius);
      glNormal3d (ny,nz,nx);
      glVertex3d (ny*radius,nz*radius,length+nx*radius);
      // rotate n,n2
      tmp = ca*ny - sa*nz;
      nz = sa*ny + ca*nz;
      ny = tmp;
      tmp = ca*ny2- sa*nz2;
      nz2 = sa*ny2 + ca*nz2;
      ny2 = tmp;
    }
    glEnd();
    start_nx = start_nx2;
    start_ny = start_ny2;
  }

  // draw second cylinder cap
  start_nx = 0;
  start_ny = 1;
  for (j=0; j<(n/4); j++) 
  {
    // get start_n2 = rotated start_n
    float start_nx2 = ca*start_nx - sa*start_ny;
    float start_ny2 = sa*start_nx + ca*start_ny;
    // get n=start_n and n2=start_n2
    nx = start_nx; ny = start_ny; nz = 0;
    float nx2 = start_nx2, ny2 = start_ny2, nz2 = 0;
    glBegin (GL_TRIANGLE_STRIP);
    for (i=0; i<=n; i++) 
    {
      glNormal3d (ny,nz,nx);
      glVertex3d (ny*radius,nz*radius,-length+nx*radius);
      glNormal3d (ny2,nz2,nx2);
      glVertex3d (ny2*radius,nz2*radius,-length+nx2*radius);
      // rotate n,n2
      tmp = ca*ny - sa*nz;
      nz = sa*ny + ca*nz;
      ny = tmp;
      tmp = ca*ny2- sa*nz2;
      nz2 = sa*ny2 + ca*nz2;
      ny2 = tmp;
    }
    glEnd();
    start_nx = start_nx2;
    start_ny = start_ny2;
  }
  glPopMatrix();

}

void DrawObject::drawSphere(
    float radius)
{
  glShadeModel (GL_SMOOTH);
  gluSphere(quadSphere, radius, 24, 24);
  glPopMatrix();
  glDisable (GL_NORMALIZE);


}

void DrawObject::drawCylinder (
    float length, float radius)
{
  
  glShadeModel (GL_SMOOTH);
  int i;
  float tmp,ny,nz,a,ca,sa;
  const int n = 24;	// number of sides to the cylinder (divisible by 4)

  length *= 0.5;
  a = float(M_PI*2.0)/float(n);
  sa = (float) sin(a);
  ca = (float) cos(a);

  // draw cylinder body
  ny=1; nz=0;		  // normal vector = (0,ny,nz)
  glBegin (GL_TRIANGLE_STRIP);
  for (i=0; i<=n; i++) 
  {
    glNormal3d (ny,nz,0);
    glVertex3d (ny*radius,nz*radius,length);
    glNormal3d (ny,nz,0);
    glVertex3d (ny*radius,nz*radius,-length);
    // rotate ny,nz
    tmp = ca*ny - sa*nz;
    nz = sa*ny + ca*nz;
    ny = tmp;
  }
  glEnd();

  // draw top cap
  glShadeModel (GL_FLAT);
  ny=1; nz=0;		  // normal vector = (0,ny,nz)
  glBegin (GL_TRIANGLE_FAN);
  glNormal3d (0,0,1);
  glVertex3d (0,0,length);
  for (i=0; i<=n; i++) 
  {
    if (i==1 || i==n/2+1)
    {
      setColor(color[0]*0.75f,color[1]*0.75f,color[2]*0.75f,color[3]);
    }
    glNormal3d (0,0,1);
    glVertex3d (ny*radius,nz*radius,length);
    if (i==1 || i==n/2+1)
    {
      setColor(color[0],color[1],color[2],color[3]);
    }
    // rotate ny,nz
    tmp = ca*ny - sa*nz;
    nz = sa*ny + ca*nz;
    ny = tmp;
  }
  glEnd();

  // draw bottom cap
  ny=1; nz=0;
  glBegin (GL_TRIANGLE_FAN);
  glNormal3d (0,0,-1);
  glVertex3d (0,0,-length);
  for (i=0; i<=n; i++) 
  {
    if (i==1 || i==n/2+1)
    {
      setColor(color[0]*0.75f,color[1]*0.75f,color[2]*0.75f,color[3]);
    }
    glNormal3d (0,0,-1);
    glVertex3d (ny*radius,nz*radius,-length);
    if (i==1 || i==n/2+1)
    {
      setColor(color[0],color[1],color[2],color[3]);
    }
    // rotate ny,nz
    tmp = ca*ny + sa*nz;
    nz = -sa*ny + ca*nz;
    ny = tmp;
  }
  glEnd();  
  glPopMatrix();

}

void DrawObject::drawLine (const float pos1[3], const float pos2[3], bool isCameraView)
{
  if(isCameraView==false)
  {
    glDisable (GL_LIGHTING);
    glLineWidth (2);
    glShadeModel (GL_FLAT);
    glBegin (GL_LINES);
    glVertex3f (pos1[0],pos1[1],pos1[2]);
    glVertex3f (pos2[0],pos2[1],pos2[2]);
    glEnd();
    glEnable(GL_LIGHTING);
  }

}

void DrawObject::setColor(float red, float green, float blue, float alpha)
{
  color[0] = red;
  color[1] = green;
  color[2] = blue;
  color[3] = alpha;
  glColor4f(red, green , blue , alpha);
  if (color[3] < 1) 
  {
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  }
  else 
  {
    glDisable (GL_BLEND);
  }
}

void DrawObject::drawGround()
{
  glBegin (GL_QUADS);
  glNormal3f (0,0,1);
  glTexCoord2f (-FLOOR_SIZE*SCALE_GROUND + OFFSET_X_FLOOR,
                 -FLOOR_SIZE*SCALE_GROUND + OFFSET_Y_FLOOR);
  glVertex3f (-FLOOR_SIZE,-FLOOR_SIZE,DO_OFFSET);
  glTexCoord2f (FLOOR_SIZE*SCALE_GROUND + OFFSET_X_FLOOR,
                -FLOOR_SIZE*SCALE_GROUND + OFFSET_Y_FLOOR);
  glVertex3f (FLOOR_SIZE,-FLOOR_SIZE,DO_OFFSET);
  glTexCoord2f (FLOOR_SIZE*SCALE_GROUND + OFFSET_X_FLOOR,
                FLOOR_SIZE*SCALE_GROUND + OFFSET_Y_FLOOR);
  glVertex3f (FLOOR_SIZE,FLOOR_SIZE,DO_OFFSET);
  glTexCoord2f (-FLOOR_SIZE*SCALE_GROUND + OFFSET_X_FLOOR,
                 FLOOR_SIZE*SCALE_GROUND + OFFSET_Y_FLOOR);
  glVertex3f (-FLOOR_SIZE,FLOOR_SIZE,DO_OFFSET);
  glEnd();

}

void DrawObject::setViewPoint(float xyz[3]){
  view_xyz[0] = xyz[0];
  view_xyz[1] = xyz[1];
  view_xyz[2] = xyz[2];
}

void DrawObject::drawSky()
{
   //height, where the sky is drawn
  const float sky_height = 15.0f;
  const float sky_scale = 5;	// sky texture scale
  static float offset = 0.0f;

  float x = SCALE_SKY;

  float z = view_xyz[2] + HEIGHT_SKY;
  if(view_xyz[2] < 0)
  {
    z = HEIGHT_SKY;
  }

  glBegin (GL_QUADS);
  glNormal3f (0,0,-1);
  glTexCoord2f (-x+offset,-x+offset);
  glVertex3f (-SKY_SIZE+view_xyz[0],-SKY_SIZE+view_xyz[1],z);
  glTexCoord2f (-x+offset,x+offset);
  glVertex3f (-SKY_SIZE+view_xyz[0],SKY_SIZE+view_xyz[1],z);
  glTexCoord2f (x+offset,x+offset);
  glVertex3f (SKY_SIZE+view_xyz[0],SKY_SIZE+view_xyz[1],z);
  glTexCoord2f (x+offset,-x+offset);
  glVertex3f (SKY_SIZE+view_xyz[0],-SKY_SIZE+view_xyz[1],z);
  glEnd();


  //Tempo of the sky
  offset = offset + 0.0005f;
  if (offset > 1) offset -= 1;
}

void DrawObject::drawSphereShadow (float px, float py, float pz, float radius)
{
//   // calculate shadow constants based on light vector
//   static int init=0;
//   static float len2,len1,scale;
//   if (!init) {
//     len2 = LIGHTX*LIGHTX + LIGHTY*LIGHTY;
//     len1 = 1.0f/(float)sqrt(len2);
//     scale = (float) sqrt(len2 + 1);
//     init = 1;
//   }
// 
//   // map sphere center to ground plane based on light vector
//   px -= LIGHTX*pz;
//   py -= LIGHTY*pz;
// 
//   const float kx = 0.96592582628907f;
//   const float ky = 0.25881904510252f;
//   float x=radius, y=0;
// 
//   glBegin (GL_TRIANGLE_FAN);
//   for (int i=0; i<24; i++) {
//     // for all points on circle, scale to elongated rotated shadow and draw
//     float x2 = (LIGHTX*x*scale - LIGHTY*y)*len1 + px;
//     float y2 = (LIGHTY*x*scale + LIGHTX*y)*len1 + py;
//     glTexCoord2f (x2*ground_scale+ground_ofsx,y2*ground_scale+ground_ofsy);
//     glVertex3f (x2,y2,0);
// 
//     // rotate [x,y] vector
//     float xtmp = kx*x - ky*y;
//     y = ky*x + kx*y;
//     x = xtmp;
//   }
//   glEnd();
}

