/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
//------------------------------------------------------------------------------
// File : rendertexture.h
//------------------------------------------------------------------------------
// Copyright 2002 Mark J. Harris and
// The University of North Carolina at Chapel Hill
//------------------------------------------------------------------------------
// Permission to use, copy, modify, distribute and sell this software and its 
// documentation for any purpose is hereby granted without fee, provided that 
// the above copyright notice appear in all copies and that both that copyright 
// notice and this permission notice appear in supporting documentation. 
// Binaries may be compiled with this software without any royalties or 
// restrictions. 
//
// The author(s) and The University of North Carolina at Chapel Hill make no 
// representations about the suitability of this software for any purpose. 
// It is provided "as is" without express or implied warranty.
// -----------------------------------------------------------------------------
// Credits:
// Original RenderTexture code: Mark J. Harris
// Original Render-to-depth-texture support: Thorsten Scheuermann
// Linux Copy-to-texture: Eric Werness
// Various Bug Fixes: Daniel (Redge) Sperl 
//                    Bill Baxter
//
// -----------------------------------------------------------------------------
/**
 * @file RenderTexture.h
 * 
 * Interface definition for class RenderTexture.  A multi-format render to 
 * texture wrapper.
 */
#ifndef __RENDERTEXTURE_HPP__
#define __RENDERTEXTURE_HPP__

#include <util/defines.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <GL/glxew.h>

#include <iostream>
#include <vector>


#define RGB_TYPE                 0
#define GREYSCALE_TYPE           1
#define RED_TYPE                 2
#define GREEN_TYPE               3
#define BLUE_TYPE                4



using namespace std;

class RenderTexture
{
  public: // enums
    enum UpdateMode
    {
      RT_RENDER_TO_TEXTURE,
      RT_COPY_TO_TEXTURE
    };

  public: // interface
    RenderTexture(int iWidth, int iHeight, int imageWidth, int imageHeight,
        bool bIsTexture = true,
        bool bIsDepthTexture = false);
    ~RenderTexture();

    //! Call this once before use.  Set bShare to true to share lists, textures, 
    //! and program objects between the render texture context and the 
    //! current active GL context.
    bool initialize(bool bShare             = true, 
        bool bDepth             = false, 
        bool bStencil           = false,
        bool bMipmap            = false, 
        bool bAnisoFilter       = false,
        unsigned int iRBits     = 8,
        unsigned int iGBits     = 8,
        unsigned int iBBits     = 8,
        unsigned int iABits     = 8,

        UpdateMode   updateMode = RT_COPY_TO_TEXTURE

        );

    // !Begin drawing to the texture. (i.e. use as "output" texture)
    bool beginCapture();
    // !End drawing to the texture.
    bool endCapture();

    // !Bind the texture to the active texture unit for use as an "input" texture
    void bind() const;   // [Redge] moved definition to cpp-file (since it's longer now)

    //! Enables the texture target appropriate for this render texture.
    void enableTextureTarget() const { if (_bInitialized) glEnable(_iTextureTarget); }
    //! Disables the texture target appropriate for this render texture.
    void disableTextureTarget() const { if (_bInitialized) glDisable(_iTextureTarget); }

    //! Returns the width of the offscreen buffer.
    int getWidth() const   { return _iWidth;  } 
    //! Returns the width of the offscreen buffer.
    int getHeight() const  { return _iHeight; }     
  
    //! True if this is a floating point buffer / texture.
    bool isFloatTexture() const     { return _bFloat; }
    //! True if this texture has non-power-of-two dimensions.
    bool isRectangleTexture() const { return _bRectangle; }

    vector<double> getTexels();
    
    void setColorType(int type);

  protected: // methods
    bool         _invalidate();

  protected: // data
    int          _iWidth;     // width of the pbuffer
    int          _iHeight;    // height of the pbuffer
    int          _imageHeight;
    int          _imageWidth;
    
    int _colorType;

    vector<double> _texels;

    bool         _bIsTexture;
    bool         _bIsDepthTexture;
    bool         _bHasArbDepthTexture; // [Redge]

    UpdateMode   _eUpdateMode;

    bool         _bInitialized;

    unsigned int _iBits[6];
    bool         _bFloat;
    bool         _bRectangle;
    bool         _bHasDepth;
    bool         _bHasStencil;
    bool         _bMipmap;
    bool         _bAnisoFilter;

    Display      *_pDpy;
    GLXContext   _hGLContext;
    GLXPbuffer   _hPBuffer;

    GLXDrawable  _hPreviousDrawable;
    GLXContext   _hPreviousContext;
    // #endif

    GLenum       _iTextureTarget;
    unsigned int _iTextureID;
    unsigned int _iDepthTextureID;

    unsigned short* _pPoorDepthTexture; // [Redge]

    GLubyte *pixelBuffer;

    int _bufferSize;
};

#endif //__RENDERTEXTURE_HPP__
