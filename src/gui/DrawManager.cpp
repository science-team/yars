/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include <gui/DrawManager.h>

#include <string> 
#include <iostream>
#include <sstream>

// #include <time.h>

using namespace std;


DrawManager::DrawManager(string pathToTextures, bool useTraces)
{
  _pathToTextures = pathToTextures;

  _defaultColor.red   = 1.0;
  _defaultColor.green = 1.0;
  _defaultColor.blue  = 1.0;
  _defaultColor.alpha = 1.0;
  _numberOfDrawLines  = 0;
  _useTraces          = useTraces;

  _drawAxes           = true;
  _axesScaling        = 1.0;

  _isCameraView = false;
  _drawHelper = new DrawObject();
  _drawHelper->setColor( _defaultColor.red, _defaultColor.green, _defaultColor.blue, _defaultColor.alpha);
  // load the ground and sky textures
  ostringstream groundTexture;
  groundTexture << _pathToTextures <<  "/textures/ground.bmp";
  ostringstream skyTexture;
  skyTexture << _pathToTextures <<  "/textures/sky.bmp";
  getBitmapImageData( (char*)groundTexture.str().c_str(), &groundTextureImage);
  getBitmapImageData( (char*)skyTexture.str().c_str(), &skyTextureImage);
  setUpTextures();
}

DrawManager::~DrawManager()
{
  glDeleteTextures(1,&ground_texture);
  glDeleteTextures(1, &sky_texture);
  delete _drawHelper;
  _items.clear();
  _trace_items.clear(); 
  _trace_line.clear();
  delete[] groundTextureImage.data;
  delete[] skyTextureImage.data;
}

void DrawManager::setViewPoint(float xyz[3]){
  _drawHelper->setViewPoint(xyz);
}

void DrawManager::setUpTextures()
{	
  _drawHelper->setColor( 1,1,1,1);
  glGenTextures( 1, &ground_texture );
  glBindTexture( GL_TEXTURE_2D, ground_texture );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexImage2D( GL_TEXTURE_2D, 0, 3, groundTextureImage.width, groundTextureImage.height, 
      0, GL_RGB, GL_UNSIGNED_BYTE, groundTextureImage.data );
  glGenTextures( 1, &sky_texture );
  glBindTexture( GL_TEXTURE_2D, sky_texture );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexImage2D( GL_TEXTURE_2D, 0, 3, skyTextureImage.width, skyTextureImage.height, 
      0, GL_RGB, GL_UNSIGNED_BYTE, skyTextureImage.data );
  // this part will result in the sky beeing composed out of many small tiles
  //   if (_useTextures) {
  //     glEnable (GL_TEXTURE_2D);
  //     glEnable (GL_TEXTURE_GEN_S);
  //     glEnable (GL_TEXTURE_GEN_T);
  //     glTexGeni (GL_S,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
  //     glTexGeni (GL_T,GL_TEXTURE_GEN_MODE,GL_OBJECT_LINEAR);
  //     static GLfloat s_params[4] = {1.0f,1.0f,0.0f,1};
  //     static GLfloat t_params[4] = {0.817f,-0.817f,0.817f,1};
  //     glTexGenfv (GL_S,GL_OBJECT_PLANE,s_params);
  //     glTexGenfv (GL_T,GL_OBJECT_PLANE,t_params);
  //   }
  //   else {
  //     glDisable (GL_TEXTURE_2D);
  //   }
}

void DrawManager::setNumberOfDrawLines(int nr)
{
  _numberOfDrawLines = nr;
}

void DrawManager::clearTraceGeoms()
{
  _trace_items.clear();
  _trace_line.clear();
}

void DrawManager::addTraceLineGeom(P3DColor pos)
{
  _trace_line.push_back(pos);
}


void DrawManager::addTraceGeom(dGeomID geom, const float red,
    const float green, const float blue)
{
  Y_DEBUG("DrawManager::addTraceGeom %f,%f,%f", red, green, blue);
  Item tmpItem;
  tmpItem.geom  = geom;
  tmpItem.color.red   = red;
  tmpItem.color.green = green;
  tmpItem.color.blue  = blue;
  tmpItem.color.alpha = 1.0;
  _trace_items.push_back(tmpItem);
}

void DrawManager::addGeomLast(dGeomID geom)
{
  Item tmpItem;
  tmpItem.geom  = geom;
  tmpItem.color = _defaultColor;
  _items.push_back(tmpItem);
}


void DrawManager::addGeom(dGeomID geom)
{
  Item tmpItem;
  tmpItem.geom  = geom;
  tmpItem.color = _defaultColor;
  _items.push_front(tmpItem);
}

void DrawManager::addGeom(dGeomID geom, const float red,
    const float green, const float blue)
{
  Item tmpItem;
  tmpItem.geom        = geom;
  tmpItem.color.red   = red;
  tmpItem.color.green = green;
  tmpItem.color.blue  = blue;
  _items.push_front(tmpItem);
}

void DrawManager::removeGeom(dGeomID geom)
{
  list<Item>::iterator iter;
  list<Item>::iterator iter_end;
  iter_end = _items.end();

  for( iter = _items.begin() ; !(iter == iter_end) ; ++iter)
  {
    if(iter->geom == geom)
    {
      _items.erase(iter);
      return;
    }
  }
}

void DrawManager::setDefaultColor(const float red, const float green,
    const float blue)
{
  _defaultColor.red   = red;
  _defaultColor.green = green;
  _defaultColor.blue  = blue;
}

void DrawManager::setDefaultColor(const float red, const float green,
    const float blue, const float alpha)
{
  _defaultColor.red   = red;
  _defaultColor.green = green;
  _defaultColor.blue  = blue;
  _defaultColor.alpha = alpha;
}


void DrawManager::changeColor(dGeomID geom, const float red, const float green,
    const float blue)
{
  for(_itemsIterator = _items.begin(); _itemsIterator != _items.end();
      _itemsIterator++)
  {
    if((*_itemsIterator).geom == geom)
    {
      (*_itemsIterator).color.red   = red;
      (*_itemsIterator).color.green = green;
      (*_itemsIterator).color.blue  = blue;
      return;
    }
  }
}

void DrawManager::changeColor(dGeomID geom, const float red, const float green,
    const float blue, const float alpha)
{
  for(_itemsIterator = _items.begin(); _itemsIterator != _items.end();
      _itemsIterator++)
  {
    if((*_itemsIterator).geom == geom)
    {
      (*_itemsIterator).color.red   = red;
      (*_itemsIterator).color.green = green;
      (*_itemsIterator).color.blue  = blue;
      (*_itemsIterator).color.alpha = alpha;
      return;
    }
  }
}

void DrawManager::drawWorld()
{
  // grid
  float stopx[3] = {START_POS_X+(1*_axesScaling), START_POS_Y, 0.02};
  float stopy[3] = {START_POS_X, START_POS_Y+(1*_axesScaling), 0.02};
  float stopz[3] = {START_POS_X,START_POS_Y, 1*_axesScaling};

  float middle[3] = {START_POS_X, START_POS_Y, 0.02};

  //draw the orientation lines
  if(_isCameraView==false && _drawAxes==true)
  {
    glColor4f(0.5,0,0,1);
    _drawHelper->drawLine(middle, stopx, _isCameraView);
    glColor4f(0,0.5,0,1);
    _drawHelper->drawLine(middle, stopy, _isCameraView);
    glColor4f(0,0,0.5,1);
    _drawHelper->drawLine(middle, stopz, _isCameraView);
    glColor4f(1,1,1,1);
  }

  glPushMatrix();
  //draw the floor
  float defaultPositioning[3] = {0,0,0};
  float defaultMatrix[12] = {1,0,0,0,0,0,-1,0,0,1,0,0};

  if(_useTextures)
  {
    glClearColor(0.5,0.7,1,0.5);
    if(_isCameraView)
    {  
      _drawHelper->setColor (0.5,0.5,0.5,1);
      glDisable (GL_TEXTURE_2D);
      _drawHelper->drawGround();
      glPopMatrix();
    }
    else
    {
      glDisable(GL_LIGHTING);
      glEnable (GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, ground_texture);
      _drawHelper->drawGround();
      glBindTexture(GL_TEXTURE_2D, sky_texture);
      _drawHelper->drawSky();
      glDisable (GL_TEXTURE_2D);
      glEnable(GL_LIGHTING);
      glPopMatrix();

    } 
  }
  else 
  {
    glClearColor(0.5,0.7,1,0.5);
    if(_isCameraView)
    {  
      glDisable (GL_TEXTURE_2D);
      _drawHelper->setColor (1,1.3,0.5,1);
      _drawHelper->drawGround();
      glPopMatrix();
    }
    else
    {
      glDisable (GL_TEXTURE_2D);
      _drawHelper->setColor (1,1.3,0.5,1);
      _drawHelper->drawGround();
      glPopMatrix();
    }
  }
}


 void DrawManager::setTransform (const dReal pos[3], const dReal R[12])
{
  GLfloat matrix[16];
  matrix[0]=R[0];
  matrix[1]=R[4];
  matrix[2]=R[8];
  matrix[3]=0;
  matrix[4]=R[1];
  matrix[5]=R[5];
  matrix[6]=R[9];
  matrix[7]=0;
  matrix[8]=R[2];
  matrix[9]=R[6];
  matrix[10]=R[10];
  matrix[11]=0;
  matrix[12]=pos[0];
  matrix[13]=pos[1];
  matrix[14]=pos[2];
  matrix[15]=1;
  glPushMatrix();
  glMultMatrixf (matrix);
}


void DrawManager::draw()
{
  drawWorld();
  float *drawLineStart = new float[3];
  float *drawLineEnd = new float[3];
  //Y_DEBUG("number of _trace_items = %d", _trace_items.size());
  if(_useTraces)
  {
    for(int i=0; i < _trace_items.size(); ++i)
    {
      this->drawGeom(_trace_items[i].geom,
          dGeomGetPosition(_trace_items[i].geom),
          dGeomGetRotation(_trace_items[i].geom),
          &_trace_items[i].color.red,
          &_trace_items[i].color.green,
          &_trace_items[i].color.blue,
          &_trace_items[i].color.alpha);      
    }

    if(_trace_line.size() > 0)
    {
      for(int j=0; j < _numberOfDrawLines; j++)
      {
        for(int i=j; i < _trace_line.size() - (_numberOfDrawLines); 
            i+=_numberOfDrawLines)
        {
          Y_DEBUG("i: %d _numberOfDrawLines %d trace_line.size %d"
              ,i, _numberOfDrawLines, _trace_line.size());
          P3DColor p1 = _trace_line[i];
          P3DColor p2 = _trace_line[i+_numberOfDrawLines];
          drawLineStart[0] = p1.x;
          drawLineStart[1] = p1.y;
          drawLineStart[2] = p1.z;
          drawLineEnd[0]   = p2.x;
          drawLineEnd[1]   = p2.y;
          drawLineEnd[2]   = p2.z;
          glColor3f(p2.r,p2.g, p2.b);
          _drawHelper->drawLine(drawLineStart, drawLineEnd, _isCameraView);
        }
      }
    }
  }

  for(_itemsIterator = _items.begin(); _itemsIterator != _items.end();
      _itemsIterator++)
  {
    if (dGeomGetClass((*_itemsIterator).geom) == dGeomTransformClass)
    {
      dGeomID g2 = dGeomTransformGetGeom((*_itemsIterator).geom);
      if( dGeomTransformGetInfo((*_itemsIterator).geom) == 0)
      {
        const dReal *pos = dGeomGetPosition((*_itemsIterator).geom);
        const dReal *rot = dGeomGetRotation((*_itemsIterator).geom);

        const dReal *pos2 = dGeomGetPosition(g2);
        const dReal *rot2 = dGeomGetRotation(g2);
        dVector3 actual_pos;
        dMatrix3 actual_rot;
        dMULTIPLY0_331 (actual_pos, rot, pos2);
        for(int j=0; j<3; ++j)
        {
          actual_pos[j] += pos[j];
        }
        dMULTIPLY0_333(actual_rot, rot, rot2);
        this->drawGeom(g2, actual_pos, actual_rot, &(*_itemsIterator).color.red, 
            &(*_itemsIterator).color.green, &(*_itemsIterator).color.blue,
            &(*_itemsIterator).color.alpha);
      }
      else
      {
        this->drawGeom( g2, dGeomGetPosition((*_itemsIterator).geom),
            dGeomGetRotation((*_itemsIterator).geom),
            &(*_itemsIterator).color.red, &(*_itemsIterator).color.green, 
            &(*_itemsIterator).color.blue,
            &(*_itemsIterator).color.alpha);
      }
    }
    else
    {
      this->drawGeom((*_itemsIterator).geom, dGeomGetPosition((*_itemsIterator).geom), 
          dGeomGetRotation((*_itemsIterator).geom), &(*_itemsIterator).color.red,
          &(*_itemsIterator).color.green, &(*_itemsIterator).color.blue,
          &(*_itemsIterator).color.alpha);      
    }
  }
  delete[] drawLineEnd;
  delete[] drawLineStart;
}

void DrawManager::drawGeom(dGeomID geom, const dReal *pos, const dReal *rot,
    const float *red, const float *green,
    const float *blue, const float *alpha)
{
  int type;
  dVector3 sides;
  dReal radius, length;
  _drawHelper->setColor (*red,*green,*blue, *alpha);
  type = dGeomGetClass (geom);
  glEnable(GL_LIGHTING);
  if (type == dBoxClass) 
  {   
    dGeomBoxGetLengths (geom, sides);
    setTransform(pos, rot);
    _drawHelper->drawBox(sides);
    glPopMatrix();
  }
  else if(type == dCCylinderClass)
  {
    setTransform(pos, rot);
    dGeomCCylinderGetParams (geom, &radius, &length);
    _drawHelper->drawCappedCylinder(length, radius);
    glPopMatrix();
  }
  else if(type == dCylinderClass)
  {
    setTransform(pos, rot);
    dGeomCylinderGetParams (geom, &radius, &length);
    _drawHelper->drawCylinder(length, radius);
    glPopMatrix();
  }
  else if(type == dSphereClass)
  {
    setTransform(pos, rot);
    _drawHelper->drawSphere(dGeomSphereGetRadius(geom));
    glPopMatrix();
  }
  else if(type == dRayClass)
  {
    float *endPos = new float[3];
    dVector3 start;
    float *startPos = new float[3];
    dVector3 dir;
    double length = dGeomRayGetLength(geom);

    dGeomRayGet(geom, start, dir);
    startPos[0] = start[0];
    startPos[1] = start[1];
    startPos[2] = start[2];

    endPos[0] = start[0]+dir[0]*length;
    endPos[1] = start[1]+dir[1]*length;
    endPos[2] = start[2]+dir[2]*length;
    _drawHelper->drawLine(startPos, endPos, _isCameraView);
    delete[] endPos;
    delete[] startPos;
    glPopMatrix();
  }
  _drawHelper->setColor (1.0f, 1.0f, 1.0f, 1);
}

void DrawManager::setTextures( bool useTextures)
{
  _useTextures = useTextures;
}

void DrawManager::setTraces( bool useTraces)
{
  _useTraces = useTraces;
}

void DrawManager::getBitmapImageData( char *pFileName, BMPImage *pImage )
{
  FILE *pFile = NULL;
  unsigned short nNumPlanes;
  unsigned short nNumBPP;
  int i;

  if( (pFile = fopen(pFileName, "rb") ) == NULL )
    cout << "ERROR: getBitmapImageData - %s not found " << pFileName << "." << endl;

  // Seek forward to width and height info
  fseek( pFile, 18, SEEK_CUR );

  if( (i = fread(&pImage->width, 4, 1, pFile) ) != 1 )
    cout << "ERROR: getBitmapImageData - Couldn't read width from " << pFileName << "." << endl; 

  if( (i = fread(&pImage->height, 4, 1, pFile) ) != 1 )
    cout << "ERROR: getBitmapImageData - Couldn't read height from " << pFileName << "." << endl; 

  if( (fread(&nNumPlanes, 2, 1, pFile) ) != 1 )
    cout << "ERROR: getBitmapImageData - Couldn't read plane count from " << pFileName << "." << endl; 

  if( nNumPlanes != 1 )
    cout << "ERROR: getBitmapImageData - Plane count from " << pFileName << " is not 1: " << nNumPlanes << "." << endl;

  if( (i = fread(&nNumBPP, 2, 1, pFile)) != 1 )
    cout << "ERROR: getBitmapImageData - Couldn't read BPP from " << pFileName << endl; 

  if( nNumBPP != 24 )
    cout << "ERROR: getBitmapImageData - BPP from " << pFileName << " is not 24: " << nNumBPP << "." << endl;

  // Seek forward to image data
  fseek( pFile, 24, SEEK_CUR );

  // Calculate the image's total size in bytes. Note how we multiply the 
  // result of (width * height) by 3. This is becuase a 24 bit color BMP 
  // file will give you 3 bytes per pixel.
  int nTotalImagesize = (pImage->width * pImage->height) * 3;

  // pImage->data = (char*) malloc( nTotalImagesize );
  pImage->data = new char[nTotalImagesize];

  if( (i = fread(pImage->data, nTotalImagesize, 1, pFile) ) != 1 )
    cout << "ERROR: getBitmapImageData - Couldn't read image data from " << pFileName << "." << endl;

  char charTemp;
  for( i = 0; i < nTotalImagesize; i += 3 )
  { 
    charTemp = pImage->data[i];
    pImage->data[i] = pImage->data[i+2];
    pImage->data[i+2] = charTemp;
  }
  fclose(pFile);
  // TODO: was a try to remove a memory leak detected by valgrind
  //free(pImage->data);
  
}

void DrawManager::setAxesVisualization(bool visualize, double scaling)
{
  this->_drawAxes    = visualize;
  this->_axesScaling = scaling;

  Y_DEBUG("DrawManager::setAxesVisualization: %d, %f", _drawAxes, _axesScaling);
}

