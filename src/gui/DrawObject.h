/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef DRAWOBJECT_H__
#define DRAWOBJECT_H__


#include <gui/RenderTexture.h>
#include <ode/ode.h>
#include <vector>
#include <iterator>

#define SHADOW_INTENSITY (0.65f)
#define DO_OFFSET         0
#define FLOOR_SIZE        100
#define OFFSET_X_FLOOR    0.5
#define OFFSET_Y_FLOOR    0.5
#define SCALE_GROUND      1.0/1.0f

#define SKY_SIZE          400
#define HEIGHT_SKY        15
#define SCALE_SKY         5

using namespace std;

// class for drawing the openGL objects
class DrawObject
{
  public:
    DrawObject();

    ~DrawObject();

    void setViewPoint(float xyz[3]);

    void drawLine(const float pos1[3], const float pos2[3], bool isCameraView);

    void drawBox(
        const dReal sides[3]);

    void drawCylinder(
        float length, float radius);

    void drawCappedCylinder (
        float length, float radius);

    void drawSphere(
        float radius);

    void setColor(float red, float green, float blue, float alpha);

    void drawGround();

    void drawSky();

    void drawSphereShadow (float px, float py, float pz, float radius);

    
  private:

    float color[4];

    float view_xyz[3];

    int _capped_Cylinder_Quality;

    int _sphere_quality;

    GLUquadric *quadSphere;

    GLfloat *matrix;

};

#endif
