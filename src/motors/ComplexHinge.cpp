/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/

//
// WARNING: This Class is under heavy development and very experimental, 
//          so don't take it for granted that everything works as expected
//
// TODO:
//
// - need to take into account acceleration of motor and motor under load
//
// - different damping constants for backlash / no backlash cases
//
// - integrate the spring coupling (extensive tests)
//
// - how to use joint torque sensor as sensor and inside motor without 
//     redundant calculations (--> use global value repo "YarsContainers")
//
// - include energy sensor (negative values for active forward / backward
//     movements and positive values for motor brake and external movement)
//

#include "ComplexHinge.h"
#include <container/YarsContainers.h>
 
//#include <unistd.h>

//---------------------------------------------------------------------- 
// Constructors
//---------------------------------------------------------------------- 

ComplexHinge::ComplexHinge()
{
}

ComplexHinge::ComplexHinge(ConnectorDescription *connStruct, dJointID *joint, 
    dBodyID *sourceBody, dBodyID *aimBody)
{
  Y_DEBUG("ComplexHinge Constructor");

  // get sim info
  stepSize_ = (*(YarsContainers::SimParams())).stepSize;

  // locally store joint info, create Amotor, allocate Feedback structures etc.
  setupJointAndFeedback(joint, sourceBody, aimBody);

  // copy the connector struct information to private structs
  copyDataFromConnectorDescription(connStruct);

  // calculated derived parameters
  calculateDerivedParameters();
  printoutDerivedParameters();

  // prepare everything for torque measurements around hinge joint axis
  prepareTorqueCalc();

  // init variables
  backlashPos_            = 0.0;
  motorSpeed_             = 0.0;
  sCouplDeflection_       = 0.0;
  motorReverseTime_       = 0.0;
  oldMotorState_          = MODE_RELEASED;
  currTime_               = 0;
  initDone_               = false;

  // TODO: Test stuff - remove
//  updateActiveComplexHinge(1.0, 1.0);
//  dJointGetHingeAxis(*joint_, hingeAxis_);
//  if(joint_ == 0)
//  {
//    printf("joint_ == 0 \n");
//  }
//  else
//  {
//    printf("joint_ = %d \n", joint_);
//    printf("hingeAxis = x: %f, y: %f, z: %f \n", hingeAxis_[0], hingeAxis_[1],
//        hingeAxis_[2]);
//  }

//  plotPWMappingFunctions();
//  plotAntagonistToMotor();
}

//---------------------------------------------------------------------- 
// Destructor
//---------------------------------------------------------------------- 

ComplexHinge::~ComplexHinge()
{
  if(feedback_ != NULL)
    delete feedback_;

  if(feedbackAMotor1_ != NULL)
    delete feedbackAMotor1_;

  if(mAForceFilter_ != NULL)
    delete mAForceFilter_;

  if(mAForceFilter2_ != NULL)
    delete mAForceFilter2_;

  if(mADamperFilter_ != NULL)
    delete mADamperFilter_;

  if(jointGroup_ != NULL)
    dJointGroupDestroy(jointGroup_);

//  for(int i=0; i<gp_.size(); i++)
//  {
//    if(gp_[i] != NULL)
//      delete gp_[i];
//  }
//  gp_.clear();
}

//---------------------------------------------------------------------- 
// Public functions 
//---------------------------------------------------------------------- 
void ComplexHinge::updatePassiveComplexHinge()
{
  // TODO: update
  //       - spring
  //       - springCoupling
  //       - damper
  //       - friction
  //       - backlash
}

void ComplexHinge::updateActiveComplexHinge(double agonist, double antagonist)
{
  double oldVel = hingeAngleVel_;
  double accel;
  double torqueFromMassAndAccel;
  double t = 0.01;

  currTime_++;

  // TODO: remove after testing
//  if(joint_ == 0)
//  {
//    printf("joint_ == 0 \n");
//  }
//  else
//  {
//    printf("joint_ = %d \n", joint_);
//    printf("hingeAxis = x: %f, y: %f, z: %f \n", hingeAxis_[0], hingeAxis_[1],
//        hingeAxis_[2]);
//  }

  // Do the Antagonist --> MotorCommand mapping
  calcMotorActivationFromAntagonists(agonist, antagonist);

  // Update internal sensors
  updateInternalSensors();

  // reset torque accumulators and velocities
  accuMaxTorqueAct_  = 0.0;
  accuMaxTorqueStop_ = 0.0;
  maxVelPositive_    = 0.0;
  maxVelNegative_    = 0.0;

  // motorCommand
  updateMotor();
  // damping
  updateDamper();
  // joint friction
  accuMaxTorqueStop_ += updateJointFriction(&_jointFriction);
  // spring 
  updateSpring();
  // spring coupling
  updateSpringCouplingTorque();


  // Filter (moving average) stop torque, because otherwise we'll have ugly
  //   oscillations
  if(! isfinite(accuMaxTorqueStop_))
  {
    accuMaxTorqueStop_ = 0.0;
  }
  accuMaxTorqueStop_ = mADamperFilter_->getFilteredOutput(accuMaxTorqueStop_); 

  //
  // now combine everything to get torque and velocity parameters for the
  // dJointSetHingeParam function call
  //
  if(accuMaxTorqueAct_ >= 0)
  {
    if(accuMaxTorqueAct_ > accuMaxTorqueStop_)
    {
      // keep going in forward direction
      accuMaxTorqueTotal_ = accuMaxTorqueAct_ - accuMaxTorqueStop_;
      maxVelTotal_        = MAX(maxVelPositive_, 1); // return maximum
    }
    else
    {
      // try to stop
      accuMaxTorqueTotal_ = accuMaxTorqueStop_ 
                          - (accuMaxTorqueAct_ * hingeAngleVelSign_);
      maxVelTotal_        = 0;
    }
  }
  else if((accuMaxTorqueAct_) < 0)
  {
    if((-1 * accuMaxTorqueAct_) > accuMaxTorqueStop_)
    {
      // keep going in backward direction
      accuMaxTorqueTotal_ = (-1 * accuMaxTorqueAct_) - accuMaxTorqueStop_;
      maxVelTotal_        = MIN(maxVelNegative_ , -1); // return minimum
    }
    else
    {
      // try to stop
      accuMaxTorqueTotal_ = accuMaxTorqueStop_ 
                          - (accuMaxTorqueAct_ * hingeAngleVelSign_);
      maxVelTotal_        = 0;
    }
  }

  //  TODO: remove function call and function after testing
  //
//  int currCase       = 0;
//  double aMotorFMax  = 0;
//  double sForceAdd   = 0;
//  double aForceAdd   = 0;
//  setTestTorques(&currCase, &aMotorFMax, &sForceAdd, &aForceAdd);

  // set the ODE parameters for the joint
  // TODO: replace

//  if((currTime_ % 100) < 50)
//  {
//    printf("---- 0  ------\n");
//    maxVelTotal_        = 0.0;
//    accuMaxTorqueTotal_ = 0.0;
//  }
//  else if ((currTime_ % 100) >= 50)
//  {
//    printf("---- 50  ------\n");
//    maxVelTotal_        = 1;
//    accuMaxTorqueTotal_ = 9.85;
//  }

  if(accuMaxTorqueTotal_ == 0.0)
  {
    // reset the feedback struct because otherwise the old values will persist
    //   until amotor is turned on again
    resetFeedback(feedbackAMotor1_, 0.0);
  }

//  printf("currTime_           = %d \n", currTime_);
//  printf("accuMaxTorqueTotal_ = %f \n", accuMaxTorqueTotal_);
//  printf("maxVelTotal_        = %f \n", maxVelTotal_);

//  dJointSetHingeParam(*joint_,        dParamFMax, accuMaxTorqueTotal_);
//  dJointSetHingeParam(*joint_,        dParamVel,  maxVelTotal_       );
//  dJointSetHingeParam(*joint_,        dParamFMax, 2);
//  dJointSetHingeParam(*joint_,        dParamVel,  0);
    dJointSetAMotorParam (jointAMotor_, dParamFMax, accuMaxTorqueTotal_);
    dJointSetAMotorParam (jointAMotor_, dParamVel,  maxVelTotal_);
//  dJointSetAMotorParam (jointAMotor_, dParamFMax, aMotorFMax         );
//  dJointSetAMotorParam (jointAMotor_, dParamVel,  aMotorVel          );

  //  TODO: remove function call and function after testing
//  int currCase = -10;
//  double aMotorFMax = 0.0;
//  double sForceAdd  = 0.0;
//  double aForceAdd  = 0.0;
//  printoutUpdateDebugInfo(currCase, aMotorFMax, sForceAdd, aForceAdd);

  // check for abort conditions
  checkAbortConditions();
}


//---------------------------------------------------------------------- 
// Private functions 
//---------------------------------------------------------------------- 

void ComplexHinge::updateInternalSensors()
{
  // update the current hinge axis
  dJointGetHingeAxis(*joint_, hingeAxis_);

  hingeAngle_       = dJointGetHingeAngle(*joint_);
  hingeAngleVel_    = dJointGetHingeAngleRate(*joint_);
  (hingeAngleVel_ >= 0.0) ? (hingeAngleVelSign_=1.0) : (hingeAngleVelSign_=-1.0);
  hingeAngleVelAbs_ = hingeAngleVel_ * hingeAngleVelSign_;

//  calcTorqueAroundJointAxis();
  calcTorqueAroundJointAxis2();

  //printHingeTorqueInformation();
}

// TODO:
// - whole joint damping / acceleration ??
void ComplexHinge::updateMotor()
{
  switch(mc_.mode)
  {
    case MODE_RELEASED:
      motorSpeed_        = 0.0;
      this->updateBacklashPosition();
      break;
    case MODE_FORWARD:
      motorSpeed_        = totalNoLoadSpeed_ 
                         * mapPulseWidth(mc_.activation, _motor.pwToNoLoadSpeed);
      this->updateBacklashPosition();
      if(backlashPos_ < backlashMaxTol_)
      {
        //printf("MF-BL: %f, %f, %f \n", motorSpeed_, hingeAngleVel_, backlashPos_);
        // backlash present, motor/gear can't act on joint
      }
      else
      {
        // no backlash - motor-gear combi acts on the joint
        maxVelPositive_    = motorSpeed_;
        accuMaxTorqueAct_ += mapPulseWidth(mc_.activation, _motor.pwToStallTorque) 
                           * totalMaxContinuousTorque_;
        // TODO: update energy sensor
      }
      break;
    case MODE_BACKWARD:
      motorSpeed_        = -1 * totalNoLoadSpeed_
                         * mapPulseWidth(mc_.activation, _motor.pwToNoLoadSpeed);
      this->updateBacklashPosition();
      if(backlashPos_ > backlashMinTol_)
      {
        //printf("MR-BL: %f, %f, %f \n", motorSpeed_, hingeAngleVel_, backlashPos_);
        // backlash present, motor/gear can't act on joint
      }
      else
      {
        // no backlash - motor-gear combi acts on the joint
        maxVelNegative_    = motorSpeed_;
        accuMaxTorqueAct_ -= mapPulseWidth(mc_.activation, _motor.pwToStallTorque) 
                           * totalMaxContinuousTorque_;
        // TODO: update energy sensor
      }
      break;
    case MODE_BREAK:
      motorSpeed_         = 0.0;
      this->updateBacklashPosition();
      if(fabs(backlashPos_) < backlashMaxTol_)
      {
        // backlash present, motor/gear can't act on joint - no energy gained
        //printf("MB-BL: %f, %f, %f \n", motorSpeed_, hingeAngleVel_, backlashPos_);
      }
      else
      {
        // no backlash - motor-gear combi acts on the joint
        accuMaxTorqueStop_ += mapPulseWidth(mc_.activation, _motor.pwToBrakeEfficiency)
                            * updateJointFriction(&(_motor.brakeFriction));
        // TODO: update energy sensor
      }
      break;
    default:
      ;
  }
}


void ComplexHinge::updateBacklashPosition()
{
  if(oldMotorState_ != mc_.mode)
  {
    // motor reversed direction
    motorReverseTime_ = _motor.mechanicalTimeConstant;
    oldMotorState_    = mc_.mode;
  }

  if(motorReverseTime_ > 0.0)
  {
    //printf("motorReverseTime_ = %f \n", motorReverseTime_);
    motorSpeed_ = 0.0;
    motorReverseTime_ -= stepSize_;
  }

  backlashPos_ += (motorSpeed_ - hingeAngleVel_) * stepSize_;

  if(backlashPos_ < backlashMinTol_)
  {
    backlashPos_ = backlashMin_;
  }
  else if(backlashPos_ > backlashMaxTol_)
  {
    backlashPos_ = backlashMax_;
  }

  return;
}

void ComplexHinge::updateSpring()
{
  if(_spring.active == true)
  {
    accuMaxTorqueAct_ -= (hingeAngle_ - _spring.initialPosition)
      * totalSpringConstant_;                 
    if((hingeAngle_ - _spring.initialPosition) < 0)
    {
      maxVelPositive_ = 1000;
    }
    else if((hingeAngle_ - _spring.initialPosition) > 0)
    {
      maxVelNegative_ = -1000;
    }
  }
}

void ComplexHinge::updateDamper()
{
  if(_damper.active == true)
  {
    accuMaxTorqueStop_ += hingeAngleVel_ * hingeAngleVelSign_ * totalDamping_; 
  }
}

double ComplexHinge::updateJointFriction(JointFrictionDescription *jFD)
{
  double frictionTorque = 0.0;

  if(jFD->active == true)
  {
    if((jFD->staticActive == true)
        && (hingeAngleVelAbs_ < jFD->staticVelThres))
    {
      frictionTorque += jFD->staticMaxForce;
    }
    else if((jFD->minVelActive == true)
        && (hingeAngleVelAbs_ >= jFD->staticVelThres)
        && (hingeAngleVelAbs_ < jFD->dynamicMinVel))
    {
      frictionTorque += jointFrictionTransitionSlope_ * hingeAngleVelAbs_;
    }
    else if(jFD->dynamicActive == true)
    {
      switch(jFD->dynamicType)
      {
        case DEF_DYNAMIC_FRICTION_TYPE_CONSTANT:
          frictionTorque += jFD->dynamicConstant
                              + jFD->dynamicOffset;
          break;
        case DEF_DYNAMIC_FRICTION_TYPE_LINEAR:
          frictionTorque += jFD->dynamicConstant 
                              * hingeAngleVelAbs_ 
                              + jFD->dynamicOffset; 
          break;
        case DEF_DYNAMIC_FRICTION_TYPE_QUADRATIC:
          frictionTorque += jFD->dynamicConstant 
                              * hingeAngleVelAbs_ * hingeAngleVelAbs_
                              + jFD->dynamicOffset; 
          break;
        default:
          ;
      }
    }
  }

  return frictionTorque;
}

// TODO: 
// - do we need to low pass | mean average filter here ??
// - complete this by updating the deflection / energy storage
// - double springConstant;
// - double maxDeflection;
// - motor influence, e.g. the two bodies are fixed, the spring has a
//     deflection != 0 and the motor takes up the spring energy ...
void ComplexHinge::updateSpringCouplingTorque()
{
  if(_springCoupling.active == true)
  {
    if(torqueHingeAxis_ == 0.0)
    {
      //sCouplCase_ = -1;
      sCouplCorrectionT_ = 0.0;
    }
    else if((torqueHingeAxis_ - sCouplCorrectionT_) > _springCoupling.minTorque)
    {
      //sCouplCase_ = 0;

      // torque in positive hinge direction
      if(sCouplDeflection_ > _springCoupling.maxDeflection)
      {
        // springCoupling can't take up any more torque
      }
      else
      {
        // springCoupling is compressed "right"
        sCouplCorrectionT_ += _springCoupling.minTorque - torqueHingeAxis_;
        // sCouplDeflection_
        // TODO
      }
    }
    else if((torqueHingeAxis_ - sCouplCorrectionT_) < (-1 * _springCoupling.minTorque))
    {
      //sCouplCase_ = 1;

      if(sCouplDeflection_ < (-1 * _springCoupling.maxDeflection))
      {
        // springCoupling can't take up any more torque
      }
      else
      {
        sCouplCorrectionT_ += (-1 * _springCoupling.minTorque) - torqueHingeAxis_;
        // springCoupling is compressed "left"
      }
    }
    else if(sCouplDeflection_ != 0.0)
    {
      //sCouplCase_ = 2;

      if(sCouplDeflection_ > 0.0)
      {
        // spring Coupling gvies back torque "right"
      }
      else
      {
        // spring Coupling gvies back torque "left"
      }
    }
    else
    {
      //sCouplCase_ = 3;

      // torque is simply passed through
      sCouplCorrectionT_ = 0.0;
    }

    accuMaxTorqueStop_ += fabs(sCouplCorrectionT_);                    
  }
}

void ComplexHinge::checkAbortConditions()
{
  if(_isPositionExceedable)
  {
    if((hingeAngle_ < _minExceedPosition) || (hingeAngle_ > _maxExceedPosition))
    {
      (*(YarsContainers::AbortCondition())).abortCondition = true;
      (*(YarsContainers::AbortCondition())).abortSource    = _name;
      (*(YarsContainers::AbortCondition())).abortCause     =
        DEF_ABORT_CAUSE_JOINT_POSITION_EXCEEDED;
    }
  }

  if(_isForceExceedable)
  {
    if(fabs(torqueHingeAxis_) > _minExceedForce)
    {
      (*(YarsContainers::AbortCondition())).abortCondition = true;
      (*(YarsContainers::AbortCondition())).abortSource    = _name;
      (*(YarsContainers::AbortCondition())).abortCause     =
        DEF_ABORT_CAUSE_JOINT_FORCE_EXCEEDED;
    }
  }
}

//
// This is called only once in the beginning (from the constructor)
// --> calculate constants for torque measurements (inertia, body to world ...)
//
void ComplexHinge::prepareTorqueCalc()
{
  dVector3 hingeAxisB0Coord;    // hinge axis in coord syst. of body 0
                                //   (normalized)
  double   inertiaJointAxisB0;  // inertia around joint axis translated
                                //   in B0 POR
  double   aB0Length;           // length of axis-Body0 Vector
                                
  dJointGetHingeAxis(*joint_, hingeAxis_);
  dJointGetHingeAnchor(*joint_, hingeAnchor_);

  body0Rot_  = dBodyGetRotation(*body0_);
  body0Pos_  = dBodyGetPosition(*body0_);
  dBodyGetMass(*body0_, &massB0_);

  // initialize aB0Length if we do not do it every step and assume that they
  //   don't change much (due to errors/softness in ode)
  aB0Length = calculateAB0Vector();

  // Calculate the factor to convert torque in body frame to torque around hinge
  //   axis via:
  //
  //   a) express joint rotation axis in body coordinate system
  //      --> body rot matrix * hinge axis
  //      --> normalized vector multiplied with body torque vector will give us
  //         torque around hinge axis
  hingeAxisB0Coord[0] =   body0Rot_[0]  * hingeAxis_[0]
                         + body0Rot_[1]  * hingeAxis_[1]
                         + body0Rot_[2]  * hingeAxis_[2]; 
  hingeAxisB0Coord[1] =   body0Rot_[4]  * hingeAxis_[0]
                         + body0Rot_[5]  * hingeAxis_[1]
                         + body0Rot_[6]  * hingeAxis_[2];
  hingeAxisB0Coord[2] =   body0Rot_[8]  * hingeAxis_[0]
                         + body0Rot_[9]  * hingeAxis_[1]
                         + body0Rot_[10] * hingeAxis_[2];

  // normalize resulting vector
  dNormalize3(hingeAxisB0Coord);

  //   b) calculate inertia of body with respect to rotation axis in the body
  //        coordinate system and translated so that it passes through the center
  //        of mass (which in the current implementation of ODE must coincide
  //        with the POR - if this changes we have to expand this):
  //        inertiaJointAxisB0 = (hingeAxisB0Coord)^T 
  //                            * ( (*massB0_).I * hingeAxisB0Coord )
  inertiaJointAxisB0 = (massB0_).I[0]  * hingeAxisB0Coord[0] * hingeAxisB0Coord[0]
                      + (massB0_).I[1]  * hingeAxisB0Coord[0] * hingeAxisB0Coord[1]
                      + (massB0_).I[2]  * hingeAxisB0Coord[0] * hingeAxisB0Coord[2]
                      + (massB0_).I[4]  * hingeAxisB0Coord[1] * hingeAxisB0Coord[0]
                      + (massB0_).I[5]  * hingeAxisB0Coord[1] * hingeAxisB0Coord[1]
                      + (massB0_).I[6]  * hingeAxisB0Coord[1] * hingeAxisB0Coord[2]
                      + (massB0_).I[8]  * hingeAxisB0Coord[2] * hingeAxisB0Coord[0]
                      + (massB0_).I[9]  * hingeAxisB0Coord[2] * hingeAxisB0Coord[1]
                      + (massB0_).I[10] * hingeAxisB0Coord[2] * hingeAxisB0Coord[2];

  //   c) calculate the factor for the inertia if a parallel translation of the
  //        rotation axis back to the joint axis is done (using the Steiner's
  //        theorem):
  //        - Factor = (inertiaJointAxisB0 + m * d²) / inertiaJointAxisB0
  torqueAxisTransFac_ = (inertiaJointAxisB0 
                          + ( (massB0_).mass * aB0Length * aB0Length)) 
                        / inertiaJointAxisB0; 

//  printf("\n");
//  printf("torqueAxisTransFac_ = %+6.4f\n", torqueAxisTransFac_);
//  printf("\n");

  // setup filter for the joint Torque thing
  mAForceFilter_    = new MovingAverage(_forceFeedbackFilter.windowSize,
      _forceFeedbackFilter.initValue, _forceFeedbackFilter.recursive,
      _forceFeedbackFilter.fullRecalc);
  mAForceFilter2_   = new MovingAverage(_forceFeedbackFilter.windowSize,
      _forceFeedbackFilter.initValue, _forceFeedbackFilter.recursive,
      _forceFeedbackFilter.fullRecalc);
}

//
// Calculate Torque around joint axis using the "torqueAxisTransFac_" factor
//   calculated in the "prepareTorqueCalc()" function:
// - map the torque vector onto the hinge axis in body coordinates
// - multiply it with the factor "torqueAxisTransFac_"
// - the result is the torque around the hinge axis applied by the joint to
//     the bodies during the last time step
//
void ComplexHinge::calcTorqueAroundJointAxis()
{
  // we assume that rotation of hinge axis doesn't change relative to body 0
  //   rotation
  dJointGetHingeAxis(*joint_, hingeAxis_);
  dNormalize3(hingeAxis_);


  // project torque on hinge axis --> feedback is given in global coordinates
  torqueHingeAxisB0_ = hingeAxis_[0] * feedback_->t1[0] 
                     + hingeAxis_[1] * feedback_->t1[1] 
                     + hingeAxis_[2] * feedback_->t1[2];

  torqueAMotorAxisB0_ = hingeAxis_[0] * feedbackAMotor1_->t1[0] 
                      + hingeAxis_[1] * feedbackAMotor1_->t1[1] 
                      + hingeAxis_[2] * feedbackAMotor1_->t1[2];

  // account for hinge axis not passing through POR of body 0
  //torqueHingeAxis_   = torqueAxisTransFac_ * torqueHingeAxisB0_;
  torqueHingeAxis_   = torqueHingeAxisB0_;
  if((! initDone_) || (! isfinite(torqueHingeAxis_)))
  {
    torqueHingeAxis_ = 0.0;
  }
//  torqueHingeAxis_   = mAForceFilter_->getFilteredOutput(torqueHingeAxis_);

  //torqueAMotorAxis_   = torqueAxisTransFac_ * torqueAMotorAxisB0_;
  torqueAMotorAxis_   = torqueAMotorAxisB0_;
  if((! initDone_) || (! isfinite(torqueAMotorAxis_)))
  {
    torqueAMotorAxis_ = 0.0;
  }
//  torqueAMotorAxis_   = mAForceFilter2_->getFilteredOutput(torqueAMotorAxis_);

  // before the first step, feedback structs may contain anything, so we ignored
  //   it, now we can use it
  initDone_ = true;

  return;
}

//
// Calculate Torque around joint axis by adding torques from the feedback
// structure and orthogonal forces from the feedback structure
//
// - transforming world into body coordinates
//
void ComplexHinge::calcTorqueAroundJointAxis2()
{
    dVector3 ht1, ht2;
    dVector3 at1, at2;
    dVector3 hcross1, hcross2;
    dVector3 across1, across2;
    dVector3 jointBody1PosDiff, jointBody2PosDiff;

    dJointGetHingeAnchor(*joint_, jointBody1PosDiff);
    dJointGetHingeAnchor(*joint_, jointBody2PosDiff);
    dJointGetHingeAxis(*joint_, hingeAxis_);
    // we assume that rotation of hinge axis doesn't change relative to body 0
    //   rotation
    dNormalize3(hingeAxis_);

    jointBody1PosDiff[0] = body0Pos_[0] - jointBody1PosDiff[0];
    jointBody1PosDiff[1] = body0Pos_[1] - jointBody1PosDiff[1];
    jointBody1PosDiff[2] = body0Pos_[2] - jointBody1PosDiff[2];
    jointBody2PosDiff[0] = body1Pos_[0] - jointBody2PosDiff[0];
    jointBody2PosDiff[1] = body1Pos_[1] - jointBody2PosDiff[1];
    jointBody2PosDiff[2] = body1Pos_[2] - jointBody2PosDiff[2];

    hcross1[0] = (jointBody1PosDiff[2] * feedback_->f1[1] - jointBody1PosDiff[1] * feedback_->f1[2]);
    hcross1[1] = (jointBody1PosDiff[0] * feedback_->f1[2] - jointBody1PosDiff[2] * feedback_->f1[0]);
    hcross1[2] = (jointBody1PosDiff[1] * feedback_->f1[0] - jointBody1PosDiff[0] * feedback_->f1[1]);

    hcross2[0] = (jointBody2PosDiff[2] * feedback_->f2[1] - jointBody2PosDiff[1] * feedback_->f2[2]);
    hcross2[1] = (jointBody2PosDiff[0] * feedback_->f2[2] - jointBody2PosDiff[2] * feedback_->f2[0]);
    hcross2[2] = (jointBody2PosDiff[1] * feedback_->f2[0] - jointBody2PosDiff[0] * feedback_->f2[1]);

    ht1[0] = -1 * feedback_->t1[0] + hcross1[0];
    ht1[1] = -1 * feedback_->t1[1] + hcross1[1];
    ht1[2] = -1 * feedback_->t1[2] + hcross1[2];

    ht2[0] = -1 * feedback_->t2[0] + hcross2[0];
    ht2[1] = -1 * feedback_->t2[1] + hcross2[1];
    ht2[2] = -1 * feedback_->t2[2] + hcross2[2];

    ht1[0] = (ht1[0] - ht2[0]) / 2;
    ht1[1] = (ht1[1] - ht2[1]) / 2;
    ht1[2] = (ht1[2] - ht2[2]) / 2;

    across1[0] = (jointBody1PosDiff[2] * feedbackAMotor1_->f1[1] - jointBody1PosDiff[1] * feedbackAMotor1_->f1[2]);
    across1[1] = (jointBody1PosDiff[0] * feedbackAMotor1_->f1[2] - jointBody1PosDiff[2] * feedbackAMotor1_->f1[0]);
    across1[2] = (jointBody1PosDiff[1] * feedbackAMotor1_->f1[0] - jointBody1PosDiff[0] * feedbackAMotor1_->f1[1]);

    across2[0] = (jointBody2PosDiff[2] * feedbackAMotor1_->f2[1] - jointBody2PosDiff[1] * feedbackAMotor1_->f2[2]);
    across2[1] = (jointBody2PosDiff[0] * feedbackAMotor1_->f2[2] - jointBody2PosDiff[2] * feedbackAMotor1_->f2[0]);
    across2[2] = (jointBody2PosDiff[1] * feedbackAMotor1_->f2[0] - jointBody2PosDiff[0] * feedbackAMotor1_->f2[1]);

    at1[0] = feedbackAMotor1_->t1[0] - across1[0];
    at1[1] = feedbackAMotor1_->t1[1] - across1[1];
    at1[2] = feedbackAMotor1_->t1[2] - across1[2];

    at2[0] = feedbackAMotor1_->t2[0] - across2[0];
    at2[1] = feedbackAMotor1_->t2[1] - across2[1];
    at2[2] = feedbackAMotor1_->t2[2] - across2[2];

    at1[0] = (at1[0] - at2[0]) / 2;
    at1[1] = (at1[1] - at2[1]) / 2;
    at1[2] = (at1[2] - at2[2]) / 2;

    // project torque on hinge axis --> feedback is given in global coordinates
    torqueHingeAxisB0_ = hingeAxis_[0] * ht1[0] 
                       + hingeAxis_[1] * ht1[1] 
                       + hingeAxis_[2] * ht1[2];

    torqueAMotorAxisB0_ = hingeAxis_[0] * at1[0] 
                        + hingeAxis_[1] * at1[1] 
                        + hingeAxis_[2] * at1[2];

    torqueHingeAxis_   = torqueHingeAxisB0_;
    if((! initDone_) || (! isfinite(torqueHingeAxis_)))
    {
      torqueHingeAxis_ = 0.0;
    }
//    torqueHingeAxis_   = mAForceFilter_->getFilteredOutput(torqueHingeAxis_);

    torqueAMotorAxis_   = torqueAMotorAxisB0_;
    if((! initDone_) || (! isfinite(torqueAMotorAxis_)))
    {
      torqueAMotorAxis_ = 0.0;
    }
//      torqueAMotorAxis_   = mAForceFilter2_->getFilteredOutput(torqueAMotorAxis_);

    // before the first step, feedback structs may contain anything, so we ignored
    //   it, now we can use it
    initDone_ = true;

    return;
}

double ComplexHinge::calculateAB0Vector()
{
  dVector3 hingeAnchorBodyVec; // vector from anchor to body center
  dVector3 sPB0Anchor;         // point on axis with shortest dist. to
                               //   body 0 center
  dVector3 aB0Vector;          // axis-Body0 Vector
  double   t;                 // temp variable for the linear equation
                               //  sPB0Anchor_ = hingeAnchor + t*axisVec
  
  // Calculates t which is the factor in the linear equation that gives the
  // point on the hinge axis with the shortest distance from the body center:
  // point = hingeAnchor_ + t * hingeAxis_
  hingeAnchorBodyVec [0] = body0Pos_[0] - hingeAnchor_[0]; 
  hingeAnchorBodyVec [1] = body0Pos_[1] - hingeAnchor_[1]; 
  hingeAnchorBodyVec [2] = body0Pos_[2] - hingeAnchor_[2]; 

  t = (  hingeAxis_[0] * hingeAnchorBodyVec[0]
        + hingeAxis_[1] * hingeAnchorBodyVec[1]
        + hingeAxis_[2] * hingeAnchorBodyVec[2])
      /(  hingeAxis_[0] * hingeAxis_[0]
        + hingeAxis_[1] * hingeAxis_[1]
        + hingeAxis_[2] * hingeAxis_[2]);

  // point on hinge axis with shortest distance to body center
  sPB0Anchor[0] = hingeAnchor_[0] + t * hingeAxis_[0]; 
  sPB0Anchor[1] = hingeAnchor_[1] + t * hingeAxis_[1]; 
  sPB0Anchor[2] = hingeAnchor_[2] + t * hingeAxis_[2]; 

  // orthogonal vector on hinge axis to body center
  aB0Vector[0] = body0Pos_[0] - sPB0Anchor[0];
  aB0Vector[1] = body0Pos_[1] - sPB0Anchor[1];
  aB0Vector[2] = body0Pos_[2] - sPB0Anchor[2];

  return getVectorLength(aB0Vector);
}

double ComplexHinge::getVectorLength(dVector3 vector)
{
  return sqrt(  vector[0] * vector[0]
              + vector[1] * vector[1]
              + vector[2] * vector[2]);
}

void ComplexHinge::resetFeedback(dJointFeedback *feedback, double value)
{
  feedback->f1[0] = value;
  feedback->f1[1] = value;
  feedback->f1[2] = value;
  feedback->t1[0] = value;
  feedback->t1[1] = value;
  feedback->t1[2] = value;
  feedback->f2[0] = value;
  feedback->f2[1] = value;
  feedback->f2[2] = value;
  feedback->t2[0] = value;
  feedback->t2[1] = value;
  feedback->t2[2] = value;
}

void ComplexHinge::printHingeTorqueInformation()
{
  printf("----------------------------------------------------------------------\n");
  printf("hingeAxis_:             %f %f %f \n", 
      hingeAxis_[0], hingeAxis_[1], hingeAxis_[2]);
  printf("hingeAnchor_:           %f %f %f \n", 
      hingeAnchor_[0], hingeAnchor_[1], hingeAnchor_[2]);
  printf("body0Pos_:              %f %f %f \n", 
      body0Pos_[0], body0Pos_[1], body0Pos_[2]);

  printf("---- \n");

  printf("feedback_->f1:         %f %f %f \n", 
      feedback_->f1[0], feedback_->f1[1], feedback_->f1[2]);
  printf("feedback_->t1:         %f %f %f \n", 
      feedback_->t1[0], feedback_->t1[1], feedback_->t1[2]);
  printf("feedback_->f2:         %f %f %f \n", 
      feedback_->f2[0], feedback_->f2[1], feedback_->f2[2]);
  printf("feedback_->t2:         %f %f %f \n", 
      feedback_->t2[0], feedback_->t2[1], feedback_->t2[2]);

  printf("---- \n");

  printf("feedbackAMotor1_->f1:         %f %f %f \n", 
      feedbackAMotor1_->f1[0], feedbackAMotor1_->f1[1], feedbackAMotor1_->f1[2]);
  printf("feedbackAMotor1_->t1:         %f %f %f \n", 
      feedbackAMotor1_->t1[0], feedbackAMotor1_->t1[1], feedbackAMotor1_->t1[2]);
  printf("feedbackAMotor1_->f2:         %f %f %f \n", 
      feedbackAMotor1_->f2[0], feedbackAMotor1_->f2[1], feedbackAMotor1_->f2[2]);
  printf("feedbackAMotor1_->t2:         %f %f %f \n", 
      feedbackAMotor1_->t2[0], feedbackAMotor1_->t2[1], feedbackAMotor1_->t2[2]);

  printf("---- \n");

  printf("torqueHingeAxis_  = %f \n", torqueHingeAxis_);
  printf("torqueAMotorAxis_ = %f \n", torqueAMotorAxis_);

  printf("----------------------------------------------------------------------\n");
}

/* 
 * update MotorCommand[mode, activation] from two antagonist activation inputs
 * --> inputs have to be in the interval ]0, 1[
 * --> output consinsts of mode {released, forward, backward, break} and
 *       activation in the interval ]0, 1[
 */
void ComplexHinge::calcMotorActivationFromAntagonists(
    double agonist, double antagonist)
{
  double a, b, tmp;

  a = agonist;
  b = antagonist;

  // 1. All outputs below 0.1 are considered to be zero
  if(a < MUSCLE_ACTIVATION_THRESHOLD) a = 0;
  if(b < MUSCLE_ACTIVATION_THRESHOLD) b = 0;

  // 2. If both inputs are considered zero, motor is fully released
  if((a == 0) && (b == 0))
  {
    Y_DEBUG("   Motor fully released \n");
    mc_.mode       = MODE_RELEASED;
    mc_.activation = 0;
    return;
  }

  // 3. in the following the term (a-b)/(a+b) is looked at to determine motor
  //    output
  //
  // TODO: consider treating excess torque here

  tmp = (a - b) / (a + b);

  // 4a. Forward torque a-b
  if(tmp >= MOTOR_BREAK_THRESHOLD)
  {
    Y_DEBUG("   Forward torque = %f \n", a - b);
    mc_.mode       = MODE_FORWARD;
    mc_.activation = a - b;
  }

  // 4b. Backward torque b-a
  else if(tmp <= (-1 * MOTOR_BREAK_THRESHOLD))
  {
    Y_DEBUG("   Backward torque = %f \n", b - a);
    mc_.mode       = MODE_BACKWARD;
    mc_.activation = b - a;
  }

  // 4c. Break (a+b)/2
  else {
    Y_DEBUG("   Break = %f \n", (a + b) / 2);
    mc_.mode       = MODE_BREAK;
    mc_.activation = (a + b) / 2;
  }

  return;
}

//
// maps any double input to an output with min = 0.0 and max = 1.0
//
double ComplexHinge::mapPulseWidth(double x, PWMappingDescription pwMD)
{
  double mapped = 0.0;

  switch(pwMD.type)
  {
    case DEF_MAPPING_TYPE_NONE:
      mapped = x; 
      break;
    case DEF_MAPPING_TYPE_CONSTANT:
      if(x < pwMD.minActivation)
      {
        mapped = 0.0;
      }
      else
      {
        mapped = pwMD.constant + pwMD.yOff;
      }
      break;
    case DEF_MAPPING_TYPE_LINEAR:
      if(x < pwMD.minActivation)
      {
        mapped = 0.0;
      }
      else if(x > pwMD.maxActivation)
      {
        mapped = (pwMD.maxActivation + pwMD.xOff) * pwMD.constant + pwMD.yOff;
      }
      else
      {
        mapped = (x + pwMD.xOff) * pwMD.constant + pwMD.yOff;
      }
      break;
    case DEF_MAPPING_TYPE_QUADRATIC:
      if(x < pwMD.minActivation)
      {
        mapped = 0.0;
      }
      else if(x > pwMD.maxActivation)
      {
        mapped = (pwMD.maxActivation + pwMD.xOff) 
               * (pwMD.maxActivation + pwMD.xOff) * pwMD.constant + pwMD.yOff;
      }
      else
      {
        mapped = (x + pwMD.xOff) * (x + pwMD.xOff) * pwMD.constant + pwMD.yOff;
      }
      break;
    case DEF_MAPPING_TYPE_POW:
      if(x < pwMD.minActivation)
      {
        mapped = 0.0;
      }
      else if(x > pwMD.maxActivation)
      {
        mapped = pow((pwMD.maxActivation + pwMD.xOff), pwMD.exponent) 
               * pwMD.constant + pwMD.yOff;
      }
      else
      {
        mapped = pow((x + pwMD.xOff), pwMD.exponent) * pwMD.constant 
               + pwMD.yOff;
      }
      break;
    case DEF_MAPPING_TYPE_SQRT:
      if(x < pwMD.minActivation)
      {
        mapped = 0.0;
      }
      else if(x > pwMD.maxActivation)
      {
        mapped = sqrt(pwMD.maxActivation + pwMD.xOff) * pwMD.constant 
               + pwMD.yOff;
      }
      else
      {
        mapped = sqrt(x + pwMD.xOff) * pwMD.constant + pwMD.yOff;
      }
      break;
    case DEF_MAPPING_TYPE_LINEAR_INTERPOLATION:
      if(x < pwMD.interpolationData[0].x)
      {
        mapped = pwMD.interpolationData[0].y;
      }
      else if(x >= pwMD.interpolationData.back().x)
      {
        mapped = pwMD.interpolationData.back().y;
      }
      else
      {
        // should be efficient for small data sets, otherwise we should use a
        // better algorithm
        for(int i=pwMD.interpolationData.size()-2; i>=0; i--)
        {
          if(x >= pwMD.interpolationData[i].x)
          {
            mapped = pwMD.interpolationData[i].y 
                   + (x - pwMD.interpolationData[i].x) 
                   * pwMD.interpolationData[i].slope;
            break;
          }
        }
      }
      break;
    default:
      ;
  }

  // consistency check
  if(mapped<0.0)
  {
    mapped = 0.0;
  }
  else if(mapped>1.0)
  {
    mapped = 1.0;
  }

  return mapped;
}



/********************************************************************* 
 *
 * Constructor sub functions
 *
 *********************************************************************/

double ComplexHinge::calculateStaticDynamicFrictionTransitionSlope(
    JointFrictionDescription *jFD)
{
  double transitionSlope = 0.0;

  switch(jFD->dynamicType)
  {
    case DEF_DYNAMIC_FRICTION_TYPE_CONSTANT:
      transitionSlope = 
        ((jFD->dynamicConstant + jFD->dynamicOffset) - jFD->staticMaxForce) 
        / jFD->dynamicMinVel;
      break;
    case DEF_DYNAMIC_FRICTION_TYPE_LINEAR:
      transitionSlope = 
        ((jFD->dynamicConstant * jFD->dynamicMinVel + jFD->dynamicOffset) 
         - jFD->staticMaxForce) 
        / jFD->dynamicMinVel;
      break;
    case DEF_DYNAMIC_FRICTION_TYPE_QUADRATIC:
      transitionSlope = 
        ((jFD->dynamicConstant * jFD->dynamicMinVel * jFD->dynamicMinVel
          + jFD->dynamicOffset) 
         - jFD->staticMaxForce
        ) 
        / jFD->dynamicMinVel;
      break;
    default:
      ;
  }

  return transitionSlope;
}

void ComplexHinge::copyDataFromConnectorDescription(ConnectorDescription *connStruct)
{
  _name                          = connStruct->name;

  _motor.stallTorque             = connStruct->motor.stallTorque;
  _motor.maxContinuousTorque     = connStruct->motor.maxContinuousTorque;
  _motor.noLoadSpeed             = connStruct->motor.noLoadSpeed;
  _motor.mechanicalTimeConstant  = connStruct->motor.mechanicalTimeConstant;
  _motor.maxEfficiency           = connStruct->motor.maxEfficiency;
  _motor.brakeEfficiency         = connStruct->motor.brakeEfficiency;
  _motor.brakeFriction.active          = connStruct->motor.brakeFriction.active         ;
  _motor.brakeFriction.staticActive    = connStruct->motor.brakeFriction.staticActive   ;
  _motor.brakeFriction.dynamicActive   = connStruct->motor.brakeFriction.dynamicActive  ;
  _motor.brakeFriction.minVelActive    = connStruct->motor.brakeFriction.minVelActive   ;
  _motor.brakeFriction.staticMaxForce  = connStruct->motor.brakeFriction.staticMaxForce ;
  _motor.brakeFriction.staticVelThres  = connStruct->motor.brakeFriction.staticVelThres ;
  _motor.brakeFriction.dynamicConstant = connStruct->motor.brakeFriction.dynamicConstant;
  _motor.brakeFriction.dynamicType     = connStruct->motor.brakeFriction.dynamicType  ;
  _motor.brakeFriction.dynamicMinVel   = connStruct->motor.brakeFriction.dynamicMinVel;
  _motor.brakeFriction.dynamicOffset   = connStruct->motor.brakeFriction.dynamicOffset;
  _motor.torqueCurrentConstant   = connStruct->motor.torqueCurrentConstant;
  _motor.speedVoltageConstant    = connStruct->motor.speedVoltageConstant;
  _motor.pwmThreshold            = connStruct->motor.pwmThreshold;

  copyPWMappingData(&(_motor.pwToStallTorque), &(connStruct->motor.pwToStallTorque));
  copyPWMappingData(&(_motor.pwToStallCurrent), &(connStruct->motor.pwToStallCurrent));
  copyPWMappingData(&(_motor.pwToNoLoadSpeed), &(connStruct->motor.pwToNoLoadSpeed));
  copyPWMappingData(&(_motor.pwToNoLoadCurrent), &(connStruct->motor.pwToNoLoadCurrent));
  copyPWMappingData(&(_motor.pwToBrakeEfficiency), &(connStruct->motor.pwToBrakeEfficiency));

  _motor.noise[0]                = connStruct->motor.noise[0];
  _motor.noiseType[0]            = connStruct->motor.noiseType[0];
  _motor.noise[1]                = connStruct->motor.noise[1];
  _motor.noiseType[1]            = connStruct->motor.noiseType[1];

  _gear.active                   = connStruct->gear.active;
  _gear.transmissionRatio        = connStruct->gear.transmissionRatio;
  _gear.backlash                 = connStruct->gear.backlash;
  _gear.efficiency               = connStruct->gear.efficiency;
  _gear.initialBacklashPosition  = connStruct->gear.initialBacklashPosition;
  _gear.noise                    = connStruct->gear.noise;
  _gear.noiseType                = connStruct->gear.noiseType;

  _springCoupling.active         = connStruct->springCoupling.active;
  _springCoupling.minTorque      = connStruct->springCoupling.minTorque;
  _springCoupling.springConstant = connStruct->springCoupling.springConstant;
  _springCoupling.maxDeflection  = connStruct->springCoupling.maxDeflection;
  _springCoupling.noise          = connStruct->springCoupling.noise;
  _springCoupling.noiseType      = connStruct->springCoupling.noiseType;

  _spring.active                 = connStruct->spring.active;
  _spring.springConstant         = connStruct->spring.springConstant;
  _spring.initialPosition        = connStruct->spring.initialPosition;
  _spring.noise                  = connStruct->spring.noise;
  _spring.noiseType              = connStruct->spring.noiseType;

  _damper.active                 = connStruct->damper.active;
  _damper.dampingConstant        = connStruct->damper.dampingConstant;

  _jointFriction.active          = connStruct->jointFriction.active;
  _jointFriction.staticActive    = connStruct->jointFriction.staticActive;
  _jointFriction.dynamicActive   = connStruct->jointFriction.dynamicActive;
  _jointFriction.minVelActive    = connStruct->jointFriction.minVelActive;
  _jointFriction.staticMaxForce  = connStruct->jointFriction.staticMaxForce;
  _jointFriction.staticVelThres  = connStruct->jointFriction.staticVelThres;
  _jointFriction.dynamicConstant = connStruct->jointFriction.dynamicConstant;
  _jointFriction.dynamicType     = connStruct->jointFriction.dynamicType;
  _jointFriction.dynamicMinVel   = connStruct->jointFriction.dynamicMinVel;
  _jointFriction.dynamicOffset   = connStruct->jointFriction.dynamicOffset;

  _forceFeedbackFilter.type       = connStruct->forceFeedbackFilter.type;
  _forceFeedbackFilter.windowSize = connStruct->forceFeedbackFilter.windowSize;
  _forceFeedbackFilter.initValue  = connStruct->forceFeedbackFilter.initValue;
  _forceFeedbackFilter.recursive  = connStruct->forceFeedbackFilter.recursive;
  _forceFeedbackFilter.fullRecalc = connStruct->forceFeedbackFilter.fullRecalc;

  _stopTorqueAccuFilter.type       = connStruct->stopTorqueAccuFilter.type;
  _stopTorqueAccuFilter.windowSize = connStruct->stopTorqueAccuFilter.windowSize;
  _stopTorqueAccuFilter.initValue  = connStruct->stopTorqueAccuFilter.initValue;
  _stopTorqueAccuFilter.recursive  = connStruct->stopTorqueAccuFilter.recursive;
  _stopTorqueAccuFilter.fullRecalc = connStruct->stopTorqueAccuFilter.fullRecalc;

  _isPositionExceedable          = connStruct->isPositionExceedable;
  _minExceedPosition             = connStruct->minExceedPosition;
  _maxExceedPosition             = connStruct->maxExceedPosition;
  _isForceExceedable             = connStruct->isForceExceedable;
  _minExceedForce                = connStruct->minExceedForce;
}

void ComplexHinge::copyPWMappingData(PWMappingDescription *pwMD, PWMappingDescription *cSPWMD)
{
  pwMD->type              = cSPWMD->type;    
  pwMD->minActivation     = cSPWMD->minActivation;    
  pwMD->maxActivation     = cSPWMD->maxActivation;    
  pwMD->xOff              = cSPWMD->xOff;    
  pwMD->yOff              = cSPWMD->yOff;    
  pwMD->constant          = cSPWMD->constant;    
  pwMD->interpolationData = cSPWMD->interpolationData;
}

void ComplexHinge::printoutDerivedParameters()
{
  Y_DEBUG("ComplexHinge.cpp: _motor.maxContinuousTorque = %f", 
      _motor.maxContinuousTorque);
  Y_DEBUG("ComplexHinge.cpp: _motor.noLoadSpeed= %f", 
      _motor.noLoadSpeed);
  Y_DEBUG("ComplexHinge.cpp: _gear.efficiency = %f", 
      _gear.efficiency);
  Y_DEBUG("ComplexHinge.cpp: _gear.transmissionRatio = %f", 
      _gear.transmissionRatio);
  Y_DEBUG("ComplexHinge.cpp: totalMaxContinuousTorque_ = %f", 
      totalMaxContinuousTorque_);
  Y_DEBUG("ComplexHinge.cpp: totalNoLoadSpeed = %f", 
      totalNoLoadSpeed_);
}

void ComplexHinge::calculateDerivedParameters()
{
  totalEfficiency_               = _motor.maxEfficiency;
  totalMaxContinuousTorque_      = _motor.maxContinuousTorque;
  totalMaxContBrakeTorque_       = _motor.maxContinuousTorque 
                                  * _motor.brakeEfficiency;  // TODO
  totalNoLoadSpeed_              = _motor.noLoadSpeed;

  if(_gear.active)
  {
    totalEfficiency_            *= _gear.efficiency;
    totalMaxContinuousTorque_   *= (_gear.efficiency * _gear.transmissionRatio);
    totalMaxContBrakeTorque_    *= (_gear.efficiency * _gear.transmissionRatio);
    totalNoLoadSpeed_           /= _gear.transmissionRatio;

    backlashMin_                 = -1 * _gear.backlash * 0.5;
    backlashMax_                 =      _gear.backlash * 0.5;
    backlashMinTol_              = backlashMin_ * 0.95;
    backlashMaxTol_              = backlashMax_ * 0.95;
  }


  if(_damper.active == true)
  {
    totalDamping_ = _damper.dampingConstant;
  }
  else
  {
    totalDamping_   = 0;
  }

  if(_jointFriction.minVelActive == true)
  {
    jointFrictionTransitionSlope_ =
      calculateStaticDynamicFrictionTransitionSlope(&_jointFriction);
  }
  if(_motor.brakeFriction.minVelActive == true)
  {
    motorBrakeFrictionTransitionSlope_ = 
      calculateStaticDynamicFrictionTransitionSlope(&(_motor.brakeFriction));
  }

  // Filter to smoothen damping (otherwise oscillations due to discrete updates
  //                             may occur)
  mADamperFilter_   = new MovingAverage(_stopTorqueAccuFilter.windowSize,
      _stopTorqueAccuFilter.initValue, _stopTorqueAccuFilter.recursive,
      _stopTorqueAccuFilter.fullRecalc);

  if(_spring.active)
  {
    totalSpringConstant_         = _spring.springConstant;
  }
  else
  {
    totalSpringConstant_           = 0;
  }
}

void ComplexHinge::setupJointAndFeedback(dJointID *joint, dBodyID *sourceBody, dBodyID
  *aimBody)
{
  // locally store joint information
  joint_    = joint;
  dJointGetHingeAxis(*joint_, hingeAxis_);
  dJointGetHingeAnchor (*joint_, hingeAnchor_);
  body0_    = sourceBody;
  body1_    = aimBody;
  body0Pos_ = dBodyGetPosition(*body0_);
  body1Pos_ = dBodyGetPosition(*body1_);

  // allocate feedback structure (but only if it doesn't exist yet)
  feedback_ = dJointGetFeedback(*joint_);
  if (feedback_ == 0)
  {
    dJointSetFeedback(*joint_, new dJointFeedback);
    feedback_ = dJointGetFeedback(*joint_);
  }  

  // create AMotor joint for the active part
  jointGroup_ = dJointGroupCreate(0);
  jointAMotor_ = dJointCreateAMotor (dBodyGetWorld(*aimBody), jointGroup_);
  dJointAttach(jointAMotor_, *sourceBody, *aimBody);
  dJointSetAMotorMode(jointAMotor_, dAMotorUser);
  dJointSetAMotorNumAxes(jointAMotor_, 1);
  dJointSetAMotorAxis(jointAMotor_, 0, 2, hingeAxis_[0], hingeAxis_[1],
      hingeAxis_[2]);
  dJointSetAMotorAngle(jointAMotor_, 0, 0.0);

  // allocate feedback structure for motor
  feedbackAMotor1_ = dJointGetFeedback(jointAMotor_);
  if (feedbackAMotor1_ == 0)
  {
    dJointSetFeedback(jointAMotor_, new dJointFeedback);
    feedbackAMotor1_ = dJointGetFeedback(jointAMotor_);
  }  

  // initialise the motors
  dJointSetHingeParam(*joint_,        dParamFMax, 0.0);
  dJointSetHingeParam(*joint_,        dParamVel,  0.0);
  dJointSetAMotorParam (jointAMotor_, dParamFMax, 0.0);
  dJointSetAMotorParam (jointAMotor_, dParamVel,  0.0);
  feedback_->f1[0]        = 0.0;
  feedback_->f1[1]        = 0.0;
  feedback_->f1[2]        = 0.0;
  feedback_->t1[0]        = 0.0;
  feedback_->t1[1]        = 0.0;
  feedback_->t1[2]        = 0.0;
  feedbackAMotor1_->f1[0] = 0.0;
  feedbackAMotor1_->f1[1] = 0.0;
  feedbackAMotor1_->f1[2] = 0.0;
  feedbackAMotor1_->t1[0] = 0.0;
  feedbackAMotor1_->t1[1] = 0.0;
  feedbackAMotor1_->t1[2] = 0.0;
}

/********************************************************************* 
 *
 * End Constructor sub functions
 *
 *********************************************************************/


/********************************************************************* 
 *
 * Debug + Test functions
 *
 * TODO: remove after testing
 *
 *********************************************************************/
void ComplexHinge::setTestTorques(int *currCase, double *aMotorFMax, double
    *sForceAdd, double *aForceAdd)
{
  int chunkSize = 30;
  double accuMaxTorqueTotal_ = 0;
  double maxVelTotal_        = 0;
  double aMotorVel           = 0;
  double dampingFactor = 0.10;
  double forceFactor   = 1;

  if(currTime_ <= 1 * chunkSize)
  {
    *currCase = 0;
    accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 2 * chunkSize)
  {
    *currCase = 1;
    accuMaxTorqueTotal_ = ((1.0 * currTime_ - chunkSize) / chunkSize) * forceFactor;
    maxVelTotal_        = 10000;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 3 * chunkSize)
  {
    *currCase = 2;
    accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 4 * chunkSize)
  {
    *currCase = 3;
    accuMaxTorqueTotal_ = 0;
    maxVelTotal_        = 0;
    *aMotorFMax          = ((1.0 * currTime_ - (3*chunkSize)) / chunkSize) * forceFactor;
    aMotorVel           = 10000;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 5 * chunkSize)
  {
    *currCase = 4;
    accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 6 * chunkSize)
  {
    *currCase = 5;
    accuMaxTorqueTotal_ = ((1.0 * currTime_ - (5*chunkSize)) / chunkSize) * forceFactor;
    maxVelTotal_        = 10000;
    *aMotorFMax          = ((1.0 * currTime_ - (5*chunkSize)) / chunkSize) * forceFactor;
    aMotorVel           = 10000;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 7 * chunkSize)
  {
    *currCase = 6;
    accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 8 * chunkSize)
  {
    *currCase = 7;
    accuMaxTorqueTotal_ = 0;
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = ((1.0 * currTime_ - (7*chunkSize)) / chunkSize) * forceFactor;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 9 * chunkSize)
  {
    *currCase = 8;
    accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = 0;
  }
  else if(currTime_ <= 10 * chunkSize)
  {
    *currCase = 9;
    accuMaxTorqueTotal_ = 0;
    maxVelTotal_        = 0;
    *aMotorFMax          = 0;
    aMotorVel           = 0;
    *sForceAdd = 0;
    *aForceAdd = ((1.0 * currTime_ - (9*chunkSize)) / chunkSize) * forceFactor;
  }
  else
  {
    currTime_ = 0;
  }

//  if ((currTime_ % 900) >= 600)
//  {
//    accuMaxTorqueTotal_ = 0;
//  }
//  else if ((currTime_ % 900) >= 300)
//  {
//    accuMaxTorqueTotal_ = 0.1;
//  }
//  else
//  {
//    accuMaxTorqueTotal_ = 1.0;
//  }
//  accuMaxTorqueTotal_ = 1;
//  accuMaxTorqueTotal_ = hingeAngleVel_ * hingeAngleVelSign_ * dampingFactor; 
//  maxVelTotal_        = -10000;
//  accuMaxTorqueTotal_ = 7.01;
//  maxVelTotal_        = 0.0;

//  if ((currTime_ % 900) >= 600)
//  {
//    *aMotorFMax = 0.000000000001;
//  }
//  else if ((currTime_ % 900) >= 300)
//  {
//    *aMotorFMax = 0.1;
//  }
//  else
//  {
//    *aMotorFMax = 1.0;
//  }
    //*aMotorFMax          = 25 / torqueAxisTransFac_;
//  if(currTime_ < 5)
//  {
//    *aMotorFMax          = 1.000000001;
//  }
//  else
//  {
//    *aMotorFMax          = 0.0;
//  }
//  *aMotorFMax          = 5.01000000000000000001; //9.890000000000001; // torqueAxisTransFac_;
//  aMotorVel           = 0.0;

  if(*aMotorFMax == 0.0)
  {
    // reset the feedback struct because otherwise the old values will persist
    //   until amotor is turned on again
    resetFeedback(feedbackAMotor1_, 0.0);
  }

  dBodyAddForce(*body0_, *sForceAdd, *sForceAdd, *sForceAdd);
  dBodyAddForce(*body1_, *aForceAdd, *aForceAdd, *aForceAdd);
  dJointSetAMotorParam (jointAMotor_, dParamFMax, *aMotorFMax         );
  dJointSetAMotorParam (jointAMotor_, dParamVel,  aMotorVel          );
}

void ComplexHinge::printoutUpdateDebugInfo(int currCase, double aMotorFMax,
    double sForceAdd, double aForceAdd)
{
  //  Y_DEBUG("desVel.: %f, desForce: %f, angle: %f, vel.: %f", maxVelTotal_,
  //      accuMaxTorqueTotal_, hingeAngle_, hingeAngleVel_);

  //  printf("Hinge pos: %+6.4f,  vel: %+6.4f,  acc: %+6.4f,  tor: %+6.4f, sCCT: %+6.4f, sCCs: %d, mc_.act: %+6.4f, mc_.mode: %d, torM: %+6.4f, torC: %+6.4f, velC: %+6.4f\n", 
  //      hingeAngle_, hingeAngleVel_, accel, torqueHingeAxis_, sCouplCorrectionT_,
  //      sCouplCase_, mc_.activation, mc_.mode, torqueFromMotor_,
  //      accuMaxTorqueTotal_, maxVelTotal_); // debugging stuff

  //  dVector3 aMotorAxisTmp;
  //  dVector3 hingeAxisTmp;
  //  dJointGetAMotorAxis(jointAMotor_, 0, aMotorAxisTmp);
  //  dJointGetHingeAxis(*joint_, hingeAxisTmp);
  //  printf("Hinge: x: %+6.4f, y: %+6.4f, z: %+6.4f\n", hingeAxisTmp[0],
  //      hingeAxisTmp[1], hingeAxisTmp[2]);
  //  printf("AMoto: x: %+6.4f, y: %+6.4f, z: %+6.4f\n", aMotorAxisTmp[0],
  //      aMotorAxisTmp[1], aMotorAxisTmp[2]);
  //  printf("\n");

  if((currTime_ % 1) == 0)
  {
    dVector3 ht1, ht2;
    dVector3 hcross1, hcross2;
    dVector3 jointBody1PosDiff, jointBody2PosDiff;
    dJointGetHingeAnchor(*joint_, jointBody1PosDiff);
    dJointGetHingeAnchor(*joint_, jointBody2PosDiff);
    jointBody1PosDiff[0] = body0Pos_[0] - jointBody1PosDiff[0];
    jointBody1PosDiff[1] = body0Pos_[1] - jointBody1PosDiff[1];
    jointBody1PosDiff[2] = body0Pos_[2] - jointBody1PosDiff[2];
    jointBody2PosDiff[0] = body1Pos_[0] - jointBody2PosDiff[0];
    jointBody2PosDiff[1] = body1Pos_[1] - jointBody2PosDiff[1];
    jointBody2PosDiff[2] = body1Pos_[2] - jointBody2PosDiff[2];
    //    jointBody1PosDiff[0] -= body0Pos_[0];
    //    jointBody1PosDiff[1] -= body0Pos_[1];
    //    jointBody1PosDiff[2] -= body0Pos_[2];
    //    jointBody2PosDiff[0] -= body1Pos_[0];
    //    jointBody2PosDiff[1] -= body1Pos_[1];
    //    jointBody2PosDiff[2] -= body1Pos_[2];

    hcross1[0] = (jointBody1PosDiff[2] * feedback_->f1[1] - jointBody1PosDiff[1] * feedback_->f1[2]);
    hcross1[1] = (jointBody1PosDiff[0] * feedback_->f1[2] - jointBody1PosDiff[2] * feedback_->f1[0]);
    hcross1[2] = (jointBody1PosDiff[1] * feedback_->f1[0] - jointBody1PosDiff[0] * feedback_->f1[1]);

    hcross2[0] = (jointBody2PosDiff[2] * feedback_->f2[1] - jointBody2PosDiff[1] * feedback_->f2[2]);
    hcross2[1] = (jointBody2PosDiff[0] * feedback_->f2[2] - jointBody2PosDiff[2] * feedback_->f2[0]);
    hcross2[2] = (jointBody2PosDiff[1] * feedback_->f2[0] - jointBody2PosDiff[0] * feedback_->f2[1]);

    //    hcross1[0] = (feedback_->f1[2] * jointBody1PosDiff[1] - feedback_->f1[1] * jointBody1PosDiff[2]);
    //    hcross1[1] = (feedback_->f1[0] * jointBody1PosDiff[2] - feedback_->f1[2] * jointBody1PosDiff[0]);
    //    hcross1[2] = (feedback_->f1[1] * jointBody1PosDiff[0] - feedback_->f1[0] * jointBody1PosDiff[1]);
    //                                                                                                     
    //    hcross2[0] = (feedback_->f2[2] * jointBody2PosDiff[1] - feedback_->f2[1] * jointBody2PosDiff[2]);
    //    hcross2[1] = (feedback_->f2[0] * jointBody2PosDiff[2] - feedback_->f2[2] * jointBody2PosDiff[0]);
    //    hcross2[2] = (feedback_->f2[1] * jointBody2PosDiff[0] - feedback_->f2[0] * jointBody2PosDiff[1]);


    ht1[0] = -1 * feedback_->t1[0] + hcross1[0];
    ht1[1] = -1 * feedback_->t1[1] + hcross1[1];
    ht1[2] = -1 * feedback_->t1[2] + hcross1[2];

    ht2[0] = -1 * feedback_->t2[0] + hcross2[0];
    ht2[1] = -1 * feedback_->t2[1] + hcross2[1];
    ht2[2] = -1 * feedback_->t2[2] + hcross2[2];

    dVector3 at1, at2;

    at1[0] = feedbackAMotor1_->t1[0] 
      - (feedbackAMotor1_->f1[1] * jointBody1PosDiff[2] - feedbackAMotor1_->f1[2] * jointBody1PosDiff[1]);
    at1[1] = feedbackAMotor1_->t1[1] 
      - (feedbackAMotor1_->f1[2] * jointBody1PosDiff[0] - feedbackAMotor1_->f1[0] * jointBody1PosDiff[2]);
    at1[2] = feedbackAMotor1_->t1[2] 
      - (feedbackAMotor1_->f1[0] * jointBody1PosDiff[1] - feedbackAMotor1_->f1[1] * jointBody1PosDiff[0]);

    at2[0] = feedbackAMotor1_->t2[0] 
      - (feedbackAMotor1_->f2[1] * jointBody2PosDiff[2] - feedbackAMotor1_->f2[2] * jointBody2PosDiff[1]);
    at2[1] = feedbackAMotor1_->t2[1] 
      - (feedbackAMotor1_->f2[2] * jointBody2PosDiff[0] - feedbackAMotor1_->f2[0] * jointBody2PosDiff[2]);
    at2[2] = feedbackAMotor1_->t2[2] 
      - (feedbackAMotor1_->f2[0] * jointBody2PosDiff[1] - feedbackAMotor1_->f2[1] * jointBody2PosDiff[0]);

    printf("torqueHingeAxisB0_ = %+6.4f, fH->t1[0] = %+6.4f, fH->t1[1] = %+6.4f, fH->t1[2] = %+6.4f, fH->t2[0] = %+6.4f, fH->t2[1] = %+6.4f, fH->t2[2] = %+6.4f\n",
        torqueHingeAxisB0_,
        feedback_->t1[0],
        feedback_->t1[1],
        feedback_->t1[2],
        feedback_->t2[0],
        feedback_->t2[1],
        feedback_->t2[2]
        );
    printf("                             hcross1[0] = %+6.4f,hcross1[1] = %+6.4f,hcross1[2] = %+6.4f,hcross2[0] = %+6.4f,hcross2[1] = %+6.4f,hcross2[2] = %+6.4f\n",
        hcross1[0],
        hcross1[1],
        hcross1[2],
        hcross2[0],
        hcross2[1],
        hcross2[2]
        );
    printf("                                 ht1[0] = %+6.4f,    ht1[1] = %+6.4f,    ht1[2] = %+6.4f,    ht2[0] = %+6.4f,    ht2[1] = %+6.4f,    ht2[2] = %+6.4f\n",
        ht1[0],
        ht1[1],
        ht1[2],
        ht2[0],
        ht2[1],
        ht2[2]
        );
    printf("                             jB1PosD[0] = %+6.4f,jB1PosD[1] = %+6.4f,jB1PosD[2] = %+6.4f,jB2PosD[0] = %+6.4f,jB2PosD[1] = %+6.4f,jB2PosD[2] = %+6.4f\n",
        jointBody1PosDiff[0],
        jointBody1PosDiff[1],
        jointBody1PosDiff[2],
        jointBody2PosDiff[0],
        jointBody2PosDiff[1],
        jointBody2PosDiff[2]
        );
    printf("                              fH->f1[0] = %+6.4f, fH->f1[1] = %+6.4f, fH->f1[2] = %+6.4f, fH->f2[0] = %+6.4f, fH->f2[1] = %+6.4f, fH->f2[2] = %+6.4f\n",
        feedback_->f1[0],
        feedback_->f1[1],
        feedback_->f1[2],
        feedback_->f2[0],
        feedback_->f2[1],
        feedback_->f2[2]
        );
    printf("torqueAMotoAxisB0_ = %+6.4f, fA->t1[0] = %+6.4f, fA->t1[1] = %+6.4f, fA->t1[2] = %+6.4f, fA->t2[0] = %+6.4f, fA->t2[1] = %+6.4f, fA->t2[2] = %+6.4f\n",
        torqueAMotorAxisB0_,
        feedbackAMotor1_->t1[0],
        feedbackAMotor1_->t1[1],
        feedbackAMotor1_->t1[2],
        feedbackAMotor1_->t2[0],
        feedbackAMotor1_->t2[1],
        feedbackAMotor1_->t2[2]
        );
    printf("                                 at1[0] = %+6.4f,    at1[1] = %+6.4f,    at1[2] = %+6.4f,    at2[0] = %+6.4f,    at2[1] = %+6.4f,    at2[2] = %+6.4f\n",
        at1[0],
        at1[1],
        at1[2],
        at2[0],
        at2[1],
        at2[2]
        );
    printf("                              fA->f1[0] = %+6.4f, fA->f1[1] = %+6.4f, fA->f1[2] = %+6.4f, fA->f2[0] = %+6.4f, fA->f2[1] = %+6.4f, fA->f2[2] = %+6.4f\n",
        feedbackAMotor1_->f1[0],
        feedbackAMotor1_->f1[1],
        feedbackAMotor1_->f1[2],
        feedbackAMotor1_->f2[0],
        feedbackAMotor1_->f2[1],
        feedbackAMotor1_->f2[2]
        );


    printf("%d, tH: %+6.4f, tA: %+6.4f, dTH: %+6.4f, dTA: %+6.4f, sBFA: %+6.4f, sAFA: %6.4f\n", 
        currCase,
        torqueHingeAxis_,
        torqueAMotorAxis_,
        accuMaxTorqueTotal_,
        aMotorFMax,
        sForceAdd,
        aForceAdd
        );
  }
}

//void ComplexHinge::plotAntagonistToMotor()
//{
//  vector<double> x_forward;
//  vector<double> y_forward;
//  vector<double> z_forward;

//  vector<double> x_backward;
//  vector<double> y_backward;
//  vector<double> z_backward;

//  vector<double> x_relaxed;
//  vector<double> y_relaxed;
//  vector<double> z_relaxed;

//  vector<double> x_brake;
//  vector<double> y_brake;
//  vector<double> z_brake;

//  if(gp_.size() < 3)
//    gp_.resize(3);

//  if(gp_[1] == NULL)
//  {
//    gp_[1] = new Gnuplot("lines");
//  }
//  if(gp_[2] == NULL)
//  {
//    gp_[2] = new Gnuplot("lines");
//  }

//  for(double i=0.0; i<=1.0; i+=0.05)
//  {
//    for(double j=0.0; j<=1.0; j+=0.05)
//    {
//      calcMotorActivationFromAntagonists(i, j);

//      switch(mc_.mode)
//      {
//        case MODE_RELEASED:
//          x_relaxed.push_back(i);
//          y_relaxed.push_back(j);
//          z_relaxed.push_back(0.0);
//          break;
//        case MODE_FORWARD:
//          x_forward.push_back(i);
//          y_forward.push_back(j);
//          z_forward.push_back(mc_.activation);
//          break;
//        case MODE_BACKWARD:
//          x_backward.push_back(i);
//          y_backward.push_back(j);
//          z_backward.push_back(-1 * mc_.activation);
//          break;
//        case MODE_BREAK:
//          x_brake.push_back(i);
//          y_brake.push_back(j);
//          z_brake.push_back(mc_.activation);
//          x_brake.push_back(i);
//          y_brake.push_back(j);
//          z_brake.push_back(-1 * mc_.activation);
//          break;
//        default:
//          ;
//      }
//    }
//  }

//  (*(gp_[1])).set_style("points");
//  (*(gp_[1])).set_xrange(0,1);
//  (*(gp_[1])).set_yrange(-1,1);
//  (*(gp_[1])).set_zrange(-1,1);
// //  (*(gp_[1])).plot_xyz(x_relaxed,y_relaxed,z_relaxed,"released");
// //  (*(gp_[1])).plot_xyz(x_forward,y_forward,z_forward,"forward");
// //  (*(gp_[1])).plot_xyz(x_backward,y_backward,z_backward,"backward");
// //  (*(gp_[1])).plot_xyz(x_brake,y_brake,z_brake,"brake");
//  (*(gp_[1])).plot_xy(x_relaxed,z_relaxed,"released");
//  (*(gp_[1])).plot_xy(x_forward,z_forward,"forward");
//  (*(gp_[1])).plot_xy(x_backward,z_backward,"backward");
//  (*(gp_[1])).plot_xy(x_brake,z_brake,"brake");
//  (*(gp_[1])).set_xlabel("Input");
//  (*(gp_[1])).set_ylabel("Output");

//  (*(gp_[2])).set_style("points");
//  (*(gp_[2])).set_xrange(0,1);
//  (*(gp_[2])).set_yrange(-1,1);
//  (*(gp_[2])).set_zrange(-1,1);
//  (*(gp_[2])).plot_xy(y_relaxed,z_relaxed,"released");
//  (*(gp_[2])).plot_xy(y_forward,z_forward,"forward");
//  (*(gp_[2])).plot_xy(y_backward,z_backward,"backward");
//  (*(gp_[2])).plot_xy(y_brake,z_brake,"brake");
//  (*(gp_[2])).set_xlabel("Input");
//  (*(gp_[2])).set_ylabel("Output");
//}

//void ComplexHinge::plotPWMappingFunctions()
//{
//  vector<double> x;
//  vector<double> y;

//  for(int i=0; i<5; i++)
//  {
//    if(gp_.size() < 5)
//      gp_.resize(5);

//    if(gp_[i] == NULL)
//    {
//      gp_[i] = new Gnuplot("lines");
//    }
//  }

// //   pwToStallTorque
//  for(double i=0; i<=1.0; i+=0.05)
//  {
//    x.push_back(i);
//    y.push_back(mapPulseWidth(i, _motor.pwToStallTorque));
//  }

//  (*(gp_[0])).plot_xy(x,y,"pwToStallTorque");
//  (*(gp_[0])).set_xlabel("Input");
//  (*(gp_[0])).set_ylabel("Output");

//  x.clear();
//  y.clear();

// //   pwToStallCurrent
//  for(double i=0; i<=1.0; i+=0.05)
//  {
//    x.push_back(i);
//    y.push_back(mapPulseWidth(i, _motor.pwToStallCurrent));
//  }

//  (*(gp_[0])).plot_xy(x,y,"pwToStallCurrent");
//  (*(gp_[0])).set_xlabel("Input");
//  (*(gp_[0])).set_ylabel("Output");

//  x.clear();
//  y.clear();

// //   pwToNoLoadSpeed //  for(double i=0; i<=1.0; i+=0.05)
//  {
//    x.push_back(i);
//    y.push_back(mapPulseWidth(i, _motor.pwToNoLoadSpeed));
//  }

//  (*(gp_[0])).plot_xy(x,y,"pwToNoLoadSpeed");
//  (*(gp_[0])).set_xlabel("Input");
//  (*(gp_[0])).set_ylabel("Output");

//  x.clear();
//  y.clear();

// //   pwToNoLoadCurrent
//  for(double i=0; i<=1.0; i+=0.05)
//  {
//    x.push_back(i);
//    y.push_back(mapPulseWidth(i, _motor.pwToNoLoadCurrent));
//  }

//  (*(gp_[0])).plot_xy(x,y,"pwToNoLoadCurrent");
//  (*(gp_[0])).set_xlabel("Input");
//  (*(gp_[0])).set_ylabel("Output");

//  x.clear();
//  y.clear();

// //   pwToBrakeEfficiency
//  for(double i=0; i<=1.0; i+=0.05)
//  {
//    x.push_back(i);
//    y.push_back(mapPulseWidth(i, _motor.pwToBrakeEfficiency));
//  }

//  (*(gp_[0])).plot_xy(x,y,"pwToBrakeEfficiency");
//  (*(gp_[0])).set_xlabel("Input");
//  (*(gp_[0])).set_ylabel("Output");
//}

/********************************************************************* 
 *
 * END Debug + Test functions
 *
 *********************************************************************/

