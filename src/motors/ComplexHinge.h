/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/

//
// WARNING: This Class is under heavy development and very experimental, 
//          so don't take it for granted that everything works as expected
//

#ifndef _COMPLEX_HINGE_H_
#define _COMPLEX_HINGE_H_

#include <base/MappingFunction.h>
#include <sensors/AngleFeedbackSensor.h>
#include <description/RobotDescription.h>

#include <util/defines.h>
#include <util/Gauss.h>
#include <util/filter/MovingAverage.h>

//#include <plot/gnuplot/gnuplot_i.hpp>

#include <ode/ode.h>

#define MUSCLE_ACTIVATION_THRESHOLD 0.1
#define MOTOR_BREAK_THRESHOLD       0.15

#define MODE_RELEASED               0
#define MODE_FORWARD                1
#define MODE_BACKWARD               2
#define MODE_BREAK                  3

using namespace std;

class ComplexHinge 
{
  public:
    ComplexHinge();

    ComplexHinge(ConnectorDescription *connStruct, dJointID *joint, 
        dBodyID *sourceBody, dBodyID *aimBody);

    ~ComplexHinge();

    void updatePassiveComplexHinge();

    void updateActiveComplexHinge(double agonist, double antagonist);


  private:
    struct MotorCommand
    {
      int mode;
      double activation;
    };

    // Constructor sub functions
    void copyDataFromConnectorDescription(ConnectorDescription *connStruct);
    void copyPWMappingData(PWMappingDescription *pwMD, PWMappingDescription
        *cSPWMD);
    void setupJointAndFeedback(dJointID *joint, dBodyID *sourceBody, dBodyID
        *aimBody);
    void calculateDerivedParameters();
    double calculateStaticDynamicFrictionTransitionSlope(
        JointFrictionDescription *jFD);
    void printoutDerivedParameters();

    void updateInternalSensors();
    void updateMotor();
    void updateSpring();
    void updateDamper();
    double updateJointFriction(JointFrictionDescription *jFD);
    void updateBacklashPosition();
    void updateSpringCouplingTorque();

    void checkAbortConditions();

    // TODO: test functions - remove after testing
    void setTestTorques(int *currCase, double *aMotorFMax, double
        *sForceAdd, double *aForceAdd);
    void printoutUpdateDebugInfo(int currCase, double aMotorFMax, double
        sForceAdd, double aForceAdd);

    // Torque around joint axis calculation
    void calcTorqueAroundJointAxis();
    void calcTorqueAroundJointAxis2();
    // Torque calc help functions
    void prepareTorqueCalc();
    double calculateAB0Vector();
    double getVectorLength(dVector3 vector);
    void resetFeedback(dJointFeedback *feedback, double value);

    // "Muscle" to H-bridge mapping (updates struct MotorCommand, s.a.)
    void calcMotorActivationFromAntagonists(double agonist, double antagonist);
    // PW duty cycle to torque/current ... mapping
    double mapPulseWidth(double x, PWMappingDescription pwMD);

    // verbosity
    void printHingeTorqueInformation();
    void plotPWMappingFunctions();
    void plotAntagonistToMotor();

    int currTime_;
    bool initDone_;

    // name of this connector
    string                    _name;

    // following structs are defined in description/RobotDescription.h
    // --> they locally store the data needed during setup / calculations
    MotorDescription          _motor;         
    GearDescription           _gear;            
    SpringDescription         _spring;          
    SpringCouplingDescription _springCoupling;  
    DamperDescription         _damper;
    JointFrictionDescription  _jointFriction;
    FilterDescription         _forceFeedbackFilter;
    FilterDescription         _stopTorqueAccuFilter;

    // abortCondition stuff
    bool                      _isPositionExceedable;
    double                    _minExceedPosition;
    double                    _maxExceedPosition;
    bool                      _isForceExceedable;
    double                    _minExceedForce;

    // body 0 information
    dBodyID        *body0_;             // 1st body joint is connected to
    dBodyID        *body1_;             // 2nd  "      "   "     "      "
    const dReal    *body0Pos_;          // Position of body 0
    const dReal    *body1Pos_;          // Position of body 1
    const dReal    *body0Rot_;          // Rotation of body 0
    dMass          massB0_;             // struct dMass of body 0 
                                        //   (mass, cog b-frame, 
                                        //    inertia tensor b-frame about POR)

    // hinge information (from ode)
    dJointID       *joint_;             // the hinge joint
    // TODO
    dJointGroupID  jointGroup_;         // the joint group
    dJointID       jointAMotor_;        // the joint
    // TODO
    dVector3       hingeAxis_;          // current hinge axis
    dVector3       hingeAnchor_;        // current hinge anchor
    dJointFeedback *feedback_;          // pointer to the feedback structure
    // TODO
    dJointFeedback *feedbackAMotor1_;   // pointer to the feedback structure
    // TODO
    dReal          hingeAngle_;         // current angle
    dReal          hingeAngleVel_;      // current angular velocity
    dReal          hingeAngleVelAbs_;   // current angular velocity
    double         hingeAngleVelSign_;  // current angular velocity
    double         hingeTorque_;        // current hinge torque
    // TODO
    double         aMotorTorque_;       // current aMotor torque
    // TODO

    // stuff needed to calculate the torque
    double         torqueAxisTransFac_; // factor to derive torque around actual
                                        //   hinge axis instead of the torque
                                        //   around a parallel axis through the
                                        //   COM of body0
    double         torqueHingeAxisB0_;  // torque around axis through POM of
                                        //   Body 0 which is parallel to the
                                        //   hinge axis
    double         torqueHingeAxis_;    // torque around hinge axis

    // TODO
    double         torqueAMotorAxisB0_; // torque around axis through POM of
                                        //   Body 0 which is parallel to the
                                        //   aMotor axis
    double         torqueAMotorAxis_;   // torque around hinge axis
    // TODO

    // filter stuff
    MovingAverage  *mAForceFilter_;     // moving average filter (torque sensor)
    // TODO
    MovingAverage  *mAForceFilter2_;    // moving average filter (torque sensor)
    // TODO
    MovingAverage  *mADamperFilter_;    // moving average filter (damping)

    // fixed parameters
    double         stepSize_;
    double         totalDamping_;    
    double         totalSpringConstant_;
    double         totalEfficiency_;
    double         totalMaxContinuousTorque_;
    double         totalMaxContBrakeTorque_;
    double         totalNoLoadSpeed_;
    double         backlashMin_;        // minimum backlash position
    double         backlashMax_;        // maximum backlash position
    double         backlashMinTol_;     // minimum backlash tolerance position
    double         backlashMaxTol_;     // maximum backlash tolerance position
    double         jointFrictionTransitionSlope_;
    double         motorBrakeFrictionTransitionSlope_;

    // variables
    double         accuMaxTorqueAct_;   // max Torque accumulator (for vel != 0)
    double         accuMaxTorqueStop_;  // " (those that act to stop movement)
    double         accuMaxTorqueTotal_; // " combined
    double         maxVelPositive_;     // max. desired Velocity in pos. dir
    double         maxVelNegative_;     // max. desired Velocity in neg. dir
    double         maxVelTotal_;        // max. desired Velocity (combined)
    double         backlashPos_;        // current position of backlash
    double         motorSpeed_;         // current rotation speed of motor
    int            oldMotorState_;      // previous timestep state of the motor
    double         motorReverseTime_;   // time to torque production after dir change
    double         sCouplDeflection_;   // current torsion of the spring coupl.
    double         sCouplCorrectionT_;  // current spring coupl. correct. torque
    MotorCommand   mc_;                 // current motor command

    // tmp
    //vector<Gnuplot*> gp_;
};

#endif
