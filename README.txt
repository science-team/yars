1. How to run YARS:

  1.1. Linux
    To run YARS on a Linux system call the following:
      > yars --nocomm -d <xml file> 
    For explanation and further parameters call:
      > yars --help

2. Known open issues:

  2.1 Camera Sensor
    The camera sensor currently only runs on machines with a NVIDIA graphics
    card. Computers with ATI have been tested but yars fails with the message:

    "Error: RenderTexture requires the following unsupported OpenGL extensions:
    GLX_SGIX_pbuffer"

  2.2 Controller and Object-Controller
    Must be adapted. The current will not work.
