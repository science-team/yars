/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "UDPCommunication.h"

struct    sockaddr_in udpClient;
int       udpSock;
struct    sockaddr_in udpServer;
struct    sockaddr_in tmpUDPServer;
socklen_t udpLength;

/**
 * Inits the UDP Connection with the specified localPort, serverPort and
 * serverName.
 */
void udpInitConnection(int localPort, int serverPort, char* serverName)
{
  int rc, i;
  struct hostent *h;
  char buf[1024];

  /* verify IP-Server-Adress */
  h = gethostbyname(serverName);
  if (h == NULL) {
    printf ("%s: unknown Host '%s' \n", "udpClient", serverName);
    exit (EXIT_FAILURE);
  }

  printf ("%s: send data to '%s' (IP : %s) \n",
          "udpClient", h->h_name,
          inet_ntoa (*(struct in_addr *) h->h_addr_list[0]));

  udpServer.sin_family = h->h_addrtype;
  memcpy ( (char *) &udpServer.sin_addr.s_addr,
           h->h_addr_list[0], h->h_length);
  udpServer.sin_port = htons(serverPort);

  /* create socket */
  udpSock = socket (AF_INET, SOCK_DGRAM, 0);
  if (udpSock < 0) {
     printf ("%s: Cannot open socket (%s) \n",
        "udpClient", strerror(errno));
     exit (EXIT_FAILURE);
  }

  /* bind port(s) */
  udpClient.sin_family      = AF_INET;
  udpClient.sin_addr.s_addr = htonl (INADDR_ANY);
  udpClient.sin_port        = htons(localPort);
  udpLength                 = sizeof(udpClient);
  rc = bind (udpSock, (struct sockaddr *) &udpClient, udpLength);
  if (rc < 0) {
     printf ("%s: Could not bind port (%s)\n",
        "udpClient", strerror(errno));
     exit (EXIT_FAILURE);
  }

  /* connect */
  buf[0] = 0;
  rc = sendto(udpSock, buf, sizeof(buf), 0, (struct sockaddr *) &udpServer,
      sizeof(udpServer));
  if(rc < 0)
  {
    printf("%s: Connect to server not successful ... ABORT", "udpClient",
        strerror(errno));
     exit (EXIT_FAILURE);
  }

  printf("...init done\n");
}


int udpReadPbuf(struct PacketBuffer *pb)
{
  int ret;

  ret = udpReadByteArray(pb->buf, PBUF_SIZE-1);
  pb->pointer = 0;

  return ret;
}

int udpSendPbuf(struct PacketBuffer *pb)
{
  int ret;

  ret =  udpSendByteArray(pb->buf, pb->pointer);
  pb->pointer = 0;

  return ret;
}

int udpReadByteArray(char *b, int size)
{
  int i;

  do
  {
    i = recvfrom(udpSock, b, size, 0,
        (struct sockaddr *) &tmpUDPServer, &udpLength);

    if (tmpUDPServer.sin_port != udpServer.sin_port)
    {
      printf("Got UDP Datagramm from unregistered client. No problem: packet dropped!\n");  
      sendto(udpSock, b, 1, 0, (struct sockaddr *) &tmpUDPServer, 
          sizeof(tmpUDPServer));
      i = -1;
    }
  } 
  while (i<0);

  return i;
}

int udpSendByteArray(char *b, int size)
{
  int i;
  
  do
  {
    i = sendto(udpSock, b, size, 0, (struct sockaddr *) &udpServer,
        sizeof(udpServer));
  } 
  while (i<0);
  return 0; // TODO
}
