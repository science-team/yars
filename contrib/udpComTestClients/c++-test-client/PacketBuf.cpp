/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "PacketBuf.h"

/**
 * C-Implementation of an PacketBuffer used for UDP communication.
 */
void pbWriteByte(struct PacketBuffer *pb, char b) 
{
  pb->buf[pb->pointer] = b;    
  pb->pointer++;
}

void pbWriteInt(struct PacketBuffer *pb, int i) 
{
  pb->buf[pb->pointer]     = (char)  i;
  pb->buf[pb->pointer + 1] = (char) (i >> 8);
  pb->buf[pb->pointer + 2] = (char) (i >> 16);
  pb->buf[pb->pointer + 3] = (char) (i >> 24);

  pb->pointer += 4;
}

void pbWriteFloat(struct PacketBuffer *pb, float f) 
{
  int *i;

  i = (int*) &f; 
  pbWriteInt(pb, *i);
}

void pbReadByte(struct PacketBuffer *pb, char *b)
{
  *b = pb->buf[pb->pointer];
  pb->pointer++;
}

void pbReadInt(struct PacketBuffer *pb, int *i)
{
  pb->pointer += 4;
  *i = 0;
  
  *i = *i | (pb->buf[pb->pointer - 1])      << 24
          | (pb->buf[pb->pointer - 2]&0xff) << 16
          | (pb->buf[pb->pointer - 3]&0xff) << 8
          | (pb->buf[pb->pointer - 4]&0xff);

}

void pbReadFloat(struct PacketBuffer *pb, float *f)
{
  pbReadInt(pb, (int*) f);
}

void pbReset(struct PacketBuffer *pb) 
{
  pb->pointer = 0;
}
