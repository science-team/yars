/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
#include "StandardYarsClient.h"

#include <string>
#include <vector>

#define SERVER_NAME      "localhost" 
#define SERVER_PORT             4500
#define LOCAL_PORT              7020 

#define SIMPLE_MODE                0
#define EXTENDED_MODE              1

#define NUM_GENERATIONS           20
#define NUM_TRIES                  5
#define NUM_STEPS                100 


char* serverName;
int   serverPort;
int   localPort;
int   comMode;
bool  parallelMode;

bool  debugMode;
int   numGenerations; // for how many generations should we run
int   numTries;       //  --> how many tries per generation
int   numSteps;       //  --> how many steps per try

using namespace std;

void init()
{
  serverName     = SERVER_NAME;
  serverPort     = SERVER_PORT;
  localPort      = LOCAL_PORT;
  comMode        = SIMPLE_MODE;
  parallelMode   = true;

  debugMode      = true;
  numGenerations = NUM_GENERATIONS;
  numTries       = NUM_TRIES;
  numSteps       = NUM_STEPS;
}

void printHelp()
{
  printf(" ------- Welcome to TestClient ---------------------------\n");
  printf(" a small programm to test the UDP communication with YARS,\n");
  printf(" for details see http://yars.sourceforge.net.\n");
  printf(" For copyright see the file COPYING.\n");
  printf(" Bug reports to twickel@users.sourceforge.net.\n");
  printf(" ---------------------------------------------------------\n");
  printf("To use it you have to specify one or several of the "   );
  printf("of the following options:\n"                            );
  printf("-sn      <Server Name>                               \n");
  printf("-sp      <Server Port>                               \n");
  printf("-lp      <Local Port>                                \n");
  printf("-par     parallel net + phySim update mode (default) \n");
  printf("-ser     serial   net + phySim update mode           \n");
  printf("-spl     simple handshake mode             (default) \n");
  printf("-ext     extended (+mapRanges +names) handshake mode \n");
  printf("-ng      <number of generations to test run for>     \n");
  printf("-nt      <number of tries per generation>            \n");
  printf("-ns      <number of steps per try>                   \n");
  printf("-debug   print debug information (default)           \n");
  printf("-nodebug do not print debug information              \n");
  printf(" ---------------------------------------------------------\n");
}

void parseCommands(int argc, char **argv)
{

  for(int i = 0; i<argc; ++i)
  {
    if(strcmp(argv[i],"-sn") == 0)
    {
      serverName = argv[i+1];
      printf("Setting serverName to %s\n", serverName); 
    }
    else if(strcmp(argv[i],"-sp") == 0)
    {
      serverPort = atoi(argv[i+1]);
      printf("Setting serverPort to %d\n", serverPort); 
    }
    else if(strcmp(argv[i],"-lp") == 0)
    {
      localPort = atoi(argv[i+1]);
      printf("Setting localPort to %d\n", localPort); 
    }
    else if (strcmp(argv[i],"-spl")==0)
    {
      comMode = SIMPLE_MODE;
      printf("Setting comMode = simple\n"); 
    }
    else if (strcmp(argv[i],"-ext")==0)
    {
      comMode = EXTENDED_MODE;
      printf("Setting comMode = extended (handshake w mapRange + sensor/motor names)\n"); 
    }
    else if (strcmp(argv[i],"-par")==0)
    {
      parallelMode = true;
      printf("Setting parallelMode = true\n"); 
    }
    else if (strcmp(argv[i],"-ser")==0)
    {
      parallelMode = false;
      printf("Setting parallelMode = false\n"); 
    }
    else if(strcmp(argv[i],"-ng") == 0)
    {
      numGenerations = atoi(argv[i+1]);
      printf("Setting numGenerations to %d\n", numGenerations); 
    }
    else if(strcmp(argv[i],"-nt") == 0)
    {
      numTries = atoi(argv[i+1]);
      printf("Setting numTries to %d\n", numTries); 
    }
    else if(strcmp(argv[i],"-ns") == 0)
    {
      numSteps = atoi(argv[i+1]);
      printf("Setting numSteps to %d\n", numSteps); 
    }
    else if(strcmp(argv[i],"-debug") == 0)
    {
      debugMode = true;
      printf("print debug info = on\n"); 
    }
    else if(strcmp(argv[i],"-nodebug") == 0)
    {
      debugMode = false;
      printf("print debug info = off\n"); 
    }
  }
}

void printConfig()
{
  printf("----------------------------------------------------------------------\n");
  printf("The following parameters are used:                                    \n");
  printf("serverName      = %s                                                  \n", serverName);
  printf("serverPort      = %d                                                  \n", serverPort);
  printf("localPort       = %d                                                  \n", localPort);
  if(comMode == SIMPLE_MODE)
  {
    printf("comMode         = SIMPEL MODE                                         \n");
  }
  else if(comMode == EXTENDED_MODE)
  {
    printf("comMode         = EXTENDED MODE                                       \n");
  }
  if(parallelMode == true)
  {
    printf("parallelMode    = true                                                \n");
  }
  else
  {
    printf("parallelMode    = false                                               \n");
  }
  if(debugMode == true)
  {
    printf("debugMode       = true                                                \n");
  }
  else
  {
    printf("debugMode       = false                                               \n");
  }
  printf("numGenerations  = %d                                                  \n", numGenerations);
  printf("numTries        = %d                                                  \n", numTries);
  printf("numSteps        = %d                                                  \n", numSteps);
  printf("----------------------------------------------------------------------\n");
}

int main(int argc, char **argv)
{  
  vector<double> motorData;
  vector<double> sensorCIData;
  vector<double> sensorAUXData;

  int robotState;

  // initialize parameters with default values
  init();

  // parse command line arguments
  if(argc > 1)
  {
    if(strcmp(argv[1],"-h")==0 || strcmp(argv[1],"--help")==0)
    {
      printHelp();
      exit(0);
    }
    else
    {
      parseCommands(argc, argv);
    }
  }

  // print out configuration
  printConfig();

  StandardYarsClient *sYC = new StandardYarsClient();
  sYC->setDebugMode(debugMode);

  printf("StandardYarsClient init \n");
  sYC->init(localPort, serverPort, serverName, comMode, parallelMode);

  // handshake: receive robot information
  printf("StandardYarsClient doHandshake\n");
  sYC->doHandshake();

  // create as many sensorCI, sensorAUX and motor entries as are requested by
  // the server
  for(int i=0; i < sYC->getRobotMotorCount(); i++)
  {
    motorData.push_back(0.0);
  }
  for(int i=0; i < sYC->getRobotSensorCICount(); i++)
  {
    sensorCIData.push_back(0.0);
  }
  for(int i=0; i < sYC->getRobotSensorAUXCount(); i++)
  {
    sensorAUXData.push_back(0.0);
  }

  for(int i = 0; i < numGenerations; i++)
  {
    // robot init
    sYC->doInit();
    // robot reset
    sYC->doReset();

    for(int j = 0; j < numTries; j++)
    {
      if(debugMode) printf("  gen %d  try %d\n", i, j);
      for(int k = 0; k < numSteps; k++)
      {
        if(debugMode) printf(".");
        sYC->doData(&motorData, &sensorCIData, &sensorAUXData, &robotState);
      }

      // next try
      sYC->doNextTry();
    }
  }

  sYC->doReset();

  if(debugMode) printf("Finished.\n");

  exit(0);
}
