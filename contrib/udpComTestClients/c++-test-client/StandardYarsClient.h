/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#ifndef __STANDARD_YARS_CLIENT_H
#define __STANDARD_YARS_CLIENT_H


#include "UDPCommunication.h"
#include "PacketBuf.h"

#include <sstream>
#include <string>
#include <vector>

#define _STANDARD_COMPORT_SERVER         4500
#define _STANDARD_COMPORT_CLIENT         7020

#define _STANDARD_IP_SERVER        "127.0.0.1" 

#define _STANDARD_COMEND                   -1

#define _STANDARD_COM_ID_PINGREQ            0
#define _STANDARD_COM_ID_PINGACK            1
#define _STANDARD_COM_ID_HANDSHAKEREQ      10
#define _STANDARD_COM_ID_HANDSHAKEACK      11
#define _STANDARD_COM_ID_HANDSHAKE_EXT_REQ 12
#define _STANDARD_COM_ID_HANDSHAKE_EXT_ACK 13
#define _STANDARD_COM_ID_SENSORREQ         20
#define _STANDARD_COM_ID_SENSORACK         21
#define _STANDARD_COM_ID_MOTORREQ          30
#define _STANDARD_COM_ID_MOTORACK          31
#define _STANDARD_COM_ID_DATAREQ           40
#define _STANDARD_COM_ID_DATAACK           41
#define _STANDARD_COM_ID_RESETREQ          50
#define _STANDARD_COM_ID_RESETACK          51
#define _STANDARD_COM_ID_INITREQ           60
#define _STANDARD_COM_ID_INITACK           61
#define _STANDARD_COM_ID_NEXT_TRYREQ       70
#define _STANDARD_COM_ID_NEXT_TRYACK       71

#define _STANDARD_COM_ID_PARALLEL_REQ      80
#define _STANDARD_COM_ID_PARALLEL_ACK      81
#define _STANDARD_COM_ID_SERIAL_REQ        82
#define _STANDARD_COM_ID_SERIAL_ACK        83

#define _STANDARD_COM_TAGROBOT              0
#define _STANDARD_COM_TAGCOMPOUND         100
#define _STANDARD_COM_TAGSEGMENT          200
#define _STANDARD_COM_TAGMOTOR            300
#define _STANDARD_COM_TAGSENSOR           400
#define _STANDARD_COM_TAGSENSOR_AUX       500

#define _STANDARD_STATEREADY                0
#define _STANDARD_STATERESET                1
#define _STANDARD_STATEINIT                 2
#define _STANDARD_STATE_NEXTTRY             3

#define _STANDARD_ROBOT_STATE_OK            0
#define _STANDARD_ROBOT_STATE_ABORT         1

#define _COM_MODE_SIMPLE                    0
#define _COM_MODE_EXTENDED                  1



class StandardYarsClient
{
 public:
  StandardYarsClient();
  ~StandardYarsClient();

  void init();
  void init(int localPort, int serverPort, char* serverName, int comMode, bool parallel);

  void doData(std::vector<double> *motorData, std::vector<double> *sensorCIData,
      std::vector<double> *sensorAUXData, int *robotState);

  void doInit();
  void doHandshake();
  void doReset();
  void doNextTry();
  void deinit();

  int  getRobotMotorCount();
  int  getRobotSensorCICount();
  int  getRobotSensorAUXCount();

  void setDebugMode(bool debug);

 private:
  struct MotorSensorInfo{
    std::string name;
    double      minMap;
    double      maxMap;
    int         robot;
    int         compound;
    int         segment;
  };

  void receiveSensorData(std::vector<double> *sensorCIData, std::vector<double>
      *sensorAUXData, int *robotState);

  void sendMotorData(std::vector<double> *motorData);

  void receiveExtendedSensorMotorInfo(MotorSensorInfo *mSInfo);

  std::vector<struct MotorSensorInfo>  _motorData;
  std::vector<struct MotorSensorInfo> _sensorsCIData;
  std::vector<struct MotorSensorInfo> _sensorsAUXData;

  /** which robot is this client responsible for ? **/
  int          _robot;
  /** how many motors has the robot */
  int          _robotMotorCount;
  /** how many sensors has the robot */
  int          _robotSensorCICount;
  int          _robotSensorAUXCount;

  char*        _serverIP;
  unsigned int _serverPort;
  unsigned int _clientPort;

  unsigned int _comMode;                      // simple or extended
  bool         _isParallelModeEnabled;        // true or false

  float        _tmpFloatValue;
  int          _tmpTag;
  char         _tmpChar;

  bool         _debugMode;

  PacketBuffer _pbuf;
};

#endif
