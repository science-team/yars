/*************************************************************************
 *                                                                       *
 * This file is part of Yet Another Robot Simulator (YARS).              *
 * Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           *
 * Steffen Wischmann. All rights reserved.                               *
 * Email: {keyan,twickel,swischmann}@users.sourceforge.net               *
 * Web: http://sourceforge.net/projects/yars                             *
 *                                                                       *
 * For a list of contributors see the file AUTHORS.                      *
 *                                                                       *
 * YARS is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; either version 2 of the License, or (at your     *
 * option) any later version.                                            *
 *                                                                       *
 * YARS is distributed in the hope that it will be useful, but WITHOUT   *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or *
 * FITNESS FOR A PARTICULAR PURPOSE.                                     *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with YARS in the file COPYING; if not, write to the Free        *
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor,               *
 * Boston, MA 02110-1301, USA                                            *
 *                                                                       *
 *************************************************************************/
 
 
 
#include "StandardYarsClient.h"

using namespace std;

StandardYarsClient::StandardYarsClient()
{
  _robotMotorCount            = 0;
  _robotSensorCICount         = 0;
  _robotSensorAUXCount        = 0;

  _clientPort                 = _STANDARD_COMPORT_CLIENT;
  _serverPort                 = _STANDARD_COMPORT_SERVER;
  _serverIP                   = _STANDARD_IP_SERVER;

  _comMode                    = _COM_MODE_SIMPLE;
  _isParallelModeEnabled      = true;

  _debugMode                  = false;
}

StandardYarsClient::~StandardYarsClient()
{
  // no udp-deinit implemented yet. socket will be destroyed with program
  // termination 
  deinit();
}

void StandardYarsClient::deinit()
{
  // 
}

void StandardYarsClient::init()
{
  udpInitConnection(_clientPort, _serverPort, _serverIP);
}

void StandardYarsClient::init(int localPort, int serverPort, char* serverName,
    int comMode, bool parallel)
{
  _clientPort            = localPort;
  _serverPort            = serverPort;
  _serverIP              = serverName;

  _comMode               = comMode;
  _isParallelModeEnabled = parallel;

  udpInitConnection(_clientPort, _serverPort, _serverIP);
}

void StandardYarsClient::doHandshake()
{
  int robotNum           = -1;
  int compoundNum        = -1;
  int segmentNum         = -1; 
  int motorNum           = -1;
  int sensorCINum        = -1;
  int sensorAUXNum       = -1;
  bool continueReceiving = true;

  pbReset(&_pbuf);

  if(_comMode == _COM_MODE_SIMPLE)
  {
    if(_debugMode) printf("sending handshake request to port (%d) ...\n", _serverPort);
    pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKEREQ);
  }
  else if(_comMode == _COM_MODE_EXTENDED)
  {

    if(_debugMode) printf("sending extended handshake request to port (%d) ...\n",
        _serverPort);
    pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_REQ);

    if(_isParallelModeEnabled)
    {
      pbWriteInt(&_pbuf, _STANDARD_COM_ID_PARALLEL_REQ);
      if(_debugMode) printf("  sending PARALLEL REQ (%d) ...\n", _serverPort);
    }
    else
    {
      pbWriteInt(&_pbuf, _STANDARD_COM_ID_SERIAL_REQ);
      if(_debugMode) printf("  sending SERIAL REQ (%d) ...\n", _serverPort);
    }
  }

  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf);

  if(_debugMode) printf("start receiving robot structure information ... \n");
  pbReset(&_pbuf);
  
  // verify the ACKs
  udpReadPbuf(&_pbuf); 
  pbReadInt(&_pbuf, &_tmpTag); 
  if(_comMode == _COM_MODE_SIMPLE)
  {
    if(_tmpTag != _STANDARD_COM_ID_HANDSHAKEACK)
    {
      if(_debugMode) printf("StandardYarsClient: no HandshakeACK received ... exiting \n");
      exit(0);
    }
    else
    {
      if(_debugMode) printf("StandardYarsClient: HandshakeACK received \n");
    }
  }
  else if(_comMode == _COM_MODE_EXTENDED)
  {
    if(_tmpTag != _STANDARD_COM_ID_HANDSHAKE_EXT_ACK)
    {
      if(_debugMode) printf("StandardYarsClient: no Handshake_EXT_ACK received ... exiting \n");
      exit(0);
    }

    pbReadInt(&_pbuf, &_tmpTag); 
    if(_isParallelModeEnabled && (_tmpTag != _STANDARD_COM_ID_PARALLEL_ACK))
    {
      if(_debugMode) printf("StandardYarsClient: no Parallel_ACK received ... exiting \n");
      exit(0);
    }
    else if((! _isParallelModeEnabled) && (_tmpTag != _STANDARD_COM_ID_SERIAL_ACK))
    {
      if(_debugMode) printf("StandardYarsClient: no Serial_ACK received ... exiting \n");
      exit(0);
    }
  }

  if(_debugMode) printf("\n");

  // receive robot structure
  while(continueReceiving)
  {
    pbReadInt(&_pbuf, &_tmpTag); 

    switch(_tmpTag)
    {
      case _STANDARD_COM_TAGROBOT:
        {
          robotNum++;
          compoundNum  = -1;
          segmentNum   = -1;
          motorNum     = -1;
          sensorCINum  = -1;
          sensorAUXNum = -1;
          if(_debugMode) printf("  Robot %d \n", robotNum);
          // print out info
          break;
        }
      case _STANDARD_COM_TAGCOMPOUND:
        {
          compoundNum++;
          segmentNum  = -1;
          motorNum     = -1;
          sensorCINum  = -1;
          sensorAUXNum = -1;
          if(_debugMode) printf("    Compound %d \n", compoundNum);
          // print out info
          break;
        }
      case _STANDARD_COM_TAGSEGMENT:
        {
          segmentNum++;
          motorNum     = -1;
          sensorCINum  = -1;
          sensorAUXNum = -1;
          if(_debugMode) printf("      Segment %d \n", segmentNum);
          // print out info
          break;
        }
      case _STANDARD_COM_TAGMOTOR:
        {
          MotorSensorInfo motor;
          motorNum++;
          motor.robot    = robotNum; 
          motor.compound = compoundNum;
          motor.segment  = segmentNum;
          if(_comMode == _COM_MODE_EXTENDED)
          {
            receiveExtendedSensorMotorInfo(&motor);
          }
          _motorData.push_back(motor);
          if(_debugMode) printf("        Motor %d (%s), min = %f, max = %f\n", motorNum,
                           motor.name.c_str(), motor.minMap, motor.maxMap);
          break;
        }
      case _STANDARD_COM_TAGSENSOR:
        {
          MotorSensorInfo sensor;
          sensorCINum++;
          sensor.robot    = robotNum; 
          sensor.compound = compoundNum;
          sensor.segment  = segmentNum;
          if(_comMode == _COM_MODE_EXTENDED)
          {
            receiveExtendedSensorMotorInfo(&sensor);
          }
          if(_debugMode) printf("        SensorCI %d (%s), min = %f, max = %f \n", sensorCINum,
                           sensor.name.c_str(), sensor.minMap, sensor.maxMap);
          _sensorsCIData.push_back(sensor);
          break;
        }
      case _STANDARD_COM_TAGSENSOR_AUX:
        {
          MotorSensorInfo sensor;
          sensorAUXNum++;
          sensor.robot    = robotNum; 
          sensor.compound = compoundNum;
          sensor.segment  = segmentNum;
          if(_comMode == _COM_MODE_EXTENDED)
          {
            receiveExtendedSensorMotorInfo(&sensor);
          }
          if(_debugMode) printf("        SensorAUX %d (%s), min = %f, max = %f \n", sensorAUXNum,
                           sensor.name.c_str(), sensor.minMap, sensor.maxMap);
          _sensorsAUXData.push_back(sensor);
          break;
        }
      case _STANDARD_COMEND:
        {
          continueReceiving = false;
          if(_debugMode) printf("  END ROBOT DESCRIPTION\n\n");
          break;
        }
      default:
        // throw some exception here
        break;
    }
  }

  _robotMotorCount      = _motorData.size();
  _robotSensorCICount   = _sensorsCIData.size();
  _robotSensorAUXCount  = _sensorsAUXData.size();

  if(_debugMode) printf("_robotMotorCount     = %d \n", _robotMotorCount);
  if(_debugMode) printf("_robotSensorCICount  = %d \n", _robotSensorCICount);
  if(_debugMode) printf("_robotSensorAUXCount = %d \n", _robotSensorAUXCount);

  // only for one robot sensor and motor information is send, so we can easily
  // determine the robot number of the server we are responsible for
  if(_robotMotorCount > 0)
  {
    _robot = _motorData[0].robot;
  }
  else if(_robotSensorCICount > 0)
  {
    _robot = _sensorsCIData[0].robot;
  }
  else if(_robotSensorCICount > 0)
  {
    _robot = _sensorsAUXData[0].robot;
  }
  else
  {
    // no information exchange with any robot !!!
  }

  if(_debugMode) printf("... receiving robot structure done.\n");
}

void StandardYarsClient::receiveExtendedSensorMotorInfo(MotorSensorInfo *mSInfo)
{
//  int   n;
  int   stringLength;
//  int   tmpValue;
//  float tmpFloatValue;

  // receive mapping min/max
  pbReadFloat(&_pbuf, &_tmpFloatValue); 
  (*mSInfo).minMap = (double)_tmpFloatValue;
  pbReadFloat(&_pbuf, &_tmpFloatValue); 
  (*mSInfo).maxMap = (double)_tmpFloatValue;

  // send handshake req
  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_REQ);
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf);

  // receive handshake ext ack, string length, string
  pbReset(&_pbuf);
  udpReadPbuf(&_pbuf); 
  pbReadInt(&_pbuf, &_tmpTag);
  if(_tmpTag != _STANDARD_COM_ID_HANDSHAKE_EXT_ACK)
  {
    printf("wrong TAG received (should have been HANDSHAKE EXT ACK) ... ABORT \n");
    exit(-1);
  }
  pbReadInt(&_pbuf, &stringLength);
  if(stringLength > (PBUF_SIZE - 3*4))
  {
    printf("stringLength out of range (%d) ... ABORT \n", stringLength);
    exit(-1);
  }
  for(int i=0; i < stringLength; i++)
  {
    pbReadByte(&_pbuf, &_tmpChar);
    (*mSInfo).name += _tmpChar;
  }

  // send handshake ext request
  pbReset(&_pbuf);
  pbWriteInt(&_pbuf, _STANDARD_COM_ID_HANDSHAKE_EXT_REQ);
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf);

  // wait for handshake ack
  pbReset(&_pbuf);
  udpReadPbuf(&_pbuf); 
  pbReadInt(&_pbuf, &_tmpTag);
  if(_tmpTag != _STANDARD_COM_ID_HANDSHAKE_EXT_ACK)
  {
    printf("wrong TAG received (should have been HANDSHAKE EXT ACK) ... ABORT");
    exit(-1);
  }
}

void StandardYarsClient::receiveSensorData(vector<double> *sensorCIData,
    vector<double> *sensorAUXData, int *robotState)
{
  pbReset(&_pbuf);  
  udpReadPbuf(&_pbuf); 

  // first of all receive robot state
  pbReadInt(&_pbuf, &_tmpTag); 
  if(_tmpTag != _STANDARD_COM_ID_DATAACK)
  {
    printf("StandardYarsClient: no DataACK received ... exiting \n");
    exit(0);
  }
  pbReadInt(&_pbuf, &_tmpTag); 
  if(_tmpTag != _robot)           
  {
    printf("StandardYarsClient: wrong robot number received ... exiting \n");
    exit(0);
  }
  pbReadInt(&_pbuf, &_tmpTag); 
  if(_tmpTag == _STANDARD_ROBOT_STATE_ABORT)
  {
    printf("StandardYarsClient: robot state ABORT ... exiting \n");
    exit(0);
  }
  else if(_tmpTag == _STANDARD_ROBOT_STATE_OK)
  {
    //printf("StandardYarsClient: robot state OK\n");
  }
  else
  {
    printf("StandardYarsClient: received robot state unknown ... exiting \n");
    exit(0);
  }

  pbReadInt(&_pbuf, &_tmpTag); 
  if(_tmpTag != (_robotSensorCICount + _robotSensorAUXCount))
  {
    printf("StandardYarsClient: wrong sensor number received ... exiting \n");
    exit(0);
  }
  
  // first receive all control input sensor values
  for(int i=0; i < _robotSensorCICount; i++)
  {
    pbReadFloat(&_pbuf, &_tmpFloatValue);
    (*sensorCIData)[i] = (double)_tmpFloatValue;
  }

  // then receive all auxiliary sensor values
  for(int i=0; i < _robotSensorAUXCount; i++)
  {
    pbReadFloat(&_pbuf, &_tmpFloatValue);
    (*sensorAUXData)[i] = (double)_tmpFloatValue;
  }
}

void StandardYarsClient::sendMotorData(vector<double> *motorData)
{
  pbReset(&_pbuf);  

  pbWriteInt(&_pbuf, _STANDARD_COM_ID_DATAREQ);
  pbWriteInt(&_pbuf, _robot);
  pbWriteInt(&_pbuf, _robotMotorCount);

  // send all motor values
  for(int i=0; i < _robotMotorCount; i++)
  {
    _tmpFloatValue = (float) (*motorData)[i];
    pbWriteFloat(&_pbuf, _tmpFloatValue);
  }

  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  udpSendPbuf(&_pbuf);
}

void StandardYarsClient::doData(vector<double> *motorData, vector<double>
    *sensorCIData, vector<double> *sensorAUXData, int *robotState)
{
  // send motor data
  sendMotorData(motorData);
  
  // receive robot state and sensor data
  receiveSensorData(sensorCIData, sensorAUXData, robotState);
}

void StandardYarsClient::doInit()
{
  if(_debugMode) printf("\nStandardYarsClient: doInit()\n");
  
  pbReset(&_pbuf);  

  pbWriteInt(&_pbuf, _STANDARD_COM_ID_INITREQ);
  pbWriteInt(&_pbuf, _robot);
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  
  udpSendPbuf(&_pbuf);

  pbReset(&_pbuf);  
  udpReadPbuf(&_pbuf); 

  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _STANDARD_COM_ID_INITACK)
  {
    printf("StandardYarsClient: no INIT ACK received ... ABORT \n");
    exit(0);
  }
  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _robot)
  {
    printf("StandardYarsClient: wrong robot number received ... ABORT \n");
    exit(0);
  }
}

void StandardYarsClient::doNextTry()
{
  if(_debugMode) printf("\nStandardYarsClient: doNextTry()\n");

  pbReset(&_pbuf);  

  pbWriteInt(&_pbuf, _STANDARD_COM_ID_NEXT_TRYREQ);
  pbWriteInt(&_pbuf, _robot);
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  
  udpSendPbuf(&_pbuf);

  pbReset(&_pbuf);  
  udpReadPbuf(&_pbuf); 

  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _STANDARD_COM_ID_NEXT_TRYACK)
  {
    printf("StandardYarsClient: no NEXT_TRY ACK received ... ABORT \n");
    exit(0);
  }

  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _robot)
  {
    printf("StandardYarsClient: wrong robot number received ... ABORT \n");
    exit(0);
  }
}


void StandardYarsClient::doReset()
{
  if(_debugMode) printf("\nStandardYarsClient: doReset()\n");
  
  pbReset(&_pbuf);  

  pbWriteInt(&_pbuf, _STANDARD_COM_ID_RESETREQ);
  pbWriteInt(&_pbuf, _robot);
  pbWriteInt(&_pbuf, _STANDARD_COMEND);
  
  udpSendPbuf(&_pbuf);

  pbReset(&_pbuf);  
  udpReadPbuf(&_pbuf); 

  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _STANDARD_COM_ID_RESETACK)
  {
    printf("StandardYarsClient: no RESET ACK received ... ABORT \n");
    exit(0);
  }
  pbReadInt(&_pbuf, &_tmpTag);
  if (_tmpTag != _robot)
  {
    printf("StandardYarsClient: wrong robot number received ... ABORT \n");
    exit(0);
  }
}

int StandardYarsClient::getRobotMotorCount()
{
  return _robotMotorCount;
}

int StandardYarsClient::getRobotSensorCICount()
{
  return _robotSensorCICount;
}

int StandardYarsClient::getRobotSensorAUXCount()
{
  return _robotSensorAUXCount;
}

void StandardYarsClient::setDebugMode(bool debug)
{
  _debugMode = debug;
}
