#########################################################################
#                                                                       #
# This file is part of Yet Another Robot Simulator (YARS).              #
# Copyright (C) 2003-2006 Keyan Zahedi, Arndt von Twickel and           #
# Steffen Wischmann. All rights reserved.                               #
# Email: {keyan,twickel,swischmann}@users.sourceforge.net               #
# Web: http://sourceforge.net/projects/yars                             #
#                                                                       #
# For a list of contributors see the file AUTHORS.                      #
#                                                                       #
# YARS is free software; you can redistribute it and/or modify it under #
# the terms of the GNU General Public License as published by the Free  #
# Software Foundation; either version 2 of the License, or (at your     #
# option) any later version.                                            #
#                                                                       #
# YARS is distributed in the hope that it will be useful, but WITHOUT   #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or #
# FITNESS FOR A PARTICULAR PURPOSE.                                     #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with YARS in the file COPYING; if not, write to the Free        #
# Software Foundation, Inc., 51 Franklin St, Fifth Floor,               #
# Boston, MA 02110-1301, USA                                            #
#                                                                       #
#########################################################################

project(yars)

# check for cmake version first:
cmake_minimum_required(VERSION 2.4.3 FATAL_ERROR)

ENABLE_TESTING()

# Set Version Information
set(YARS_MAJOR 0)
set(YARS_MINOR 0)
set(YARS_PATCH 1)

INCLUDE(modules/SetDefaults.cmake)
INCLUDE(modules/SetOptions.cmake)
INCLUDE(modules/IncludePackages.cmake)
INCLUDE(modules/IncludeFunctions.cmake)
INCLUDE(modules/PrintConfiguration.cmake)
INCLUDE(modules/SetDefinitions.cmake)
INCLUDE(modules/SetDirectories.cmake)

CONFIGURE_FILE(config.h.cmake "${PROJECT_BINARY_DIR}/include/config.h")

add_subdirectory (src)
add_subdirectory (src/controller)
add_subdirectory (src/objectcontroller)

INCLUDE(modules/InstallConfiguration.cmake)
INCLUDE(modules/CPackConfiguration.cmake)
