INCLUDE(CMakeDetermineCXXCompiler)
INCLUDE(FindOpenGL)
INCLUDE(FindGLUT)
INCLUDE(FindBoost)

INCLUDE(./modules/FindODE.cmake)
INCLUDE(./modules/FindXercesC.cmake)

INCLUDE(FindDoxygen)
#INCLUDE(FindwxWidgets)
#INCLUDE(FindwxWindows)


