IF(CMAKE_VERBOSE)
  INCLUDE(modules/CMakeOutputVariables.cmake)
ENDIF(CMAKE_VERBOSE)

MESSAGE("----- Required Library Status --------------------")

IF(CMAKE_COMPILER_IS_GNUCXX)
  MESSAGE("Compiler GNUCXX is used")
  MESSAGE("Compiler location = " ${CMAKE_CXX_COMPILER_FULLPATH})
ELSE(CMAKE_COMPILER_IS_GNUCXX)
  MESSAGE(FATAL_ERROR "Compiler GNUCXX is not used")
ENDIF(CMAKE_COMPILER_IS_GNUCXX)
IF(CMAKE_COMPILER_IS_MINGW)
  MESSAGE("Compiler MINGW is used")
ENDIF(CMAKE_COMPILER_IS_MINGW)
IF(CMAKE_COMPILER_IS_CYGWIN)
  MESSAGE("Compiler CYGWIN is used")
ENDIF(CMAKE_COMPILER_IS_CYGWIN)

IF(OPENGL_FOUND)
  MESSAGE("OpenGL found")

  # IF(OPENGL_XMESA_FOUND)
  #   MESSAGE("OpenGL_XMesa found")
  # ELSE(OPENGL_XMESA_FOUND)
  #   MESSAGE("OpenGL_XMesa not found")
  # ENDIF(OPENGL_XMESA_FOUND)

  IF(OPENGL_GLU_FOUND)
    MESSAGE("OpenGL_GLU found")
  ELSE(OPENGL_GLU_FOUND)
    MESSAGE(FATAL_ERROR "OpenGL_GLU not found")
  ENDIF(OPENGL_GLU_FOUND)

  MESSAGE("OpenGL libs:     " ${OPENGL_LIBRARY})
  MESSAGE("OpenGL includes: " ${OPENGL_INCLUDE_PATH})
ELSE(OPENGL_FOUND)
  MESSAGE(FATAL_ERROR "OpenGL not found")
ENDIF(OPENGL_FOUND)

IF(GLUT_FOUND)
  MESSAGE("GLUT found")
  MESSAGE("GLUT libs:     " ${GLUT_LIBRARY})
  MESSAGE("GLUT includes: " ${GLUT_INCLUDE_PATH})
ELSE(GLUT_FOUND)
  MESSAGE(FATAL_ERROR "GLUT not found")
ENDIF(GLUT_FOUND)

IF(ODE_FOUND)
  MESSAGE("ODE found")
  MESSAGE("ODE libs:     " ${ODE_LIBRARY_FILE})
  MESSAGE("ODE includes: " ${ODE_INCLUDE_FILE})
ELSE(ODE_FOUND)
  MESSAGE(FATAL_ERROR "ODE not found")
ENDIF(ODE_FOUND)

IF(YARS_USE_LOG4CPP_OUTPUT)
  INCLUDE(./modules/FindLog4cpp.cmake)
  IF(LOG4CPP_FOUND)
    include_directories(${LOG4CPP_INCLUDE_DIR})
    link_directories(${LOG4CPP_LIBRARY_DIR})
    MESSAGE("LOG4CPP found")
    MESSAGE("LOG4CPP lib dir:     " ${LOG4CPP_LIBRARY_FILE})
    MESSAGE("LOG4CPP include dir: " ${LOG4CPP_INCLUDE_FILE})
  ELSE(LOG4CPP_FOUND)
    MESSAGE(FATAL_ERROR "LOG4CPP not found")
  ENDIF(LOG4CPP_FOUND)
ENDIF(YARS_USE_LOG4CPP_OUTPUT)

IF(XERCESC_FOUND)
  MESSAGE("Xerces-C found")
  MESSAGE("Xerces-C libs:     " ${XERCESC_LIBRARY_FILE})
  MESSAGE("Xerces-C includes: " ${XERCESC_INCLUDE_FILE})
ELSE(XERCESC_FOUND)
  MESSAGE(FATAL_ERROR "Xerces-C not found")
ENDIF(XERCESC_FOUND)

IF(DOXYGEN_FOUND)
  MESSAGE("Doxygen found")
  MESSAGE("Doxygen executable:         " ${DOXYGEN_EXECUTABLE})
  MESSAGE("Doxygen dot executable:     " ${DOXYGEN_DOT_EXECUTABLE})
ELSE(DOXYGEN_FOUND)
  MESSAGE("Doxygen not found")
ENDIF(DOXYGEN_FOUND)

#  Boost_FOUND        - True when the Boost include directory is found.
#  Boost_INCLUDE_DIRS - the path to where the boost include files are.
#  Boost_LIBRARY_DIRS - The path to where the boost library files are.
#  Boost_LIB_DIAGNOSTIC_DEFINITIONS - Only set if using Windows.


IF(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  link_directories(${Boost_LIBRARY_DIRS})
  MESSAGE("Boost found")
  MESSAGE("Boost lib dir:     " ${Boost_LIBRARY_DIRS})
  MESSAGE("Boost include dir: " ${Boost_INCLUDE_DIRS})
  INCLUDE(./modules/FindBoostProgramOptions.cmake)
  INCLUDE(./modules/FindBoostFilesystem.cmake)
  IF(NOT BOOST_PROGRAM_OPTIONS_FOUND)
    MESSAGE(FATAL_ERROR "BOOST found, but please also install boost program options")
  ENDIF(NOT BOOST_PROGRAM_OPTIONS_FOUND)
  IF(NOT BOOST_FILESYSTEM_FOUND)
    MESSAGE(FATAL_ERROR "BOOST found, but please also install boost filesystem")
  ENDIF(NOT BOOST_FILESYSTEM_FOUND)
ELSE(Boost_FOUND)
  MESSAGE(FATAL_ERROR "BOOST not found")
ENDIF(Boost_FOUND)


MESSAGE("------- User configuration -----------------------")

IF(YARS_DEBUG)
  set(CMAKE_BUILD_TYPE Debug)
  message("Compiling yars in DEBUG mode.")
else(YARS_DEBUG)
  set(CMAKE_BUILD_TYPE Release)
  message("Compiling yars in RELEASE mode.")
ENDIF(YARS_DEBUG)

IF(YARS_SUPPRESS_OUTPUT)
  message("- supressing all output.")
  set(SUPPRESS_ALL_OUTPUT 1)
endif(YARS_SUPPRESS_OUTPUT)

IF(YARS_USE_LOG4CPP_OUTPUT)
  message("- log4cpp: using log4cpp support.")
  #add_definitions(-DUSE_LOG4CPP_OUTPUT)
  set(USE_LOG4CPP_OUTPUT 1)
endif(YARS_USE_LOG4CPP_OUTPUT)

IF(YARS_TARGET STREQUAL "yars")
  #message("YARS Binary name chosen: ${YARS_TARGET}.")
ELSE(YARS_TARGET STREQUAL "yars")
  message("- YARS Binary name chosen: ${YARS_TARGET}.")
endif(YARS_TARGET STREQUAL "yars")

MESSAGE("--------------------------------------------------")



