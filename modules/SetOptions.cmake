IF(EXISTS $ENV{HOME}/local)
  SET(YARS_EXTERNAL_PACKAGES_PREFIX_PATH "$ENV{HOME}/local")
ELSEIF(EXISTS $ENV{HOME}/.local)
  SET(YARS_EXTERNAL_PACKAGES_PREFIX_PATH "$ENV{HOME}/.local")
ELSE(EXISTS $ENV{HOME}/local)
  SET(YARS_EXTERNAL_PACKAGES_PREFIX_PATH "")
ENDIF(EXISTS $ENV{HOME}/local)

SET(YARS_EXTERNAL_PACKAGES_PREFIX ${YARS_EXTERNAL_PACKAGES_PREFIX_PATH} CACHE STRING 
  "Where to look for the lib and include directories, which are not in the standard search path."
  FORCE)

SET(YARS_TARGET "yars" CACHE STRING
  "YARS binary name.")

OPTION(CMAKE_VERBOSE 
  "Printout cmake variables"
  OFF)

OPTION(YARS_USE_LOG4CPP_OUTPUT
  "Building YARS with log4cpp support (test for libs must follow in CMakeLists.txt)."
  OFF)

OPTION(YARS_DEBUG
  "Building YARS with debug options."
  OFF)

OPTION(YARS_EXAMPLES
  "Building YARS with examples."
  OFF)

OPTION(YARS_SUPPRESS_OUTPUT
  "Suppressing all output to console."
  OFF)

MARK_AS_ADVANCED(
  YARS_TARGET
  CMAKE_VERBOSE
  YARS_USE_LOG4CPP_OUTPUT
  YARS_SUPPRESS_OUTPUT
  YARS_DEBUG
)



