# - Find the BOOST_FILESYSTEM includes and libraries.

#
# Look for file system header
#
SET(BOOST_FILESYSTEM_INCLUDE_FILE ${Boost_INCLUDE_DIRS}/boost/filesystem/path.hpp)

# Assume we didn't find it.
SET(BOOST_FILESYSTEM_FOUND 0)
  IF(EXISTS ${BOOST_FILESYSTEM_INCLUDE_FILE})
  SET(BOOST_FILESYSTEM_FOUND 1)
ENDIF(EXISTS ${BOOST_FILESYSTEM_INCLUDE_FILE})


